/**
 * GUI class to create a popup form to allow the user to add a new 
 * semester profile.
 * The form will ask the user to select a json file in order to upload 
 * the information relative to the semester file.
 */

package SemesterProfilePage;

import java.awt.event.KeyEvent;
import java.io.File;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import studyplanner.StudyPlanner;
import studyplanner.controller.FileController;
import utilities.exceptions.*;


public class AddNewSemesterForm extends JDialog {

    // Variable to save the file 
    private File file;
    
    
    /**
     * Constructor to create a popup form to add a new semester profile 
     */
    public AddNewSemesterForm() {
        
        // Initialize the form 
        initComponents();
        
        // Set the modality of the Dialog to true
        setModal(true);
        
        // Set the position of this form 
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        FileChooser = new javax.swing.JFileChooser();
        panel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        textArea = new javax.swing.JTextArea();
        Confirm = new javax.swing.JButton();
        Close = new javax.swing.JButton();
        Browse = new javax.swing.JButton();

        FileChooser.setAcceptAllFileFilterUsed(false);
        FileChooser.setDialogTitle("Choose File");
        FileChooser.setFileFilter(new MyCustomFilter());

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Create new Semester profile", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18))); // NOI18N

        textArea.setEditable(false);
        textArea.setColumns(20);
        textArea.setRows(1);
        textArea.setText("Select file ... ");
        jScrollPane1.setViewportView(textArea);

        Confirm.setText("Confirm");
        Confirm.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ConfirmMouseClicked(evt);
            }
        });
        Confirm.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ConfirmKeyPressed(evt);
            }
        });

        Close.setText("Close");
        Close.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CloseMouseClicked(evt);
            }
        });
        Close.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CloseKeyPressed(evt);
            }
        });

        Browse.setText("Browse ");
        Browse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BrowseMouseClicked(evt);
            }
        });
        Browse.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BrowseKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                        .addGap(0, 303, Short.MAX_VALUE)
                        .addComponent(Browse)
                        .addGap(277, 277, 277))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addGap(153, 153, 153)
                .addComponent(Confirm, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Close, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(152, 152, 152))
        );

        panelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {Browse, Close, Confirm});

        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(Browse)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Close, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Confirm))
                .addContainerGap())
        );

        panelLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {Browse, Close, Confirm});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

/**************************** Event Listeners ********************************/  
      
    /*
     * Event listener for the ENTER key on close button 
     */
    private void CloseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CloseKeyPressed
        
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            
            this.close();
        }
    }//GEN-LAST:event_CloseKeyPressed
    
    /*
     * Event listener for a mouse click on the close button 
     */
    private void CloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CloseMouseClicked
        
        this.close();
    }//GEN-LAST:event_CloseMouseClicked
    
    /*
     * Event listener for the ENTER key on the confirm button 
     */
    private void ConfirmKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ConfirmKeyPressed
         
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            
            this.confirm();
        }
    }//GEN-LAST:event_ConfirmKeyPressed
    
    /*
     * Event listener for a mouse click on the confirm button 
     */
    private void ConfirmMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ConfirmMouseClicked
        
        this.confirm();
    }//GEN-LAST:event_ConfirmMouseClicked

    /*
     * Event listener for the ENTER key on the browse button 
     */ 
    private void BrowseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BrowseKeyPressed
        
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            
            this.browse();
        }
    }//GEN-LAST:event_BrowseKeyPressed
    
    /*
     * Event listener for a mouse click on the browse button 
     */
    private void BrowseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BrowseMouseClicked
        
        this.browse();
    }//GEN-LAST:event_BrowseMouseClicked


/****************************** Methods ***************************************/  
 
    /*
     * Method called when the browse button is clicked
     */
    private void browse() {
        
        // Start FileChooser in the user default directory 
        FileChooser.setCurrentDirectory(null);
        
        // Pops up the file chooser dialog
        int returnVal = FileChooser.showOpenDialog(this);
        
        // In case everything goes well
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            
            // Save the file selected by FileChooser
            file = FileChooser.getSelectedFile();
            
            // Display the absolute path of the file in the textArea
            textArea.setText(file.getAbsolutePath());               
        }
    }
    
    /*
     * Method called when the confirm button is clicked
     */
    private void confirm() {
        
        try {
            
            // Pass the absolute path of the file to the FileController
            // in order to create a new semester profile
            FileController.loadSemesterDataFile(file.getAbsolutePath());
            
            // Set file to null
            //file = null;
            
            // Show confirmation message
            JOptionPane.showMessageDialog(this,
                        "New semester profile sucessfully created.");
            
            // Dispose of the popup
            this.dispose();
            
            // Reload the page 
            StudyPlanner.reloadMainPage();

        } catch (InputOutOfRangeException | NullInputException | IllegalAssignmentsWeightingException | 
                 ParseException                  | ItemNotFoundException       |
                 NumberFormatException           | IllegalDateException        |
                 IllegalTypeException            | EmptyInputException         | 
                 IllegalSemesterProfileException | IllegalFileException        | 
                 NullPointerException            | IllegalCodeException        |  
                 IllegalValueException           | DuplicateDataException      |
                 IncorrectInputDataException ex) {

            // Log the exception 
            Logger.getLogger(AddNewSemesterForm.class.getName())
                                                .log(Level.SEVERE, null, ex);
            
            // In case an excpetion is caught display it in a JOptionPane
            if (ex.getClass().getSimpleName().equals("NullPointerException")){
                
                // In case is a NullPointerException this is the only way 
                // to catch it so that we can personalized the Error message
                // to show
                JOptionPane.showMessageDialog(this,"No file has been selected",
                                            "Error",JOptionPane.ERROR_MESSAGE);
                
            } else {
                
                JOptionPane.showMessageDialog(this,ex.getMessage(),
                                            "Error",JOptionPane.ERROR_MESSAGE);
            }
            
            // Reset the test area
            textArea.setText("Select file ...");
            
            // Set file to null
            file = null;
        }
    }
    
    /*
     * Method called when the close button is clicked
     */
    private void close() {
        
        // Dispose of the popup 
        this.dispose();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Browse;
    private javax.swing.JButton Close;
    private javax.swing.JButton Confirm;
    private javax.swing.JFileChooser FileChooser;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panel;
    private javax.swing.JTextArea textArea;
    // End of variables declaration//GEN-END:variables
}

/*
 * Inner class so that the file chooser can accept only .json file 
 */
class MyCustomFilter extends javax.swing.filechooser.FileFilter {
    
    @Override
    public boolean accept(File file) {
        
        // Allow only directories, or files with ".json" extension
        return file.isDirectory() || file.getAbsolutePath().endsWith(".json");
    }

    @Override
    public String getDescription() {
        
        // This description will be displayed in the dialog
        return "Json documents (*.json)";
    }
}