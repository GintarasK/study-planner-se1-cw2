/**
 * GUI class to create the frame and main page of the application 
 * The application will be composed of two part : a static part and 
 * a dynamic part.
 * The static part is the infoPanel on the right side of the application 
 * that will show different information which are not going to change 
 * during while the application is running.
 * The dynamic part is the dynamicPanel which ,depending on which 
 * action the user is going to take, updates itself in order to show 
 * different information. 
 * The dynamicPanel is the placeholder to display the SemesterProfileEmptyGUI,
 * SemesterProfileGUI and the DetailedModulePage.
 */

package SemesterProfilePage;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import DetailedModulePage.MainFrame;
import utilities.exceptions.IllegalSemesterProfileException;
import studyplanner.controller.SemesterProfileController;
import utilities.exceptions.ItemNotFoundException;

public class MainPageGUI extends JFrame {
    
/********************* Variables **********************************/    
    
    // Layout variable used to display elment in the right position 
    private GridBagLayout layout  = new GridBagLayout();
    
    // DateFormat to display the date in the Generic info Panel 
    private DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
    
    // DateFormat to display the time in the Generic info Panel
    private DateFormat DATE_FORMAT = new SimpleDateFormat("EEE, d MMM yyyy");
    
    // Instance variable for the various page to attach in the dynamic 
    // dynamicPanel
    private SemesterProfileEmptyPanel     emptySemesterProfile;
    private SemesterProfilePanel          semesterProfile;
    private MainFrame                     detailedModulePage;
    
    
    /**
     * Creates a new MainPageGUI and attach to it a new semester profile  
     * or an empty semester profile panel depending on the value of the
     * given parameter
     * 
     * @param noSemesters parameter to test if there are semester or no
     * 
     * @throws IllegalSemesterProfileException in case it failed to create a 
     *                                         semester profile 
     */
    public MainPageGUI(boolean noSemesters) 
                                throws IllegalSemesterProfileException, ItemNotFoundException {
       
        initComponents();
        
        // Initialise the toolkit 
        this.toolkitInitialisation();
        
        // Initialise the element in the genericPanel
        this.genericPanelInit();

        // Set layout of the dynamicPanel
        dynamicPanel.setLayout(layout);
           
        // Set constraint for the position of the semester profile inside 
        // the dynamicPane
        GridBagConstraints constraints = this.constraintsInit();
        
        // Test if there are saved semesters 
        if (noSemesters) {
            
            // If there are no semesters create a main page with a empty 
            // semester profile panel in it 

            // Initialising an empty semester profile
            emptySemesterProfile = new SemesterProfileEmptyPanel();
            
            // Add the empty semester profile to the dynamicPanel 
            // with the constraints
            dynamicPanel.add(emptySemesterProfile, constraints);

        } else {

            // If there are semesters create a main page with a 
            // semester profile panel in it 
            
            // Initialise a SemesterProfile panel
            semesterProfile = new SemesterProfilePanel();

            // Add the panel to the dynamicPanel with the constraints
            dynamicPanel.add(semesterProfile, constraints);
        }
    }

    /**
     * Reload the main page by updating the view depending  
     * as well as if there are semesters initialised
     * 
     * @param noSemesters parameter to test if there are semester or no
     */
    public void reloadPage(boolean noSemesters) throws ItemNotFoundException {
           
        // DELETE IF EVERYTHING STILL WORKING ---------------------------------------------------------------------------------
        //this.toolkitInitialisation();
        //this.genericPanelInit();
        
        // Remove all element from the dynamicPanel so that a new panel 
        // can be displayed on it 
        this.dynamicPanel.removeAll();
       
        // Set layout of the dynamicPanel
        dynamicPanel.setLayout(layout);
        
        // Set constraint for the position of the semester profile inside 
        // the dynamicPane
        GridBagConstraints constraints = this.constraintsInit();
        
        // Test if there are saved semesters 
        if (noSemesters) {
            
            // If there are no semesters create a main page with a empty 
            // semester profile panel in it 
            
            // Initialise a new empty SemesterProfile 
            emptySemesterProfile = new SemesterProfileEmptyPanel();
            
            // Add the semester profile to the dynamicPanel 
            this.dynamicPanel.add(emptySemesterProfile, constraints);
            
            // Set the update view visible 
            this.setVisible(true);
            
            semesterProfile = null;
            
        } else {
            
            // If there are semesters create a main page with a 
            // semester profile panel in it 
            
            // Create a new semesterProfile
            semesterProfile = new SemesterProfilePanel();
            
            // Add the semesterProfile to the dynamic panel 
            this.dynamicPanel.add(semesterProfile, constraints);

            
            // In order to make the main page display correctly the 
            // semesterProfile and fast after having added   
            // or deleted a profile I needed to use the following 
            // three commands.
            SwingUtilities.updateComponentTreeUI(this);
            this.dynamicPanel.revalidate();
            this.dynamicPanel.repaint();

            // Set emptySemesterProfile to null jsut to be 
            // sure that is not initialised 
            emptySemesterProfile = null;
        }  
        
        // Reload information in the genericInfo panel
        this.genericPanelInit();
    }
    
    
    /**
     * Method to attach and display a DetailedModulePage to the dynamicPanel
     * 
     * @param moduleInfo the information relative to that module
     */
    public void displayDetailedModulePage (ArrayList<String> moduleInfo) {

        // Remove all element from the dynamicPanel so that a new panel 
        // can be displayed on it        
        this.dynamicPanel.removeAll();
        
        // Set layout of the dynamicPanel
        dynamicPanel.setLayout(layout);
        
        // Set constraint for the position of the semester profile inside 
        // the dynamicPane
        GridBagConstraints constraints = this.constraintsInit();
        
        // Initialise a new detailedModulePage passing the information relative
        // to the module 
        detailedModulePage = new MainFrame(moduleInfo, this);
        this.dynamicPanel.add(detailedModulePage,constraints);

        // In order to make the main page display correctly the 
        // semesterProfile and fast after having added   
        // or deleted a profile I needed to use the following 
        // three commands.        
        SwingUtilities.updateComponentTreeUI(this);
        this.dynamicPanel.revalidate();
        this.dynamicPanel.repaint();
        
        // Need to set border to null otherwise repaint will set it
        // No need here to reload all genericInfo panel
        bugTextAreaBorder.setBorder(null);
    }
    
    /**
     * Public method used to return the current detailedModule page 
     * initialised. 
     * Method only used inside the DetailedModule page GUI to 
     * access required data
     * 
     * @return the current detailedModule page
     */
    public MainFrame getDetailedModulePage() {
        
        return detailedModulePage;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        infoPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        currentTime = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        currentWeekNumber = new javax.swing.JLabel();
        currentDate = new javax.swing.JLabel();
        reportBugPanel = new javax.swing.JPanel();
        reportBug = new javax.swing.JButton();
        bugTextAreaBorder = new javax.swing.JScrollPane();
        bugTextArea = new javax.swing.JTextPane();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        dynamicPanel = new javax.swing.JDesktopPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        infoPanel.setAutoscrolls(true);
        infoPanel.setName(""); // NOI18N

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Generic info"));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setText("Date :");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel2.setText("Time :");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setText("Week Number : ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(currentTime, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(currentDate))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(currentWeekNumber)))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel4});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {currentDate, currentTime, currentWeekNumber});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(currentDate))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(currentTime))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(currentWeekNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel4});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {currentDate, currentTime, currentWeekNumber});

        reportBugPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Report Bug"));

        reportBug.setText("Report Bug");
        reportBug.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                reportBugMouseClicked(evt);
            }
        });
        reportBug.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                reportBugKeyPressed(evt);
            }
        });

        bugTextAreaBorder.setBorder(null);
        bugTextAreaBorder.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        bugTextAreaBorder.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        bugTextArea.setEditable(false);
        bugTextArea.setBackground(new java.awt.Color(240, 240, 240));
        bugTextArea.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        bugTextArea.setText("In case you have encountered problem \n with the application, please, let us know.");
        bugTextAreaBorder.setViewportView(bugTextArea);

        javax.swing.GroupLayout reportBugPanelLayout = new javax.swing.GroupLayout(reportBugPanel);
        reportBugPanel.setLayout(reportBugPanelLayout);
        reportBugPanelLayout.setHorizontalGroup(
            reportBugPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(reportBugPanelLayout.createSequentialGroup()
                .addGap(112, 112, 112)
                .addComponent(reportBug, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(reportBugPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bugTextAreaBorder)
                .addContainerGap())
        );
        reportBugPanelLayout.setVerticalGroup(
            reportBugPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(reportBugPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bugTextAreaBorder, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(reportBug, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Information Details"));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setText("Elizabeth Fry Hub");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel5.setText("Email : ebf_hub@uea.ac.uk");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel6.setText("Telephone : 01603 597578");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel7.setText("School of Computing Science");

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel8.setText("Email CMPMTH.LSO@uea.ac.uk");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel9.setText("Telephone : 01603 592300");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel3, jLabel5, jLabel6, jLabel7, jLabel8, jLabel9});

        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel3, jLabel5, jLabel6, jLabel7, jLabel8, jLabel9});

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Copyrights"));

        jLabel10.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel10.setText("Copyright ©  2017");

        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel11.setText("by Artjoms Gorpincenko");

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel12.setText("Gintaras Kislinkas");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel13.setText("Martino Gonzales");

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel14.setText("Paulius Vaicius");

        jLabel15.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel15.setText("All rights reserved.");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel14))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel10, jLabel11, jLabel12, jLabel13, jLabel14});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel15)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout infoPanelLayout = new javax.swing.GroupLayout(infoPanel);
        infoPanel.setLayout(infoPanelLayout);
        infoPanelLayout.setHorizontalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(reportBugPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        infoPanelLayout.setVerticalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(infoPanelLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(reportBugPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        dynamicPanel.setBackground(new java.awt.Color(255, 255, 255));
        dynamicPanel.setForeground(new java.awt.Color(255, 255, 255));
        dynamicPanel.setPreferredSize(new java.awt.Dimension(650, 650));

        javax.swing.GroupLayout dynamicPanelLayout = new javax.swing.GroupLayout(dynamicPanel);
        dynamicPanel.setLayout(dynamicPanelLayout);
        dynamicPanelLayout.setHorizontalGroup(
            dynamicPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 652, Short.MAX_VALUE)
        );
        dynamicPanelLayout.setVerticalGroup(
            dynamicPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dynamicPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(infoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(dynamicPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 733, Short.MAX_VALUE)
                    .addComponent(infoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
/**************************** Event Listener ********************************/     
    
    /*
     * Event listener for a mouse click on reportBug button 
     */
    private void reportBugMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reportBugMouseClicked
        
        // Call the method to display the bug form 
        this.displayReportBugForm();
    }//GEN-LAST:event_reportBugMouseClicked

    /*
     * Event listener for the ENTER key on reportBug button 
     */
    private void reportBugKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_reportBugKeyPressed
        
        // In case the ENTER key is clicked call the method to display 
        // the bug form 
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.displayReportBugForm();
        }
        
    }//GEN-LAST:event_reportBugKeyPressed

/****************************** Methods ***************************************/ 
    
    /*
     * Method to display the bug form when bug button is clicked
     */
    private void displayReportBugForm() {

        // Initialise a new ReportBugForm to report a bug
        ReportBugForm f = new ReportBugForm();

        // Set it visible and at the center of the MainPage
        f.setVisible(true);
        f.setLocationRelativeTo(this);
    }
    
    /*
     * Method to set the size and the position of the application relative
     * to the dimension of the screen
     */
    private void toolkitInitialisation() {
        
        //Create a toolkit variable
        Toolkit tk = Toolkit.getDefaultToolkit();
        
        //Set the size of the application frame relative to the size of the 
        // screen
        int xSize = (int) (tk.getScreenSize().width * 0.90);
        int ySize = (int) (tk.getScreenSize().height * 0.90);
        this.setSize(xSize, ySize);
        
        //Display the application at the center of the screen 
        this.setLocationRelativeTo(null);
    }
    
    /*
     * Create a new ActionListener to constantly update the clock 
     * displayed on the application.
     * Code taken and modified for our application form :
     * http://stackoverflow.com/questions/2959718/dynamic-clock-in-java
     */
    ActionListener updateClockAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            currentTime.setText(TIME_FORMAT.format(new Date())); 
        }
    };
    
    /**
     * Method to initialise the constraint used to add panel 
     * to the dynamicPanel
     * 
     * @return a GridBagConstraint object 
     */
    private GridBagConstraints constraintsInit() {
        
        // Set constraint for the position of the semester profile inside 
        // the dynamicPane
        GridBagConstraints c = new GridBagConstraints();
            
        // Specify the number of columns and rows of the grid layout
        c.weightx = 1;
        c.weighty = 1;
            
        // The component will fill the space both horizontally and vertically 
        c.fill = GridBagConstraints.BOTH;
        
        // Retunr the constraints
        return c;
    }
    
    /*
     * Method to initialise and set the element in the genericInfo panel 
     * on the right side of the application
     */
    private void genericPanelInit() {
        
        // Initialise a Date object used to set the data
        Date date = new Date();
        
        // Set the date 
        currentDate.setText(DATE_FORMAT.format(date));

        // Initialise the clock 
        Timer t = new Timer(1000,updateClockAction);
        t.start();
        
        // Get the number of week from the starting of the current 
        // valid semester study profie
        String week = SemesterProfileController.getWeekNo();
        
        // Check if week is null or not.
        if (week == null) {
            
            // If week = nell means that there is no current valid semester
            // initialised or that today's date is outised the range
            // of any semester in the application 
            currentWeekNumber.setText("No valid semester");
            
        } else {
            
            // Otherwise set it 
            currentWeekNumber.setText(week);
        }
        
        // Need to add it otherwise when reloading the page would
        // put a border to it.
        bugTextAreaBorder.setBorder(null);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane bugTextArea;
    private javax.swing.JScrollPane bugTextAreaBorder;
    private javax.swing.JLabel currentDate;
    private javax.swing.JLabel currentTime;
    private javax.swing.JLabel currentWeekNumber;
    private javax.swing.JDesktopPane dynamicPanel;
    private javax.swing.JPanel infoPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JButton reportBug;
    private javax.swing.JPanel reportBugPanel;
    // End of variables declaration//GEN-END:variables
}
