package GanttChartPage;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class Line extends JPanel{
    
    //x, y, x1, y1 of a Line (Graphics object)
    private int x, y, x1, y1;
    
    //gridX and gridY position of this JPanel
    private int gridX, gridY;
    
    //color of the Line
    private Color color;
    
    /**
     * A constructor, duh.
     * 
     * @param x     x0 position of this Line
     * @param y     y0 position of this Line
     * @param x1    x1 position of this Line
     * @param y1    y1 position of this Line
     * @param color colour of the Line
     * @param gridX gridX position of this JPanel
     * @param gridY gridY position of this JPanel
     */
    public Line(int x, int y, int x1, int y1, Color color, int gridX, int gridY){
        this.x = x;
        this.y = y;
        this.x1 = x1;
        this.y1 = y1;
        this.color = color;
        this.gridX = gridX;
        this.gridY = gridY;
    }
    
    /**
     * A method to draw a Line.
     * 
     * @param g         Graphics object
     */
    public void paint (Graphics g){
        g.setColor(color);
        g.drawLine(x, y, x1, y1);
    }
    
    /**
     * A method to get gridX position of this JPanel
     * 
     * @return      gridX position of this JPanel
     */
    public int getGridX(){
        return this.gridX;
    }
    
    /**
     * A method to get gridY position of this JPanel
     * 
     * @return      gridY position of this JPanel
     */
    public int getGridY(){
        return this.gridY;
    }

}
