package GanttChartPage;

import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

public class GanttChart extends JFrame implements Printable {

    //body which holds all the displayable elements
    private JPanel ganttBody = new JPanel();
    
    //holds all drawn lines in this gantt chart
    //used to deal with overlapping lines
    ArrayList<Line> drawnLines = new ArrayList<>();
    
    //a set of settings for grid layout
    GridBagConstraints headerGBC = new GridBagConstraints();
    
    //used for grid y
    private int lineNumber;
    
    //used for assignments to put numbers in the table
    private int number;
    
    //holds the amount of tasks of an assignment
    private int tasks;

    /**
     * A constructor for this GanttChart. Takes information about milestones,
     * tasks and relationships of a module in form of an ArrayList 
     * and builds a GanttChart for the module.
     * 
     * @param info              information about milestones,
     *                          tasks and relationships of a module
     * 
     * @throws ParseException   if parsing by using SimpleDateFormat was failed
     */
    public GanttChart(ArrayList<String> info) throws ParseException {
        //initialize everything
        init(headerGBC);

        //number of assignments
        int assignments = Integer.parseInt(info.remove(0));

        //for each assignment
        for (int i = 0; i < assignments; i++) {
            //generate gantt chart
            drawAssignment(info);
            emptyPanels(headerGBC);
        }

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());

        //add close button
        JButton closeButton = new JButton("Close");
        closeButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CloseMouseClicked();
            }
        });

        //add print button
        JButton printButton = new JButton("Print Chart");
        printButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PrintMouseClicked();
            }
        });

        //add buttons and their settings
        buttonPanel.add(closeButton);
        buttonPanel.add(printButton);
        this.add(buttonPanel, BorderLayout.PAGE_END);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        //pack and init
        pack();
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }

    /**
     * A method to draw a single assignment. Takes information about milestones,
     * tasks and relationships of an assignment in form of an ArrayList 
     * and builds a GanttChart for the assignment.
     * 
     * @param info              information about milestones,
     *                          tasks and relationships of an assignment
     * 
     * @throws ParseException   if parsing by using SimpleDateFormat was failed
     */
    private void drawAssignment(ArrayList<String> info) throws ParseException {
        //start date of items in the current assignment
        Date earliestDate;
        
        //holds all drawn tasks for the current assignment
        ArrayList<GanttLine> drawnTasks = new ArrayList<>();

        //in case if there is too much stuff in the window
        JScrollPane scrollPane = new JScrollPane();

        //if no tasks and no milestones, don't draw anything
        if (Integer.parseInt(info.get(3)) == 0 && 
                Integer.parseInt(info.get(4)) == 0) {
            return;
        }

        //exctract earliest date
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
        earliestDate = sdf.parse(info.get(1));

        //name of assignment and border around it
        assignmentHeader(info.remove(0));

        //column names and dates
        headers(headerGBC);

        //earliest date
        Date startDate = sdf.parse(info.remove(0));

        //for duration in days, draw dates
        int iterations = Integer.parseInt(info.remove(0));
        drawDates(headerGBC, iterations, startDate);

        lineNumber++;
        number = 1;

        //create a line of empty JPanels
        emptyPanels(headerGBC);

        //how many tasks
        tasks = Integer.parseInt(info.remove(0));

        //draw tasks
        for (int i = 0; i < tasks; i++) {
            drawTask(headerGBC, info, drawnTasks, earliestDate);
        }

        //how many milestones
        int milestones = Integer.parseInt(info.remove(0));

        //draw milestones
        for (int i = 0; i < milestones; i++) {
            drawMilestone(headerGBC, info, drawnTasks, earliestDate);
        }

        scrollPane.getViewport().add(ganttBody);
        this.add(scrollPane);
    }

    /**
     * A method to draw a milestone (rhombus + all connections with assigned
     * tasks).
     * 
     * @param headerGBC         a set of settings for grid layout
     * 
     * @param info              information about the milestone,
     *                          tasks and relationships of the milestone
     * 
     * @param drawnTasks        all drawn tasks for the assignment
     *                          that the milestone belongs to
     * 
     * @param earliestDate      earliest date of an item in the assignment
     *                          that the milestone belongs to
     * 
     * @throws ParseException   if parsing by using SimpleDateFormat was failed
     */
    private void drawMilestone(GridBagConstraints headerGBC, 
            ArrayList<String> info, ArrayList<GanttLine> drawnTasks, 
            Date earliestDate) throws ParseException {

        headerGBC.gridx = 0;
        headerGBC.gridy = lineNumber;

        //number
        JLabel tempLabel = new JLabel(String.valueOf(number));
        ganttBody.add(tempLabel, headerGBC);
        headerGBC.gridx++;

        //name
        String milName = info.remove(0);
        String tempName = milName;
        if (tempName.length() > 8) {
            tempName = tempName.substring(0, 6) + "...";
        }
        tempLabel = new JLabel(tempName);
        ganttBody.add(tempLabel, headerGBC);
        headerGBC.gridx += 2;

        //deadline
        String deadline = info.remove(0).substring(4, 10);
        tempLabel = new JLabel(deadline);
        ganttBody.add(tempLabel, headerGBC);
        headerGBC.gridx++;

        //rhombus below
        //how many days since the earliest one
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
        Date deadlineDate = sdf.parse(deadline);
        deadlineDate.setYear(1970);
        earliestDate.setYear(1970);
        Date d = new Date(deadlineDate.getTime() - earliestDate.getTime());
        int days = (int) Math.floor(d.getTime() / 86400000);

        //draw the actual rhombus
        tempLabel = new JLabel();
        ImageIcon iconLogo = new ImageIcon("milestone.png");
        tempLabel.setIcon(iconLogo);
        headerGBC.gridx = days + 5;
        ganttBody.add(tempLabel, headerGBC);

        //draw lines from the milestone to tasks
        connectMilestonesTasks(drawnTasks, milName, days);

        number++;
        lineNumber++;
    }

    /**
     * A method to draw a task (progress bar + all connections 
     * with dependencies).
     * 
     * @param headerGBC         a set of settings for grid layout
     * 
     * @param info              information about the task and its dependencies
     * 
     * @param drawnTasks        all drawn tasks for the assignment
     *                          that the task belongs to
     * 
     * @param earliestDate      earliest date of an item in the assignment
     *                          that the task belongs to
     * 
     * @throws ParseException   if parsing by using SimpleDateFormat was failed
     */
    private void drawTask(GridBagConstraints headerGBC, ArrayList<String> info,
            ArrayList<GanttLine> drawnTasks, Date earliestDate)
            throws ParseException {

        headerGBC.gridx = 0;
        headerGBC.gridy = lineNumber;

        //number
        JLabel tempLabel = new JLabel(String.valueOf(number));
        ganttBody.add(tempLabel, headerGBC);
        headerGBC.gridx++;

        //name 
        String taskName = info.remove(0);
        String tempName = taskName;
        if (tempName.length() > 8) {
            tempName = tempName.substring(0, 6) + "...";
        }
        tempLabel = new JLabel(tempName);
        ganttBody.add(tempLabel, headerGBC);
        headerGBC.gridx++;

        //startDate
        String startDate = info.remove(0).substring(4, 10);
        tempLabel = new JLabel(startDate);
        ganttBody.add(tempLabel, headerGBC);
        headerGBC.gridx++;

        //deadline
        String deadline = info.remove(0).substring(4, 10);
        tempLabel = new JLabel(deadline);
        ganttBody.add(tempLabel, headerGBC);
        headerGBC.gridx++;

        //critical
        boolean critical;
        String tempCrit = info.remove(0);
        if (tempCrit.equalsIgnoreCase("yes")) {
            critical = true;
        } else {
            critical = false;
        }

        //duration
        int duration = Integer.parseInt(info.remove(0));
        tempLabel = new JLabel(String.valueOf(duration + 1) + " days");
        ganttBody.add(tempLabel, headerGBC);
        headerGBC.gridx++;

        //progress
        int progress = Integer.parseInt(info.remove(0));

        //rectangle below
        //how many days since the earliest one
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
        Date start = sdf.parse(startDate);
        start.setYear(1970);
        earliestDate.setYear(1970);
        Date d = new Date(start.getTime() - earliestDate.getTime());
        int days = (int) Math.floor(d.getTime() / 86400000);

        //draw the actual rectangle
        headerGBC.gridx = days + 5;
        headerGBC.gridwidth = duration + 1;
        JProgressBar taskBar = new JProgressBar(0, 100);
        taskBar.setPreferredSize(new Dimension(1, 15));
        taskBar.setValue(progress);
        taskBar.setStringPainted(true);
        ganttBody.add(taskBar, headerGBC);

        //referenceName
        String referenceName = info.remove(0);

        //connect with reference if exists
        connectTasks(referenceName, drawnTasks, days, duration, critical);

        //dependencyName
        String dependencyName = info.remove(0);

        //how many milestones
        int milestones = Integer.parseInt(info.remove(0));

        //loop for milestones
        ArrayList<String> milestoneList = new ArrayList<>();
        for (int k = 0; k < milestones; k++) {
            milestoneList.add(info.remove(0));
        }

        //don't connect to the milestone if dependency is also in this milestone
        if (dependencyName != null) {
            //how many milestones dependency has
            int dependencyMilestones = Integer.parseInt(info.get(8));

            ArrayList<String> dependencyMilestonesNames = new ArrayList<>();
            for (int k = 0; k < dependencyMilestones; k++) {
                dependencyMilestonesNames.add(info.get(k + 9));
            }

            //remove milestones that dependency belongs to from the current one
            for (String s : dependencyMilestonesNames) {
                if (milestoneList.contains(s)) {
                    milestoneList.remove(s);
                    milestones--;
                }
            }
        }

        drawnTasks.add(new GanttLine(taskName, milestoneList, days + 6 + 
                duration, lineNumber));
        lineNumber++;

        //create a line of empty JPanels
        emptyPanels(headerGBC);
        number++;
        
        //if there is a dependency, draw it next
        if (dependencyName != null) {
            tasks--;
            drawTask(headerGBC, info, drawnTasks, earliestDate);
        }
    }

    /**
     * A method to define what will happen when the close button is selected.
     */
    private void CloseMouseClicked() {
        this.dispose();
    }

    /**
     * A method to define what will happen when the print button is selected.
     */
    private void PrintMouseClicked() {
        // Set up a printer job
        PrinterJob job = PrinterJob.getPrinterJob();

        // Set the page format
        PageFormat format = job.defaultPage();

        // Set landscape orientation 
        format.setOrientation(PageFormat.LANDSCAPE);

        // Set this page as printable
        job.setPrintable(this, format);

        try {
            // Open the operating system dialog for the printer
            if (job.printDialog()) {

                // If everything works print the frame
                job.print();
            }
        } catch (HeadlessException | PrinterException ex) {

            // Display error message in a dialog
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Actual method to print the contents.
     */
    @Override
    public int print(Graphics g, PageFormat format, int pagenum) {

        // Since I am going to print only a page test if there are more
        if (pagenum > 0) {

            return Printable.NO_SUCH_PAGE;
        }

        g.translate((int) format.getImageableX(), (int) format.getImageableY());

        // Set page dimension 
        float pageWidth = (float) format.getImageableWidth();
        float pageHeight = (float) format.getImageableHeight();

        // Set image dimension 
        float imageHeight = (float) this.getHeight();
        float imageWidth = (float) this.getWidth();

        // Create scale factor 
        float scaleFactor = Math.min((float) pageWidth / (float) imageWidth,
                (float) pageHeight / (float) imageHeight);

        // Scale the dimentsion of the image
        int scaledWidth = (int) (((float) imageWidth) * scaleFactor);
        int scaledHeight = (int) (((float) imageHeight) * scaleFactor);

        // Create a canvas 
        BufferedImage canvas = new BufferedImage(this.getWidth(),
                this.getHeight(),
                BufferedImage.TYPE_INT_RGB);

        // Create a 2D graphic from the canvas
        Graphics2D gg = canvas.createGraphics();

        // Paint the graphic
        this.paint(gg);

        // Create the image form the canvas
        Image img = canvas;

        // Draw the image
        g.drawImage(img, 0, 0, scaledWidth, scaledHeight, null);

        // Return if successfully executed
        return Printable.PAGE_EXISTS;
    }

    /**
     * A method to attach an assignment header to this Gantt chart.
     * 
     * @param name      name of the assignment
     */
    private void assignmentHeader(String name) {
        headerGBC.gridy = lineNumber;
        headerGBC.gridx = 0;
        headerGBC.gridwidth = 40;
        JLabel assignmentHeader = new JLabel(name);
        assignmentHeader.setFont(new Font("Garamond", Font.BOLD, 16));
        assignmentHeader.setBorder(BorderFactory.
                                createMatteBorder(1, 0, 1, 0, Color.GRAY));
        ganttBody.add(assignmentHeader, headerGBC);
        headerGBC.gridwidth = 1;
        lineNumber++;
    }

    /**
     * A method to initialise windows, settings and global variables.
     * 
     * @param headerGBC         a set of settings for grid layout
     */
    private void init(GridBagConstraints headerGBC) {
        //global vars
        this.number = 1;
        this.lineNumber = 1;

        //size of the window
        Toolkit tk = Toolkit.getDefaultToolkit();
        this.setPreferredSize(
                new Dimension((int) (tk.getScreenSize().width * 0.9),
                        (int) (tk.getScreenSize().height * 0.9)));

        //layouts
        this.setLayout(new BorderLayout());
        ganttBody.setLayout(new GridBagLayout());

        headerGBC.insets = new Insets(0, 15, 0, 15);
        headerGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        headerGBC.fill = GridBagConstraints.HORIZONTAL;
        headerGBC.gridwidth = 1;
        headerGBC.gridx = 0;
        headerGBC.gridy = 0;
        headerGBC.weightx = 0;
        headerGBC.weighty = 0;
    }

    /**
     * A method to add column headers to this Gantt chart.
     * 
     * @param headerGBC     a set of settings for grid layout
     */
    private void headers(GridBagConstraints headerGBC) {
        headerGBC.gridy = lineNumber;

        JLabel headerNumberLabel = new JLabel("Number");
        headerGBC.gridx = 0;
        ganttBody.add(headerNumberLabel, headerGBC);

        JLabel headerNameLabel = new JLabel("Name");
        headerGBC.gridx = 1;
        ganttBody.add(headerNameLabel, headerGBC);

        JLabel headerSetDateLabel = new JLabel("Start Date");
        headerGBC.gridx = 2;
        ganttBody.add(headerSetDateLabel, headerGBC);

        JLabel headerEndDateLabel = new JLabel("Deadline");
        headerGBC.gridx = 3;
        ganttBody.add(headerEndDateLabel, headerGBC);

        JLabel headerDurationLabel = new JLabel("Duration");
        headerGBC.gridx = 4;
        ganttBody.add(headerDurationLabel, headerGBC);

        lineNumber++;
    }

    /**
     * A method to attach a line of empty panels to this Gantt chart.
     * 
     * @param gbc       a set of settings for grid layout
     */
    private void emptyPanels(GridBagConstraints gbc) {
        JPanel empty = new JPanel();
        gbc.gridy = lineNumber;
        empty.setPreferredSize(new Dimension(0, 15));
        ganttBody.add(empty, gbc);
        lineNumber++;
    }

    /**
     * A method to draw dates on this Gantt chart.
     * 
     * @param gbc           a set of settings for grid layout
     * 
     * @param duration      how many days in total
     * 
     * @param startDate     the earliest date possible
     */
    private void drawDates(GridBagConstraints gbc, int duration, 
                                                            Date startDate) {
        for (int k = 0; k < duration; k++) {

            startDate.setDate(startDate.getDate() + 1);

            int m = startDate.getMonth() + 1;
            String month = String.format("%02d", m);
            JLabel dateLabel = new JLabel(startDate.getDate() + "/" + month);
            gbc.gridx = k + 6;
            ganttBody.add(dateLabel, gbc);
        }
    }
    
    /**
     * A method to draw lines between milestones and tasks that belong to them.
     * 
     * @param drawnTasks    a list of tasks that are already on this Gantt chart
     * 
     * @param milName       the name of a milestone
     * 
     * @param days          the difference in days between the milestone's
     *                      deadline and the earliest date on this Gantt chart
     */
    private void connectMilestonesTasks(ArrayList<GanttLine> drawnTasks, 
                                                    String milName, int days) {
        for (GanttLine gl : drawnTasks) {
            if (gl.getMilestones().contains(milName)) {
                int x = gl.getX();
                int y = gl.getY();

                headerGBC.gridx = x - 1;
                headerGBC.gridy = y;

                //lines for x axis
                for (int l = 0; l < (days + 6) - x; l++) {
                    Line line = new Line(70, 5, 0, 5, Color.red, 
                                            headerGBC.gridx, headerGBC.gridy);
                    for (Line lin : drawnLines) {
                        //if overlaps with something, get rid of it since
                        //milestone connections have priority
                        if (lin.getGridX() == headerGBC.gridx && 
                                lin.getGridY() == headerGBC.gridy) {
                            lin.setVisible(false);
                        }
                    }
                    drawnLines.add(line);
                    ganttBody.add(line, headerGBC);
                    headerGBC.gridx++;
                }

                //small line when the highest x value and lowest y value meet
                Line line = new Line(10, 100, 10, 5, Color.red, 
                                            headerGBC.gridx, headerGBC.gridy);
                drawnLines.add(line);
                ganttBody.add(line, headerGBC);
                headerGBC.gridy++;

                //lines for y axis
                for (int l = 0; l < lineNumber - y - 1; l++) {
                    line = new Line(10, 100, 10, 0, Color.red, 
                                        headerGBC.gridx, headerGBC.gridy);
                    for (Line lin : drawnLines) {
                        //if overlaps with something, get rid of it since
                        //milestone connections have priority
                        if (lin.getGridX() == headerGBC.gridx && 
                                        lin.getGridY() == headerGBC.gridy) {
                            lin.setVisible(false);
                        }
                    }
                    drawnLines.add(line);
                    ganttBody.add(line, headerGBC);
                    headerGBC.gridy++;
                }
            }
        }
    }
    
    /**
     * A method to draw lines to represent dependencies between tasks.
     * 
     * @param referenceName reference name of a task
     * 
     * @param drawnTasks    a list of tasks that are already on this Gantt chart
     * 
     * @param days          the difference in days between the tasks's
     *                      deadline and the earliest date on this Gantt chart
     * 
     * @param duration      the difference in days between the tasks's
     *                      deadline and the earliest date on this Gantt chart
     * 
     * @param critical      a boolean to tell whether or not a Task is critical
     *                      to the assignment
     */
    private void connectTasks(String referenceName, 
            ArrayList<GanttLine> drawnTasks, int days, 
            int duration, boolean critical) {

        //if reference exists, connect
        if (referenceName != null) {
            for (GanttLine gl : drawnTasks) {
                
                //"connect" to the reference
                if (gl.getName().equalsIgnoreCase(referenceName)) {

                    //to avoid silly mistakes..
                    int holderX = headerGBC.gridx;
                    int holderY = headerGBC.gridy;

                    //draw lines from the last point of the current task 
                    //to the last point of the reference
                    //x and y of the reference
                    int refX = gl.getX();
                    int refY = gl.getY();

                    //x and y of the current task
                    int x = days + 6 + duration;
                    int y = lineNumber;

                    Color color;
                    if (critical) {
                        color = Color.RED;
                    } else {
                        color = Color.BLACK;
                    }

                    headerGBC.gridx = refX - 1;
                    headerGBC.gridy = refY;

                    //lines for x axis
                    for (int l = 0; l < x - refX; l++) {
                        Line line = new Line(70, 5, 0, 5, color, 
                                            headerGBC.gridx, headerGBC.gridy);
                        drawnLines.add(line);
                        ganttBody.add(line, headerGBC);
                        headerGBC.gridx++;
                    }

                    //small line where the highest x value meets the lowest y value
                    Line line = new Line(10, 100, 10, 5, color, 
                                            headerGBC.gridx, headerGBC.gridy);
                    drawnLines.add(line);
                    ganttBody.add(line, headerGBC);
                    headerGBC.gridy++;

                    //lines for y axis
                    for (int l = 0; l < y - refY; l++) {
                        line = new Line(10, 100, 10, 0, color, 
                                            headerGBC.gridx, headerGBC.gridy);
                        drawnLines.add(line);
                        ganttBody.add(line, headerGBC);
                        headerGBC.gridy++;
                    }

                    //to avoid silly mistakes..
                    headerGBC.gridx = holderX;
                    headerGBC.gridy = holderY;
                }
            }
        }
    }

}
