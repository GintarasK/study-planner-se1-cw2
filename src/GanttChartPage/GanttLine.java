package GanttChartPage;

import java.util.ArrayList;

/**
 * A class to represent a task on a Gantt chart.
 */

public class GanttLine {
    //task name
    private String name;
    
    //gridx and gridy
    private int x, y;
    
    //list of milestone that the task belongs to
    private ArrayList<String> milestones;
    
    /**
     * A constructor, duh.
     * 
     * @param name          name of the Task
     * @param milestones    list of milestone names that the Task belongs to
     * @param x             gridx
     * @param y             gridy
     */
    public GanttLine(String name, ArrayList<String> milestones, int x, int y){
        this.milestones = milestones;
        this.name = name;
        this.x = x;
        this.y = y;
    }
    
    /**
     * An accessor method to return the name of this GanttLine.
     * 
     * @return          the name of this GanttLine
     */
    public String getName(){
        return this.name;
    }
    
    /**
     * An accessor method to return the gridX of this GanttLine.
     * 
     * @return          the gridX of this GanttLine
     */
    public int getX(){
        return this.x;
    }
    
    /**
     * An accessor method to return the gridY of this GanttLine.
     * 
     * @return          the gridY of this GanttLine
     */
    public int getY(){
        return this.y;
    }
    
    /**
     * An accessor method to return the list of Milestone names that this
     * GanttLine is associated with.
     * 
     * @return          the list of Milestone names that this
     *                  GanttLine is associated with
     */
    public ArrayList<String> getMilestones(){
        return this.milestones;
    }
}
