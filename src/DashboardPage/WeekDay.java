/**
 * GUI class to create a panel showing the deadline in a single day of the week
 * The layout of the panel inside this panel will change depending 
 * on how many components need to be displayed.
 * 
 * The data received from the DashboardController is in the following 
 * structure ( relative to a single day) : 
 * [ Number of assignments, 
 *          Assignment name, start time/ deadline time ( depending if is a 
 *          Exam/Course test or a Coursework)
 *          ...
 *  number of Milestones, 
 *          milestone name,
 *          ...
 * number of tasks
 *          task name, progress
 *          ... ]
 * 
 */

package DashboardPage;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import studyplanner.controller.DashBoardController;

public class WeekDay extends JPanel {

    // Initialise a GridBagLayout object with its constraints object
    GridBagLayout      layout = new GridBagLayout();
    GridBagConstraints c      = new GridBagConstraints();
    
    /**
     * Constructor to create a new JPanel for a specific day of the week
     * giving the information to display
     * 
     * @param weekDate      the week date for this panel
     * @param weekNumber    the week number for this panel
     */
    public WeekDay(String weekDate,String weekNumber) {
        
        // Initialise the component of this panel
        initComponents();
        
        // Set the title of this panel 
        weekTitle.setText(weekDate + "  " + weekNumber);
       
        // Set the layout for this panel
        dayInfoList.setLayout(layout);
        
        // Get the information to display asking to the 
        // controller
        ArrayList<String> info = DashBoardController.getDayInfo(weekNumber);
        
        // Temporary ArrayList to store the information for a signle
        // component.
        ArrayList<String> panelInfo = new ArrayList<>();
        
        // Temporary list where to store WeekDayPanel created
        ArrayList<WeekDayPanel> tempList = new ArrayList<>();
        
        // Counter to access the data in info in the righe sequence
        int infoCount = 0;
        
        // Get the number of assignment for the current day
        int loopCount = Integer.parseInt(info.get(infoCount++));
        
        // Initialise a sum variable to keep track of how many WeekDayPanel
        // are going to be in the day
        int sum = loopCount;
        
        // Iterate through each assignment 
        for (int i = 0; i < loopCount; i++) {
            
            // Add assignment name
            panelInfo.add(info.get(infoCount++));
            
            // Add Deadline or Start time 
            panelInfo.add(info.get(infoCount++));
            
            // Create a WeekDayPanel with the information of an
            // assignment
            WeekDayPanel p = new WeekDayPanel("ASSIGNMENT",panelInfo);
            
            // Add it to the temporary ArrayList
            tempList.add(p);
            
            // Empty the array list
            panelInfo.clear();
            
        }
        
        // Get the number of milestones
        loopCount = Integer.parseInt(info.get(infoCount++));
        
        // Update the sum counter
        sum += loopCount;
        
        // Interate through eacyh milestone
        for (int i = 0; i < loopCount; i++) {
            
            // Add milestone name
            panelInfo.add(info.get(infoCount++));
            
            // Create a WeekDayPanel with the information for a milestone 
            WeekDayPanel p = new WeekDayPanel("MILESTONE",panelInfo);
            
            // Add it to the temporary ArrayList
            tempList.add(p);
            
            // Empty the array list
            panelInfo.clear();
        }
        
        // Get the number of task 
        loopCount = Integer.parseInt(info.get(infoCount++));
        
        // Update the sum counter
        sum += loopCount;
        
        // Iterate through each task 
        for (int i = 0; i < loopCount; i++) {
            
            // Add task name
            panelInfo.add(info.get(infoCount++));
            
            // Add progress
            panelInfo.add(info.get(infoCount++));
            
            // Create a WeekDayPanel with the information for a task 
            WeekDayPanel p = new WeekDayPanel("TASK",panelInfo);
           
            // Add it to the temporary ArrayList
            tempList.add(p);
            
            // Empty the array list
            panelInfo.clear();
        }
        
        // Initialise the constraints for this panel given the number 
        // of task to display
        this.initialiseConstraints(sum);
        
        // Initialise a counter for keep track of the position on which 
        // each WeekDayPanel should go 
        int positionCounter = 0;
        
        // Set grix to 0
        c.gridx = 0;
        
        // Iterate through eac WeekDayPanel in the tempList
        for (WeekDayPanel p : tempList) {
            
            // Set the row position constraint
            c.gridy = positionCounter++;
            
            // Add the panel to the dayInfoList panel
            dayInfoList.add(p,c);
        }
        
        
        this.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 1, Color.BLACK));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titlePanel = new javax.swing.JPanel();
        weekTitle = new javax.swing.JLabel();
        dayInfoList = new javax.swing.JPanel();

        setPreferredSize(new java.awt.Dimension(1000, 610));

        titlePanel.setPreferredSize(new java.awt.Dimension(500, 22));

        weekTitle.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        weekTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout titlePanelLayout = new javax.swing.GroupLayout(titlePanel);
        titlePanel.setLayout(titlePanelLayout);
        titlePanelLayout.setHorizontalGroup(
            titlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(weekTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
        );
        titlePanelLayout.setVerticalGroup(
            titlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(weekTitle, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
        );

        dayInfoList.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N

        javax.swing.GroupLayout dayInfoListLayout = new javax.swing.GroupLayout(dayInfoList);
        dayInfoList.setLayout(dayInfoListLayout);
        dayInfoListLayout.setHorizontalGroup(
            dayInfoListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        dayInfoListLayout.setVerticalGroup(
            dayInfoListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 555, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titlePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(dayInfoList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titlePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(dayInfoList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
   
    /**
     * Method to initialise the constraints to use in the GridBagLayout
     * 
     * @param numberOfRows number of rows to have in the grid 
     *                     ( associated with number of module )
     */
    private void initialiseConstraints(int numberOfRows) {
        
        // Place the label to the NORTH of its container 
        c.anchor = GridBagConstraints.NORTH;
        
        // Set the padding for the label
        c.insets = new Insets(0, 0, 10, 0);
        
        // Specify the number of columns and rows of the grid layout
        c.weightx = 1;
        c.weighty = numberOfRows;

        // The component will fill HORIZONTAL space
        c.fill = GridBagConstraints.HORIZONTAL;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel dayInfoList;
    private javax.swing.JPanel titlePanel;
    private javax.swing.JLabel weekTitle;
    // End of variables declaration//GEN-END:variables
}
