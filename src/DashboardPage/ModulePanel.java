/**
 * GUI class to model a panel showing weekly information about a model.
 * The panel is composed of two panel one displaying the name of the module
 * and how many hours have been spent in that week, and a information panel
 * in which are going to be added several MilestoneTaskPanel to it to display
 * the actual information about the module
 * 
 * The information are passed in an ArrayList of String in the following 
 * structure :
 * [ Module name, number of assignments
 *      assignment name, number of milestones
 *          milestone name, milestone deadline, number of tasks
 *              task name, contribution to completion, task deadline,
 *              ...
 *      number of task
 *          task name, contribution to completion, task deadline,
 *          ... ]
 */

package DashboardPage;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

public class ModulePanel extends javax.swing.JPanel {
    
    // Initialise a GridBagLayout object with its constraints object
    GridBagLayout      layout = new GridBagLayout();
    GridBagConstraints c      = new GridBagConstraints();
    
    /**
     * Constructors to create a new ModulePanel in which are going to 
     * be displayed all the information relative to the current dashboard 
     * week for a single module 
     * 
     * @param informations  ArrayList storing the information of a module
     * @param time          weekly time spent for the module
     */
    public ModulePanel(ArrayList<String> informations, String time) {

        // Initialise the components of the panel
        initComponents();
        
        // Count variable to access the information inside the ArrayList
        int infoCount = 0;

        // Set the moduleName label  
        moduleName.setText(" *** " + informations.get(infoCount++) + " ***");

        // Set the text timeSpent label 
        timeSpent.setText("Time spent this week : " + time + " hours");

        // Set the layout for the panel 
        infoPanel.setLayout(layout);

        // Get the number of assignments 
        int numberOfAssignments = Integer.parseInt(informations.get(infoCount++));
        
        // Set the constraints for the layout
        this.initialiseConstraints(numberOfAssignments);
        
        // Iterate through though each Assignments
        for (int j = 0; j < numberOfAssignments; j++) {
            
            // Inialise ArrayList for assignment information 
            ArrayList<String> assignmentInfo = new ArrayList<>();
           
            // Add assignment name
            assignmentInfo.add(informations.get(infoCount++));

            // Get the number of milestones for that assignment 
            int numberOfMilestones = Integer.parseInt(informations.get(infoCount++));

            // Test if the number of milestone is 0
            if (0 == numberOfMilestones) {
                
                //No milestone with that assignment - do nothing
            } else {
                
                // Otherwise iterate through all of them 
                for (int k = 0; k < numberOfMilestones; k++) {

                    // Add name of milestone 
                    assignmentInfo.add(informations.get(infoCount++));
                    
                    // Add deadline of milestone
                    assignmentInfo.add(informations.get(infoCount++));

                    // Get number of task 
                    int numberOfTask = Integer.parseInt(informations.get(infoCount++));
                    
                    // Add number of task
                    assignmentInfo.add(String.valueOf(numberOfTask));
                    
                    // Tesk if number of task is 0
                    if (0 == numberOfTask) {
                        
                        //No task with the milestone - do nothing 
                    } else {

                        // Iterate through each task
                        for (int l = 0; l < numberOfTask; l++) {
                            
                            // Add task name
                            assignmentInfo.add(informations.get(infoCount++));
                            
                            // Add contribution
                            assignmentInfo.add(informations.get(infoCount++));
                            
                            // Add task deadline
                            assignmentInfo.add(informations.get(infoCount++));
                        }
                    }
                }
            }

            // Get the number of individual task 
            int numberOfIndividualTasks = Integer.parseInt(informations.get(infoCount++));

            // Test if the number of individual task is 0
            if (0 == numberOfIndividualTasks) {
                
                // No individual task - do nothing 
            } else {
                
                // Iterate through each task
                for (int k = 0; k < numberOfIndividualTasks; k++) {
                    
                    // Add task name
                    assignmentInfo.add(informations.get(infoCount++));
                    
                    // Add contribution
                    assignmentInfo.add(informations.get(infoCount++));
                    
                    // Add task deadline
                    assignmentInfo.add(informations.get(infoCount++));
                }
            }

            // Create a new AssignmnetPanel with the given information 
            AssignmentPanel p = new AssignmentPanel(assignmentInfo, numberOfMilestones, numberOfIndividualTasks);

            // Constaint to set the position of the assignment panel inside
            // the ModulePanel
            c.gridy = j ;

            // Add panel to infopanel
            infoPanel.add(p, c);
        }  
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        moduleName = new javax.swing.JLabel();
        timeSpent = new javax.swing.JLabel();
        infoPanel = new javax.swing.JPanel();

        moduleName.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        moduleName.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(moduleName, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
            .addComponent(timeSpent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(moduleName, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(timeSpent, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout infoPanelLayout = new javax.swing.GroupLayout(infoPanel);
        infoPanel.setLayout(infoPanelLayout);
        infoPanelLayout.setHorizontalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        infoPanelLayout.setVerticalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 221, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(infoPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(infoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Method to initialise the constraints to use in the GridBagLayout
     * 
     * @param numberOfRows number of rows to have in the grid 
     *                     ( associated with number of module )
     */
    private void initialiseConstraints(int numberOfRows ) {
        
        // Place the label to the NORTH of its container 
        c.anchor = GridBagConstraints.NORTH;
        
        // Set the padding for the label
        c.insets = new Insets(0, 0, 20, 0);
        
        // Specify the number of columns and rows of the grid layout
        c.weightx = 1;
        c.weighty = numberOfRows;

        // The component will fill HORIZONTAL space
        c.fill = GridBagConstraints.HORIZONTAL;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel infoPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel moduleName;
    private javax.swing.JLabel timeSpent;
    // End of variables declaration//GEN-END:variables
}
