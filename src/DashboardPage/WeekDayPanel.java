/**
 * GUI class to design a Panel showing information about an ASSIGNMENT, 
 * MILESTONE or TASK for a specific day of the week.
 * The information show will depend on which type the panel will be created
 * and as well as the background colour will change: 
 * - ASSIGNMENT :  ~ Exam        :  RED
 *                 ~ Course Test :  GREEN
 *                 ~ Coursework  :  CYAN
 * - MILESTONE  : ORANGE
 * - TASK       : YELLOW
 */
package DashboardPage;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

public class WeekDayPanel extends javax.swing.JPanel {

    // Initialise a GridBagLayout object with its constraints object
    GridBagLayout      layout = new GridBagLayout();
    GridBagConstraints c      = new GridBagConstraints();

    /**
     * Constructor to create a new WeekDayPanel showing the information 
     * relative to an ASSIGNMENT, MILESTONE or TASK;
     * Depending on the type of component to show the panel will have
     * different characteristics and background colour.
     * 
     * @param type  the type of the component to show 
     *              ( ASSIGNMENT, MILESTONE or TASK)
     * @param info ArrayList storing the information of the component to display
     */
    public WeekDayPanel(String type, ArrayList<String> info) {
        
        // Initialise panel component
        initComponents();
        
        // Set the layout of the panel
        this.setLayout(layout);
        
        // Create a label to store the name 
        JLabel info1 = new JLabel();
        
        // Set the font for the label 
        info1.setFont(new Font("Times New Roman", Font.BOLD, 14));
        
        // Initialise a second label 
        JLabel info2 =  new JLabel();
        
        // Initialise a progress bar
        JProgressBar bar = null;

        // Get the first data form the array list -> the name 
        info1.setText(info.get(0));
        
        // Test if is going to be for an Assignment 
        if (type.compareTo("ASSIGNMENT") == 0) {
            
            // The assignement is the only one type which has a second label 
            // So set the font for it 
            info2.setFont(new Font("Times New Roman", Font.BOLD, 14));
            
            // tes if is an Exam or Course Test 
            if (!(info1.getText().contains("Exam") || info1.getText().contains("test"))) {
                
                // If not is a coursework
                // Take the second information 
                info2.setText("Deadline : " + info.get(1));
                
                // Set the background color
                this.setBackground(new Color(204, 255, 255));
                
            } else {
                
                // Is going to be an exam or course test so 
                // take the starting hour 
                info2.setText("Starting hour : " + info.get(1));
                
                if (info1.getText().contains("Exam")) {
                    
                    // Set background colour if is an exam 
                    this.setBackground(new Color(255, 128, 128));
                    
                } else {
                    
                    // Set background colour if is a course test
                    this.setBackground(new Color(187, 255, 153));
                }
            }
        } else if (type.compareTo("MILESTONE") == 0) {
            
            // Set background colour if is a milestone 
            this.setBackground(new Color(255, 179, 128));
            
        } else {
            // Otherwise is going to be a task 
            
            // Initialise a progress bar for the task 
            bar = new JProgressBar(0,100);
            
            // Get the progress 
            int temp = Integer.parseInt(info.get(1));
            
            // Set the progress of the progress bar
            bar.setValue(temp);
            
            // Display the progress in the bar
            bar.setStringPainted(true);
            
                    
            // Set the background for the task
            this.setBackground(new Color(255, 255, 153));
        }
        
        // Set the padding for the label
        c.insets = new Insets(5, 0, 10, 0);
  
        // Specify the number of columns and rows of the grid layout
        c.weightx = 1;
        c.weighty = 2; // Since only two elements
        
        // Set the position in the grid for the first label
        c.gridx = 0;
        c.gridy = 0;
        
        // Set the anchor
        c.anchor = GridBagConstraints.CENTER;
        
        // Set the fill none 
        c.fill = GridBagConstraints.NONE;
        
        // Add the first 
        this.add(info1,c);
        
        // Set the position in the grid for the second label
        c.gridy = 1;
        
        // Test if the bar is null
        if (bar == null ) {
            
            // If is null means is not for a task so display the second label 
            this.add(info2,c);
            
        } else {
            
            // Set padding for the bar
            c.insets = new Insets(0,5,10,5);
            
            // Set the fill 
            c.fill = GridBagConstraints.HORIZONTAL;
            
            // Otherwise display the bar
            this.add(bar,c);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 372, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 321, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
