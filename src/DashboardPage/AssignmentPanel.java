/**
 * GUI class to create a panel with the given information relative to that 
 * assignment.
 * In this panel will be added several MilestoneTaskPanle depending 
 * on the information passed
 * 
 * The information are passed in an ArrayList of String in the following 
 * structure :
 * [ assignment name, 
 *      milestone name, milestone deadline, number of task
 *          task name, contribution to completion, task deadline
 *          ...
 *      individual task name, contribution to completion, task deadline
 *      ... ]
 */

package DashboardPage;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import javax.swing.BorderFactory;

public class AssignmentPanel extends javax.swing.JPanel {

    // Initialise a GridBagLayout object with its constraints object
    GridBagLayout      layout = new GridBagLayout();
    GridBagConstraints c      = new GridBagConstraints();
    
    /**
     * Constructor to create a new AssignmentPanel given the needed
     * information.
     * 
     * @param informations          ArrayList of informations
     * @param numberOfMilestone     number of milestones 
     * @param numberOfIndividualTask    number of single task 
     */
    public AssignmentPanel(ArrayList<String> informations, 
                            int numberOfMilestone, int numberOfIndividualTask) {
        
        // Initialise the components of the panel 
        initComponents();

        // Initialise a counter to access the information in the
        // ArrayList
        int infoCount = 0;
        
        // Set the a titled border for this panel 
        this.setBorder(BorderFactory.createTitledBorder(
            BorderFactory.createMatteBorder(1,0,1,0,new Color(0,0,0)), 
            (informations.get(infoCount++))));

        // Set the layout for this panel 
        this.setLayout(layout);

        // Initialise the constraints for this panel 
        this.initialiseConstraints(numberOfMilestone + numberOfIndividualTask);
        
        // Create an ArrayList for the infromation 
        ArrayList<String> info = new ArrayList<>();
        
        // Initialise a counter to see in which position the panel to add 
        // should go
        int positionCount = 0; 
        
        // Check if there are milestones 
        if (0 != numberOfMilestone) {
            
            // Iterate through it for each milestone 
            for (int i = 0; i < numberOfMilestone; i++) {

                // Get milestone name
                info.add(informations.get(infoCount++));

                // Get deadline 
                info.add(informations.get(infoCount++));

                // Get number of task associated with the milestone
                int numberOfTask = Integer.parseInt(informations.get(infoCount++));

                // Add number of task
                info.add(String.valueOf(numberOfTask));

                // Iterate through each Task
                for (int j = 0; j < numberOfTask; j++) {

                    // Get number of task
                    info.add(informations.get(infoCount++));

                    // Get completition of task
                    info.add(informations.get(infoCount++));

                    // Get deadline of task
                    info.add(informations.get(infoCount++));
                }

                // Create a panel for the Milestone information 
                MilestoneTaskPanel p = new MilestoneTaskPanel("MILESTONE",info);

                // Clear the ArrayList
                info.clear();

                // Set the position for the panel 
                c.gridy = positionCount++;

                // Add the panel to the AssignmentPanel 
                this.add(p,c);
            }
        }
        
        // Check if there are 
        if (0 != numberOfIndividualTask) {
            
            // Add number of individual task 
            info.add(String.valueOf(numberOfIndividualTask));
            
            // Iterate through each of them 
            for (int i = 0; i < numberOfIndividualTask; i++) {

                // get task name
                info.add(informations.get(infoCount++));

                // Get completition of task
                info.add(informations.get(infoCount++));

                // Get deadline of task 
                info.add(informations.get(infoCount++));
            } 
            
            // Create a panel to display the individual task information 
            MilestoneTaskPanel p = new MilestoneTaskPanel("TASKS",info);
            
            // Set the position for the panel 
            c.gridy = positionCount++;

            // Add the panel to this AssignmentPanel
            this.add(p,c);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 350, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 270, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    
    /**
     * Method to initialise the constraints to use in the GridBagLayout
     * 
     * @param numberOfRows number of rows to have in the grid 
     *                     ( associated with number of module )
     */
    private void initialiseConstraints(int numberOfRows ) {
        
        // Place the label to the North-West of its container 
        c.anchor = GridBagConstraints.NORTHWEST;
        
        // Set the padding for the panel  
        c.insets = new Insets(0,0,15,0);
        
        // Specify the number of columns and rows of the grid layout
        c.weightx = 1;  
        c.weighty = numberOfRows ; 
                  
        // The component will fill BOTH space
        c.fill = GridBagConstraints.BOTH; 
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
