/**
 * GUI class to initialise a frame where to display the study dashboard.
 * The frame is divided in two session:
 *  - one where there will be a week view showing the deadline in that week
 *  - the other one displaying information about the weekly deadline 
 * 
 * Methods to print a Jframe has been copied and modified from :
 * https://www.coderanch.com/t/339783/java/Print-JFrame-components
 */

package DashboardPage;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.SwingUtilities;
import studyplanner.controller.DashBoardController;


public class DashboardPage extends JFrame implements Printable{

    // Array to store the date of the current view
    private Date [] week = new Date[7];
    
    // ArrayList to store the information about the current week of the 
    // dashboard view.
    // The information stored in the ArrayList are in the following 
    // structure :
    // [ number of modules,
    //      Module Name, number of assignments
    //          assignment Name, number of milestone
    //          milestone name, milestone deadline, number of tasks
    //              task name, contribution to completion, task deadline
    //              ...
    //          number of individual tasks,
    //              task name, contribution to completition, task deadline 
    //              ... ]
    private ArrayList<String> informations = new ArrayList<>();
    
    // ArrayList to store the hoursSpent for each module in the 
    // current week of the dashboard view
    // The information stored in the ArrayList are in the following 
    // structore :
    // [ Module name, hour spent during the week, ... ]
    private ArrayList<String> hoursSpent   = new ArrayList<>();
    
    // Date to store the starting date to pass to the controller
    private Date startDateRange;
    
    // Date to store the ending date to pass to the controller
    private Date endDateRange;
    
    // startDateRange/endDateRange are one day before and after the actual 
    // week displayed on the dashbord and are need given the way in which 
    // java compare two Date object using the method Date.before()/Date.after
    
    // Initialise a Calendar object
    private Calendar calendar = GregorianCalendar.getInstance(Locale.getDefault());
    
    // Initialise a DateFormat object
    DateFormat        df   = new SimpleDateFormat("dd/MM", Locale.getDefault());
    
    // Initialise a GridBagLayout object with its constraints object
    GridBagLayout      layout = new GridBagLayout();
    GridBagConstraints c      = new GridBagConstraints();
    
    // Initialise a scroll layout to use for the dashInfoScrollPanel
    ScrollPaneLayout scrollLayout = new ScrollPaneLayout();
    
    /**
     * Creates new frame for a DashboardPage
     */
    public DashboardPage() {
        
        // Initialise the component in the frame
        initComponents();
       
        // Initialise tool kit so to set windows dimension and position
        this.toolkitInitialisation();
        
        // Set the layout of the weekPanel
        this.WeekPanel.setLayout(layout);
        
        // Set the calendar to monday of the current week
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        
        // Decrement calendar of one day store the date in the 
        // startDateRange and increment again the calendar
        calendar.add(Calendar.DATE, -1);
        startDateRange = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        
        // For loop to iterate through all the week
        for (int i = 0; i < week.length; i++) {
            
            // Save each day Date in the array week
            week[i] = calendar.getTime();
            
            // Add one day to the calendar
            calendar.add(Calendar.DATE, 1);
        }
        
        // Save the end date of the range
        endDateRange = calendar.getTime();
        
        // Update the information for the current week
        this.updateWeekInformation();
        
        // Update the view of the calendar DELETE -------------------------------------------------
        this.updateWeekCalendar(); 
        //-------------------------------------------------------------------------------------------
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titlePanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        previous = new javax.swing.JButton();
        next = new javax.swing.JButton();
        WeekPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        print = new javax.swing.JButton();
        close = new javax.swing.JButton();
        legend = new javax.swing.JButton();
        dashInfoScrollPanel = new javax.swing.JScrollPane();
        dashInfoPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Dashboard");

        previous.setText("Previous Week");
        previous.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                previousMouseClicked(evt);
            }
        });
        previous.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                previousKeyPressed(evt);
            }
        });

        next.setText("Next Week");
        next.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nextMouseClicked(evt);
            }
        });
        next.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nextKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout titlePanelLayout = new javax.swing.GroupLayout(titlePanel);
        titlePanel.setLayout(titlePanelLayout);
        titlePanelLayout.setHorizontalGroup(
            titlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(titlePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(previous, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(next, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        titlePanelLayout.setVerticalGroup(
            titlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(titlePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(titlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(titlePanelLayout.createSequentialGroup()
                        .addComponent(previous)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(titlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(next)))
                .addContainerGap())
        );

        titlePanelLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {next, previous});

        javax.swing.GroupLayout WeekPanelLayout = new javax.swing.GroupLayout(WeekPanel);
        WeekPanel.setLayout(WeekPanelLayout);
        WeekPanelLayout.setHorizontalGroup(
            WeekPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        WeekPanelLayout.setVerticalGroup(
            WeekPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 395, Short.MAX_VALUE)
        );

        jPanel1.setForeground(new java.awt.Color(240, 240, 240));
        jPanel1.setPreferredSize(new java.awt.Dimension(313, 20));

        print.setText("Print");
        print.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                printMouseClicked(evt);
            }
        });
        print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                printKeyPressed(evt);
            }
        });

        close.setText("Close");
        close.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeMouseClicked(evt);
            }
        });
        close.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                closeKeyPressed(evt);
            }
        });

        legend.setText("Legend");
        legend.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                legendMouseClicked(evt);
            }
        });
        legend.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                legendKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(close, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(legend, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(print, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {close, print});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(print, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(close, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(legend))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {close, legend, print});

        dashInfoScrollPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Week Informations", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 18))); // NOI18N
        dashInfoScrollPanel.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        dashInfoScrollPanel.setFont(new java.awt.Font("Times New Roman", 2, 11)); // NOI18N

        javax.swing.GroupLayout dashInfoPanelLayout = new javax.swing.GroupLayout(dashInfoPanel);
        dashInfoPanel.setLayout(dashInfoPanelLayout);
        dashInfoPanelLayout.setHorizontalGroup(
            dashInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 258, Short.MAX_VALUE)
        );
        dashInfoPanelLayout.setVerticalGroup(
            dashInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 444, Short.MAX_VALUE)
        );

        dashInfoScrollPanel.setViewportView(dashInfoPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titlePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(WeekPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dashInfoScrollPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(titlePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(WeekPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, Short.MAX_VALUE))
                    .addComponent(dashInfoScrollPanel))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

/**************************** Event Listeners ********************************/          
    

    /*
     * Event listener for the ENTER key on close button
     */  
    private void closeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_closeKeyPressed
        
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
         
            this.close();
        }
    }//GEN-LAST:event_closeKeyPressed

    /*
     * Event listener for a mouse click on the close button 
     */
    private void closeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeMouseClicked
        
        this.close();
    }//GEN-LAST:event_closeMouseClicked

    /*
     * Event listener for the ENTER key on print button
     */  
    private void printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_printKeyPressed
        
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
         
            this.startPrinting();
        }
    }//GEN-LAST:event_printKeyPressed

    /*
     * Event listener for a mouse click on the print button 
     */
    private void printMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_printMouseClicked

        this.startPrinting();
    }//GEN-LAST:event_printMouseClicked

    /*
     * Event listener for the ENTER key on previous week button
     */ 
    private void previousKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_previousKeyPressed
        
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            
            this.previousWeek();
        }
    }//GEN-LAST:event_previousKeyPressed

    /*
     * Event listener for a mouse click on the previouse week button 
     */
    private void previousMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_previousMouseClicked
        
        this.previousWeek();
    }//GEN-LAST:event_previousMouseClicked

    /*
     * Event listener for the ENTER key on next week button
     */ 
    private void nextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nextKeyPressed
        
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            
            this.nextWeek();
        }
    }//GEN-LAST:event_nextKeyPressed

    /*
     * Event listener for a mouse click on the next week button 
     */
    private void nextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nextMouseClicked
        
        this.nextWeek();
    }//GEN-LAST:event_nextMouseClicked

    private void legendKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_legendKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            
            this.displayLegend();
        }
    }//GEN-LAST:event_legendKeyPressed

    private void legendMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_legendMouseClicked
        this.displayLegend();
    }//GEN-LAST:event_legendMouseClicked

    
/****************************** Methods ***************************************/  
    
    /*
     * Method called when the close button is clicked, it will close the 
     * application 
     */
    private void close() {
        
        this.dispose();
    }
    

    /**
     * Method called when the print button is clicked
     */
    private void startPrinting(){
  
        // Set up a printer job
        PrinterJob job = PrinterJob.getPrinterJob();
     
        // Set the page format
        PageFormat format = job.defaultPage();
        
        // Set landscape orientation 
        format.setOrientation(PageFormat.LANDSCAPE);
        
        // Set this page as printable
        job.setPrintable(this, format);
         
        try{
            // Open the operating system dialog for the printer
            if(job.printDialog()) {
                
                // If everything works print the frame
                job.print();
            }
        }
        catch(HeadlessException | PrinterException ex){
        
            // Display error message in a dialog
            JOptionPane.showMessageDialog(this,ex.getMessage(),
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
         
    }
  

    /*
     * Actual method to print the frame
     */
    @Override
    public int print(Graphics g, PageFormat format, int pagenum) {
        
        // Since I am going to print only a page test if there are more
        if (pagenum > 0){
            
            return Printable.NO_SUCH_PAGE;
        }
       
       g.translate((int)format.getImageableX(), (int)format.getImageableY());
       
       // Set page dimension 
       float pageWidth  = (float)format.getImageableWidth();
       float pageHeight = (float)format.getImageableHeight();
        
       // Set image dimension 
       float imageHeight = (float)this.getHeight();
       float imageWidth  = (float)this.getWidth();
       
       // Create scale factor 
       float scaleFactor = Math.min((float)pageWidth/(float)imageWidth,
                            (float)pageHeight/(float)imageHeight);
  
       // Scale the dimentsion of the image
       int scaledWidth  = (int)(((float)imageWidth)*scaleFactor);
       int scaledHeight = (int)(((float)imageHeight)*scaleFactor);  
        
       // Create a canvas 
       BufferedImage canvas = new BufferedImage( this.getWidth(),  
                                                 this.getHeight(), 
                                                 BufferedImage.TYPE_INT_RGB);
       
       // Create a 2D graphic from the canvas
       Graphics2D gg = canvas.createGraphics();
       
       // Paint the graphic
       this.paint( gg );  
       
       // Create the image form the canvas
       Image img = canvas ;
  
       // Draw the image
       g.drawImage(img, 0, 0, scaledWidth, scaledHeight, null );
  
       // Return if successfully executed
       return Printable.PAGE_EXISTS;  
    }
    
    
    /**
     * Method called when the previous week button is clicked
     * The method will update with the previous week value 
     * the Date stored in the week variable 
     */
    private void previousWeek() {

        // Set the calendar 15 days before 
        calendar.add(Calendar.DATE, -15);
        
        // update the startDateRange 
        startDateRange = calendar.getTime();
        
        // Increment calendar by one
        calendar.add(Calendar.DATE, 1);
        
        // Update the Date objects in the week array 
        for (int i = 0; i < week.length; i++) {
            
            // Update the Date in the week variable
            week[i] = calendar.getTime();
            
            // Increment the calendar by one day
            calendar.add(Calendar.DATE, 1);
        }
        
        // Update the endDateRange 
        endDateRange = calendar.getTime();
       
        // Update the information in the dashInfoPanel given the new week
        this.updateWeekInformation();
        
        // Update the view of the week
        this.updateWeekCalendar();
    }
    
    /**
     * Method called when the next week button is clicked
     * The method will update with the next week value 
     * the Date stored in the week variable 
     */
    private void nextWeek() {

        // Decrement the calendar by one day
        calendar.add(Calendar.DATE, -1);
        
        // Update the startDateRange
        startDateRange = calendar.getTime();
        
        // Increment calendar by one day
        calendar.add(Calendar.DATE, 1);
        
        // Update the Date objects in the week array 
        for (int i = 0; i < week.length; i++) {
           
            // Update the Date in the week variable
            week[i] = calendar.getTime();
            
            // Increment the calendar by one day
            calendar.add(Calendar.DATE, 1);
        }
        
        // Update the endDateRange
        endDateRange = calendar.getTime();
        
        // Update the information in the dashInfoPanel given the new week
        this.updateWeekInformation();
        
        // Update the view DELETE --------------------------------------------------
        this.updateWeekCalendar();
        //---------------------------------------------------------------------------
    }
    
    /**
     * Method used to update the week view of the dashboard
     */
    private void updateWeekCalendar () {
        
        // Set the padding constraints
        c.insets = new Insets(0,2,0,2);
        
        // Place the label to the WEST of its container 
        c.anchor = GridBagConstraints.WEST; 
            
        // Specify the number of columns and rows of the grid layout
        c.weightx = 7;  // Since 7 days per week
        c.weighty = 1; 
                           
        // The component will fill BOTH space
        c.fill = GridBagConstraints.BOTH;
        
        // Empty the panel
        WeekPanel.removeAll();
        
        // For loop to create a panel for each day of the week
        for (int i = 0; i < week.length; i++) {

            String weekDate;
            
            // Switch statement to see what day of the week is 
            switch (i) {
                
                case 0:
                    weekDate = "MON";
                    break;
                    
                case 1:
                    weekDate = "TUE";
                    break;
                    
                case 2:
                    weekDate = "WED";
                    break;
                    
                case 3:
                    weekDate = "THU";
                    break;
                    
                case 4:
                    weekDate = "FRI";
                    break;
                    
                case 5:
                    weekDate = "SAT";
                    break;
                    
                default:
                    weekDate = "SUN";
                    break;
            }
            
            // Create a week day panel given the weekDate and the week number 
            WeekDay panel = new WeekDay(weekDate, df.format(week[i]));
            
            // Constaint to set the position of the weekDay panel inside
            // the WeekPanel
            c.gridx = i+1;
            c.gridy = 1;
            
            // Add panel to WeekPanel
            WeekPanel.add(panel, c);
        }
        
        //updateWeekInformation();
        updateDashInfoPanel();
        
     
        // Revalidate and repaint the frame so to show the updated view
        SwingUtilities.updateComponentTreeUI(this);
        this.WeekPanel.revalidate();
        this.WeekPanel.repaint();
    }
    
    /**
     * Method used to update the information stored relative to the 
     * current week displayed.
     */
    private void updateWeekInformation() {
        
        // Update the information stored in the ArrayList
        informations = DashBoardController.getUpcomingDeadlines(startDateRange, endDateRange);
        
        // Update the hours spent in the ArrayList
        hoursSpent = DashBoardController.getHoursSpent(startDateRange, endDateRange);
    }
    
    /**
     * Method to update the information in the dashInfoPanel.
     * All the information are going to be relative only to the 
     * week displayed in the dashboard
     */
    private void updateDashInfoPanel(){ 
        
        // Create variable to store number of tasks for that week
        // Represent the total number of tasks in the week ( both tasks 
        // associated with a milestone and not )
        int numberOfIndividualTasks = 0;
        
        // Create a variable to store the number of Milestones for that week
        int numberOfMilestones = 0;
        
        // Variable used to access the right information stored in the 
        // ArrayList
        int infoCount = 0;
        
        // Get the number of module of the current semester

 
  
        int numberOfModules = Integer.parseInt(informations.get(infoCount++));


        
        // Remove all the element in the dashInfoPanel 
        dashInfoPanel.removeAll();
        
        // Reset the layout in the dashInfoPanel
        dashInfoPanel.setLayout(layout);

        // Set the constraints for the layout
        this.initialiseConstraints(numberOfModules);
        
        // Iterate in the loop for each module 
        for (int i = 0; i < numberOfModules; i++) {
            
            // Create an ArrayList with the information relative to 
            // a single module
            ArrayList<String> moduleInfo = new ArrayList<>();
            
            // Add name of module
            moduleInfo.add(informations.get(infoCount++));
            
            // Get the number of assignment for the current module
            int numberOfAssignments = 
                            Integer.parseInt(informations.get(infoCount++));
            
            // Store it in the ArrayList
            moduleInfo.add(String.valueOf(numberOfAssignments));
            
            // Iterate for each assignment 
            for (int j = 0; j < numberOfAssignments; j++) {
                
                // Get assignment name
                String StringTemp = informations.get(infoCount++);
                
                // In case the assignment name is too long truncate it 
                if (StringTemp.length() > 38) {
                    
                    StringTemp = StringTemp.substring(0,38) + "...";
                }
                
                // Add assignment name
                moduleInfo.add(StringTemp);
                
                // Get the number of milestones for the assignment 
                numberOfMilestones = Integer.parseInt(informations.get(infoCount++));
                
                // Add the number of milestones 
                moduleInfo.add(String.valueOf(numberOfMilestones));
                
                // Test if there are milestones
                if ( 0 == numberOfMilestones) {
                    
                    //No milestone -- Do nothing 
                } else {
                   
                    // Otherwise iterate through the loop for each milestones
                    for (int k = 0; k < numberOfMilestones; k++) {
                        
                        // Add name of milestone 
                        moduleInfo.add(informations.get(infoCount++));
                        
                        // Add deadline of milestoner
                        moduleInfo.add(informations.get(infoCount++));
                        
                        // Get the number of tasks associated with the milestone
                        int numberOfTasks = Integer.parseInt(informations.get(infoCount++));
                        
                        // Add number of tasks
                        moduleInfo.add(String.valueOf(numberOfTasks));
                        if (0 == numberOfTasks) {
                            
                            //No task with the milestone - Do nothing 
                        } else {
                            
                            // Otherwise iterate in the loop for each task 
                            for (int l = 0; l < numberOfTasks; l++) {
                                
                                // Add task name
                                moduleInfo.add(informations.get(infoCount++));
                                
                                // Add contribution
                                moduleInfo.add(informations.get(infoCount++));
                                
                                // Add task deadline
                                moduleInfo.add(informations.get(infoCount++));   
                            }
                        }  
                    }
                }
                
                // Get the number of individual task 
                numberOfIndividualTasks = Integer.parseInt(informations.get(infoCount++));
                
                // Add it 
                moduleInfo.add(String.valueOf(numberOfIndividualTasks));
                
                // Test if there are tasks
                if ( 0 == numberOfIndividualTasks ) {
                    
                    //No individual task - Do nothing 
                } else {
                    // Iterate through each task
                    for (int k = 0; k < numberOfIndividualTasks; k++ ) {
                        
                        // Add task name
                        moduleInfo.add(informations.get(infoCount++));
                        
                        // Add contribution
                        moduleInfo.add(informations.get(infoCount++));
                        
                        // Add task deadline
                        moduleInfo.add(informations.get(infoCount++));  
                    }
                } 
                
                // Test if the number of individual task and the number 
                // of milestone are both 0s. If that is the case there 
                // are no milestone and task associated with the current 
                // assignement. So delete all the information entered 
                // during last iteration.
                if (numberOfIndividualTasks == 0 && numberOfMilestones == 0 ) {
                    
                    // Decrease the number of assignment by one 
                    int temp = Integer.parseInt(moduleInfo.get(1)) - 1;
                    moduleInfo.set(1, String.valueOf(temp));
                    
                    // Find the index from where starting to delete
                    int removeIndex = moduleInfo.size() -3;
                    
                    // Delete the last 3 String added to the ArrayList
                    // corresponding to : assignment name, number of milestone 
                    // and number of task
                    for (int p = 0; p < 3; p++) {
                        
                        moduleInfo.remove(removeIndex);
                    }
                    
                    // Update number of rows
                    c.weighty -= 1;
                }
            }
            
            // In case the size of the ArrayList is 2 means there are no 
            // information relative to the current module so do not create
            // a section for it 
            if (moduleInfo.size() != 2) {
                
                // Find out ours spent for it 
                String timeSpent = "";
                
                // Initialise loop count to iterate through the hoursSpent
                int z = 0;
                while (z < hoursSpent.size()) {
                    
                    // Test if the modules' name are the same 
                    if (hoursSpent.get(z).compareTo(moduleInfo.get(0)) == 0) {
                        
                        // If yes extract the time spent  
                        timeSpent = hoursSpent.get(z+1);
                        
                        break;
                        
                    }  else {
                        
                        // Otherwise increment the counter so that point 
                        // to the next name of module
                        z += 2;
                        
                    }
                }

                
                // Create a new ModulePanel with the given information 
                ModulePanel p = new ModulePanel(moduleInfo, timeSpent);
            
                // Constaint to set the position of the ModulePanel depending 
                // on the number of Module to show
                c.gridy = i;

                // Add panel to dashInfoPanel
                dashInfoPanel.add(p, c);
            }
        }
        
        // Set the layout of the scroll Panel
        this.dashInfoScrollPanel.setLayout(scrollLayout);
        
        // Add the dashInfoPanel to the scroll panel 
        this.dashInfoScrollPanel.getViewport().add(dashInfoPanel);
    }
    
    private void displayLegend() {
        
        LegendFrame f = new LegendFrame();
        
        f.setVisible(true);
        
        f.setLocationRelativeTo(null);  
    }  
    
    
    /*
     * Method to set the size and the position of the application relative
     * to the dimension of the screen
     */
    private void toolkitInitialisation() {
        
        //Create a toolkit variable
        Toolkit tk = Toolkit.getDefaultToolkit();
        
        //Set the size of the application frame relative to the size of the 
        // screen
        int xSize = (int) (tk.getScreenSize().width * 0.9);
        int ySize = (int) (tk.getScreenSize().height * 0.9);
        this.setSize(xSize, ySize);
        
        //Display the application at the center of the screen 
        this.setLocationRelativeTo(null);
    }
    
    /**
     * Method to initialise the constraints to use in the GridBagLayout
     * 
     * @param numberOfRows number of rows to have in the grid 
     *                     ( associated with number of module )
     */
    private void initialiseConstraints(int numberOfRows ) {
        
        // Place the label to the NORTHWEST of its container 
        c.anchor = GridBagConstraints.NORTHWEST; 
            
        // Specify the number of columns and rows of the grid layout
        c.weightx = 0;  
        c.weighty = numberOfRows; 
                           
        // The component will fill HORIZONTAL space
        c.fill = GridBagConstraints.HORIZONTAL;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel WeekPanel;
    private javax.swing.JButton close;
    private javax.swing.JPanel dashInfoPanel;
    private javax.swing.JScrollPane dashInfoScrollPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton legend;
    private javax.swing.JButton next;
    private javax.swing.JButton previous;
    private javax.swing.JButton print;
    private javax.swing.JPanel titlePanel;
    // End of variables declaration//GEN-END:variables

}
