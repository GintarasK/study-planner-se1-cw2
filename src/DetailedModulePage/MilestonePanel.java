package DetailedModulePage;

//----------IMPORTS----------
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXTaskPane;
//---------------------------

public class MilestonePanel extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    private ArrayList<String> listOfMilestones;
    private ArrayList<Milestone> listOfMilestoneJXTaskPane;
    private GridBagConstraints milestonePanelGBC;
    
    private String courseworkName;
    
    private int flag=0;
    
    private JLabel milestoneLabel;
    private JLabel dueDateLabel;
    //-----------------------------------------------------

    /**
     * A constructor to create a JPanel containing milestones for assignment
     *
     * @param listOfMilestones An ArrayList containing information about the
     * milestones
     */
    public MilestonePanel(ArrayList<String> listOfMilestones, String cwName) {
        this.courseworkName = cwName;
        this.listOfMilestones = listOfMilestones;

        //Set Layout for this JPanel
        this.setLayout(new GridBagLayout());

        //Create ArrayList and populate it with milestones by extracting
        //each milestone information from the ArrayList of milestones
        listOfMilestoneJXTaskPane = new ArrayList<>();
        createMilestones();

        //Add the information labels to this JPanel
        addLabels();
        //Add created courseworks to this JPanel
        addMilestones();

    }

    /**
     * A method to add information labels to this JPanel
     */
    private void addLabels() {
        
        //Create constraints for information Labels for this JPanel
        milestonePanelGBC = new GridBagConstraints();
        milestonePanelGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        milestonePanelGBC.fill = GridBagConstraints.HORIZONTAL;
        milestonePanelGBC.weighty = 0;
        milestonePanelGBC.weightx = 1;
        milestonePanelGBC.gridy = 0;
        milestonePanelGBC.gridwidth = 1;
        milestonePanelGBC.insets = new Insets(5, 0, 10, 0);

        if((listOfMilestoneJXTaskPane.size()!=0 && flag==0) || flag==1){
        //Add information Label to this JPanel
        milestonePanelGBC.gridx = 0;
        milestoneLabel = new JLabel("Milestones");
        this.add(milestoneLabel, milestonePanelGBC);
        milestonePanelGBC.gridx = 1;
        dueDateLabel = new JLabel("Due Date");
        this.add(dueDateLabel, milestonePanelGBC);
        flag=2;
        }

    }

    /**
     * A method to add already created milestones to this panel
     */
    private void addMilestones() {
        
        //Create constraints to add milestones to this JPanel
        milestonePanelGBC.fill = GridBagConstraints.HORIZONTAL;
        milestonePanelGBC.gridx = 0;
        milestonePanelGBC.gridwidth = 2;
        milestonePanelGBC.insets = new Insets(0, 5, 7, 5);

        //Add milestones to this JPanel
        for (int i = 0; i < listOfMilestoneJXTaskPane.size(); i++) {
            milestonePanelGBC.gridy = i + 1;
            this.add(listOfMilestoneJXTaskPane.get(i), milestonePanelGBC);
        }

    }

    /**
     * A method to extract milestone information and to create them
     */
    private void createMilestones() {
                
        //Create milestones with tasks and add them to ArrayList
        //Get the number of milestones
        int numberOfMilestones = Integer.parseInt(listOfMilestones.get(0));
        int countList = 1;
        for (int i = 0; i < numberOfMilestones; i++) {

            //New ArrayList to populate with information about milestone
            ArrayList<String> milestoneInformation = new ArrayList<>();
            //Get the name and due date of milestone
            for (int j = 0; j < 2; j++) {
                milestoneInformation.add(listOfMilestones.get(countList));
                countList++;
            }
            //Get the number of tasks within milestone
            String numberOfTasks = listOfMilestones.get(countList);
            milestoneInformation.add(numberOfTasks);
            countList++;

            //Get tasks
            for (int j = 0; j < Integer.parseInt(numberOfTasks); j++) {
                //Get task name, due date, type and goal
                for (int k = 0; k < 4; k++) {
                    milestoneInformation.add(listOfMilestones.get(countList));
                    countList++;
                }
            }
            //Add created milestone to ArrayList
            listOfMilestoneJXTaskPane.add(
                    new Milestone(milestoneInformation, courseworkName));
        }
    }
    
    
    
    /**
     * A method to create a new milestone
     * @param milestoneName Milestone name
     * @param date          Milestone due date
     */
    public void createMilestone(String milestoneName, String date){
        //Check for labels
        if(flag==0) flag=1;
        addLabels();
        
        //Create a new ArrayList containing all the information about this
        //milestone and send to create a new milestone
        ArrayList<String> newMilestoneInfo = new ArrayList<>();
        newMilestoneInfo.add(milestoneName);
        newMilestoneInfo.add(date);
        
        Milestone newMilestone = 
           new Milestone(newMilestoneInfo, courseworkName);
        
        //Add milestone to the list
        listOfMilestoneJXTaskPane.add(newMilestone);
        
        //Set constraints
        milestonePanelGBC.gridy=listOfMilestoneJXTaskPane.size();
        milestonePanelGBC.fill = GridBagConstraints.HORIZONTAL;
        milestonePanelGBC.gridx = 0;
        milestonePanelGBC.gridwidth = 2;
        milestonePanelGBC.insets = new Insets(0, 5, 7, 5);
        
        //add
        this.add(newMilestone, milestonePanelGBC);
        this.revalidate();
        this.repaint();
    }
    
    
    
    /**
     * An accessor method to get the list of milestones
     * @return List of milestones
     */
    public ArrayList<Milestone> getListOfMilestoneJXTaskPane(){
        
        return listOfMilestoneJXTaskPane;
    }
    
    
    /**
     * A method to remove a milestone from assignment
     * @param milestoneName A name of the milestone to remove
     */
    public void removeMilestone(String milestoneName){
        
        JXTaskPane removedMilestone = null;
        //find the milestone with the same name
        for(Milestone milestone : listOfMilestoneJXTaskPane){
            if(milestone.getMilestoneName().equalsIgnoreCase(milestoneName)){
                removedMilestone = milestone;
            }
        }
        //remove the milestone
        listOfMilestoneJXTaskPane.remove(removedMilestone);
        this.remove(removedMilestone);

        //check for labels
        if(listOfMilestoneJXTaskPane.size()==0){
            this.remove(milestoneLabel);
            this.remove(dueDateLabel);
            flag=0;
        }
        
        this.revalidate();
        this.repaint();
    }
    
}
