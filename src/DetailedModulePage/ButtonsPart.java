package DetailedModulePage;

//----------IMPORTS----------
import GanttChartPage.GanttChart;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JPanel;
import studyplanner.controller.GanttChartController;
import studyplanner.controller.SemesterProfileController;
import SemesterProfilePage.MainPageGUI;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

//---------------------------

public class ButtonsPart extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    private MainPageGUI parent;
    
    private JButton closeButton;
    private JButton backButton;
    private JButton generateGanttChartButton;
    //-----------------------------------------------------

    /**
     * A constructor to create three buttons which are positioned at the
     * bottom of the detailed module page. 
     *
     * @param parent
     */
    public ButtonsPart(MainPageGUI parent) {

        this.parent = parent;
        this.setLayout(new FlowLayout(FlowLayout.CENTER, 90, 0));
        generateGanttChartButton = new JButton("Generate Gantt Chart");
        backButton = new JButton("Back");
        closeButton = new JButton("Close");
        

        double ganttChartWidth = generateGanttChartButton.getPreferredSize().width;
        double ganttChartHeight = generateGanttChartButton.getPreferredSize().height;
        ganttChartHeight += 10;

        Dimension buttonDimension = new Dimension();
        buttonDimension.setSize(ganttChartWidth, ganttChartHeight);

        generateGanttChartButton.setPreferredSize(buttonDimension);
        backButton.setPreferredSize(generateGanttChartButton.getPreferredSize());
        closeButton.setPreferredSize(buttonDimension);
        
        JPanel mainBtnPanel = new JPanel();
        mainBtnPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 40, 0));
        mainBtnPanel.add(generateGanttChartButton);
        mainBtnPanel.add(backButton);
        
        this.add(mainBtnPanel);
        this.add(closeButton);
        
        addMouseListeners();

    }
    
    /**
     * Add action listeners to all the buttons
     */
    private void addMouseListeners() {
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    parent.reloadPage(SemesterProfileController.isEmpty());
                } catch (ItemNotFoundException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        generateGanttChartButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    GanttChart gc = new GanttChart(GanttChartController.getGanttChartInfo());
                    gc.setVisible(true);
                } catch (EmptyInputException | NullInputException | ItemNotFoundException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(ButtonsPart.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                System.exit(0);
            }
        });
    }

}
