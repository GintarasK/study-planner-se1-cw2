package DetailedModulePage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import org.jdesktop.swingx.JXDatePicker;
import studyplanner.controller.TaskController;
import utilities.exceptions.*;

public class NewTaskForm extends JDialog {

    //-------------------CLASS VARIABLES-------------------
    private JTextField taskNameText;
    private JXDatePicker taskStartDatePicker;
    private JXDatePicker taskDatePicker;
    private JComboBox typeBox;
    private JTextField goalText;
    private JComboBox dependantTaskBox;
    private JComboBox criticalBox;
    
    private JButton createButton;
    private JButton cancelButton;
    
    private String assignmentName;
    
    private JPanel notesPanel;
    private ArrayList<String> notesList;
    
    private JTextArea noteArea;
    
    private JPanel formPanel;
    private GridBagConstraints gbc;
    private JPanel mainJPanel;
    //-----------------------------------------------------
    
    /**
     * A constructor to create a new task form to create a new task
     * @param assignmentName    The name of the assignment this task will be
     *                          created in
     */
    public NewTaskForm(String assignmentName) {
        this.assignmentName = assignmentName;
        
        //Initialise all the methods
        initialise();
        
        //To make this form visible
        this.pack();
        this.setModal(true);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
    
    /**
     * A method to create a name label with a name area
     */
    private void createNameField() {
        JLabel taskNameLabel = new JLabel("Name:      ");
        taskNameText = new JTextField();
        taskNameText.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        taskNameText.setDocument(new JTextFieldLimit(25));

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridy = 0;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        formPanel.add(taskNameLabel, gbc);
        gbc.gridwidth = 3;
        gbc.gridx = 1;
        formPanel.add(taskNameText, gbc);
    }

    /**
     * A method to create a start date label and a start date picker
     */
    private void createStartDateField() {
        JLabel taskStartDateLabel = new JLabel("Start date:");
        taskStartDatePicker = new JXDatePicker();
        taskStartDatePicker.setDate(taskStartDatePicker.getLinkDay());
        taskStartDatePicker.getEditor().setEditable(false);
        taskStartDatePicker.setPreferredSize(
                new Dimension(250, taskStartDatePicker.getPreferredSize().height));
        gbc.gridy = 1;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        formPanel.add(taskStartDateLabel, gbc);
        gbc.gridwidth = 3;
        gbc.gridx = 1;
        formPanel.add(taskStartDatePicker, gbc);
    }

    /**
     * A method to create a due date label and a due date picker
     */
    private void createDueDateField() {
        JLabel taskDateLabel = new JLabel("Due date:");
        taskDatePicker = new JXDatePicker();
        Date tempDate = new Date();
        tempDate.setDate(tempDate.getDate() + 1);
        taskDatePicker.setDate(tempDate);
        taskDatePicker.getEditor().setEditable(false);
        taskDatePicker.setPreferredSize(
                new Dimension(250, taskDatePicker.getPreferredSize().height));
        taskNameText.setPreferredSize(
                new Dimension(250, taskDatePicker.getPreferredSize().height));
        gbc.gridy = 2;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        formPanel.add(taskDateLabel, gbc);
        gbc.gridwidth = 3;
        gbc.gridx = 1;
        formPanel.add(taskDatePicker, gbc);
    }

    /**
     * A method to create a type label and a type area
     */
    private void createTypeField() {
        JLabel typeLabel = new JLabel("Type:        ");
        typeBox = new JComboBox(TaskController.getTypes().toArray());
        gbc.gridy = 3;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        formPanel.add(typeLabel, gbc);
        gbc.gridx = 1;
        gbc.gridwidth = 1;
        formPanel.add(typeBox, gbc);
    }

    /**
     * A method to create a goal label with goal area
     */
    private void createGoalField() {
        JLabel goalLabel = new JLabel("Goal:");
        goalText = new JTextField();
        goalText.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        goalText.setDocument(new JTextFieldLimit(3));
        goalText.setPreferredSize(new Dimension(60,
                taskDatePicker.getPreferredSize().height));
        gbc.gridx = 3;
        formPanel.add(goalText, gbc);
        gbc.insets = new Insets(5, 30, 5, 5);
        gbc.gridx = 2;
        formPanel.add(goalLabel, gbc);
    }

    /**
     * A method to create a critical label with a combo box to choose yes or no
     */
    private void createCriticalField() {
        JLabel criticalLabel = new JLabel("Critical:");
        ArrayList<String> yesNo = new ArrayList<>();
        yesNo.add("NO");
        yesNo.add("YES");
        criticalBox = new JComboBox(yesNo.toArray());
        criticalBox.setPreferredSize(new Dimension(typeBox.getPreferredSize().width, typeBox.getPreferredSize().height));
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridy = 4;
        gbc.gridx = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(criticalLabel, gbc);
        gbc.gridx = 1;
        formPanel.add(criticalBox, gbc);
        gbc.fill = GridBagConstraints.NONE;
    }

    /**
     * A method to create a note panel
     */
    private void createNoteField() {
        JButton addNote = new JButton("Add");
        addNote.addActionListener(addNoteAction);
        gbc.insets = new Insets(20, 0, 0, 0);
        gbc.gridy = 5;
        gbc.gridx = 0;
        //formPanel.add(noteLabel, gbc);
        gbc.gridx = 3;
        formPanel.add(addNote, gbc);

        notesPanel = new JPanel();
        notesPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(new Color(0, 0, 0)), "Notes"));
        notesPanel.setLayout(new BoxLayout(notesPanel, BoxLayout.PAGE_AXIS));
        notesList = new ArrayList<>();
        addNoteField();
        gbc.insets = new Insets(10, 0, 10, 0);
        gbc.gridy = 6;
        gbc.gridwidth = 4;
        gbc.gridx = 0;
        formPanel.add(notesPanel, gbc);
    }

    /**
     * A method to create a dependant on label with combobox list
     */
    private void createDependantField() {
        JLabel dependantTaskLabel = new JLabel("Choose dependant Task:");
        gbc.insets = new Insets(10, 0, 5, 0);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        gbc.gridy = 7;
        gbc.gridx = 0;
        gbc.gridwidth = 2;
        formPanel.add(dependantTaskLabel, gbc);

        ArrayList<String> taskList = null;
        try {
            taskList = TaskController.getDependencyList(assignmentName, null);
        } catch (NullInputException | ItemNotFoundException | EmptyInputException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(NewTaskForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        taskList.add(0, "None");
        dependantTaskBox = new JComboBox(taskList.toArray());
        dependantTaskBox.setPreferredSize(new Dimension(formPanel.getPreferredSize().width, dependantTaskBox.getPreferredSize().height));
        gbc.gridy = 8;
        gbc.gridx = 0;
        gbc.gridwidth = 4;
        formPanel.add(dependantTaskBox, gbc);
    }


    /**
     * A method that calls the required methods to populate this form
     */
    private void initialise(){
        this.setLayout(new GridLayout(0,1));
        this.setTitle("Task Form");
        
        mainJPanel = new JPanel();
        mainJPanel.setLayout(new BorderLayout());
        JScrollPane mainScrollPane = new JScrollPane();
        mainScrollPane.setHorizontalScrollBar(null);
        mainScrollPane.getViewport().add(mainJPanel);
        this.add(mainScrollPane);
        
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(20, 65, 20, 65));
        panel.setLayout(new GridLayout(0, 1));

        formPanel = new JPanel();
        formPanel.setLayout(new GridBagLayout());
        gbc = new GridBagConstraints();
        
        createNameField();
        createStartDateField();
        createDueDateField();
        createTypeField();
        createGoalField();
        createCriticalField();
        createNoteField();
        createDependantField();
        panel.add(formPanel);

        createName();
        
        createButtons();
                
                
        mainJPanel.add(panel, BorderLayout.CENTER);
    }
    
    
    /**
     * A method to create a name at the top of the form
     */
    private void createName(){
        JLabel mainLabel = new JLabel("New Task");
        float size = 15;
        mainLabel.setFont(mainLabel.getFont().deriveFont(size));
        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new GridBagLayout());
        gbc = new GridBagConstraints();
        gbc.insets = new Insets(15, 0, 5, 0);
        labelPanel.add(mainLabel, gbc);
        
        mainJPanel.add(labelPanel, BorderLayout.PAGE_START);
    }
    
    /**
     * A method to create button at the bottom of the form
     */
    private void createButtons(){
        createButton = new JButton("Create");
        createButton.addActionListener(createAction);
        createButton.setPreferredSize(new Dimension(110, 25));
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        gbc.insets = new Insets(50, 10, 15, 10);
        buttonPanel.add(createButton, gbc);

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(cancelAction);
        cancelButton.setPreferredSize(new Dimension(110, 25));
        buttonPanel.add(cancelButton, gbc);
        
        mainJPanel.add(buttonPanel, BorderLayout.PAGE_END);
    }
    
    /**
     * A method to add a note field to the notes panel when a button is clicked
     */
    private void addNoteField(){
        GridBagConstraints gbc = new GridBagConstraints();
        JPanel notePanel = new JPanel();
        notePanel.setLayout(new GridBagLayout());
        gbc.insets = new Insets(4, 10, 10, 10);
        gbc.gridy = notesPanel.getComponentCount();
        gbc.gridx=0;
        gbc.weightx=0;
        int count = notesPanel.getComponentCount()+1;
        notePanel.add(new JLabel(String.valueOf(count)+". "), gbc);
        gbc.gridx=1;
        gbc.weightx=1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth=1;
        
        noteArea = new JTextArea(1, 23);
        noteArea.setBorder(BorderFactory.createCompoundBorder
                                (BorderFactory.createLineBorder(Color.GRAY), 
                                  BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        noteArea.setEditable(true);
        
        noteArea.setLineWrap(true);
        noteArea.setWrapStyleWord(true);

        notePanel.add(noteArea, gbc);
        notesPanel.add(notePanel);
    }
    
    
    /**
     * Action listener to create a new task
     */
    private ActionListener createAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            notesList = new ArrayList<>();
            for (Component panel : notesPanel.getComponents()) {
                JPanel pnl = (JPanel) panel;
                for (Component component : pnl.getComponents()) {
                    if (component instanceof JTextArea) {
                        String note = ((JTextArea) component).getText().trim();
                        if(!note.equals("")) notesList.add(note);
                    }
                }
            }
            
            String dependantTask;
            if(dependantTaskBox.getSelectedItem().toString().equalsIgnoreCase("none")){
                dependantTask = null;
            }else{
                dependantTask = dependantTaskBox.getSelectedItem().toString();
            }
            try {
                TaskController.newTask(taskNameText.getText(), 
                        taskStartDatePicker.getDate(), taskDatePicker.getDate(),
                        typeBox.getSelectedItem().toString(), goalText.getText(), 
                                        notesList, dependantTask, assignmentName,
                                        criticalBox.getSelectedItem().toString());
                dispose();
            } catch (IllegalDateException | IllegalValueException | 
                    IllegalTypeException | EmptyInputException | 
                    NullInputException | IllegalTasksException | 
                    InputOutOfRangeException | CurrentDateException | 
                    IllegalFileException | DuplicateDataException | 
                    ItemNotFoundException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(NewTaskForm.class.getName()).
                                                    log(Level.SEVERE, null, ex);
            }
        }
    };

    /**
     * Action listener to cancel this form
     */
    private ActionListener cancelAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            dispose();
        }
    };

    /**
     * Action listener to add additional note to the notes panel
     */
    private ActionListener addNoteAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (noteArea.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, 
                            "Notes field must be filled to add another note!");
            } else {
                addNoteField();
                notesPanel.revalidate();
                notesPanel.repaint();
            }
        }
    };

}
