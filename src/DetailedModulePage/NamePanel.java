package DetailedModulePage;

//----------IMPORTS----------
import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
//---------------------------

public class NamePanel extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    private String nameOfModule;
    //-----------------------------------------------------

    /**
     * A constructor to create a JPanel containing a JLabel in the middle of the
     * panel with the name of the module.
     *
     * @param nameOfModule Name of the module to display
     */
    public NamePanel(String nameOfModule) {

        this.nameOfModule = nameOfModule;

        //set this JPanel
        this.setLayout(new BorderLayout());
        this.setBorder(new EmptyBorder(8, 0, 8, 0));

        //Create a label to display module name
        JLabel nameOfModuleLabel = new JLabel(nameOfModule);
        nameOfModuleLabel.setFont(new Font("Times New Roman", Font.BOLD, 30));
        nameOfModuleLabel.setHorizontalAlignment(JLabel.CENTER);

        //add JLabel to this JPanel
        this.add(nameOfModuleLabel, BorderLayout.CENTER);
    }

    
    /**
     * An accessor method which returns the name of the module
     *
     * @return this modules name
     */
    public String getNameOfModule() {
        return nameOfModule;
    }

}
