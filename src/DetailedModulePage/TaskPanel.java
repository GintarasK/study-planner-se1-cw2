package DetailedModulePage;

//----------IMPORTS----------
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
//---------------------------

public class TaskPanel extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    private ArrayList<String> listOfTasks;
    private ArrayList<Task> listOfTasksPanels;
    private GridBagConstraints taskPanelGBC;

    private String cwName;
    private String milestoneName;

    private int flag = 0;

    private JLabel taskLabel;
    private JLabel dueDateLabel;
    private JLabel progressLabel;
    //-----------------------------------------------------

    /**
     * A constructor to create a JPanel containing task JPanels
     *
     * @param listOfTasks An ArrayList containing information about the tasks
     * @param cwName The name of the coursework this task is in
     * @param milestoneName The name of the milestone this task is in, ("null"
     * if not)
     *
     */
    public TaskPanel(ArrayList<String> listOfTasks,
            String cwName, String milestoneName) {
        this.listOfTasks = listOfTasks;
        this.cwName = cwName;
        this.milestoneName = milestoneName;
        //Set Layout for this JPanel
        this.setLayout(new GridBagLayout());

        //Create ArrayList and populate it with task JPanels by extracting
        //each tasks information from the ArrayList of tasks
        listOfTasksPanels = new ArrayList<>();
        createTasks();

        //Add the information labels to this JPanel
        addLabels();
        //Add created Task JPanels to this JPanel
        addTasks();

    }

    /**
     * A method to add information labels to this JPanel
     */
    private void addLabels() {

        //Create constraints for information Labels for this JPanel
        taskPanelGBC = new GridBagConstraints();
        taskPanelGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        taskPanelGBC.fill = GridBagConstraints.HORIZONTAL;
        taskPanelGBC.weighty = 0;
        taskPanelGBC.weightx = 1;
        taskPanelGBC.gridy = 0;
        taskPanelGBC.gridwidth = 1;
        taskPanelGBC.insets = new Insets(0, 0, 10, 0);
        taskPanelGBC.gridx = 0;

        if ((listOfTasksPanels.size() != 0 && flag == 0) || flag == 1) {
            //Create new JPanel to store JLabels for tasks with different layout
            JPanel labelPanel = new JPanel();
            labelPanel.setLayout(new GridLayout(0, 3));
            //Add JLabels to labelPanel
            taskLabel = new JLabel("Tasks");
            labelPanel.add(taskLabel, taskPanelGBC);
            dueDateLabel = new JLabel(" Due Date");
            labelPanel.add(dueDateLabel, taskPanelGBC);
            taskPanelGBC.gridx = 1;
            progressLabel = new JLabel("Progress");
            taskPanelGBC.gridx = 2;
            labelPanel.add(progressLabel, taskPanelGBC);

            //Add labelPanel to this JPanel
            this.add(labelPanel, taskPanelGBC);
            flag = 2;
        }
    }

    /**
     * A method to add already created task JPanels to this panel
     */
    private void addTasks() {

        //Create constraints to add task JPanels to this JPanel
        taskPanelGBC.fill = GridBagConstraints.HORIZONTAL;
        taskPanelGBC.gridx = 0;
        taskPanelGBC.gridwidth = 3;
        taskPanelGBC.insets = new Insets(0, 5, 7, 5);

        //Add task Jpanels to this JPanel
        for (int i = 0; i < listOfTasksPanels.size(); i++) {
            taskPanelGBC.gridy = i + 1;
            this.add(listOfTasksPanels.get(i), taskPanelGBC);
        }

    }

    /**
     * A method to extract task information and to create them
     */
    private void createTasks() {
        //Create tasks and add them to ArrayList
        if (listOfTasks.size() != 0) {
            //Get the number of tasks
            int numberOfTasks = Integer.parseInt(listOfTasks.get(0));

            int countList = 1;
            for (int i = 0; i < numberOfTasks; i++) {
                //Create new ArrayList to store each tasks information
                ArrayList<String> taskInformation = new ArrayList<>();

                //Get name, due date, type and goal
                for (int j = 0; j < 4; j++) {
                    taskInformation.add(listOfTasks.get(countList));
                    countList++;
                }
                Task taskJPanel
                        = new Task(taskInformation, cwName, milestoneName);

                //Create and add task to ArrayList
                listOfTasksPanels.add(taskJPanel);

            }
        }
    }

    /**
     * A method to create a new task
     *
     * @param name The name of the new task
     * @param deadline The deadline of the new task
     */
    public void createTask(String name, String deadline) {
        if (flag == 0) {
            flag = 1;
        }
        addLabels();

        //Populate the arraylist required to create a new task
        ArrayList<String> newTaskInformation = new ArrayList<>();
        newTaskInformation.add(name);
        newTaskInformation.add(deadline);
        newTaskInformation.add("0");
        newTaskInformation.add("0");
        //create new task
        Task newTask = new Task(newTaskInformation, cwName, null);

        //add it to the list of the tasks
        listOfTasksPanels.add(newTask);
        //set the constraints
        taskPanelGBC.gridy = listOfTasksPanels.size();
        taskPanelGBC.fill = GridBagConstraints.HORIZONTAL;
        taskPanelGBC.gridx = 0;
        taskPanelGBC.gridwidth = 3;
        taskPanelGBC.insets = new Insets(0, 5, 7, 5);

        //add it to this panel
        this.add(newTask, taskPanelGBC);
        this.revalidate();
        this.repaint();

    }

    /**
     * Accessor method to get the list of all task panels
     * @return  List of all task panels
     */
    public ArrayList<Task> getListOfTasksPanels() {
        return listOfTasksPanels;
    }

    /**
     * A method to delete a task
     * @param taskPanel     A task to be deleted
     */
    public void deleteTask(Task taskPanel) {
        //delete it from the list and the view
        listOfTasksPanels.remove(taskPanel);
        this.remove(taskPanel);

        //Check if the size is 0, remove labels
        if (listOfTasksPanels.size() == 0) {
            this.remove(taskLabel);
            this.remove(dueDateLabel);
            this.remove(progressLabel);
            flag = 0;
            this.removeAll();
        }

        this.revalidate();
        this.repaint();
    }

    /**
     * A method to add task to the main task panel
     * @param task  Task to be added
     */
    public void addTaskToMainPanel(Task task) {
        //check if there are no labels, add them
        if (flag == 0) {
            flag = 1;
        }
        addLabels();
        
        //Set the milestone to null, because it will not be in milestone
        task.setMilestone(null);
        listOfTasksPanels.add(task);
        taskPanelGBC.gridy = listOfTasksPanels.size();
        taskPanelGBC.fill = GridBagConstraints.HORIZONTAL;
        taskPanelGBC.gridx = 0;
        taskPanelGBC.gridwidth = 3;
        taskPanelGBC.insets = new Insets(0, 5, 7, 5);
        //Add the task to main panel
        this.add(task, taskPanelGBC);
        
        this.revalidate();
        this.repaint();
    }

    /**
     * A method to attach a task to a milestone
     * @param task          Task to attach
     * @param milestoneName The name of the milestone to attach to
     */
    public void attachTask(Task task, String milestoneName) {
        //Check if the milestone has no labels for the task
        if (flag == 0) {
            flag = 1;
        }
        addLabels();
        
        //Add task to the list
        listOfTasksPanels.add(task);
        //set constraints
        taskPanelGBC.gridy = this.getComponentCount();
        taskPanelGBC.fill = GridBagConstraints.HORIZONTAL;
        taskPanelGBC.gridx = 0;
        taskPanelGBC.gridwidth = 3;
        taskPanelGBC.insets = new Insets(0, 5, 7, 5);
        //Set milestone of this task
        task.setMilestone(milestoneName);
        
        this.add(task, taskPanelGBC);
        this.revalidate();
        this.repaint();
    }

    /**
     * A method to create a copy of the task and to attach it to a milestone.
     * This method is used when one or more of the tasks have been attach,
     * because java swing cannot copy the objects, it is done manually
     * @param task          Task to copy and attach
     * @param milestoneName The name of the milestone where to attach
     */
    public void attachAndCopyTask(Task task, String milestoneName) {
        //Create a copy of the task
        Task taskCopy = new Task(task);

        //Add labels if none
        if (flag == 0) {
            flag = 1;
        }
        addLabels();
        
        //Add to list
        listOfTasksPanels.add(taskCopy);
        //Set constraints
        taskPanelGBC.gridy = listOfTasksPanels.size();
        taskPanelGBC.fill = GridBagConstraints.HORIZONTAL;
        taskPanelGBC.gridx = 0;
        taskPanelGBC.gridwidth = 3;
        taskPanelGBC.insets = new Insets(0, 5, 7, 5);
        //Set milestone
        taskCopy.setMilestone(milestoneName);
        
        this.add(taskCopy, taskPanelGBC);
        this.revalidate();
        this.repaint();
    }

}
