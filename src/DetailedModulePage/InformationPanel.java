package DetailedModulePage;

//----------IMPORTS----------
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.MatteBorder;
//---------------------------

public class InformationPanel extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    private String moduleCode;
    private String moduleOrganiser;
    private String moduleCredits;
    //-----------------------------------------------------

    /**
     * A constructor to create a JPanel containing JLabels in one line
     * with the information about the module
     *
     * @param moduleCode Code of this module
     * @param moduleOrganiser Organiser of this module
     * @param moduleCredits Credits for this module
     */
    public InformationPanel(String moduleCode,
                                 String moduleOrganiser, String moduleCredits) {
        this.moduleCode = moduleCode;
        this.moduleOrganiser = moduleOrganiser;
        this.moduleCredits = moduleCredits;
        
        //Set layout of this panel
        this.setBorder(new MatteBorder(1,0,0,0,Color.LIGHT_GRAY));
        this.setLayout(new GridLayout(0,1));
        addInformation();
    }
    
    
    /**
     * A method create a JPanel containing three JLabels in one line
     * with the information about the module: Module code, Module organiser and
     * Module credits.
     */
    private void addInformation(){
        //Create new JPanel to have a different border and to put information
        //labels in this JPanel
        JPanel informationJPanel=new JPanel();
        informationJPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        informationJPanel.setLayout(new GridBagLayout());
        GridBagConstraints infoGBC = new GridBagConstraints();
        infoGBC.weightx = 1;

        //Create JLabels for each Information field
        //Module Code
        JLabel moduleCodeLabel = new JLabel("Module Code: " + moduleCode);
        Font font = moduleCodeLabel.getFont();
        moduleCodeLabel.setFont(font.deriveFont(font.getStyle() ^ Font.BOLD));
        //Module Organiser
        JLabel moduleOrganiserLabel = new JLabel("Module Organiser: " + moduleOrganiser);
        font = moduleOrganiserLabel.getFont();
        moduleOrganiserLabel.setFont(font.deriveFont(font.getStyle() ^ Font.BOLD));
        //Credit Value
        JLabel moduleCreditsLabel = new JLabel("Credit Value: " + moduleCredits);
        font = moduleCreditsLabel.getFont();
        moduleCreditsLabel.setFont(font.deriveFont(font.getStyle() ^ Font.BOLD));

        //Set constraints for JLabels and add them to this informationJPanel
        infoGBC.gridx = 0;
        informationJPanel.add(moduleCodeLabel, infoGBC);
        infoGBC.gridx = 1;
        informationJPanel.add(moduleOrganiserLabel, infoGBC);
        infoGBC.gridx = 2;
        informationJPanel.add(moduleCreditsLabel, infoGBC);
        
        //add information JPanel which already has information to this JPanel
        this.add(informationJPanel);
    }

    /**
     * An accessor method which returns module code of this module
     *
     * @return Module Code
     */
    public String getModuleCode() {
        return moduleCode;
    }

    /**
     * An accessor method which returns module organiser of this module
     *
     * @return Module Organiser
     */
    public String getModuleOrganiser() {
        return moduleOrganiser;
    }

    /**
     * An accessor method which returns module credits of this module
     *
     * @return Module Credits
     */
    public String getModuleCredits() {
        return moduleCredits;
    }

}
