package DetailedModulePage;

//----------IMPORTS----------
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXTaskPane;
import studyplanner.controller.MilestoneController;
import studyplanner.controller.TaskController;
import utilities.exceptions.*;
//---------------------------

public class Milestone extends JXTaskPane {

    //-------------------CLASS VARIABLES-------------------
    private final ArrayList<String> milestoneInformation;
    
    private TaskPanel taskPanel;
    
    private String name;
    private String dueDate;
    private String courseworkName;
    
    private GridBagConstraints milestoneGBC;
    //-----------------------------------------------------

    /**
     * A constructor to create JXTaskPane with milestone information
     *
     * @param milestoneInformation ArrayList containing information about
     *                             milestone
     * @param cwName               The name of assignment this milestone is in
     */
    public Milestone(ArrayList<String> milestoneInformation, String cwName) {
        this.courseworkName = cwName;
        this.milestoneInformation = milestoneInformation;
        //Set this JXTaskPane collapsed and its layout
        this.setCollapsed(true);
        this.setLayout(new GridBagLayout());
        milestoneGBC = new GridBagConstraints();
        
        name = milestoneInformation.get(0);
        dueDate = milestoneInformation.get(1);
        //Add title to this JXTaskPane
        addTitle();
        
        //Add Tasks to this JXTaskPane
        addTasks();
        
        //Add buttons to the bottom of this panel
        addButtons();
    }
    
    
    /**
     * A method to create and add new tasks to this JXTaskPane
     */
    private void addTasks(){
        
        //Create new ArrayList containing information about tasks
        ArrayList<String> taskInformation = new ArrayList<>();
        taskInformation = milestoneInformation;
        //Remove first two elements of the ArrayList because it is information
        //about milesotnes
        taskInformation.remove(0);
        taskInformation.remove(0);
        
        //Create JPanel with tasks
        TaskPanel taskJPanel = 
                new TaskPanel(taskInformation, courseworkName, name);
        taskPanel = taskJPanel;
        
        //Add task JPanel to this JXTaskPane
        milestoneGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        milestoneGBC.fill = GridBagConstraints.HORIZONTAL;
        milestoneGBC.gridwidth = 7;
        milestoneGBC.weightx=7;
        milestoneGBC.weighty=1;
        milestoneGBC.gridx = 0;
        milestoneGBC.gridy = 0;
        
        this.add(taskJPanel, milestoneGBC);
    }
    
    
    
    /**
     * A method to set the title of the milestone JXTaskPane
     */
    private void addTitle(){
        
        //Format string so it is spaced out evenly, because the milestone
        //information is displayed on the title of the JXTaskPane and it can
        //have only String
        //String is formatted in a way so each milestone information is below
        //corresponding label
        Toolkit tk = Toolkit.getDefaultToolkit();
        int xSize = (int) tk.getScreenSize().width;
        
        String titleFormattedString = String.format("%-"+xSize/26+"s%s", 
                                                                name, dueDate);

        //To space out each string evenly, each character needs to have the width
        this.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
        //Set formatted String as a title of this JXTaskPane
        this.setTitle(titleFormattedString);
    }
    

    /**
     * A method to create "Update", "Attach" and "Delete" buttons, 
     * add action listeners to them and add the at the bottom of the main panel
     */
    private void addButtons(){
        //Create a panel for buttons
        JPanel buttonJPanel = new JPanel();
        buttonJPanel.setLayout(new GridBagLayout());

        milestoneGBC.gridy = 1;
        milestoneGBC.insets = new Insets(15, 0, 0, 0);
        this.add(buttonJPanel, milestoneGBC);
        
        GridBagConstraints buttonPanelGBC = new GridBagConstraints();
        buttonPanelGBC.insets = new Insets(0, 5, 0, 5);
        
        //Create buttons and add action listeners
        JButton attachTaskButton = new JButton("Attach a task");
        attachTaskButton.addActionListener(attachAction);
        attachTaskButton.setPreferredSize(new Dimension(140, 25));
        buttonJPanel.add(attachTaskButton, buttonPanelGBC);
        
        JButton updateButton = new JButton("Update Milestone");
        updateButton.addActionListener(updateAction);
        updateButton.setPreferredSize(new Dimension(140, 25));
        buttonJPanel.add(updateButton, buttonPanelGBC);
        
        JButton deleteButton = new JButton("Delete Milestone");
        deleteButton.addActionListener(deleteAction);
        deleteButton.setPreferredSize(new Dimension(140, 25));
        buttonJPanel.add(deleteButton, buttonPanelGBC);
    }
    
    
    
    /**
     * Action listener which will pop a message if there are no tasks to attach,
     * otherwise a pop up will appear to select a task to attach
     */
    private ActionListener attachAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try {
                if (TaskController.getTaskList(courseworkName, name).isEmpty()){
                    JOptionPane.showMessageDialog(null, 
                                                "There are no tasks to attach");
                } else {
                    new AttachTaskForm(courseworkName, name);
                }
            } catch (NullInputException | ItemNotFoundException | 
                    EmptyInputException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(Milestone.class.getName()).
                                                    log(Level.SEVERE, null, ex);
            }
        }
    };

    /**
     * Action listener to open a milestone update form on click
     */
    private ActionListener updateAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent ae) {
            new UpdateMilestoneForm(courseworkName, name);
        }
    };

    
    /**
     * Action listener to delete a milestone on click, it will ask if the
     * user is sure and wants to delete a milestone, if click yes,
     * the milestone will be deleted
     */
    private ActionListener deleteAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent ae) {
            int reply = JOptionPane.showConfirmDialog(null,
                    "Are you sure you want to delete it?", "Delete Milestone",
                    JOptionPane.YES_NO_OPTION);

            if (reply == JOptionPane.YES_OPTION) {
                try {
                    MilestoneController.removeMilestone(courseworkName, name);
                    JOptionPane.showMessageDialog(null, 
                                    "Milestone has been successfully deleted!");
                } catch (ItemNotFoundException | NullInputException |
                        IllegalMilestoneException | IllegalFileException | 
                                                       EmptyInputException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(Milestone.class.getName())
                                                   .log(Level.SEVERE, null, ex);
                }
            }
        }
    };
    

    /**
     * An accessor method to return the name of this milestone
     * @return Name of this milestone
     */
    public String getMilestoneName(){
        return name;
    }
    
    
    
    /**
     * An accessor method to return the due date of this milestone
     * @return Due date of this milestone
     */
    public String getMilestoneDueDate(){
        return dueDate;
    }
    
    
    /**
     * An accessor method to the taskPanel this milestone has
     * @return get the task panel of this milestone
     */
    public TaskPanel getTaskPanel(){
        return taskPanel;
    }
    
    /**
     * A method to update a milestone view, when the milestone is updated, this
     * method is called to update the view of the milestone
     * @param newMilestoneName  The new name of the milestone
     * @param date              The new due date of the milestone
     */
    public void updateMilestone(String newMilestoneName, String date){
        this.name = newMilestoneName;
        this.dueDate = date;     
        addTitle();
        this.revalidate();
        this.repaint();
    }
}
