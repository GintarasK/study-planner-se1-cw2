package DetailedModulePage;

//----------IMPORTS----------
import java.util.ArrayList;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import SemesterProfilePage.MainPageGUI;
//---------------------------

public class MainFrame extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    private JScrollPane mainScrollPane;
    private JPanel mainJPanel;
    private ArrayList<String> moduleInformation;
    private GridBagConstraints mainGBC;
    
    private InformationPanel infoPart;
    
    private MainPageGUI parent;
    
    private AssignmentsPanel assignmentPart;
    //-----------------------------------------------------

    /**
     * A constructor to create a JFrame for the Detailed Module Page.
     *
     * @param moduleInformation an ArrayList containing all the information
     * about the selected module and its assignments
     */
    public MainFrame(ArrayList<String> moduleInformation, MainPageGUI parent) {
        
        //MainPage for back button
        this.parent = parent;
        
        this.moduleInformation = moduleInformation;

        this.setLayout(new GridLayout(0, 1));

        //Main Scroll Pane
        mainScrollPane = new JScrollPane();
        mainScrollPane.setHorizontalScrollBar(null);
        this.add(mainScrollPane);

        //Main JPanel
        mainJPanel = new JPanel();
        mainJPanel.setLayout(new GridBagLayout());
        mainJPanel.setBorder(BorderFactory.createEmptyBorder(5, 20, 20, 20));
        mainScrollPane.getViewport().add(mainJPanel);
        

        mainJPanel.setPreferredSize(null);
        //this.setPreferredSize(test.getPreferredSize());

        //Add all JPanels to main JPanel
        addComponents();
    }

    /**
     * A Method to add all JPanels together to the main JPanel
     *
     */
    private void addComponents() {

        //Create constraints for all JPanels
        mainGBC = new GridBagConstraints();
        mainGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        mainGBC.fill = GridBagConstraints.HORIZONTAL;
        mainGBC.weightx = 1;
        mainGBC.weighty = 0;
        mainGBC.gridx = 0;

        //Add all JPanels to main JPanel
        addName();
        addInformation();
        addAssignments();
        addButtons();

    }

    /**
     * A Method to add name JPanel to the main JPanel
     */
    private void addName() {

        //Create JPanel for name
        JPanel namePart = (JPanel) new NamePanel(moduleInformation.get(0));
        moduleInformation.remove(0);

        //Adjust constraints for name JPanel
        mainGBC.gridy = 0;

        //Add name JPanel
        mainJPanel.add(namePart, mainGBC);
    }

    /**
     * A Method to add description and info JPanels together and to add both to
     * the main JPanel
     */
    private void addInformation() {

        //Create JPanel for information
        JPanel descriptionAndNameParts = new JPanel();
        descriptionAndNameParts.setBorder(BorderFactory.createTitledBorder(
            BorderFactory.createLineBorder(new Color(0, 0, 0)), "Information"));
        descriptionAndNameParts.setLayout(new GridBagLayout());

        //--------------------------DESCRIPTION--------------------------
        JPanel descriptionPart = (JPanel) new DescriptionPanel(
                                                    moduleInformation.get(0));
        moduleInformation.remove(0);

        //--------------------------INFORMATION--------------------------
        infoPart = new InformationPanel(
                        moduleInformation.get(0), moduleInformation.get(1),
                                                    moduleInformation.get(2));
        moduleInformation.remove(0);
        moduleInformation.remove(0);
        moduleInformation.remove(0);

        //Add Description and Information JPanels to one panel
        //so they can have one border
        mainGBC.gridy = 1;
        descriptionAndNameParts.add(descriptionPart, mainGBC);
        mainGBC.gridy = 2;
        descriptionAndNameParts.add(infoPart, mainGBC);

        //Add information JPanel to main JPanel
        mainJPanel.add(descriptionAndNameParts, mainGBC);

    }

    /**
     * A Method to add assignments JPanel to the main JPanel
     */
    private void addAssignments() {

        //Create JPanel for assignments
        assignmentPart =  new AssignmentsPanel(moduleInformation);
        assignmentPart.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)),
                                                                "Assignments"));

        //Adjust constraints for assignments JPanel
        mainGBC.weighty = 1;
        mainGBC.gridy = 3;
        mainGBC.insets = new Insets(15, 0, 0, 0);

        //Add assignments JPanel to main JPanel
        mainJPanel.add(assignmentPart, mainGBC);
    }
    
    /**
     * A method to add buttons to the bottom of main JPanel
     */
    private void addButtons(){
        
        ButtonsPart buttonPart = new ButtonsPart(parent);
        mainGBC.weighty=0;
        mainGBC.gridy=4;
        mainGBC.insets = new Insets(15, 0, 15, 0);
        
        mainJPanel.add(buttonPart, mainGBC);
    }

    
    
    
    
    /**
     * Method to update the view when attaching a task to a milestone
     * @param assignmentName    The assignment name
     * @param taskName          The task name to attach
     * @param milestoneName     The milestone name to attach to
     */
    public void attachTask(String assignmentName, String taskName, String milestoneName) {
        Task taskToAttach = null;
        
        if (assignmentName.equalsIgnoreCase("exam") || assignmentName.equalsIgnoreCase("course test")) {
            this.attachTaskExam(assignmentName, taskName, milestoneName);
        } else {
            this.attachTaskCoursework(assignmentName, taskName, milestoneName);
        }
    }

    /**
     * Method to update the view when attaching a task to a milestone
     * @param assignmentName    The assignment name
     * @param taskName          The task name to attach
     * @param milestoneName     The milestone name to attach to
     */
    private void attachTaskExam(String assignmentName, String taskName, String milestoneName) {
        Task taskToAttach = null;

        for (Exam exam : assignmentPart.getExamPanel().getJXTaskPanes()) {
            if (exam.getExamName().equalsIgnoreCase(assignmentName)) {
                for (Task task : exam.getTaskPanel().getListOfTasksPanels()) {
                    if (task.getTaskName().equalsIgnoreCase(taskName)) {
                        taskToAttach = task;

                        for (Milestone milestone : exam.getMilestonePanel().getListOfMilestoneJXTaskPane()) {
                            if (milestone.getMilestoneName().equalsIgnoreCase(milestoneName)) {
                                milestone.getTaskPanel().attachTask(taskToAttach, milestoneName);
                            }
                        }
                    }
                }
                if (taskToAttach != null) {
                    exam.getTaskPanel().deleteTask(taskToAttach);
                } else if (taskToAttach == null) {
                    for (Milestone milestone : exam.getMilestonePanel().getListOfMilestoneJXTaskPane()) {
                        for (Task task : milestone.getTaskPanel().getListOfTasksPanels()) {
                            if (task.getTaskName().equalsIgnoreCase(taskName)) {
                                taskToAttach = task;
                                break;
                            }
                        }
                    }
                    if (taskToAttach != null) {
                        for (Milestone milestone : exam.getMilestonePanel().getListOfMilestoneJXTaskPane()) {
                            if (milestone.getMilestoneName().equalsIgnoreCase(milestoneName)) {
                                milestone.getTaskPanel().attachAndCopyTask(taskToAttach, milestoneName);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    /**
     * Method to update the view when attaching a task to a milestone
     * @param assignmentName    The assignment name
     * @param taskName          The task name to attach
     * @param milestoneName     The milestone name to attach to
     */
    private void attachTaskCoursework(String assignmentName, String taskName, String milestoneName) {
        Task taskToAttach = null;
        for (Coursework n : assignmentPart.getCourseworkPanel().getJXTaskPanes()) {
            if (n.getCourseworkName().equals(assignmentName)) {
                for (Task j : n.getTaskPanel().getListOfTasksPanels()) {
                    if (j.getTaskName().equals(taskName)) {
                        taskToAttach = j;
                        MilestonePanel milestonePanel = n.getMilestonePanel();
                        ArrayList<Milestone> milestoneJXTaskPanes = milestonePanel.getListOfMilestoneJXTaskPane();
                        for (Milestone k : milestoneJXTaskPanes) {
                            if (k.getMilestoneName().equals(milestoneName)) {
                                TaskPanel taskP = k.getTaskPanel();
                                taskP.attachTask(taskToAttach, milestoneName);
                            }
                        }
                    }
                }
                if (taskToAttach != null) {
                    n.getTaskPanel().deleteTask(taskToAttach);
                } else if (taskToAttach == null) {
                    MilestonePanel milestonePanel = n.getMilestonePanel();
                    ArrayList<Milestone> milestoneJXTaskPanes = milestonePanel.getListOfMilestoneJXTaskPane();
                    for (Milestone milestone : milestoneJXTaskPanes) {
                        TaskPanel taskPanelInsideMilestone = milestone.getTaskPanel();
                        ArrayList<Task> taskJPanelsInsideMilestone = taskPanelInsideMilestone.getListOfTasksPanels();
                        for (Task task : taskJPanelsInsideMilestone) {
                            if (task.getTaskName().equals(taskName)) {
                                taskToAttach = task;
                                break;
                            }
                        }
                    }
                    if (taskToAttach != null) {
                        for (Milestone milestone : milestoneJXTaskPanes) {
                            if (milestone.getMilestoneName().equals(milestoneName)) {
                                TaskPanel taskPanelToAttach = milestone.getTaskPanel();
                                taskPanelToAttach.attachAndCopyTask(taskToAttach, milestoneName);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    
    /**
     * Method to update the view when new milestone is created
     * @param assignmentName    The assignment name
     * @param milestoneName     The new milestone name 
     * @param date              The due date of the milestone
     */
    public void newMilestone(String assignmentName, String milestoneName, String date) {
        if (assignmentName.equalsIgnoreCase("exam") || assignmentName.equalsIgnoreCase("course test")) {
            ArrayList<Exam> examPanes = assignmentPart.getExamPanel().getJXTaskPanes();

            for (Exam exam : examPanes) {
                if (exam.getExamName().equalsIgnoreCase(assignmentName)) {
                    exam.getMilestonePanel().createMilestone(milestoneName, date);
                }
            }
        } else {
            ArrayList<Coursework> courseworkPanes
                    = assignmentPart.getCourseworkPanel().getJXTaskPanes();

            for (Coursework coursework : courseworkPanes) {
                if (coursework.getCourseworkName().equalsIgnoreCase(assignmentName)) {
                    coursework.getMilestonePanel().createMilestone(milestoneName, date);
                }
            }
        }
    }

    /**
     *  Method to update the view when new task is created
     * @param assignmentName    The assignment name
     * @param taskName          The new task name 
     * @param deadline          The due date of the task
     */
    public void newTask(String assignmentName, String taskName, String deadline) {
        if (assignmentName.equalsIgnoreCase("exam") || assignmentName.equalsIgnoreCase("course test")) {
            ArrayList<Exam> examPanes = assignmentPart.getExamPanel().getJXTaskPanes();

            for (Exam exam : examPanes) {
                if (exam.getExamName().equalsIgnoreCase(assignmentName)) {
                    exam.getTaskPanel().createTask(taskName, deadline);
                }
            }
        } else {
            ArrayList<Coursework> courseworkPanes
                    = assignmentPart.getCourseworkPanel().getJXTaskPanes();

            for (Coursework coursework : courseworkPanes) {
                if (coursework.getCourseworkName().equalsIgnoreCase(assignmentName)) {
                    coursework.getTaskPanel().createTask(taskName, deadline);
                }
            }
        }

    }

    
    
    /**
     * Method to update the view when removing the milestone
     * @param assignmentName    The assignment name
     * @param milestoneName     milestone name to remove
     */
    public void removeMilestone(String assignmentName, String milestoneName) {
        if (assignmentName.equalsIgnoreCase("exam") || assignmentName.equalsIgnoreCase("course test")) {
            ArrayList<Exam> examPanes = assignmentPart.getExamPanel().getJXTaskPanes();

            for (Exam exam : examPanes) {
                if (exam.getExamName().equalsIgnoreCase(assignmentName)) {
                    exam.getMilestonePanel().removeMilestone(milestoneName);
                }
            }
        } else {
            ArrayList<Coursework> courseworkPanes
                    = assignmentPart.getCourseworkPanel().getJXTaskPanes();

            for (Coursework coursework : courseworkPanes) {
                if (coursework.getCourseworkName().equalsIgnoreCase(assignmentName)) {
                    coursework.getMilestonePanel().removeMilestone(milestoneName);
                }
            }
        }
    }

    
    /**
     * Method to update the view when the milestone has been updated
     * @param assignmentName         The assignment name
     * @param oldMilestoneName       Old milestone name, to find it
     * @param newMilestoneName       New milestone name to change it to
     * @param dueDate               new Milestone due date
     */
    public void updateMilestone(String assignmentName, String oldMilestoneName,
            String newMilestoneName, String dueDate) {

        if (assignmentName.equalsIgnoreCase("exam") || assignmentName.equalsIgnoreCase("course test")) {
            ArrayList<Exam> examPanes = assignmentPart.getExamPanel().getJXTaskPanes();

            for (Exam exam : examPanes) {
                if (exam.getExamName().equalsIgnoreCase(assignmentName)) {
                    for (Milestone milestone : exam.getMilestonePanel().getListOfMilestoneJXTaskPane()) {
                        if (milestone.getMilestoneName().equalsIgnoreCase(oldMilestoneName)) {
                            milestone.updateMilestone(newMilestoneName, dueDate);
                        }
                    }
                }
            }
        } else {

            ArrayList<Coursework> courseworkPanes
                    = assignmentPart.getCourseworkPanel().getJXTaskPanes();

            for (Coursework coursework : courseworkPanes) {
                if (coursework.getCourseworkName().equalsIgnoreCase(assignmentName)) {

                    MilestonePanel milestonePanel
                            = coursework.getMilestonePanel();
                    ArrayList<Milestone> milestoneJXTaskPanes
                            = milestonePanel.getListOfMilestoneJXTaskPane();
                    for (Milestone milestone : milestoneJXTaskPanes) {
                        if (milestone.getMilestoneName().equalsIgnoreCase(oldMilestoneName)) {
                            milestone.updateMilestone(newMilestoneName, dueDate);
                            //break;
                        }
                    }

                }
            }
        }
    }

    
    
    
    /**
     * Method to update the view when the task has been updated
     * @param assignmentName    The assignment name
     * @param oldTaskName       Old task name, to find it
     * @param newTaskName       New task name to change it to
     * @param dueDate           new task due date
     */
    public void updateTask(String assignmentName, String oldTaskName,
            String newTaskName, String dueDate) {
        if (assignmentName.equalsIgnoreCase("exam") || assignmentName.equalsIgnoreCase("course test")) {
            ArrayList<Exam> examPanes = assignmentPart.getExamPanel().getJXTaskPanes();

            for (Exam exam : examPanes) {
                if (exam.getExamName().equalsIgnoreCase(assignmentName)) {
                    for (Milestone milestone : exam.getMilestonePanel().getListOfMilestoneJXTaskPane()) {
                        for (Task task : milestone.getTaskPanel().getListOfTasksPanels()) {
                            if (task.getTaskName().equalsIgnoreCase(oldTaskName)) {
                                task.updateTask(newTaskName, dueDate);
                            }
                        }
                    }
                    for (Task task : exam.getTaskPanel().getListOfTasksPanels()) {
                        if (task.getTaskName().equalsIgnoreCase(oldTaskName)) {
                            task.updateTask(newTaskName, dueDate);
                        }
                    }
                }
            }
        } else {
            ArrayList<Coursework> courseworkPanes
                    = assignmentPart.getCourseworkPanel().getJXTaskPanes();

            for (Coursework coursework : courseworkPanes) {
                if (coursework.getCourseworkName().equalsIgnoreCase(assignmentName)) {
                    MilestonePanel milestonePanel
                            = coursework.getMilestonePanel();
                    ArrayList<Milestone> milestoneJXTaskPanes
                            = milestonePanel.getListOfMilestoneJXTaskPane();
                    for (Milestone milestone : milestoneJXTaskPanes) {
                        TaskPanel taskPanel
                                = milestone.getTaskPanel();

                        ArrayList<Task> taskPanels
                                = taskPanel.getListOfTasksPanels();

                        for (Task task : taskPanels) {
                            if (task.getTaskName().equalsIgnoreCase(oldTaskName)) {
                                task.updateTask(newTaskName, dueDate);
                            }
                        }
                    }

                    TaskPanel taskPanel
                            = coursework.getTaskPanel();
                    ArrayList<Task> taskPanels
                            = taskPanel.getListOfTasksPanels();
                    for (Task task : taskPanels) {
                        if (task.getTaskName().equalsIgnoreCase(oldTaskName)) {
                            task.updateTask(newTaskName, dueDate);
                        }
                    }
                }
            }
        }
    }

    
    
    
    
    
    
    /**
     * A method to update the view when the task has been removed
     * @param assignmentName    The assignment name
     * @param milestoneName     milestone name from where it was removed
     * @param taskName          task name that was removed
     */
    public void removeTask(String assignmentName, String milestoneName, String taskName) {
        Task taskToRemove = null;
        if (milestoneName == null) {
            
            if (assignmentName.equalsIgnoreCase("exam") || assignmentName.equalsIgnoreCase("course test")) {
                this.removeTaskExamNoMilestone(assignmentName, milestoneName, taskName);
            } else {
                
                this.removeTaskCourseworkNoMilestone(assignmentName, milestoneName, taskName);
            }
        } else {
            int numberOfTasks = 0;
            if (assignmentName.equalsIgnoreCase("exam") || assignmentName.equalsIgnoreCase("course test")) {
                this.removeTaskExam(assignmentName, milestoneName, taskName);
            } else {
                this.removeTaskCoursework(assignmentName, milestoneName, taskName);
            }
        }
    }

    
    /**
     * A method to update the view when the task has been removed for exam
     * @param assignmentName    The assignment name
     * @param milestoneName     milestone name from where it was removed
     * @param taskName          task name that was removed
     */
    private void removeTaskExamNoMilestone(String assignmentName,
            String milestoneName, String taskName) {
        Task taskToRemove = null;

        for (Exam exam : assignmentPart.getExamPanel().getJXTaskPanes()) {
            if (exam.getExamName().equalsIgnoreCase(assignmentName)) {
                for (Task task : exam.getTaskPanel().getListOfTasksPanels()) {
                    if (task.getTaskName().equalsIgnoreCase(taskName)) {
                        taskToRemove = task;
                    }
                }
                if (taskToRemove != null) {
                    exam.getTaskPanel().deleteTask(taskToRemove);
                }
            }
        }
    }

    
    /**
     * A method to update the view when the task has been removed for cw
     * @param assignmentName    The assignment name
     * @param milestoneName     milestone name from where it was removed
     * @param taskName          task name that was removed
     */
    private void removeTaskCourseworkNoMilestone(String assignmentName,
            String milestoneName, String taskName) {
        Task taskToRemove = null;

        for (Coursework coursework : assignmentPart.getCourseworkPanel().getJXTaskPanes()) {
            if (coursework.getCourseworkName().equalsIgnoreCase(assignmentName)) {
                TaskPanel taskPanel
                        = coursework.getTaskPanel();
                ArrayList<Task> taskPanels
                        = taskPanel.getListOfTasksPanels();

                for (Task task : taskPanels) {
                    if (task.getTaskName().equalsIgnoreCase(taskName)) {
                        taskToRemove = task;

                    }
                }
                if (taskToRemove != null) {
                    taskPanel.deleteTask(taskToRemove);
                }
            }
        }
    }

    
    /**
     * A method to update the view when the task has been removed for exam
     * @param assignmentName    The assignment name
     * @param milestoneName     milestone name from where it was removed
     * @param taskName          task name that was removed
     */
    private void removeTaskExam(String assignmentName, String milestoneName,
            String taskName) {
        Task taskToRemove = null;
        int numberOfTasks = 0;

        for (Exam exam : assignmentPart.getExamPanel().getJXTaskPanes()) {
            if (exam.getExamName().equalsIgnoreCase(assignmentName)) {
                for (Milestone milestone : exam.getMilestonePanel().getListOfMilestoneJXTaskPane()) {
                    for (Task task : milestone.getTaskPanel().getListOfTasksPanels()) {
                        if (task.getTaskName().equalsIgnoreCase(taskName)) {
                            numberOfTasks++;
                        }
                    }
                }
                for (Milestone milestone : exam.getMilestonePanel().getListOfMilestoneJXTaskPane()) {
                    if (milestone.getMilestoneName().equalsIgnoreCase(milestoneName)) {
                        for (Task task : milestone.getTaskPanel().getListOfTasksPanels()) {
                            if (task.getTaskName().equalsIgnoreCase(taskName)) {
                                taskToRemove = task;
                                break;
                            }
                        }
                        if (taskToRemove != null) {
                            milestone.getTaskPanel().deleteTask(taskToRemove);
                        }
                        break;
                    }
                }
            }
            if (taskToRemove != null && numberOfTasks < 2) {
                exam.getTaskPanel().addTaskToMainPanel(taskToRemove);
                exam.getTaskPanel().revalidate();
                exam.getTaskPanel().repaint();
                break;
            }
        }
    }

    
    /**
     * A method to update the view when the task has been removed for cw
     * @param assignmentName    The assignment name
     * @param milestoneName     milestone name from where it was removed
     * @param taskName          task name that was removed
     */
    private void removeTaskCoursework(String assignmentName, String milestoneName,
            String taskName) {
        Task taskToRemove = null;
        int numberOfTasks = 0;

        for (Coursework coursework : assignmentPart.getCourseworkPanel().getJXTaskPanes()) {
            if (coursework.getCourseworkName().equalsIgnoreCase(assignmentName)) {
                MilestonePanel milestonePanel
                        = coursework.getMilestonePanel();
                ArrayList<Milestone> milestoneJXTaskPanes
                        = milestonePanel.getListOfMilestoneJXTaskPane();
                for (Milestone milestone : milestoneJXTaskPanes) {
                    for (Task task : milestone.getTaskPanel().getListOfTasksPanels()) {
                        if (task.getTaskName().equalsIgnoreCase(taskName)) {
                            numberOfTasks++;
                        }
                    }
                }
                for (Milestone milestone : milestoneJXTaskPanes) {
                    if (milestone.getMilestoneName().equalsIgnoreCase(milestoneName)) {
                        TaskPanel taskPanel
                                = milestone.getTaskPanel();

                        ArrayList<Task> taskPanels
                                = taskPanel.getListOfTasksPanels();
                        for (Task task : taskPanels) {
                            if (task.getTaskName().equalsIgnoreCase(taskName)) {
                                taskToRemove = task;
                                break;
                            }
                        }
                        if (taskToRemove != null) {
                            taskPanel.deleteTask(taskToRemove);
                        }
                        break;
                    }
                }

            }
            if (taskToRemove != null && numberOfTasks < 2) {
                coursework.getTaskPanel().addTaskToMainPanel(taskToRemove);
                coursework.getTaskPanel().revalidate();
                coursework.getTaskPanel().repaint();
                break;
            }
        }
    }


    
    /**
     * A method to update task bar when the activity has been done and the
     * progress has been increased
     * @param assignmentName    The assignment name
     * @param taskName          Task name that was updated
     * @param progressValue     The progress of the task now
     */
    public void updateTaskBar(String assignmentName, String taskName, 
                                                            int progressValue) {
        
        if (assignmentName.equalsIgnoreCase("exam") || 
                               assignmentName.equalsIgnoreCase("course test")) {
            
            this.updateExamTaskBar(assignmentName, taskName, progressValue);
            
        } else {
            
            this.updateCoursworkTaskBar(assignmentName, taskName, progressValue);
            
        }
        
    }
    
    /**
     * A method to update task bar when the activity has been done and the
     * progress has been increased for exam
     * @param assignmentName    The assignment name
     * @param taskName          Task name that was updated
     * @param progressValue     The progress of the task now
     */
    private void updateExamTaskBar(String assignmentName, String taskName,
                                                            int progressValue) {
        for (Exam exam : assignmentPart.getExamPanel().getJXTaskPanes()) {
            if (exam.getExamName().equalsIgnoreCase(assignmentName)) {
                for (Task task : exam.getTaskPanel().getListOfTasksPanels()) {
                    if (task.getTaskName().equalsIgnoreCase(taskName)) {
                        task.updateTaskBar(progressValue);
                    }
                }
                for (Milestone milestone : exam.getMilestonePanel().
                                               getListOfMilestoneJXTaskPane()) {
                    for (Task task : milestone.getTaskPanel().
                                                       getListOfTasksPanels()) {
                        if (task.getTaskName().equalsIgnoreCase(taskName)) {
                            task.updateTaskBar(progressValue);
                        }
                    }
                }
            }
        }
    }

    /**
     * A method to update task bar when the activity has been done and the
     * progress has been increased for cw
     * @param assignmentName    The assignment name
     * @param taskName          Task name that was updated
     * @param progressValue     The progress of the task now
     */
    private void updateCoursworkTaskBar(String assignmentName, String taskName,
                                                            int progressValue) {
        for (Coursework pane : assignmentPart.getCourseworkPanel().getJXTaskPanes()) {
            if (pane.getCourseworkName().equalsIgnoreCase(assignmentName)) {
                for (Task task : pane.getTaskPanel().getListOfTasksPanels()) {
                    if (task.getTaskName().equalsIgnoreCase(taskName)) {
                        task.updateTaskBar(progressValue);
                    }
                }
                for (Milestone milestone : pane.getMilestonePanel().
                                               getListOfMilestoneJXTaskPane()) {
                    for (Task task : milestone.getTaskPanel().
                                                       getListOfTasksPanels()) {
                        if (task.getTaskName().equalsIgnoreCase(taskName)) {
                            task.updateTaskBar(progressValue);
                        }
                    }
                }
            }
        }
    }


    /**
     * An accessor method to return the information part of this module
     * @return Information panel
     */
    public InformationPanel getInformationPart() {
        return infoPart;
    }

}
