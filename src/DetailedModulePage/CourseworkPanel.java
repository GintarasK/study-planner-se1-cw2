package DetailedModulePage;

//----------IMPORTS----------
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
//---------------------------

public class CourseworkPanel extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    private ArrayList<String> listOfCourseworks;
    private ArrayList<Coursework> listOfCwJXTaskPane;
    private GridBagConstraints cwPanelGBC;
    //-----------------------------------------------------

    /**
     * A constructor to create a JPanel containing courseworks for this module
     *
     * @param listOfCourseworks         An ArrayList containing information 
     *                                  about the courseworks
     */
    public CourseworkPanel(ArrayList<String> listOfCourseworks) {
        this.listOfCourseworks = listOfCourseworks;

        //Set Layout for this JPanel
        this.setLayout(new GridBagLayout());

        //Create ArrayList and populate it with courseworks by extracting
        //each coursework information from the ArrayList of courseworks
        listOfCwJXTaskPane = new ArrayList<>();
        createCourseworks();

        //Add the information labels to this JPanel
        addLabels();
        //Add created courseworks to this JPanel
        addCourseworks();

    }

    /**
     * A method to add information labels to this JPanel
     */
    private void addLabels() {

        //Create constraints for information Labels for this JPanel
        cwPanelGBC = new GridBagConstraints();
        cwPanelGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        cwPanelGBC.weighty = 0;
        cwPanelGBC.weightx = 1;
        cwPanelGBC.gridy = 0;
        cwPanelGBC.gridwidth = 1;
        cwPanelGBC.insets = new Insets(0, 0, 10, 0);

        //Add information Labels to this JPanel
        cwPanelGBC.gridx = 0;
        this.add(new JLabel("Courseworks"), cwPanelGBC);
        cwPanelGBC.gridx = 1;
        this.add(new JLabel("Weighting"), cwPanelGBC);
        cwPanelGBC.gridx = 2;
        this.add(new JLabel("Set Date"), cwPanelGBC);
        cwPanelGBC.gridx = 3;
        this.add(new JLabel("Due Date"), cwPanelGBC);

    }

    /**
     * A method to add already created courseworks to this panel
     */
    private void addCourseworks() {

        //Create constraints to add courseworks to this JPanel
        cwPanelGBC.fill = GridBagConstraints.HORIZONTAL;
        cwPanelGBC.gridx = 0;
        cwPanelGBC.gridwidth = 4;
        cwPanelGBC.insets = new Insets(0, 0, 7, 0);

        //Add courseworks to this JPanel
        for (int i = 0; i < listOfCwJXTaskPane.size(); i++) {
            cwPanelGBC.gridy = i + 1;
            this.add(listOfCwJXTaskPane.get(i), cwPanelGBC);
        }

    }

    /**
     * A method to extract coursework information and to create them
     */
    private void createCourseworks() {
        //Create courseworks and add them to ArrayList
        int countList = 0;
        while (countList < listOfCourseworks.size()) {
            ArrayList<String> courseworkInformation = new ArrayList<>();
            //Get name, set date, due date and weighting
            for (int i = 0; i < 4; i++) {
                courseworkInformation.add(listOfCourseworks.get(countList));
                countList++;
            }
            //Get how many tasks without milestones
            String numberOfTasksWithoutMilestones
                    = listOfCourseworks.get(countList);
            courseworkInformation.add(numberOfTasksWithoutMilestones);
            countList++;

            //Get tasks without milestones
            for (int i = 0;
                    i < Integer.parseInt(numberOfTasksWithoutMilestones); i++) {
                //Get tasks name, due date, type and goal
                for (int j = 0; j < 4; j++) {
                    courseworkInformation.add(listOfCourseworks.get(countList));
                    countList++;
                }
            }
            //Get how many milestones
            String numberOfMilestones = listOfCourseworks.get(countList);
            courseworkInformation.add(numberOfMilestones);
            countList++;

            //Get milestones
            for (int i = 0; i < Integer.parseInt(numberOfMilestones); i++) {
                //Get milestones name and due date
                for (int j = 0; j < 2; j++) {
                    courseworkInformation.add(listOfCourseworks.get(countList));
                    countList++;
                }

                //Get how many tasks with this milestone
                String numberOfTasksForMilestone = listOfCourseworks.get(countList);
                courseworkInformation.add(numberOfTasksForMilestone);
                countList++;
                
                //Get tasks
                for (int j = 0;
                        j < Integer.parseInt(numberOfTasksForMilestone); j++) {
                    //Get tasks name, due date, type and goal
                    for (int n = 0; n < 4; n++) {
                        courseworkInformation.add(listOfCourseworks.get(countList));
                        countList++;
                    }
                }
            }
            //Create a coursework and add it to the list
            listOfCwJXTaskPane.add(new Coursework(courseworkInformation));
        }
    }
    
    
    
    /**
     * An accessor method to get the JXTaskPanes which are milestones
     * @return JXTaskPanes this coursework has
     */
    public ArrayList<Coursework> getJXTaskPanes(){
        return listOfCwJXTaskPane;
    }
    
    

}
