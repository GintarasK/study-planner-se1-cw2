package DetailedModulePage;

//----------IMPORTS----------
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import javax.swing.JPanel;
//---------------------------

public class AssignmentsPanel extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    //Lists containing info for courseworks and exams
    private ArrayList<String> listOfAssignments;
    private ArrayList<String> listOfCourseworks;
    private ArrayList<String> listOfExams;
    
    //Panels containing all the created courseworks or exams
    private CourseworkPanel courseworkPanel;
    private ExamPanel examPanel;
    
    //Number to keep track of the listOfAssignments
    private int countList;
    //-----------------------------------------------------

    /**
     * A constructor to create a JPanel containing two JPanels, one for
     * courseworks, another one for exams
     *
     * @param listOfAssignments ArrayList containing all the information about
     *                          assignments
     */
    public AssignmentsPanel(ArrayList<String> listOfAssignments) {
        
        //Get the list with all the information about assignments
        this.listOfAssignments = listOfAssignments;

        //Set Layout for this JPanel
        this.setLayout(new GridBagLayout());
        
        //Create coursework panel
        createCourseworks();
        
        //Create exam panel
        createExams();
        
        //Add created courseworks and exams to this panel
        addAssignments();
    }
    
    
    /**
     * A method to create a coursework panel containing all the courseworks
     * provided in the list of assignments
     */
    private void createCourseworks() {
        //Create ArrayList just for courseworks and populate it with
        //coursework information
        listOfCourseworks = new ArrayList<>();
        
        //variable to keep track of ArrayList
        countList = 0;
        
        //Loop through while the String is not "exam" or "course test"
        //and is less than the number of courseworks
        while (countList < listOfAssignments.size()
                && !listOfAssignments.get(countList).equalsIgnoreCase("exam")
                && !listOfAssignments.get(countList).
                                              equalsIgnoreCase("course test")) {

            listOfCourseworks.add(listOfAssignments.get(countList));
            countList++;
        }

        //Create coursework panel with the information received
        courseworkPanel
                = new CourseworkPanel(listOfCourseworks);
    }
    
    
    
    /**
     * A method to create an exam panel containing all the exams
     * provided in the list of assignments
     */
    private void createExams() {

        //Create ArrayList just for exams and populate it with exams information
        listOfExams = new ArrayList<>();
        
        //Loop through until the end
        for (int i = countList; i < listOfAssignments.size(); i++) {
            listOfExams.add(listOfAssignments.get(i));
        }

        //Create new ExamPanel with the information
        examPanel = new ExamPanel(listOfExams);
    }
    
    
    
    /**
     * A method to assign GridBagConstraints values and to add created exam
     * and coursework panels to this assignments panel
     */
    private void addAssignments() {

        //Create constraints how coursework and exam panels should be added
        //to this assignments panel
        GridBagConstraints assignmentGBC = new GridBagConstraints();
        assignmentGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        assignmentGBC.fill = GridBagConstraints.HORIZONTAL;
        assignmentGBC.weightx = 1;
        assignmentGBC.weighty = 0;
        assignmentGBC.gridx = 0;
        assignmentGBC.gridy = 0;
        assignmentGBC.insets = new Insets(9, 10, 4, 10);

        //Add coursework JPanel to this assignments JPanel
        this.add(courseworkPanel, assignmentGBC);

        //Adjust constraints for exam JPanel and add to this assignemnts JPanel
        assignmentGBC.gridy = 1;
        this.add(examPanel, assignmentGBC);
    }


    
    /**
     *  Accessor method to get the list of courseworks information for 
     *  this module
     * 
     * @return  ArrayList with all the courseworks information
     */
    public ArrayList<String> getListOfCourseworks(){
        return listOfCourseworks;
    }
    
    /**
     *  Accessor method to get the list of exams information for this module
     * 
     * @return  ArrayList with all the exams information
     */
    public ArrayList<String> getListOfExams(){
        return listOfExams;
    }
    
    /**
     * Accessor method to get the coursework panel, containing 
     * all the courseworks
     * 
     * @return Coursework panel, with all the courseworks
     */
    public CourseworkPanel getCourseworkPanel(){
        return courseworkPanel;
    }
    
    /**
     * Accessor method to get the exam panel, containing all the exams and
     * course tests
     * 
     * @return Exam panel, with all the exams
     */
    public ExamPanel getExamPanel(){
        return examPanel;
    }

}
