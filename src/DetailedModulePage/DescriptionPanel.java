package DetailedModulePage;

//----------IMPORTS----------
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
//---------------------------

public class DescriptionPanel extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    private String descriptionOfModule;
    //-----------------------------------------------------

    /**
     * A constructor to create a JPanel containing a JLabel("Overview") and
     * JTextArea in the middle of the panel with the description of the module.
     *
     * @param descriptionOfModule Description of the module to display
     */
    public DescriptionPanel(String descriptionOfModule) {

        this.descriptionOfModule = descriptionOfModule;

        //Set Layout for this JPanel
        this.setLayout(new BorderLayout());
        
        //Create a label to display word "Overview"
        JLabel overviewLabel = new JLabel("Overview");
        overviewLabel.setHorizontalAlignment(JLabel.CENTER);
        overviewLabel.setFont(new Font("Times New Roman", Font.BOLD, 15));

        //Create a JTextArea to display a description about the module
        JTextArea descriptionTextArea = new JTextArea(descriptionOfModule);
        descriptionTextArea.setLineWrap(true);
        descriptionTextArea.setWrapStyleWord(true);
        descriptionTextArea.setOpaque(false);
        descriptionTextArea.setEditable(false);
        descriptionTextArea.setBorder(new EmptyBorder(10, 34, 10, 34));
        descriptionTextArea.setBackground(new Color(214, 217, 223));
        descriptionTextArea.setHighlighter(null);
        
        //Add JLabel and JTextArea to this JPanel
        this.add(overviewLabel, BorderLayout.NORTH);
        this.add(descriptionTextArea, BorderLayout.CENTER);
    }

    
    /**
     * An accessor method which returns the description of the module
     *
     * @return this modules description
     */
    public String getDescrptionOfModule() {
        return descriptionOfModule;
    }

}
