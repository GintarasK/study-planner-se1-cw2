package DetailedModulePage;

//----------IMPORTS----------
import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
//---------------------------

public class ActivityPanel extends JPanel{
    
    //-------------------CLASS VARIABLES-------------------
    //Fields for activity
    private int number;
    private String name;
    private String dueDate;
    private String contribution;
    
    //Values that are passed to this object
    private String milestoneName;
    private String taskName;
    private String assignmentName;
    //-----------------------------------------------------
    
    
    
    /**
     *  A constructor which assigns all the provided values with class variables
     * and calls the method to populate this JPanel with the gives values
     * 
     * @param number            The number which represents which activity it is
     * @param name              The name of the activity
     * @param dueDate           The date this activity has been finished
     * @param contribution      How much it contributes towards task goal
     * @param assignmentName    The assignment name this activity is in
     * @param taskName          The task name this activity is in
     * @param milestoneName     The milestone name this activity is in
     *                          ("null" if none)
     */
    public ActivityPanel(int number, String name, String dueDate, 
                String contribution, String assignmentName, String taskName, 
                                                         String milestoneName) {
        //Assign all the fields
        this.number = number;
        this.name = name;
        this.dueDate = dueDate;
        this.contribution = contribution;

        this.milestoneName = milestoneName;
        this.assignmentName = assignmentName;
        this.taskName = taskName;

        //add components to this JPanel
        addComponents();

    }
    
    
    /**
     * Method set the parameters of this JPanel and to add JLabels required
     * to show activity information
     */
    private void addComponents() {

        //Set the parameters of this JPanel
        this.setLayout(new GridLayout(0, 4));
        this.setBorder(new EmptyBorder(2, 2, 2, 2));
        
        //Add the required labels to this JPanel
        this.add(new JLabel(Integer.toString(number) + ".", SwingConstants.CENTER));
        this.add(new JLabel(name, SwingConstants.CENTER));
        this.add(new JLabel(dueDate, SwingConstants.CENTER));
        this.add(new JLabel(contribution, SwingConstants.CENTER));

        //add mouse listener to this JPanel
        addMouseListener();
    }
    
    /**
     * Method to add mouse listeners to this JPanel, when the JPanel is hovered
     * it will change its border and its cursor, when it exits it goes back to
     * how it was. When the JPanel is clicked, it will dispose all the open
     * JDialog windows and will open the activity information form
     */
    private void addMouseListener() {
        //Add mouseListener to this JPanel
        this.addMouseListener(new MouseAdapter() {

            //On mouse click
            @Override
            public void mousePressed(MouseEvent e) {
                //Get all the open windows
                Window[] windows = Window.getWindows();
                //Loop through all the windows
                for (Window win : windows) {
                    //if window is JDialog, dispose it
                    if (win instanceof JDialog) {
                        win.dispose();
                    }
                }
                //Open Activity form
                new Activity(name, assignmentName, taskName, milestoneName);
            }

            //When mouse enters the JPanel, set the border
            @Override
            public void mouseEntered(MouseEvent e) {
                //set the border when mouse enters
                setBorder();
                revalidate();
                repaint();
            }

            //When mouse exits the JPanel, remoev the border
            @Override
            public void mouseExited(MouseEvent e) {
                //remove the border when mouse exits
                removeBorder();
                revalidate();
                repaint();
            }
        });
        
    }

    /**
     * Method to set the border with color and to set the cursor
     * to show that this JPanel is clickable
     */
    private void setBorder() {
        //On mouse enter the the border to be colored
        this.setBorder(new MatteBorder(2, 2, 2, 2, Color.LIGHT_GRAY));
        //Set the cursos, to show that its clickable
        this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    /**
     * Method to set back empty border to this JPanel 
     * and change the cursor to default
     */
    private void removeBorder() {
        //On mouse exit, remove the color from the border
        this.setBorder(new EmptyBorder(2, 2, 2, 2));
        //Set the default cursor
        this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    
}
