package DetailedModulePage;

//----------IMPORTS----------
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXTaskPane;
//---------------------------

public class Coursework extends JXTaskPane {

    //-------------------CLASS VARIABLES-------------------
    private final ArrayList<String> courseworkInformation;

    private String name;
    private String weighting;
    private String setDate;
    private String dueDate;

    private TaskPanel taskPanel;
    private MilestonePanel milestonePanel;
    //-----------------------------------------------------

    /**
     * A constructor to create JXTaskPane with coursework information
     *
     * @param courseworkInformation ArrayList containing information about
     * coursework
     */
    public Coursework(ArrayList<String> courseworkInformation) {


        this.courseworkInformation = courseworkInformation;
        this.setCollapsed(true);
        //Format string so it is spaced out evenly, because the coursework
        //information is displayed on the title of the JXTaskPane and it can
        //have only String
        //String is formatted in a way so each cw information is below
        //corresponding label
        name = courseworkInformation.get(0);
        this.setToolTipText(name);
        String tempName = name;
        if (name.length() > 25) {
            tempName = name.substring(0, 25);
            tempName = tempName + "...";
        }
        weighting = courseworkInformation.get(1);
        setDate = courseworkInformation.get(2);
        dueDate = courseworkInformation.get(3);

        //Get the size of window
        Toolkit tk = Toolkit.getDefaultToolkit();
        int xSize = (int) tk.getScreenSize().width;

        String titleFormattedString = String.format("%-" + 
                xSize / 42 + "s%-" + xSize / 56 + "s%-" + xSize / 56 + "s%s",
                tempName, weighting, setDate, dueDate);

        //To space out each string evenly, each character needs to have the width
        this.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
        //Set formatted String as a title of this JXTaskPane
        this.setTitle(titleFormattedString);

        //Add milestone JPanel to this coursework JXTaskPane
        addMilestones();

        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(this.getPreferredSize().width, 7));
        this.add(panel);
        //Add tasks JPanel to this coursework JXTaskPane
        addTasks();

        //Add buttons
        addButtons();
    }

    /**
     * A method to add buttons to the end of the form and to add action listeners
     * to the buttons
     */
    private void addButtons() {

        JPanel buttonJPanel = new JPanel();
        buttonJPanel.setLayout(new GridBagLayout());

        this.add(buttonJPanel);

        GridBagConstraints buttonPanelGBC = new GridBagConstraints();
        buttonPanelGBC.insets = new Insets(20, 5, 0, 5);

        JButton addMilestoneButton = new JButton("Add Milestone");
        addMilestoneButton.setPreferredSize(new Dimension(140, 25));
        buttonJPanel.add(addMilestoneButton, buttonPanelGBC);

        JButton addTaskButton = new JButton("Add Task");
        addTaskButton.setPreferredSize(new Dimension(140, 25));
        buttonJPanel.add(addTaskButton, buttonPanelGBC);

        addMilestoneButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new NewMilestoneForm(name);
            }
        });

        addTaskButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new NewTaskForm(name);
            }
        });

    }

    /**
     * A method to extract milestone information from coursework ArrayList, to
     * create milestone JPanel with the extracted information and to add it to
     * coursework JPanel
     */
    private void addMilestones() {

        //Create new ArrayList which will be populated with milestone information
        ArrayList<String> listOfMilestones = new ArrayList<>();
        //Get the position where milestone starts
        int milestonePlace = Integer.parseInt(courseworkInformation.get(4)) * 4 + 5;
        //When the position is received, get how many milestones
        String numberOfMilestones = courseworkInformation.get(milestonePlace);
        //Add how many milestones will there be
        listOfMilestones.add(numberOfMilestones);
        int countList = milestonePlace + 1;

        //Extract milestones information from the ArrayList
        for (int i = 0; i < Integer.parseInt(numberOfMilestones); i++) {
            //Get milestone name and due date
            for (int j = 0; j < 2; j++) {
                listOfMilestones.add(courseworkInformation.get(countList));
                countList++;
            }
            //Get the number of how many tasks in this milestone
            String numberOfTasks = courseworkInformation.get(countList);
            listOfMilestones.add(numberOfTasks);
            countList++;

            //Get tasks
            for (int j = 0; j < Integer.parseInt(numberOfTasks); j++) {
                //Get task name, due date, type and goal
                for (int k = 0; k < 4; k++) {
                    listOfMilestones.add(courseworkInformation.get(countList));
                    countList++;
                }
            }
        }
        //Create new Milestone JPanel with the extracted information
        milestonePanel = new MilestonePanel(listOfMilestones, name);
        //Add the received milestone JPanel to this coursework JPanel
        this.add(milestonePanel);
    }

    /**
     * A method to extract task information from coursework ArrayList, to create
     * task JPanel with the extracted information and to add it to coursework
     * JPanel
     */
    private void addTasks() {
        //Create ArrayList to store task information
        ArrayList<String> listOfTasks = new ArrayList<>();
        //Get the number of tasks
        String numberOfTasks = courseworkInformation.get(4);
        listOfTasks.add(numberOfTasks);
        //Task information starts from position 5
        int countList = 5;
        for (int i = 0; i < Integer.parseInt(numberOfTasks); i++) {
            //Get task name, due date, type, goal
            for (int j = 0; j < 4; j++) {
                listOfTasks.add(courseworkInformation.get(countList));
                countList++;
            }
        }
        //Create task JPanel
        taskPanel = new TaskPanel(listOfTasks, name, null);
        //Add created task JPanel to this coursework JPanel
        this.add(taskPanel);
    }

    /**
     * An accessor method to return the name of this coursework
     *
     * @return Name of this coursework
     */
    public String getCourseworkName() {
        return name;
    }

    /**
     * An accessor method to return weighting of this coursework
     *
     * @return weighting of this coursework
     */
    public String getCourseworkWeighting() {
        return weighting;
    }

    /**
     * An accessor method to return the set date of this coursework
     *
     * @return Set Date of this exam
     */
    public String getCourseworkSetDate() {
        return setDate;
    }

    /**
     * An accessor method to return the due date of this coursework
     *
     * @return Due date of this coursework
     */
    public String getCourseworkDueDate() {
        return dueDate;
    }

    /**
     * An accessor method to return the taskPanel this coursework has
     * @return task panel in this coursework
     */
    public TaskPanel getTaskPanel() {
        return taskPanel;
    }

    /**
     * An accessor method to return the milestone panel
     * @return milestone panel this coursework has
     */
    public MilestonePanel getMilestonePanel() {
        return milestonePanel;
    }

}
