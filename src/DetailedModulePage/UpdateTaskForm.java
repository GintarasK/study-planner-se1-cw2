package DetailedModulePage;

//----------IMPORTS----------
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import org.jdesktop.swingx.JXDatePicker;
import studyplanner.controller.TaskController;
import utilities.exceptions.*;
//---------------------------

public class UpdateTaskForm extends JDialog {

    //-------------------CLASS VARIABLES-------------------
    private JTextField taskNameText;
    private JXDatePicker taskStartDatePicker;
    private JXDatePicker taskDueDatePicker;
    private JTextField goalText;
    private JComboBox dependantTaskBox;
    private JComboBox criticalBox;

    private JButton createButton;
    private JButton cancelButton;

    private String assignmentName;
    private String taskName;
    private String milestoneName;
    private ArrayList<String> taskInfo;

    private JPanel notesPanel;
    private ArrayList<String> notesList;

    private JTextArea noteArea;
    private JPanel notePanel;
    private GridBagConstraints notePanelGBC;

    private JLabel taskNameLabel;
    private JLabel taskStartDateLabel;
    private JLabel taskDueDateLabel;
    private JLabel taskTypeLabel;
    private JLabel taskType;
    private JLabel goalLabel;
    private JLabel criticalLabel;
    private JButton addNote;
    private JLabel noteInfoLabel;
    private JLabel dependantTaskLabel;

    private JPanel mainJPanel;
    private GridBagConstraints gbc;

    private JPanel formPanel;
    private JPanel formHoldPanel;
    //-----------------------------------------------------

    
    
    
    /**
     *  A constructor to open UpdateTaskForm object, which lets to update a 
     * task information. Get task information, with the provided information, 
     * by calling a method in a controller and assign the received information
     * with the fields they should be in.
     * 
     * @param assignmentName    The assignment name this task is in
     * @param taskName          The name of this task
     * @param milestoneName     The name of the milestone this task is in
     *                          (if not, "null")
     */
    public UpdateTaskForm(String assignmentName, String taskName, 
                                                        String milestoneName) {
        //get the information about this task
        try {
            taskInfo = TaskController.getFormInfo(assignmentName, taskName);
        } catch (NullInputException | ItemNotFoundException | 
                                                       EmptyInputException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(UpdateTaskForm.class.getName()).
                                                    log(Level.SEVERE, null, ex);
        }
        //Assign variables
        this.milestoneName = milestoneName;
        this.assignmentName = assignmentName;
        this.taskName = taskName;
        
        //Method to initialise and populate the whole object
        initialise();

        if (this.getPreferredSize().height > 700) {
            
            this.setPreferredSize(new Dimension(this.getPreferredSize().width, 700));
            
        }
        
        //To make this form visible
        this.pack();
        this.setModal(true);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    /**
     * A method to initialise the main panel, add it to the scroll pane, set
     * the title of this form and call the methods to populate the main panel
     */
    private void initialise() {

        //Creating a scroll pane for the main panel
        JScrollPane mainScrollPane = new JScrollPane();
        mainScrollPane.setHorizontalScrollBar(null);
        this.add(mainScrollPane);

        //Creating main panel and adding it to scroll pane
        mainJPanel = new JPanel();
        mainJPanel.setLayout(new BorderLayout());
        mainScrollPane.getViewport().add(mainJPanel);

        //Setting the title of the form
        this.setTitle("Update Task");

        //Create and add the name label at the top of this form
        createName();

        //Create a panel which will store a formPanel
        formHoldPanel = new JPanel();
        formHoldPanel.setBorder(new EmptyBorder(20, 65, 20, 65));
        //Panel which will hold all the information about this task
        formPanel = new JPanel();
        formPanel.setLayout(new GridBagLayout());
        //Create and add the main information about the task
        createComponents();
        
        //Make the dependant task box width of its own panel
        dependantTaskBox.setPreferredSize(
                    new Dimension(formPanel.getPreferredSize().width, 
                                   dependantTaskBox.getPreferredSize().height));

        //Create and add the buttons at the bottom of this form
        createButtons();
    }

    /**
     * A method to create a label which will show the name of the form 
     * and to adjust its size, so it would be bigger, and add it to the panel, 
     * which will be placed at the top of the main panel
     */
    private void createName() {

        //Create the panel where the name label will be stored
        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new GridBagLayout());

        //Create the label for the name
        JLabel mainLabel = new JLabel("Update Task");
        float size = 17;
        mainLabel.setFont(mainLabel.getFont().deriveFont(size));
        mainLabel.setBorder(new EmptyBorder(15, 0, 5, 0));

        //Add the label to its panel and add panel to the main panel top
        labelPanel.add(mainLabel);
        mainJPanel.add(labelPanel, BorderLayout.PAGE_START);
    }

    /**
     * A method to create all the required components which will show the main
     * information about this task and will be added to the formPanel,
     * which will be added to the center of the main panel
     */
    private void createComponents() {

        //Create name label
        taskNameLabel = new JLabel("Name:      ");
        //Create task name text field
        taskNameText = new JTextField();
        taskNameText.setDocument(new JTextFieldLimit(25));
        taskNameText.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        taskNameText.setText(taskInfo.get(0));

        //Create task start date label
        taskStartDateLabel = new JLabel("Start date:");
        //Create task start date picker
        taskStartDatePicker = new JXDatePicker();
        taskStartDatePicker.setDate(taskStartDatePicker.getLinkDay());
        taskStartDatePicker.getEditor().setEditable(false);
        taskStartDatePicker.setEditable(false);
        taskStartDatePicker.setPreferredSize(
             new Dimension(250, taskStartDatePicker.getPreferredSize().height));
        try {
            taskStartDatePicker.setDate(TaskController.getStartDate
                                                    (assignmentName, taskName));
        } catch (NullInputException | ItemNotFoundException | 
                                                       EmptyInputException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(UpdateTaskForm.class.getName()).
                                                    log(Level.SEVERE, null, ex);
        }

        //Create task due date label
        taskDueDateLabel = new JLabel("Due date:");
        //Create task due date picker
        taskDueDatePicker = new JXDatePicker();
        taskDueDatePicker.setDate(taskDueDatePicker.getLinkDay());
        taskDueDatePicker.getEditor().setEditable(false);
        taskDueDatePicker.setPreferredSize(
               new Dimension(250, taskDueDatePicker.getPreferredSize().height));
        taskNameText.setPreferredSize(
               new Dimension(250, taskDueDatePicker.getPreferredSize().height));
        try {

            taskDueDatePicker.setDate(TaskController.getDeadline(
                                                     assignmentName, taskName));
        } catch (NullInputException | ItemNotFoundException | 
                           EmptyInputException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(UpdateTaskForm.class.getName()).
                                                    log(Level.SEVERE, null, ex);


        }

        //Create task type label
        taskTypeLabel = new JLabel("Type:        ");
        //Create task type text field
        taskType = new JLabel(taskInfo.get(3));

        //Create task goal label
        goalLabel = new JLabel("Goal:");
        //Create task goal text field
        goalText = new JTextField();
        goalText.setDocument(new JTextFieldLimit(3));
        goalText.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        goalText.setPreferredSize(new Dimension(60, 
                                  taskDueDatePicker.getPreferredSize().height));
        goalText.setText(taskInfo.get(4));

        //Create critical label
        criticalLabel = new JLabel("Critical:");
        //Create critical combo box for yes/no
        String criticalString = taskInfo.remove(5);
        ArrayList<String> yesNo = new ArrayList<>();
        yesNo.add("NO");
        yesNo.add("YES");
        criticalBox = new JComboBox(yesNo.toArray());
        for (String str : yesNo) {
            if (str.equalsIgnoreCase(criticalString)) {
                criticalBox.setSelectedItem(str);
            }
        }

        //Create add note button and add action listener
        addNote = new JButton("Add");
        addNote.addActionListener(addNoteAction);
        //Create a label with information of how to delete a note
        noteInfoLabel = new JLabel("To delete a note, leave empty field!");

        //Create a notesPanel which will store all the notes
        notesPanel = new JPanel();
        notesPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(new Color(0, 0, 0)), "Notes"));
        notesPanel.setLayout(new BoxLayout(notesPanel, BoxLayout.PAGE_AXIS));
        notesList = new ArrayList<>();
        //Call the method to add all existing notes
        addNotes();

        //Create dependant task label
        dependantTaskLabel = new JLabel("Choose dependant Task:");
        //Create combo box which containts all possible dependant task choices
        ArrayList<String> taskList = new ArrayList<>();
        //Get list of task that can be dependant on
        try {
            taskList = TaskController.getDependencyList
                                                     (assignmentName, taskName);
        } catch (NullInputException | ItemNotFoundException | 
                                                       EmptyInputException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(NewTaskForm.class.getName()).
                                                    log(Level.SEVERE, null, ex);
        }
        //Add possibility "none" in the list
        taskList.add(0, "None");
        //Get the task that was chosen as dependant before if there was
        try {

            String dependantTask = TaskController.getDependency(assignmentName, taskName);
            if (dependantTask != null) {
                taskList.add(1, dependantTask);
            }
        } catch (NullInputException | ItemNotFoundException | EmptyInputException ex) {
            Logger.getLogger(UpdateTaskForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        //populate combo box with the created list
        dependantTaskBox = new JComboBox(taskList.toArray());

        //Check if this task had any dependant tasks, if yes, make it selected
        int notesStart = Integer.parseInt(taskInfo.get(5)) + 5 + 1;
        int activitiesStart = (Integer.parseInt(taskInfo.get(notesStart))*3)+1;
        if (taskInfo.get(activitiesStart + notesStart).equals("0")) {
            dependantTaskBox.setSelectedIndex(0);
        } else {
            dependantTaskBox.setSelectedIndex(taskList.indexOf
                                           (taskInfo.get(taskInfo.size() - 3)));
        }
        
        //Method to add everything in place
        addTaskInformation();

    }

    /**
     * Method to assign the GridBagConstraints for each already created 
     * component, add them to one panel and to add that panel to the main panel
     * in the center
     */
    private void addTaskInformation() {
        //Create GridBagConstraints for panel
        gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);

        //Add task name label
        gbc.gridy = 0;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        formPanel.add(taskNameLabel, gbc);
        //Add task name text area
        gbc.gridwidth = 3;
        gbc.gridx = 1;
        formPanel.add(taskNameText, gbc);

        //Add task start date label
        gbc.gridy = 1;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        formPanel.add(taskStartDateLabel, gbc);
        //Add task start date picker
        gbc.gridwidth = 3;
        gbc.gridx = 1;
        formPanel.add(taskStartDatePicker, gbc);

        //Add task due date label
        gbc.gridy = 2;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        formPanel.add(taskDueDateLabel, gbc);
        //Add task due date picker
        gbc.gridwidth = 3;
        gbc.gridx = 1;
        formPanel.add(taskDueDatePicker, gbc);

        //Add task type label
        gbc.gridy = 3;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        formPanel.add(taskTypeLabel, gbc);
        //Add task type
        gbc.gridx = 1;
        gbc.gridwidth = 1;
        formPanel.add(taskType, gbc);

        //Add task goal label
        gbc.gridy = 3;
        gbc.gridx = 3;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.NONE;
        formPanel.add(goalText, gbc);
        //Add task goal text area
        gbc.insets = new Insets(5, 30, 5, 5);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 2;
        formPanel.add(goalLabel, gbc);

        //Add task critical label
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridy = 4;
        gbc.gridx = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(criticalLabel, gbc);
        //Add task critical combo box
        gbc.gridx = 1;
        formPanel.add(criticalBox, gbc);
        gbc.fill = GridBagConstraints.NONE;

        //Add note "add" button
        gbc.insets = new Insets(20, 0, 10, 0);
        gbc.gridwidth = 1;
        gbc.gridy = 5;
        gbc.gridx = 3;
        formPanel.add(addNote, gbc);
        //Add note information label
        gbc.gridx = 0;
        gbc.gridwidth = 3;
        formPanel.add(noteInfoLabel, gbc);

        //Add note panel, which has all notes
        gbc.insets = new Insets(0, 0, 10, 0);
        gbc.gridy = 6;
        gbc.gridwidth = 4;
        gbc.gridx = 0;
        formPanel.add(notesPanel, gbc);

        //Add dependant task label
        gbc.insets = new Insets(10, 0, 10, 0);
        gbc.gridy = 7;
        gbc.gridwidth = 2;
        formPanel.add(dependantTaskLabel, gbc);

        //Add dependant task combo box
        gbc.gridy = 8;
        gbc.gridx = 0;
        gbc.gridwidth = 4;
        formPanel.add(dependantTaskBox, gbc);

        //Add the whole formPanel which has all the information about task
        //to panel which will be added to mainJPanel because of the layout
        formHoldPanel.add(formPanel);
        mainJPanel.add(formHoldPanel, BorderLayout.CENTER);
    }

    /**
     * A method to create "Update" and "Cancel" buttons, add action listeners
     * to them and add the at the bottom of the main panel
     */
    private void createButtons() {
        //Create a panel which will store all the buttons for this form
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        //Create GridBagConstraints for this panel
        GridBagConstraints buttonsGBC = new GridBagConstraints();
        buttonsGBC.insets = new Insets(50, 10, 15, 10);

        //Create update button and add it
        createButton = new JButton("Update");
        createButton.addActionListener(updateAction);
        createButton.setPreferredSize(new Dimension(110, 25));
        buttonPanel.add(createButton, buttonsGBC);

        //Create cancel button and add it
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(cancelAction);
        cancelButton.setPreferredSize(new Dimension(110, 25));
        buttonPanel.add(cancelButton, buttonsGBC);

        //Add the button panel with buttons to the main panel
        mainJPanel.add(buttonPanel, BorderLayout.PAGE_END);
    }

    /**
     * A method to add all the notes from the provided list by controllers to
     * note panel, which will have the number of the note and the note itself
     * and add note panel to the whole notes panel which is added to main panel,
     * If there are no notes, it will just add empty note area
     */
    private void addNotes() {
        
        //Get the number of notes
        int numberOfNotes = Integer.parseInt(taskInfo.get(5));

        //If number of notes is 0, make it 1, so it goes once through the loop
        if (numberOfNotes == 0) {
            numberOfNotes = 1;
        }
        
        //Create notoes that have been already created by user
        for (int i = 0; i < numberOfNotes; i++) {
            
            //Create notePanel
            notePanel = new JPanel();
            notePanel.setLayout(new GridBagLayout());
            //Create note GridBagConstraints
            notePanelGBC = new GridBagConstraints();

            //Number to show which note it is
            int count = i + 1;
            //Set constraints for number
            notePanelGBC.insets = new Insets(5, 10, 10, 10);
            notePanelGBC.gridy = i;
            notePanelGBC.gridx = 0;
            notePanelGBC.weightx = 0;
            //Add number to show which note it is
            notePanel.add(new JLabel(String.valueOf(count) + ". "), notePanelGBC);
            
            //Create noteArea which contains a note if there is a note
            noteArea = createJTextAreaForNotes();
            if (Integer.parseInt(taskInfo.get(5)) != 0) {
                noteArea.setText(taskInfo.get(6 + i));
            }
            //Set constraints for note
            notePanelGBC.gridx = 1;
            notePanelGBC.weightx = 1;
            notePanelGBC.fill = GridBagConstraints.HORIZONTAL;
            notePanelGBC.gridwidth = 1;
            //Add the note area to notePanel
            notePanel.add(noteArea, notePanelGBC);
            
            //Add full notePanel to the whole notesPanel
            notesPanel.add(notePanel);
        }
    }
    
    /**
     * A method to create a JTextArea and set its required attributes for the
     * note panel
     *
     * @return JTextArea required for note panel
     */
    private JTextArea createJTextAreaForNotes() {
        //Create JTextArea and set attributes
        JTextArea noteArea = new JTextArea(1, 23);
        noteArea.setLineWrap(true);
        noteArea.setWrapStyleWord(true);
        noteArea.setEditable(true);
        noteArea.setBorder(BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.LIGHT_GRAY),
                                  BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        //return already created JTextArea with set attributes for note
        return noteArea;
    }

    /**
     * A method to add another empty noteArea on click
     */
    private void addNoteField() {
        //create a panel for notes
        notePanel = new JPanel();
        notePanel.setLayout(new GridBagLayout());
        //Create constraints
        notePanelGBC = new GridBagConstraints();
        
        //Number to show which note it is
        int count = notesPanel.getComponentCount() + 1;
        //Set constraints for number
        notePanelGBC.insets = new Insets(5, 10, 10, 10);
        notePanelGBC.gridy = notesPanel.getComponentCount();
        notePanelGBC.gridx = 0;
        notePanelGBC.weightx = 0;
        //Add number to notePanel
        notePanel.add(new JLabel(String.valueOf(count) + ". "), notePanelGBC);
        
        //Create note area
        noteArea = createJTextAreaForNotes();
        //set constraints for noteArea
        notePanelGBC.gridx = 1;
        notePanelGBC.weightx = 1;
        notePanelGBC.fill = GridBagConstraints.HORIZONTAL;
        notePanelGBC.gridwidth = 1;
        //add noteArea to notePanel
        notePanel.add(noteArea, notePanelGBC);
        
        //Add notePanel to notesPanel which containts all the notes
        notesPanel.add(notePanel);
    }

    /**
     * Action listener which gather all the notes, get the selected dependant
     * task and will call a method in controller class to update a task, after
     * it will dispose this form and will open TaskInfoForm again
     */
    private ActionListener updateAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            //create notesList which contains all the notes
            notesList = new ArrayList<>();
            //loop through all component and find JTextArea and get the text
            for (Component panel : notesPanel.getComponents()) {
                JPanel pnl = (JPanel) panel;
                for (Component component : pnl.getComponents()) {
                    if (component instanceof JTextArea) {
                        String note = ((JTextArea) component).getText().trim();
                        if (!note.equals("")) {
                            notesList.add(note);
                        }
                    }
                }
            }

            //if user selected "none" as dependant task, it will initialise it
            //as "null"
            String dependantTask;
            if (dependantTaskBox.getSelectedItem().toString().
                                                    equalsIgnoreCase("none")) {
                dependantTask = null;
            } else {
                dependantTask = dependantTaskBox.getSelectedItem().toString();
            }
            
            //update task with all the given information and open TaskInfoForm
            try {
                TaskController.updateTask(assignmentName, taskName, 
                        taskNameText.getText(), taskDueDatePicker.getDate(),
                        goalText.getText(), dependantTask, notesList, 
                        criticalBox.getSelectedItem().toString());
                JOptionPane.showMessageDialog(null, 
                                        "Task has been successfully updated!");
                dispose();
                new TaskInfoForm(assignmentName, taskNameText.getText(), 
                                                                milestoneName);
            } catch (NullInputException | InputOutOfRangeException | 
                    DuplicateDataException | IllegalDateException | 
                    ItemNotFoundException | EmptyInputException | 
                    IllegalValueException | IllegalFileException | 
                                                    IllegalTasksException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(NewTaskForm.class.getName()).
                                                    log(Level.SEVERE, null, ex);
            }
        }
    };

    /**
     * An action listener to cancel this form, which will dispose this form
     * and will open back a TaskInfoForm on click
     */
    private ActionListener cancelAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            dispose();
            new TaskInfoForm(assignmentName, taskName, milestoneName);
        }
    };

    
    /**
     * An action listener to add another note. If the last note is empty,
     * it will prompt that you cannot add another note, unless it is not empty
     */
    private ActionListener addNoteAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            //if a noteArea is empty, it will not let to add another note,
            //until it gets any input
            if (noteArea == null) {
                addNoteField();
            } else if (!noteArea.getText().isEmpty()) {
                addNoteField();
            } else {
                JOptionPane.showMessageDialog(null, 
                            "Notes field must be filled to add another note!");
            }
            notesPanel.revalidate();
            notesPanel.repaint();
        }
    };

}
