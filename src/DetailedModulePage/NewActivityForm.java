package DetailedModulePage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import studyplanner.controller.ActivityController;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalFileException;
import utilities.exceptions.IllegalTypeException;
import utilities.exceptions.IllegalValueException;
import utilities.exceptions.IncorrectInputDataException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;


public class NewActivityForm extends JDialog {
    
    //-------------------CLASS VARIABLES-------------------
    private String taskName;
    private String assignmentName;
    private String milestoneName;
    
    private JTextField nameTextField;
    private JComboBox timeTakenComboBox;
    private JTextField contributionTextField;

    private JPanel mainPanel;
    private JPanel notesPanel;
    
    private JButton createButton;
    private JButton closeButton;
    
    private JTextArea noteArea=null;
    
    private JPanel infoPanel;
    private GridBagConstraints infoGBC;
    //-----------------------------------------------------
    
    /**
     * A constructor to create a new form to create a new activity
     * @param taskName          The name of the task, this acitivity is in
     * @param assignmentName    The name of assignment this acitivity is in
     * @param milestoneName     The name of the milestone this activity is in
     *                          ("null" if none)
     */
    public NewActivityForm(String taskName, String assignmentName, String milestoneName) {
        this.milestoneName = milestoneName;
        this.taskName = taskName;
        this.assignmentName = assignmentName;

        display();  
         
        pack();
        this.setModal(true);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    /**
     * A method that initialises and populates all the form by calling
     * the needed methods
     */
    private void display() {

        JScrollPane mainScrollPane = new JScrollPane();
        mainScrollPane.setHorizontalScrollBar(null);
        this.add(mainScrollPane);

        mainPanel = new JPanel();
        mainScrollPane.getViewport().add(mainPanel);

        mainPanel.setLayout(new BorderLayout());

        this.setTitle("New Activity");

        createName();
        createActivityInformation();
        createButtons();
    }
    
    /**
     * A method to create a name label at the top of the form
     */
    private void createName() {

        JPanel namePanel = new JPanel();
        namePanel.setLayout(new GridBagLayout());

        JLabel nameLabel = new JLabel("Create Activity");
        float size = 15;
        nameLabel.setFont(nameLabel.getFont().deriveFont(size));
        nameLabel.setBorder(new EmptyBorder(20, 0, 20, 0));
        namePanel.add(nameLabel);
        mainPanel.add(namePanel, BorderLayout.PAGE_START);

    }

    /**
     * A method that creates a name label and name area
     */
    private void createNameField(){
        JLabel nameLabel = new JLabel("Name: ");
        nameTextField = new JTextField();
        nameTextField.setDocument(new JTextFieldLimit(25));
        nameTextField.setPreferredSize(new Dimension(180, 25));
        nameTextField.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        infoGBC.gridx = 0;
        infoGBC.gridy = 0;
        infoPanel.add(nameLabel, infoGBC);
        infoGBC.gridwidth = 3;
        infoGBC.gridx = 1;
        infoPanel.add(nameTextField, infoGBC);
        infoGBC.gridwidth = 1;
    }
    
    /**
     * A method that create a time taken field and time taken area
     */
    private void createTimeTakenField(){
        JLabel timeTakenNameLabel = new JLabel("Time taken:");
        
        ArrayList<Integer> hours = new ArrayList<>();
        for (int i = 1; i <= 24; i++) {
            hours.add(i);
        }
        timeTakenComboBox = new JComboBox(hours.toArray());
        infoGBC.gridy = 1;
        infoGBC.gridx = 0;
        infoPanel.add(timeTakenNameLabel, infoGBC);
        infoGBC.fill = GridBagConstraints.NONE;
        infoGBC.gridx = 1;
        infoPanel.add(timeTakenComboBox, infoGBC);
        infoGBC.fill = GridBagConstraints.HORIZONTAL;
    }
    
    
    /**
     * A method that creates the contribution field label and contribution area
     */
    private void createContributionField(){
        JLabel contributionNameLabel = new JLabel("Contribution:");
        contributionTextField = new JTextField();
        contributionTextField.setDocument(new JTextFieldLimit(5));
        contributionTextField.setPreferredSize(new Dimension(50, 25));
        contributionTextField.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        infoGBC.gridy = 1;
        infoGBC.gridx = 2;
        infoPanel.add(contributionNameLabel, infoGBC);
        infoGBC.gridx = 3;
        infoPanel.add(contributionTextField, infoGBC);
    }
    
    /**
     * A method that creates a note panel where all the note areas will be 
     * created
     */
    private void createNotePanel(){
        
        JButton addNote = new JButton("Add");
        addNote.setPreferredSize(new Dimension(addNote.getPreferredSize().width, addNote.getPreferredSize().height - 5));
        addNote.addActionListener(addNoteAction);
        infoGBC.fill = GridBagConstraints.NONE;
        infoGBC.anchor = GridBagConstraints.EAST;
        infoGBC.insets = new Insets(25, 0, 10, 0);
        infoGBC.gridy = 3;
        infoGBC.gridx = 3;
        infoPanel.add(addNote, infoGBC);

        notesPanel = new JPanel();
        notesPanel.setLayout(new GridLayout(0, 1));
        notesPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(new Color(0, 0, 0)), "Notes"));
        addNote();
        infoGBC.fill = GridBagConstraints.HORIZONTAL;
        infoGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        infoGBC.insets = new Insets(0, 0, 10, 0);
        infoGBC.gridy = 4;
        infoGBC.gridx = 0;
        infoGBC.gridwidth = 4;
        infoPanel.add(notesPanel, infoGBC);
        
    }
    
    /**
     * A method to call the methods to populate the infoPanel, which will
     * be added to the middle of this form
     */
    private void createActivityInformation() {
        //Create panel with fields
        infoPanel = new JPanel();
        infoPanel.setBorder(new EmptyBorder(10, 50, 10, 50));
        infoPanel.setLayout(new GridBagLayout());
        
        //Set constraints
        infoGBC = new GridBagConstraints();
        infoGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        infoGBC.fill = GridBagConstraints.HORIZONTAL;
        infoGBC.insets = new Insets(5, 10, 5, 10);

        //Create all the fields
        createNameField();
        createTimeTakenField();
        createContributionField();
        createNotePanel();

        //Add everything to main Panel
        mainPanel.add(infoPanel, BorderLayout.CENTER);
    }

    /**
     * A method to add a note area when the button is clicked
     */
    private void addNote() {

        GridBagConstraints gbc = new GridBagConstraints();
        JPanel notePanel = new JPanel();
        notePanel.setLayout(new GridBagLayout());
        gbc.insets = new Insets(3, 10, 10, 10);
        gbc.gridy = notesPanel.getComponentCount();
        gbc.gridx = 0;
        gbc.weightx = 0;
        int count = notesPanel.getComponentCount() + 1;
        notePanel.add(new JLabel(String.valueOf(count) + ". "), gbc);
        gbc.gridx = 1;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = 1;

        noteArea = new JTextArea(1, 23);
        noteArea.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        noteArea.setEditable(true);

        noteArea.setLineWrap(true);
        noteArea.setWrapStyleWord(true);

        notePanel.add(noteArea, gbc);
        notesPanel.add(notePanel);
    }

    /**
     * A method to create buttons for this form and to place them at the bottom
     * of this form
     */
    private void createButtons() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setBorder(new EmptyBorder(30, 0, 30, 0));
        buttonPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 15, 5, 15);

        createButton = new JButton("Create");
        createButton.addActionListener(createAction);
        createButton.setPreferredSize(new Dimension(110, 25));

        gbc.gridy = 0;
        gbc.gridx = 0;
        buttonPanel.add(createButton, gbc);

        closeButton = new JButton("Close");
        closeButton.addActionListener(closeAction);
        closeButton.setPreferredSize(new Dimension(110, 25));

        gbc.gridy = 0;
        gbc.gridx = 1;
        buttonPanel.add(closeButton, gbc);

        mainPanel.add(buttonPanel, BorderLayout.PAGE_END);
    }

    /**
     * Action listener to add additional note, but will pop a message,
     * if the last note area is empty
     */
    private ActionListener addNoteAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (noteArea == null) {
                addNote();
            } else if (!noteArea.getText().isEmpty()) {
                addNote();
            } else {
                JOptionPane.showMessageDialog(null, 
                        "Notes field must be filled to add another note!");
            }
            notesPanel.revalidate();
            notesPanel.repaint();
        }

    };

    /**
     * Action listener to create a new activity
     */
    private ActionListener createAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {

            ArrayList<String> noteList = new ArrayList<>();

            for (Component comp : notesPanel.getComponents()) {
                JPanel pnl = (JPanel) comp;
                for (Component compon : pnl.getComponents()) {
                    if (compon instanceof JTextArea) {
                        String note = ((JTextArea) compon).getText().trim();
                        if (!note.equalsIgnoreCase("")) {
                            noteList.add(note);
                        }
                    }
                }
            }

            if (noteList.isEmpty()) {
                noteList = null;
            }

            try {
                ActivityController.newActivity(nameTextField.getText(),
                        timeTakenComboBox.getSelectedItem().toString(), 
                        contributionTextField.getText(),
                        noteList, taskName, assignmentName);
                JOptionPane.showMessageDialog(null, 
                        "Activity has been successfully created!");
                dispose();
                new TaskInfoForm(assignmentName, taskName, milestoneName);

            } catch (IllegalValueException | 
                    EmptyInputException | DuplicateDataException | 
                    NullInputException | IllegalTypeException | 
                    InputOutOfRangeException | IllegalFileException | 
                    ItemNotFoundException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), 
                        "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(NewActivityForm.class.getName()).
                        log(Level.SEVERE, null, ex);

            }
        }

    };

    /**
     * Action listener to close/dispose this form and to go back to TaskInfoForm
     */
    private ActionListener closeAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            dispose();
            new TaskInfoForm(assignmentName, taskName, milestoneName);
        }

    };
    
}
