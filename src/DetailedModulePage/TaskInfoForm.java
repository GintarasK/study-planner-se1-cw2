package DetailedModulePage;

//----------IMPORTS----------
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import studyplanner.controller.ActivityController;
import studyplanner.controller.TaskController;
import utilities.exceptions.*;
//---------------------------

public class TaskInfoForm extends JDialog {

    //-------------------CLASS VARIABLES-------------------
    private JButton updateButton;
    private JButton deleteButton;
    private JButton closeButton;
    private JButton attachActivityButton;
    private JButton createActivityButton;

    private String assignmentName;
    private String taskName;
    private String milestoneName;

    private JPanel notesPanel;
    private ArrayList<String> notesList;

    private JPanel activitiesPanel;
    private ArrayList<String> activitiesList;

    private JPanel panel;
    private JPanel formPanel;
    private JPanel infoPanel;
    private GridBagConstraints gbc;
    private JPanel mainJPanel;
    private JPanel buttonPanel;
    
    private ArrayList<String> taskInfo = null;
    //-----------------------------------------------------

    /**
     * A constructor to create a new form which will display all the information
     * about the this task
     * @param assignmentName    The name of the assignment this task is in
     * @param taskName          The name of this task
     * @param milestoneName     The name of the milestone this task is in
     *                          ("null" if none)
     */
    public TaskInfoForm(String assignmentName, String taskName, String milestoneName) {
        this.assignmentName = assignmentName;
        this.taskName = taskName;
        this.milestoneName = milestoneName;
        
        //Set parameters for panel
        this.setLayout(new GridLayout(0, 1));
        this.setTitle("Task Information");
        
        //Initialise and populate the form
        initialise();
        
        //Set the panels parameters
        this.pack();
        this.setModal(true);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    /**
     * A method to initialise all the variables, get the information
     * and to call methods, to populate the form
     */
    private void initialise() {
        try {
            taskInfo = TaskController.getFormInfo(assignmentName, taskName);
        } catch (NullInputException | ItemNotFoundException | EmptyInputException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(TaskInfoForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Create main panel and add it to scrollpane
        mainJPanel = new JPanel();
        mainJPanel.setLayout(new BorderLayout());
        JScrollPane mainScrollPane = new JScrollPane();
        mainScrollPane.setHorizontalScrollBar(null);
        mainScrollPane.getViewport().add(mainJPanel);
        this.add(mainScrollPane);

        //Create name label at the top of the form
        createName();
        
        //create panels for information
        panel = new JPanel();
        panel.setBorder(new EmptyBorder(20, 65, 20, 65));
        panel.setLayout(new GridLayout(0, 1));
        formPanel = new JPanel();
        formPanel.setLayout(new GridBagLayout());
        infoPanel = new JPanel();
        infoPanel.setLayout(new GridBagLayout());
        infoPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.
                createLineBorder(new Color(0, 0, 0)),"General Information"));
        gbc = new GridBagConstraints();
        //Populate panels with information
        createAllComponents();
        panel.add(formPanel);
        
        //Create button panel and populate with buttons
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        createUpperButtons();
        createLowerButtons();

        if (this.getPreferredSize().height > 700) {
            this.setPreferredSize(new Dimension(this.getPreferredSize().width, 700));
        }
    }
    
    /**
     * A method to create set date label and set day
     */
    private void createSetDateFields() {
        
        //Create labels
        JLabel taskSetDateLabel = new JLabel("Set Date:");
        JLabel taskSetDate = new JLabel(taskInfo.get(2).substring(0, 10));
        
        //Set constraints and add
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 10, 5, 5);
        gbc.gridy = 0;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        infoPanel.add(taskSetDateLabel, gbc);
        
        //Set constraints and add
        gbc.insets = new Insets(5, 5, 5, 50);
        gbc.gridwidth = 1;
        gbc.gridx = 1;
        infoPanel.add(taskSetDate, gbc);
    }

    /**
     * A method to create a due date label and due date
     */
    private void createDueDateFields() {
        
        //Create fields
        JLabel taskDueDateLabel = new JLabel("Due date:");
        JLabel taskDueDate = new JLabel(taskInfo.get(1));
        
        //Set constraints and add
        gbc.insets = new Insets(5, 5, 5, 10);
        gbc.gridy = 0;
        gbc.gridx = 2;
        gbc.gridwidth = 1;
        infoPanel.add(taskDueDateLabel, gbc);
        
        //Set constraints and add
        gbc.gridwidth = 1;
        gbc.gridx = 3;
        infoPanel.add(taskDueDate, gbc);
    }

    /**
     * a method to add a type label and a type
     */
    private void createTypeFields() {
        
        //Create fields
        JLabel taskTypeLabel = new JLabel("Type:");
        JLabel taskType = new JLabel(taskInfo.get(3));
        
        //Set constraints and add
        gbc.insets = new Insets(5, 10, 5, 5);
        gbc.gridy = 1;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        infoPanel.add(taskTypeLabel, gbc);
        
        //Set constraints and add
        gbc.insets = new Insets(5, 5, 5, 50);
        gbc.gridx = 1;
        gbc.gridwidth = 1;
        infoPanel.add(taskType, gbc);

    }

    /**
     * A method to add a goal label and a goal
     */
    private void createGoalFields() {
        
        //Create fields
        JLabel taskGoalLabel = new JLabel("Goal:");
        JLabel taskGoal = new JLabel(taskInfo.get(4));
        
        //Set constraints and add
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridx = 3;
        infoPanel.add(taskGoal, gbc);
        
        //Set constraints and add
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridx = 2;
        infoPanel.add(taskGoalLabel, gbc);
    }

    /**
     * A method to add a critical label and show what user selected
     */
    private void createCriticalFields() {
        
        //Create fields
        JLabel criticalLabel = new JLabel("Critical:");
        JLabel criticalAns = new JLabel(taskInfo.remove(5).toUpperCase());
        
        //Set constraints and add
        gbc.insets = new Insets(5, 10, 8, 5);
        gbc.gridy = 2;
        gbc.gridx = 0;
        infoPanel.add(criticalLabel, gbc);
        
        //Set constraints and add
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridx = 1;
        infoPanel.add(criticalAns, gbc);
        gbc.fill = GridBagConstraints.NONE;
    }

    /**
     * A method to create a notes panel, which will show all the notes this
     * task has and will be populated later
     */
    private void createNotesFields() {
        
        //Create panel
        notesPanel = new JPanel();
        notesPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(new Color(0, 0, 0)), "Notes"));
        notesPanel.setLayout(new BoxLayout(notesPanel, BoxLayout.PAGE_AXIS));
        //Initialise a list for notes
        notesList = new ArrayList<>();
        //Add notes
        addNotes();
        
        //Set constraints and add
        gbc.insets = new Insets(10, 0, 10, 0);
        gbc.gridy = 4;
        gbc.gridwidth = 4;
        gbc.gridx = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        formPanel.add(notesPanel, gbc);
    }

    /**
     * A method to create the panel for activities, which will be populated
     * later
     */
    private void createActivitiesFields() {
        
        //Create panel
        activitiesPanel = new JPanel();
        activitiesPanel.setLayout(new BoxLayout(activitiesPanel, BoxLayout.PAGE_AXIS));
        activitiesList = new ArrayList<>();
        //Add activities to the panel
        addActivities();
        
        //Set constraints and add
        gbc.insets = new Insets(10, 0, 10, 0);
        gbc.gridy = 6;
        gbc.gridwidth = 4;
        gbc.gridx = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(activitiesPanel, gbc);
    }

    /**
     * A method to create dependant tasks information panel, which shows
     * the name of the task this task is dependant on and the progress of it
     */
    private void createDependantFields() {
        //Create a panel for dependency
        JPanel dependentTaskPanel = new JPanel();
        dependentTaskPanel.setBorder(BorderFactory.createTitledBorder(
           BorderFactory.createLineBorder(new Color(0, 0, 0)), "Dependant on"));
        dependentTaskPanel.setLayout(new GridBagLayout());
        
        //Calculate the position where dependency starts
        int activitiesStart = Integer.parseInt(taskInfo.get(5)) + 6;
        int dependanciesStart = Integer.parseInt(taskInfo.get(activitiesStart)) * 3 + activitiesStart + 1;
        if (Integer.parseInt(taskInfo.get(dependanciesStart)) == 1) {
            //Creating constraints
            GridBagConstraints depTaskgbc = new GridBagConstraints();
            //dependant task name label
            JLabel depLabel = new JLabel(taskInfo.get(dependanciesStart + 1));
            //Dependant task progress bar
            JProgressBar depBar = new JProgressBar(0, 100);
            depBar.setValue(Integer.parseInt(taskInfo.get(dependanciesStart + 2)));
            depBar.setStringPainted(true);
            depBar.setPreferredSize(new Dimension(formPanel.getPreferredSize().width / 2, depBar.getPreferredSize().height));

            //Set constraints and add
            depTaskgbc.insets = new Insets(5, 30, 5, 10);
            depTaskgbc.gridy = 0;
            depTaskgbc.gridx = 0;
            depTaskgbc.weightx = 0;
            dependentTaskPanel.add(new JLabel("Task name"), depTaskgbc);
            depTaskgbc.gridy = 1;
            depTaskgbc.insets = new Insets(5, 30, 10, 10);
            dependentTaskPanel.add(depLabel, depTaskgbc);
            //Set constraints and add
            depTaskgbc.insets = new Insets(5, 30, 5, 10);
            depTaskgbc.gridy = 0;
            depTaskgbc.gridx = 1;
            depTaskgbc.weightx = 1;
            depTaskgbc.fill = GridBagConstraints.NONE;
            depTaskgbc.gridwidth = 1;
            dependentTaskPanel.add(new JLabel("Progress of the task"), depTaskgbc);
            depTaskgbc.insets = new Insets(5, 30, 10, 10);
            depTaskgbc.gridy = 1;
            dependentTaskPanel.add(depBar, depTaskgbc);

            //Set constraints to dependant panel to the whole panel
            gbc.insets = new Insets(10, 0, 10, 0);
            gbc.gridy = 8;
            gbc.gridwidth = 4;
            gbc.gridx = 0;
            formPanel.add(dependentTaskPanel, gbc);
        } else {
            //If task is not dependant, add label
            dependentTaskPanel.add(new JLabel("This task has no dependencies"),
                new GridBagConstraints(0, 0, 0, 0, 0, 0, 
                  GridBagConstraints.CENTER, 0, new Insets(5, 0, 10, 0), 0, 0));
            gbc.gridy = 8;
            gbc.gridwidth = 4;
            gbc.gridx = 0;
            formPanel.add(dependentTaskPanel, gbc);
        }
    }

    /**
     * A method to create a progress bar for this task
     */
    private void createProgressBar() {
        
        //Create Label
        JLabel progressLabel = new JLabel("Progress of the Task");
        //Set constraints and add
        gbc.insets = new Insets(30, 0, 10, 0);
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridy = 9;
        gbc.gridwidth = 4;
        gbc.gridx = 0;
        formPanel.add(progressLabel, gbc);

        //Create progress bar
        JProgressBar taskBar = new JProgressBar(0, 100);
        taskBar.setValue(Integer.parseInt(taskInfo.get(taskInfo.size() - 1)));
        taskBar.setStringPainted(true);
        taskBar.setPreferredSize(new Dimension(formPanel.getPreferredSize().width, 
                                            taskBar.getPreferredSize().height));
        //Set constraints and add
        gbc.insets = new Insets(10, 0, 10, 0);
        gbc.gridy = 10;
        gbc.gridwidth = 4;
        gbc.gridx = 0;
        formPanel.add(taskBar, gbc);
    }

    /**
     * A method to call all methods together, to populate formPanel, which is
     * the main panel in the center of the form
     */
    private void createAllComponents() {
        
        //Create all the needed components
        createSetDateFields();
        createDueDateFields();
        createTypeFields();
        createGoalFields();
        createCriticalFields();
        createNotesFields();
        createActivitiesFields();
        createDependantFields();
        createProgressBar();
        
        //Set constraints for infoPanel and add to the whole form
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 4;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        formPanel.add(infoPanel, gbc);
        mainJPanel.add(panel, BorderLayout.CENTER);
    }

    /**
     * A method to create a name label for this form
     */
    private void createName() {
        //Create the panel where the name label will be stored
        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new GridBagLayout());
        
        //Create the label for the name
        JLabel mainLabel = new JLabel(taskInfo.get(0));
        float size = 15;
        mainLabel.setFont(mainLabel.getFont().deriveFont(size));

        //Set the constraints for this label
        gbc = new GridBagConstraints();
        gbc.insets = new Insets(15, 0, 5, 0);
        
        //Add the label to its panel and add panel to the main panel top
        labelPanel.add(mainLabel, gbc);
        mainJPanel.add(labelPanel, BorderLayout.PAGE_START);

    }

    /**
     * A method to create upper buttons which are related to activity,
     * such as "Attach Activity" and "Create Acitivity"
     */
    private void createUpperButtons() {
        //Create panel to store activity buttons
        JPanel activityPanel = new JPanel();
        activityPanel.setLayout(new GridBagLayout());
        //Create constraints
        GridBagConstraints activityGBC = new GridBagConstraints();
        activityGBC.insets = new Insets(0, 10, 0, 10);

        //Create buttons
        attachActivityButton = new JButton("Attach Activity");
        attachActivityButton.addActionListener(attachActivity);
        attachActivityButton.setPreferredSize(new Dimension(125, 25));
        activityPanel.add(attachActivityButton, activityGBC);

        createActivityButton = new JButton("Create Activity");
        createActivityButton.addActionListener(createActivity);
        createActivityButton.setPreferredSize(new Dimension(125, 25));
        activityPanel.add(createActivityButton, activityGBC);

        //Add buttons
        gbc.insets = new Insets(50, 10, 15, 10);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 6;
        buttonPanel.add(activityPanel, gbc);
    }

    /**
     * create lower buttons for this form, "Update", "Delete", "Close"
     */
    private void createLowerButtons() {
        //Create buttons and add
        updateButton = new JButton("Update");
        updateButton.addActionListener(updateAction);
        updateButton.setPreferredSize(new Dimension(110, 25));

        gbc.gridwidth = 2;
        gbc.gridx = 0;
        gbc.insets = new Insets(0, 10, 15, 10);
        gbc.gridy = 1;
        buttonPanel.add(updateButton, gbc);

        deleteButton = new JButton("Delete");
        deleteButton.addActionListener(deleteAction);
        deleteButton.setPreferredSize(new Dimension(110, 25));
        gbc.gridx = 2;
        buttonPanel.add(deleteButton, gbc);

        closeButton = new JButton("Close");
        closeButton.addActionListener(closeAction);
        closeButton.setPreferredSize(new Dimension(110, 25));
        gbc.gridx = 4;
        buttonPanel.add(closeButton, gbc);

        mainJPanel.add(buttonPanel, BorderLayout.PAGE_END);

    }

    /**
     * A method to create notes and populate notesPanel with created notes
     */
    private void addNotes() {
        //Create constraints and panel for the note
        GridBagConstraints gbc = new GridBagConstraints();
        JPanel notePanel = new JPanel();
        notePanel.setLayout(new GridBagLayout());
        //If there are any notes, loop through all of them
        if (Integer.parseInt(taskInfo.get(5)) != 0) {
            for (int i = 0; i < Integer.parseInt(taskInfo.get(5)); i++) {
                //get the number of the note
                int count = i + 1;
                //create constraints
                gbc.insets = new Insets(5, 10, 10, 10);
                gbc.gridy = i;
                gbc.gridx = 0;
                gbc.weightx = 0;
                notePanel.add(new JLabel(String.valueOf(count) + ". "), gbc);
                //Create text area for note
                JTextArea noteArea = new JTextArea(1, 23);
                noteArea.setText(taskInfo.get(6 + i));
                noteArea.setLineWrap(true);
                noteArea.setWrapStyleWord(true);
                noteArea.setOpaque(false);
                noteArea.setEditable(false);
                noteArea.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)));
                noteArea.setBackground(new Color(214, 217, 223));
                noteArea.setHighlighter(null);
                //Set constraints and add
                gbc.gridx = 1;
                gbc.weightx = 1;
                gbc.fill = GridBagConstraints.HORIZONTAL;
                gbc.gridwidth = 1;
                notePanel.add(noteArea, gbc);
                //Add the note panel to the main notesPanel
                notesPanel.add(notePanel);
            }
        } else {
            //if there are no notes, the label will be shown
            JPanel labelPanel = new JPanel();
            labelPanel.setBorder(new EmptyBorder(1, 0, 6, 0));
            labelPanel.add(new JLabel("This task has no notes"),
             new GridBagConstraints(0, 0, 0, 0, 0, 0, GridBagConstraints.CENTER,
                     0, new Insets(5, 0, 25, 0), 0, 0));
            notesPanel.add(labelPanel);
        }
    }

    /**
     * A method to populate activities panel with activities
     */
    private void addActivities() {
        activitiesPanel = new JPanel();
        activitiesPanel.setLayout(new GridBagLayout());
        activitiesPanel.setBorder(BorderFactory.createTitledBorder(
            BorderFactory.createLineBorder(new Color(0, 0, 0)), "Activities"));

        JPanel activityLabelPanel = new JPanel();
        activityLabelPanel.setLayout(new GridLayout(0, 4));
        if ((Integer.parseInt(taskInfo.get(Integer.parseInt(taskInfo.get(5)) + 6))) != 0) {
            //Create labels for activity
            activityLabelPanel.add(new JLabel("Number", SwingConstants.CENTER));
            activityLabelPanel.add(new JLabel("Name", SwingConstants.CENTER));
            activityLabelPanel.add(new JLabel("Date", SwingConstants.CENTER));
            activityLabelPanel.add(new JLabel("Contribution", SwingConstants.CENTER));

            GridBagConstraints activityGBC = new GridBagConstraints();
            activityGBC.anchor = GridBagConstraints.CENTER;
            activityGBC.fill = GridBagConstraints.HORIZONTAL;
            activityGBC.weightx = 1;
            activityGBC.insets = new Insets(3, 0, 5, 0);
            activityGBC.gridx = 0;
            activityGBC.gridy = 0;
            activitiesPanel.add(activityLabelPanel, activityGBC);

            //Calculate where actiities start in the list
            int activitiesStart = Integer.parseInt(taskInfo.get(5)) + 6;
            int numbeer = Integer.parseInt(taskInfo.get(activitiesStart));
            for (int i = activitiesStart + 1; i <= (numbeer * 3) + activitiesStart; i++) {
                activityGBC.gridy = activitiesPanel.getComponentCount();
                activitiesPanel.add(new ActivityPanel(activitiesPanel.getComponentCount(), taskInfo.get(i),
                        taskInfo.get(++i), taskInfo.get(++i), assignmentName, taskName, milestoneName), activityGBC);
            }
        } else {
            activitiesPanel.add(new JLabel("This task has no activities"),
             new GridBagConstraints(0, 0, 0, 0, 0, 0, GridBagConstraints.CENTER,
                     0, new Insets(5, 0, 10, 0), 0, 0));
        }
    }

    /**
     * Action listener to attach activity to this task, which will open new form
     */
    private ActionListener attachActivity = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {

            try {
                if (ActivityController.getActivities(assignmentName, taskName).isEmpty()) {
                    JOptionPane.showMessageDialog(null, "There are no activities to attach");
                } else {
                    dispose();
                    new AttachActivityForm(assignmentName, taskName, milestoneName);
                }
            } catch (EmptyInputException | NullInputException | ItemNotFoundException ex) {
                Logger.getLogger(TaskInfoForm.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    };

    /**
     * action listener to create an activity, which will open new form
     */
    private ActionListener createActivity = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            dispose();
            new NewActivityForm(taskName, assignmentName, milestoneName);

        }
    };

    /**
     * action listener to update this task, which will open new form
     */
    private ActionListener updateAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            dispose();
            new UpdateTaskForm(assignmentName, taskName, milestoneName);
        }
    };

    /**
     * Action listener to delete this task, and message will shown if the user
     * wants to delete it for sure
     */
    private ActionListener deleteAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            int reply = JOptionPane.showConfirmDialog(null,

                    "Are you sure you want to delete it?", "Delete Task",
                    JOptionPane.YES_NO_OPTION);

            if (reply == JOptionPane.YES_OPTION) {
                try {
                    TaskController.removeTask(assignmentName, milestoneName, taskName);
                    dispose();
                    JOptionPane.showMessageDialog(null, "Task has been successfully deleted!");

                } catch (EmptyInputException | IllegalDependencyException 
                        | IllegalFileException | NullInputException | 
                        ItemNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), 
                            "Error", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(TaskInfoForm.class.getName()).
                            log(Level.SEVERE, null, ex);

                }
            }

        }
    };

    /**
     * Action listener to close the form that is displayed now, which will
     * dispose this form
     */
    private ActionListener closeAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            dispose();
        }
    };

}
