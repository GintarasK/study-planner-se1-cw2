package DetailedModulePage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import studyplanner.controller.ActivityController;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalFileException;
import utilities.exceptions.IllegalValueException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

public class UpdateActivityForm extends JDialog {

    private String taskName;
    private String assignmentName;
    private String milestoneName;
    private String oldActivityName;

    private JTextField nameTextField;
    private JComboBox timeTakenComboBox;
    private JTextField contributionTextField;

    private JPanel mainPanel;
    private JPanel notesPanel;

    private JButton createButton;
    private JButton closeButton;

    private JTextArea noteArea = null;

    private ArrayList<String> activityInfo;
    private JPanel infoPanel;
    private GridBagConstraints infoGBC;
    
    
    /**
     *  A constructor to open UpdateActivityForm object, which lets to update
     * activity information. With the provided variables, it gets information
     * about the activity by calling a method in controller classes and assigns
     * the received information to the fields they should be in.
     * 
     * @param taskName          The name of the task this activity is in
     * @param assignmentName    The name of the assignment this activity is in
     * @param milestoneName     The name of the milestone this activity is in
     * @param oldActivityName   The name of the activity
     */
    public UpdateActivityForm(String taskName, String assignmentName, 
                            String milestoneName, String oldActivityName) {
        this.oldActivityName = oldActivityName;
        this.milestoneName = milestoneName;
        this.taskName = taskName;
        this.assignmentName = assignmentName;

        //Get the information about this activity
        try {
            activityInfo = ActivityController.getFormInfo(assignmentName, oldActivityName);
        } catch (EmptyInputException | ItemNotFoundException | NullInputException ex) {
            Logger.getLogger(UpdateActivityForm.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Method which calls the required method to populate this form
        initialise();
        
        //To make this form visible
        this.pack();
        this.setModal(true);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    /**
     * A method to initialise the main panel, add it to the scroll pane, set
     * the title of this form and call the methods to populate the main panel
     */
    private void initialise() {
        //Creating a scroll pane for the main panel
        JScrollPane mainScrollPane = new JScrollPane();
        mainScrollPane.setHorizontalScrollBar(null);
        this.add(mainScrollPane);

        //Creating main panel and adding it to scroll pane
        mainPanel = new JPanel();
        mainScrollPane.getViewport().add(mainPanel);
        mainPanel.setLayout(new BorderLayout());

        //Setting the title of the form
        this.setTitle("Update activity");

        //Create and add the name label at the top of this form
        createName();
        
        //Create the panel in which the main components will be stored
        infoPanel = new JPanel();
        infoPanel.setBorder(new EmptyBorder(10, 50, 10, 50));
        infoPanel.setLayout(new GridBagLayout());
        //Create constraints for these components
        infoGBC = new GridBagConstraints();
        infoGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        infoGBC.fill = GridBagConstraints.HORIZONTAL;
        infoGBC.insets = new Insets(5, 10, 5, 10);
        //Create and add the main information about the activity
        createActivityInformation();
        
        //Create and add the buttons at the bottom of this form
        createButtons();
        
        
        if (this.getPreferredSize().height > 700) {
            
            this.setPreferredSize(new Dimension(this.getPreferredSize().width, 700));
            
        }
        
    }

    
    /**
     * A method to create a label which will show the name of the form 
     * and to adjust its size, so it would be bigger, and add it to the panel, 
     * which will be placed at the top of the main panel
     */
    private void createName() {
        //Create the panel where the name label will be stored
        JPanel namePanel = new JPanel();
        namePanel.setLayout(new GridBagLayout());

        //Create the label for the name
        JLabel nameLabel = new JLabel("Update Activity");
        float size = 15;
        nameLabel.setFont(nameLabel.getFont().deriveFont(size));
        nameLabel.setBorder(new EmptyBorder(20, 0, 20, 0));
        
        //Add the label to its panel and add panel to the main panel top
        namePanel.add(nameLabel);
        mainPanel.add(namePanel, BorderLayout.PAGE_START);

    }

    /**
     * A method to create all the required components which will show the main
     * information about this activity and will be able to update that info,
     * it will be added to the center of the main panel
     */
    private void createActivityInformation() {
        
        //Create name Label
        JLabel nameLabel = new JLabel("Name: ");
        //Create name text field
        nameTextField = new JTextField();
        nameTextField.setDocument(new JTextFieldLimit(25));
        nameTextField.setPreferredSize(new Dimension(180, 25));
        nameTextField.setText(activityInfo.get(0));
        //Set constraints and add name label
        infoGBC.gridx = 0;
        infoGBC.gridy = 0;
        infoPanel.add(nameLabel, infoGBC);
        //Set constraints and add name text field
        infoGBC.gridwidth = 3;
        infoGBC.gridx = 1;
        infoPanel.add(nameTextField, infoGBC);
        infoGBC.gridwidth = 1;

        //Create time taken name label
        JLabel timeTakenNameLabel = new JLabel("Time taken:");
        //Create combo box to choose hours
        ArrayList<Integer> hours = new ArrayList<>();
        for (int i = 1; i <= 24; i++) {
            hours.add(i);
        }
        timeTakenComboBox = new JComboBox(hours.toArray());
        timeTakenComboBox.setSelectedItem(Integer.parseInt(activityInfo.get(2)));
        //Set constraints and add
        infoGBC.gridy = 1;
        infoGBC.gridx = 0;
        infoPanel.add(timeTakenNameLabel, infoGBC);
        infoGBC.fill = GridBagConstraints.NONE;
        infoGBC.gridx = 1;
        infoPanel.add(timeTakenComboBox, infoGBC);
        infoGBC.fill = GridBagConstraints.HORIZONTAL;

        //Create contribution
        JLabel contributionNameLabel = new JLabel("Contribution:");
        contributionTextField = new JTextField();
        contributionTextField.setDocument(new JTextFieldLimit(5));
        contributionTextField.setPreferredSize(new Dimension(50, 25));
        contributionTextField.setText(activityInfo.get(3));
        //Set constraints and add
        infoGBC.gridy = 1;
        infoGBC.gridx = 2;
        infoPanel.add(contributionNameLabel, infoGBC);
        infoGBC.gridx = 3;
        infoPanel.add(contributionTextField, infoGBC);

        //create note panel
        createNotePanel();

        //add everything to the main panel in the center
        mainPanel.add(infoPanel, BorderLayout.CENTER);

    }
    
    /**
     * A method to create a note panel which will be added to the bottom of
     * formPanel, but in the middle of the mainPanel. You can add notes to this
     * panel by clicking a add button
     */
    private void createNotePanel(){
        //Add "add" note button
        JButton addNote = new JButton("Add");
        addNote.setPreferredSize(new Dimension(addNote.getPreferredSize().width, addNote.getPreferredSize().height - 5));
        addNote.addActionListener(addNoteAction);
        //set constraints
        infoGBC.fill = GridBagConstraints.NONE;
        infoGBC.anchor = GridBagConstraints.EAST;
        infoGBC.insets = new Insets(25, 0, 10, 0);
        infoGBC.gridy = 3;
        infoGBC.gridx = 3;
        infoPanel.add(addNote, infoGBC);

        //Add note panel
        notesPanel = new JPanel();
        notesPanel.setLayout(new GridLayout(0, 1));
        notesPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(new Color(0, 0, 0)), "Notes"));
        addNotes();
        //set constraints
        infoGBC.fill = GridBagConstraints.HORIZONTAL;
        infoGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        infoGBC.insets = new Insets(0, 0, 10, 0);
        infoGBC.gridy = 4;
        infoGBC.gridx = 0;
        infoGBC.gridwidth = 4;
        infoPanel.add(notesPanel, infoGBC);
    }

    
    /**
     * A method to add all the notes from the provided list by controllers to
     * note panel, which will have the number of the note and the note itself
     * and add note panel to the whole notes panel which is added to main panel,
     * If there are no notes, it will just add empty note area
     */
    private void addNotes() {
        int numberOfNotes = Integer.parseInt(activityInfo.get(4));
        if (numberOfNotes == 0) {
            numberOfNotes++;
        }
        for (int i = 0; i < numberOfNotes; i++) {
            GridBagConstraints gbc = new GridBagConstraints();
            JPanel notePanel = new JPanel();
            notePanel.setLayout(new GridBagLayout());
            gbc.insets = new Insets(3, 10, 10, 10);
            gbc.gridy = notesPanel.getComponentCount();
            gbc.gridx = 0;
            gbc.weightx = 0;
            int count = notesPanel.getComponentCount() + 1;
            notePanel.add(new JLabel(String.valueOf(count) + ". "), gbc);
            gbc.gridx = 1;
            gbc.weightx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.gridwidth = 1;

            noteArea = new JTextArea(1, 23);
            noteArea.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                    BorderFactory.createEmptyBorder(5, 5, 5, 5)));
            noteArea.setEditable(true);

            noteArea.setLineWrap(true);
            noteArea.setWrapStyleWord(true);

            if (Integer.parseInt(activityInfo.get(4)) != 0) {
                noteArea.setText(activityInfo.get(i + 5));
            }

            notePanel.add(noteArea, gbc);
            notesPanel.add(notePanel);
        }
    }

    
    /**
     * A method to add another empty noteArea on click
     */
    private void addNote() {

        GridBagConstraints gbc = new GridBagConstraints();
        JPanel notePanel = new JPanel();
        notePanel.setLayout(new GridBagLayout());
        gbc.insets = new Insets(3, 10, 10, 10);
        gbc.gridy = notesPanel.getComponentCount();
        gbc.gridx = 0;
        gbc.weightx = 0;
        int count = notesPanel.getComponentCount() + 1;
        notePanel.add(new JLabel(String.valueOf(count) + ". "), gbc);
        gbc.gridx = 1;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = 1;

        noteArea = new JTextArea(1, 23);
        noteArea.setBorder(BorderFactory.createCompoundBorder(
                                    BorderFactory.createLineBorder(Color.GRAY),
                                  BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        noteArea.setEditable(true);

        noteArea.setLineWrap(true);
        noteArea.setWrapStyleWord(true);

        notePanel.add(noteArea, gbc);
        notesPanel.add(notePanel);

    }

    /**
     * A method to create "Update" and "Close" buttons, add action listeners
     * to them and add the at the bottom of the main panel
     */
    private void createButtons() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setBorder(new EmptyBorder(30, 0, 30, 0));
        buttonPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 15, 5, 15);

        createButton = new JButton("Update");
        createButton.addActionListener(updateAction);
        createButton.setPreferredSize(new Dimension(110, 25));

        gbc.gridy = 0;
        gbc.gridx = 0;
        buttonPanel.add(createButton, gbc);

        closeButton = new JButton("Close");
        closeButton.addActionListener(closeAction);
        closeButton.setPreferredSize(new Dimension(110, 25));

        gbc.gridy = 0;
        gbc.gridx = 1;
        buttonPanel.add(closeButton, gbc);

        mainPanel.add(buttonPanel, BorderLayout.PAGE_END);
    }

    /**
     * An action listener to add another note. If the last note is empty,
     * it will prompt that you cannot add another note, unless it is not empty
     */
    private ActionListener addNoteAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (noteArea == null) {
                addNote();
            } else if (!noteArea.getText().isEmpty()) {
                addNote();
            } else {
                JOptionPane.showMessageDialog(null, 
                            "Notes field must be filled to add another note!");
            }
            notesPanel.revalidate();
            notesPanel.repaint();
        }

    };

    /**
     * Action listener which gather all the notes, will call a method in 
     * controller class to update a task, after it will dispose this form 
     * and will open Activity again
     */
    private ActionListener updateAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {

            ArrayList<String> noteList = new ArrayList<>();

            for (Component comp : notesPanel.getComponents()) {
                JPanel pnl = (JPanel) comp;
                for (Component compon : pnl.getComponents()) {
                    if (compon instanceof JTextArea) {
                        String note = ((JTextArea) compon).getText().trim();
                        if (!note.equalsIgnoreCase("")) {
                            noteList.add(note);
                        }
                    }
                }
            }

            if (noteList.isEmpty()) {
                noteList = null;
            }

            try {
                ActivityController.updateActivity(assignmentName, 
                                        oldActivityName, nameTextField.getText(),
                        timeTakenComboBox.getSelectedItem().toString(), 
                        contributionTextField.getText(), noteList);
                dispose();
                JOptionPane.showMessageDialog(null, 
                                    "Activity has been successfully updated!");
                new Activity(nameTextField.getText(), assignmentName, 
                                                    taskName, milestoneName);
            } catch (EmptyInputException | NullInputException | 
                    ItemNotFoundException | IllegalValueException | 
                    InputOutOfRangeException | DuplicateDataException | 
                                                    IllegalFileException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(UpdateActivityForm.class.getName()).
                                                    log(Level.SEVERE, null, ex);
            }
        }

    };

    
    /**
     * An action listener to close this form, which will dispose this form
     * and will open back a Activity on click
     */
    private ActionListener closeAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            dispose();
            new Activity(oldActivityName, assignmentName, taskName, milestoneName);
        }

    };

}
