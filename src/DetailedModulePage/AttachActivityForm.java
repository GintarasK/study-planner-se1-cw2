package DetailedModulePage;

//----------IMPORTS----------
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import studyplanner.controller.ActivityController;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.IllegalFileException;
import utilities.exceptions.IllegalTypeException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;
//---------------------------


public class AttachActivityForm extends JDialog{
    
    
    //-------------------CLASS VARIABLES-------------------
    private JButton attachButton;
    private JButton cancelButton;
    private JComboBox activityList;

    private String assignmentName;
    private String taskName;
    private String milestoneName;
    
    private GridBagConstraints gbc;
    //-----------------------------------------------------
    
    
    
    /**
     * A constructor to create a JDialog which allows to attach an activity
     * to the task, by choosing the activity from the given list
     * 
     * @param assignmentName        Assignment name this task is in
     * @param taskName              Name of the task an activity needs to be
     *                              attached to
     * @param milestoneName         Name of the milestone this task is in
     *                              ("null" if none)
     */
    public AttachActivityForm(String assignmentName, String taskName, String milestoneName) {
        
        this.milestoneName = milestoneName;
        this.assignmentName = assignmentName;
        this.taskName = taskName;

        //Initialise the whole form
        initialise();
    }
    
    
    
    
    
    /**
     * A method to initialise, to set parameters for this JDialog and to call
     * the appropriate methods to populate this JDialog
     */
    private void initialise(){
        //Set the parameters for this JDialog
        this.setLayout(new BorderLayout());
        this.setTitle("Activity Selection");
        
        //Create GridBagConstraints to make constraints for components being 
        //added to this JDialog
        gbc = new GridBagConstraints();
        
        //Method to add a name label at the top of the form
        addNameLabel();
        
        //Method to add all components at the center of this form
        addComponents();
        
        //Method to add buttons to end of this form
        addButtons();

        //Set the parameters for this JDialog
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setModal(true);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
    
    
    
    
    /**
     * A method to create a label, adjust its size and add it 
     * at the top of the form
     */
    private void addNameLabel(){
        
        //Create a panel where name JLabel will be added
        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new GridBagLayout());
        
        //Create label
        JLabel mainLabel = new JLabel("Attach an Activity");
        //Adjust its size
        float size = 15;
        mainLabel.setFont(mainLabel.getFont().deriveFont(size));
        
        //Add JLabel to its panel
        gbc.insets = new Insets(15, 0, 5, 0);
        labelPanel.add(mainLabel, gbc);
        
        //Add name panel to the main frame
        this.add(labelPanel, BorderLayout.PAGE_START);
    }
    
    
    
    
    
    /**
     * A method to add main components to this form 
     * for selection of the activity
     */
    private void addComponents(){
        
        //Create a panel in which the components will be stored
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(20, 65, 20, 65));
        panel.setLayout(new GridLayout(0, 2));

        //Create a label and add it to the panel
        JLabel activityLabel = new JLabel("Select an Activity:");
        activityLabel.setBorder(new EmptyBorder(0,0,0,30));
        panel.add(activityLabel);
        
        //Create JComboBox to choose which activity to add
        activityList = new JComboBox();

        //Get the list of all the activities that can be attached to task
        try {
            ArrayList<String> activities = ActivityController.
                                        getActivities(assignmentName, taskName);
            
            activityList.setModel(
                                new DefaultComboBoxModel(activities.toArray()));
        } catch (EmptyInputException | NullInputException | 
                                                     ItemNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(AttachActivityForm.class.getName()).
                                                    log(Level.SEVERE, null, ex);
        }
        
        //Add populated JComboBox to the panel
        panel.add(activityList);
        
        //Add the panel containing components to this JPanel
        this.add(panel, BorderLayout.CENTER);
    }
    
    
    
    
    
    /**
     * Method to add buttons to this JPanel bottom
     */
    private void addButtons(){
        
        //Create a panel which will add the buttons
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        
        //Set insets, to space out buttons
        gbc.insets = new Insets(5, 10, 15, 10);
        
        //Create a new button "Attach" and add action listener
        attachButton = new JButton("Attach");
        attachButton.addActionListener(attachAction);
        attachButton.setPreferredSize(new Dimension(110, 25));
        //Add button to the button panel
        buttonPanel.add(attachButton, gbc);

        //Create a new button "Cancel" and add action listener
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(cancelAction);
        cancelButton.setPreferredSize(new Dimension(110, 25));
        //Add button to the button panel
        buttonPanel.add(cancelButton, gbc);
        
        //Add the button panel to this panel at the bottom
        this.add(buttonPanel, BorderLayout.PAGE_END);
    }
    
    
    
    
    
    
    /**
     * Action listener for "Attach" button to attach activity to task and
     * to open TaskInfoForm
     */
    private ActionListener attachAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            try {
                //Call a method in controller to attach acttivity to task
                ActivityController.attachActivity(assignmentName, taskName, 
                                     activityList.getSelectedItem().toString());
                //Dispose this JDialog
                dispose();
                //Create new TaskInfoForm to show task information
                new TaskInfoForm(assignmentName, taskName, milestoneName);
            } catch (EmptyInputException | IllegalTypeException | 
                                    NullInputException | ItemNotFoundException | 
                                 IllegalDateException | DuplicateDataException | 
                                                      IllegalFileException ex) {
                //If error occurs, pop up shows error
                JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(AttachActivityForm.class.getName()).
                                                    log(Level.SEVERE, null, ex);
            }
        }
    };

    
    
    /**
     * An action listener for button "Cancel" to dispose the open JDialog
     * and to open TaskInfoForm
     */
    private ActionListener cancelAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            dispose();
            new TaskInfoForm(assignmentName, taskName, milestoneName);
        }
    };
    
    
}