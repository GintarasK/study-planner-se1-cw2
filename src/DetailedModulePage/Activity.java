package DetailedModulePage;

//----------IMPORTS----------
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import studyplanner.controller.ActivityController;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDataManipulationException;
import utilities.exceptions.IllegalFileException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;
//---------------------------


public class Activity extends JDialog {

    //-------------------CLASS VARIABLES-------------------
    //Activity object fields
    private String name;
    private String date;
    private String timeTaken;
    private int contribution;
    private ArrayList<String> noteList;

    //Main JPanel
    private JPanel mainPanel;
    //contains information labels about this activity
    private JPanel infoPanel;
    //JPanel for notes
    private JPanel notesPanel;
    
    //Values that are passed to this object
    private String activityName;
    private String assignmentName;
    private String milestoneName;
    private String taskName;
    
    //Buttons at the bottom of this form
    private JButton updateButton;
    private JButton deleteButton;
    private JButton closeButton;
    
    //Labels which show information about the activity
    private JLabel setDateNameLabel;
    private JLabel setDateLabel;
    private JLabel timeTakenNameLabel;
    private JLabel timeTakenLabel;
    private JLabel contributionNameLabel;
    private JLabel contributionLabel;
    
    //GridbagConstraints for info panel
    private GridBagConstraints infoGBC;
    //-----------------------------------------------------

    
    
    /**
     *  A constructor to create Activity object, which assigns all provided
     * variables to class variables, gets activity information by calling a
     * method in controller with the provided variables and assigns required
     * fields with the information controller provided.
     * 
     * @param activityName      The name of this activity
     * @param assignmentName    The name of the assignment this activity is in
     * @param taskName          The name of the task this activity is in
     * @param milestoneName     The name of the milestone this actvity is in
     *                          ("null" if none)
     */
    public Activity(String activityName, String assignmentName, String taskName,
                                                         String milestoneName) {

        //Assign all the fields
        this.activityName = activityName;
        this.assignmentName = assignmentName;
        this.milestoneName = milestoneName;
        this.taskName = taskName;

        //Get the information about activity
        try {
            ArrayList<String> activityInfo = 
                   ActivityController.getFormInfo(assignmentName, activityName);
            
            //Extract all the information from the received list
            this.name = activityInfo.get(0);
            this.date = activityInfo.get(1);
            this.timeTaken = activityInfo.get(2);
            this.contribution = Integer.parseInt(activityInfo.get(3));
            noteList = new ArrayList<>();
            int notesStart = Integer.parseInt(activityInfo.get(4));
            for (int i = 0; i < notesStart; i++) {
                noteList.add(activityInfo.get(i + 5));
            }
            
        } catch (EmptyInputException | ItemNotFoundException |
                                                        NullInputException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", 
                                                     JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(Activity.class.getName()).log(Level.SEVERE, null, 
                                                                            ex);
        }

        //Create and add all the components to this object
        initialise();
        
        // Resize the panel 
        if (this.getPreferredSize().height > 700) {
            
            this.setPreferredSize(new Dimension(this.getPreferredSize().width, 700));
            
        }
        
        //To make this form visible
        this.pack();
        this.setModal(true);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    }
    
    
    
    /**
     * A method to initialise the main panel, add it to the scroll pane, set
     * the title of this form and call the methods to populate the main panel
     */
    private void initialise() {
        
        //Creating a scroll pane for the main panel
        JScrollPane mainScrollPane = new JScrollPane();
        mainScrollPane.setHorizontalScrollBar(null);
        this.add(mainScrollPane);

        //Creating main panel and adding it to scroll pane
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainScrollPane.getViewport().add(mainPanel);

        //Setting the title of the form
        this.setTitle("Activity information");

        //Create and add the name label at the top of this form
        createName();
        
        //Create and add the main information about the activity
        createActivityInformation();
        
        //Create and add the buttons at the bottom of this form
        createButtons();
    }
    
    
    
    
    /**
     * A method to create a label which will show the name of the activity
     * and to adjust its size, so it would be bigger, and add it to the panel, 
     * which will be placed at the top of the main panel
     */
    private void createName() {

        //Create the panel where the name label will be stored
        JPanel namePanel = new JPanel();
        namePanel.setLayout(new GridBagLayout());

        //Create the label for the name
        JLabel nameLabel = new JLabel(name);
        float size = 15;
        nameLabel.setFont(nameLabel.getFont().deriveFont(size));
        nameLabel.setBorder(new EmptyBorder(20, 0, 20, 0));
        
        //Add the label to its panel and add panel to the main panel top
        namePanel.add(nameLabel);
        mainPanel.add(namePanel, BorderLayout.PAGE_START);

    }

    
    
    
    
    
    
    /**
     * A method to create all the required components which will show the main
     * information about this activity and will be added to the info panel,
     * which will be added to the center of the main panel
     */
    private void createActivityInformation() {

        //Create set date labels
        setDateNameLabel = new JLabel("Set Date: ");
        setDateLabel = new JLabel(date);

        //Create time taken labels
        timeTakenNameLabel = new JLabel("Time taken:");
        timeTakenLabel = null;
        if (Integer.parseInt(timeTaken) == 1) {
            timeTakenLabel = new JLabel(timeTaken + " hour");
        } else {
            timeTakenLabel = new JLabel(timeTaken + " hours");
        }

        //Create contribution label
        contributionNameLabel = new JLabel("Contribution:");
        contributionLabel = new JLabel(Integer.toString(contribution));

        //Create notes panel, which will store all the notes
        notesPanel = new JPanel();
        notesPanel.setLayout(new GridLayout(0, 1));
        notesPanel.setBorder(BorderFactory.createTitledBorder(
                  BorderFactory.createLineBorder(new Color(0, 0, 0)), "Notes"));
        //Add the notes to this panel
        addNotes();

        //Add all of the created components to the main panel
        initialiseActivityInformation();
    }
    

    
    
    /**
     * A method to add all the notes from the provided list by controllers to
     * note panel, which will have the number of the note and the note itself
     * and add note panel to the whole notes panel which is added to main panel,
     * If there are no notes, it will show that this activity has no notes
     * in the middle of the panel
     */
    private void addNotes() {
        //If there are no notes, create label to show that
        if (noteList.isEmpty()) {
            JPanel notePanel = new JPanel();
            notePanel.setBorder(new EmptyBorder(1, 0, 6, 0));
            notePanel.add(new JLabel("This Activity has no notes!"));
            notesPanel.add(notePanel);
        } else {
            //Loop through each note and add them to the notes panel
            for (String note : noteList) {
                //Create panel which will store one note
                JPanel notePanel = new JPanel();
                notePanel.setLayout(new GridBagLayout());
                
                //Create GridBagConstraints for notePanel
                GridBagConstraints gbc = new GridBagConstraints();
                gbc.insets = new Insets(3, 10, 10, 10);
                
                //Assign GridBagConstraints values to add number label
                gbc.gridy = notesPanel.getComponentCount();
                gbc.gridx = 0;
                gbc.weightx = 0;
                //Number to show which note it is
                int count = notesPanel.getComponentCount() + 1;
                notePanel.add(new JLabel(String.valueOf(count) + ". "), gbc);
               
                //Create note JTextArea and populate it
                JTextArea noteArea = createJTextAreaForNotes();
                noteArea.setText(note);
                //Assign GridBagConstraints to add note JTextArea
                gbc.gridx = 1;
                gbc.weightx = 1;
                gbc.fill = GridBagConstraints.HORIZONTAL;
                gbc.gridwidth = 1;
                notePanel.add(noteArea, gbc);

                //Add notePanel to a notesPanel
                notesPanel.add(notePanel);
            }

        }
    }
    
    /**
     * A method to create a JTextArea and set its required attributes for the
     * note panel, make it read-only
     * @return JTextArea required for note panel
     */
    private JTextArea createJTextAreaForNotes() {
        //Create JTextArea and set attributes
        JTextArea noteArea = new JTextArea(1, 23);
        noteArea.setBorder(BorderFactory.createCompoundBorder(
                               BorderFactory.createLineBorder(Color.LIGHT_GRAY),
                                  BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        noteArea.setEditable(false);
        noteArea.setLineWrap(true);
        noteArea.setWrapStyleWord(true);
        noteArea.setOpaque(false);
        noteArea.setBackground(new Color(214, 217, 223));
        noteArea.setHighlighter(null);
        
        //return already created JTextArea with set attributes for note
        return noteArea;
    }
    
    
    /**
     * A method to initialise all the required things for the infoPanel,
     * which will hold main informationa about this activity and will be added
     * to the center of the main panel
     */
    private void initialiseActivityInformation(){
        
        //Create the panel where the information will be added
        infoPanel = new JPanel();
        infoPanel.setBorder(new EmptyBorder(10, 50, 10, 50));
        infoPanel.setLayout(new GridBagLayout());
        //Create GridBagConstraints for the info panel
        infoGBC = new GridBagConstraints();

        //Assign values to the grid bag constraints
        infoGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        infoGBC.fill = GridBagConstraints.HORIZONTAL;
        infoGBC.insets = new Insets(5, 10, 5, 10);
        
        //add all created components
        addActivityInformation();
    }
    
    
    /**
     * Method to assign the GridBagConstraints for each already created 
     * component, add them to one panel and to add that panel to the main panel
     * in the center
     */
    private void addActivityInformation(){
        //Assign grid bag constraints for the set date name label
        infoGBC.gridx = 0;
        infoGBC.gridy = 0;
        infoPanel.add(setDateNameLabel, infoGBC);
        //Assign grid bag constraints for the set date label
        infoGBC.gridx = 1;
        infoGBC.gridwidth = 3;
        infoPanel.add(setDateLabel, infoGBC);
        infoGBC.gridwidth = 1;
        
        //Assign grid bag constraints for the time taken name label
        infoGBC.gridy = 1;
        infoGBC.gridx = 0;
        infoPanel.add(timeTakenNameLabel, infoGBC);
        //Assign grid bag constraints for the time taken label
        infoGBC.gridx = 1;
        infoGBC.weightx = 1;
        infoPanel.add(timeTakenLabel, infoGBC);
        infoGBC.weightx = 0;
        
        //Assign grid bag constraints for the contribution name label
        infoGBC.gridy = 2;
        infoGBC.gridx = 0;
        infoPanel.add(contributionNameLabel, infoGBC);
        //Assign grid bag constraints for the contribution label
        infoGBC.gridx = 1;
        infoPanel.add(contributionLabel, infoGBC);
        
        //Assign grid bag constraints for note panel
        infoGBC.insets = new Insets(25, 0, 10, 0);
        infoGBC.gridy = 3;
        infoGBC.gridx = 0;
        infoGBC.gridwidth = 4;
        infoPanel.add(notesPanel, infoGBC);
        
        //Add the info panel with all the information to the main panel
        mainPanel.add(infoPanel, BorderLayout.CENTER);
    }
    
    
    
    

    
    
    /**
     * Method to create a panel with 3 buttons ("Update" button, 
     * "Delete" button, "Close" button) and to add this panel at the bottom
     * of the main panel
     */
    private void createButtons(){
        //Create a panel which will store all the buttons for this form
        JPanel buttonPanel = new JPanel();
        buttonPanel.setBorder(new EmptyBorder(30, 0, 30, 0));
        buttonPanel.setLayout(new GridBagLayout());
        //Create GridBagConstraints for this panel
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 10, 5, 10);
        gbc.gridy=0;
                
        //Create update button and add it
        updateButton = new JButton("Update");
        updateButton.addActionListener(updateAction);
        updateButton.setPreferredSize(new Dimension(110, 25));
        gbc.gridx=0;
        buttonPanel.add(updateButton, gbc);
        
        //Create delete button and add it
        deleteButton = new JButton("Delete");
        deleteButton.addActionListener(deleteAction);
        deleteButton.setPreferredSize(new Dimension(110, 25));
        gbc.gridx=1;
        buttonPanel.add(deleteButton, gbc);
        
        //Create close button and add it
        closeButton = new JButton("Close");
        closeButton.addActionListener(closeAction);
        closeButton.setPreferredSize(new Dimension(110, 25));
        gbc.gridx=2;
        buttonPanel.add(closeButton, gbc);
        
        //Add the button panel with buttons to the main panel
        mainPanel.add(buttonPanel, BorderLayout.PAGE_END);
    }
    
    
    
    
    
    
    /**
     * Action listener which will dispose the form
     * and open UpdateActivityForm on click. Used for "Update" button
     */
    private ActionListener updateAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            //Dispose this form
            dispose();
            //Open UpdateActivityForm
            new UpdateActivityForm(taskName, assignmentName, milestoneName, name);
        }
    };

    
    /**
     * Action listener which will ask if you really want to delete this
     * activity. If pressed no, nothing will happen. If pressed yes, method
     * will be called in ActivityController class to remove the activity, then 
     * dispose this form, pop up to confirm this activity has been removed
     * and will open a TaskInfoForm to show the updated activity
     */
    private ActionListener deleteAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            //dialog will pop up asking are you sure with yes/no buttons
            int reply = JOptionPane.showConfirmDialog(null,

                    "Are you sure you want to delete it?", "Delete Activity",
                    JOptionPane.YES_NO_OPTION);

            //if users reply matches YES
            if (reply == JOptionPane.YES_OPTION) {
                try {
                    //Call method in ActivityController to remove the activity
                    ActivityController.removeActivity(assignmentName, taskName, 
                                                                  activityName);
                    //dispose this form
                    dispose();
                    //Show message that activity has been deleted succseffully
                    JOptionPane.showMessageDialog(null, 
                                     "Activity has been successfully deleted!");
                    //open TaskInfoForm to show updates
                    new TaskInfoForm(assignmentName, taskName, milestoneName);
                } catch (EmptyInputException | ItemNotFoundException | 
                                     NullInputException | IllegalFileException ex) {
                    
                    //if something went wrong, show a message with an error
                    JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(Activity.class.getName()).log
                                                       (Level.SEVERE, null, ex);
                    
                }
            }
        }
    };

    
    
    /**
     * An action listener to close this form, which will dispose this form
     * and will open back a TaskInfoForm on click
     */
    private ActionListener closeAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            //Dispose this form
            dispose();
            //Open TaskInfoForm
            new TaskInfoForm(assignmentName, taskName, milestoneName);
        }
    };

    

}
            
