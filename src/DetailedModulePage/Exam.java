package DetailedModulePage;

//----------IMPORTS----------
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXTaskPane;
//---------------------------

public class Exam extends JXTaskPane {

    //-------------------CLASS VARIABLES-------------------
    private ArrayList<String> examInformation;
    
    private String name;
    private String weighting;
    private String date;
    private String duration;
    private String location;
    
    private TaskPanel taskPanel;
    private MilestonePanel milestonePanel;
    //-----------------------------------------------------

    /**
     * A constructor to create JXTaskPane with exam information
     *
     * @param examInformation   ArrayList containing information about exam
     */
    public Exam(ArrayList<String> examInformation) {

        this.examInformation = examInformation;
        this.setCollapsed(true);
        //Format string so it is spaced out evenly, because the exam
        //information is displayed on the title of the JXTaskPane and it can
        //have only String
        //String is formatted in a way so each exam information is below
        //corresponding label
        name = examInformation.get(0);
        this.setToolTipText(name);
        if(name.length()>25){
            name = name.substring(0, 25);
            name = name + "...";
        }
        weighting = examInformation.get(1);
        date = examInformation.get(2);
        duration = examInformation.get(3);
        location = examInformation.get(4);
        
        //Get the size of window
        Toolkit tk = Toolkit.getDefaultToolkit();
        int xSize = (int) tk.getScreenSize().width;
        
        String titleFormattedString = String.format("%-"+ xSize/60 +"s%-"+ xSize/60 +
                "s%-"+ xSize/60+"s%-"+ xSize/60+"s%s",
                name, weighting, date, duration, location);

        //To space out each string evenly, each character needs to have the width
        this.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
        //Set formatted String as a title of this JXTaskPane
        this.setTitle(titleFormattedString);

        
        //Create and add milestones to this exam JPanel
        addMilestones();
        //Create and add tasks to this exam JPanel
        addTasks();
        
        //Add buttons
        addButtons();
    }
    
    
    
    
    /**
     * A method to add buttons to the end of the form and to add action listeners
     * to the buttons
     */
    private void addButtons() {

        JPanel buttonJPanel = new JPanel();
        buttonJPanel.setLayout(new GridBagLayout());

        this.add(buttonJPanel);

        GridBagConstraints buttonPanelGBC = new GridBagConstraints();
        buttonPanelGBC.insets = new Insets(20, 5, 0, 5);

        JButton addMilestoneButton = new JButton("Add Milestone");
        addMilestoneButton.setPreferredSize(new Dimension(140, 25));
        buttonJPanel.add(addMilestoneButton, buttonPanelGBC);

        JButton addTaskButton = new JButton("Add Task");
        addTaskButton.setPreferredSize(new Dimension(140, 25));
        buttonJPanel.add(addTaskButton, buttonPanelGBC);

        addMilestoneButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new NewMilestoneForm(name);
            }
        });

        addTaskButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new NewTaskForm(name);
            }
        });

    }
    
    
    
    
    /**
     * A method to extract milestone information from exam ArrayList,
     * to create milestone JPanel with the extracted information and to add it
     * to exam JPanel
     */
    private void addMilestones(){

        //Create new ArrayList which will be populated with milestone information
        ArrayList<String> listOfMilestones = new ArrayList<>();
        //Get the position where milestone starts
        int milestonePlace = Integer.parseInt(examInformation.get(5)) * 4 + 6;
        //When the position is received, get how many milestones
        String numberOfMilestones = examInformation.get(milestonePlace);
        //Add how many milestones will there be
        listOfMilestones.add(numberOfMilestones);
        int countList = milestonePlace+1;
        
        //Extract milestones information from the ArrayList
        for(int i=0;i<Integer.parseInt(numberOfMilestones);i++){
            
            //Get milestone name and due date
            for(int j=0;j<2;j++){
                listOfMilestones.add(examInformation.get(countList));
                countList++;
            }
            
            //Get the number of how many tasks in this milestone
            String numberOfTasks = examInformation.get(countList);
            listOfMilestones.add(numberOfTasks);
            countList++;

            //Get tasks
            for(int j=0;j<Integer.parseInt(numberOfTasks);j++){
                
                //Get task name, due date, type and goal
                for(int k=0;k<4;k++){
                    listOfMilestones.add(examInformation.get(countList));
                    countList++;
                }
            }
        }
        
        
        //Create new Milestone JPanel with the extracted information
        milestonePanel =
                new MilestonePanel(listOfMilestones, name);
        //Add the received milestone JPanel to this exam JPanel
        this.add(milestonePanel);
        
    }
    
    
    /**
     * A method to extract task information from exam ArrayList,
     * to create task JPanel with the extracted information and to add it
     * to exam JPanel
     */
    private void addTasks(){
        
        //Create ArrayList to store task information
        ArrayList<String> listOfTasks = new ArrayList<>();
        //Get the number of tasks
        String numberOfTasks = examInformation.get(5);
        listOfTasks.add(numberOfTasks);
        //Task information starts from position 6
        int countList = 6;
        for(int i=0;i<Integer.parseInt(numberOfTasks);i++){
            //Get task name, due date, type, goal
            for(int j=0;j<4;j++){
                listOfTasks.add(examInformation.get(countList));
                countList++;
            }
        }
        
        //Create task JPanel
        taskPanel = new TaskPanel(listOfTasks, name, null);
        
        //Add created task JPanel to this exam JPanel
        this.add(taskPanel);
    }
    
    
    
    
    /**
     * An accessor method to return the name of this exam
     * @return Name of this exam
     */
    public String getExamName(){
        return name;
    }
    
    
    
    /**
     * An accessor method to return weighting of this exam
     * @return weighting of this exam
     */
    public String getExamWeighting(){
        return weighting;
    }
    
    
    
    /**
     * An accessor method to return the date of this exam
     * @return Date of this exam
     */
    public String getExamDate(){
        return date;
    }
    
    
    
    /**
     * An accessor method to return the duration of this exam
     * @return Duration of this exam
     */
    public String getExamDuration(){
        return duration;
    }
    
    
    
    /**
     * An accessor method to return the location of this exam
     * @return Location of this exam
     */
    public String getExamLocation(){
        return location;
    }
    
    
    
    
    /**
     * An accessor method to return the taskPanel of this exam
     * @return task panel in this coursework
     */
    public TaskPanel getTaskPanel() {
        return taskPanel;
    }

    
    /**
     * An accessor method to return the milestone panel
     * @return milestone panel this coursework has
     */
    public MilestonePanel getMilestonePanel() {
        return milestonePanel;
    }
    
}
