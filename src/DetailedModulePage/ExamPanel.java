package DetailedModulePage;

//----------IMPORTS----------
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;


public class ExamPanel extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    private ArrayList<String> listOfExams;
    private ArrayList<Exam> listOfExamJXTaskPane;
    private GridBagConstraints examPanelGBC;
    //-----------------------------------------------------

    /**
     * A constructor to create a JPanel containing exams for this module
     *
     * @param listOfExams An ArrayList containing information about the exams
     */
    public ExamPanel(ArrayList<String> listOfExams) {

        this.listOfExams = listOfExams;

        //Set Layout for this JPanel
        this.setLayout(new GridBagLayout());

        //Create ArrayList and populate it with exams by extracting
        //each exam information from the ArrayList of courseworks
        listOfExamJXTaskPane = new ArrayList<>();
        createExams();

        //Add the information labels to this JPanel
        addLabels();
        //Add created exams to this JPanel
        addCourseworks();

    }

    /**
     * A method to add information labels to this JPanel
     */
    private void addLabels() {

        //Create constraints for information Labels for this JPanel
        examPanelGBC = new GridBagConstraints();
        examPanelGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        examPanelGBC.weighty = 0;
        examPanelGBC.weightx = 1;
        examPanelGBC.gridy = 0;
        examPanelGBC.gridwidth = 1;
        examPanelGBC.insets = new Insets(0, 0, 10, 0);

        //Add information Labels to this JPanel
        examPanelGBC.gridx = 0;
        this.add(new JLabel("Exams"), examPanelGBC);
        examPanelGBC.gridx = 1;
        this.add(new JLabel("Weighting"), examPanelGBC);
        examPanelGBC.gridx = 2;
        this.add(new JLabel("Start Time"), examPanelGBC);
        examPanelGBC.gridx = 3;
        this.add(new JLabel("Duration"), examPanelGBC);
        examPanelGBC.gridx = 4;
        this.add(new JLabel("Location"), examPanelGBC);

    }

    /**
     * A method to add already created courseworks to this panel
     */
    private void addCourseworks() {

        //Create constraints to add exams to this JPanel
        examPanelGBC.fill = GridBagConstraints.HORIZONTAL;
        examPanelGBC.gridx = 0;
        examPanelGBC.gridwidth = 5;
        examPanelGBC.insets = new Insets(0, 0, 5, 0);

        //Add exams to this JPanel
        for (int i = 0; i < listOfExamJXTaskPane.size(); i++) {
            examPanelGBC.gridy = i + 1;
            this.add(listOfExamJXTaskPane.get(i), examPanelGBC);
        }

    }

    /**
     * A method to extract exams information and to create them
     */
    private void createExams() {
        //Create courseworks and add them to ArrayList
        int countList = 0;
        while (countList < listOfExams.size()) {
            ArrayList<String> examInformation = new ArrayList<>();
            //Get name, set date, due date, weighting and location
            for (int i = 0; i < 5; i++) {
                examInformation.add(listOfExams.get(countList));
                countList++;
            }
            //Get how many tasks without milestones
            String numberOfTasksWithoutMilestones
                    = listOfExams.get(countList);
            examInformation.add(numberOfTasksWithoutMilestones);
            countList++;

            //Get tasks without milestones
            for (int i = 0; i < 
                        Integer.parseInt(numberOfTasksWithoutMilestones); i++) {
                //Get tasks name, due date, type and goal
                for (int j = 0; j < 4; j++) {
                    examInformation.add(listOfExams.get(countList));
                    countList++;
                }
            }
            //Get how many milestones
            String numberOfMilestones = listOfExams.get(countList);
            examInformation.add(numberOfMilestones);
            countList++;

            //Get milestones
            for (int i = 0; i < Integer.parseInt(numberOfMilestones); i++) {
                //Get milestones name and due date
                for (int j = 0; j < 2; j++) {
                    examInformation.add(listOfExams.get(countList));
                    countList++;
                }

                //Get how many tasks with this milestone
                String numberOfTasksForMilestone
                        = listOfExams.get(countList);
                examInformation.add(numberOfTasksForMilestone);
                countList++;
                //Get tasks
                for (int j = 0; j < 
                            Integer.parseInt(numberOfTasksForMilestone); j++) {
                    //Get tasks name, due date, type and goal
                    for (int n = 0; n < 4; n++) {
                        examInformation.add(listOfExams.get(countList));
                        countList++;
                    }
                }
            }
            //Create an exam and add it to the list
            listOfExamJXTaskPane.add(new Exam(examInformation));
        }
    }
    
    
    
    /**
     * An accessor method to get the JXTaskPanes which are milestones
     * @return JXTaskPanes this coursework has
     */
    public ArrayList<Exam> getJXTaskPanes(){
        return listOfExamJXTaskPane;
    }
    
    
    
    
}


