package DetailedModulePage;

//----------IMPORTS----------
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import org.jdesktop.swingx.JXDatePicker;
import studyplanner.controller.MilestoneController;
import utilities.exceptions.*;
//---------------------------


public class UpdateMilestoneForm extends JDialog {

    //-------------------CLASS VARIABLES-------------------
    private JTextField milestoneNameText;
    private JXDatePicker milestoneDateText;
    
    private JButton updateButton;
    private JButton cancelButton;
    
    private String assignmentName;
    private String milestoneName;
    
    private GridBagConstraints gbc;
    private JPanel formHoldPanel;
    private JPanel formPanel;
    //-----------------------------------------------------
            
    /**
     * A method to create a form to update a milestone
     * 
     * @param assignmentName    The name of the assignment this milestone is in
     * @param milestoneName     The name of this milestone
     */
    public UpdateMilestoneForm(String assignmentName, String milestoneName) {
        this.assignmentName = assignmentName;
        this.milestoneName = milestoneName;
        
        initialise();
    }
    
    /**
     * A method to initialise this JDialog, set the title of this form 
     * and call the methods to populate this JDialog
     */
    private void initialise() {
        
        //Set the parameters for this JDialog
        this.setLayout(new BorderLayout());
        this.setTitle("Milestone update");
        
        //Create and add the name label at the top of this form
        createName();
        
        //Create a panel which will store a formPanel
        formHoldPanel = new JPanel();
        formHoldPanel.setBorder(new EmptyBorder(20, 65, 20, 65));
        formHoldPanel.setLayout(new GridLayout(0, 1));
        //Panel which will hold all the information about this milestone
        formPanel = new JPanel();
        formPanel.setLayout(new GridBagLayout());
        //Create and add the main information about the milestone
        createComponents();
        
        //Create and add the buttons at the bottom of this form
        createButtons();

        //To make this form visible
        this.pack();
        this.setModal(true);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
    
    
    /**
     * A method to create a label which will show the type of the form 
     * and to adjust its size, so it would be bigger, and add it to the panel, 
     * which will be placed at the top of the main panel
     */
    private void createName(){
        
        //Create the panel where the name label will be stored
        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new GridBagLayout());
        
        //Create the label for the name
        JLabel mainLabel = new JLabel("Update a Milestone");
        float size = 15;
        mainLabel.setFont(mainLabel.getFont().deriveFont(size));
        //Set the constraints for this label
        gbc = new GridBagConstraints();
        gbc.insets = new Insets(15, 0, 5, 0);
        
        //Add the label to its panel and add panel to the main panel top
        labelPanel.add(mainLabel, gbc);
        this.add(labelPanel, BorderLayout.PAGE_START);
    }
    
    
    /**
     * A method to create all the required components which will be editable
     * information about this milestone and will be added to the formPanel,
     * which will be added to the center of this form
     */
    private void createComponents(){
        //Create constraints
        gbc = new GridBagConstraints();
        
        //Create name label
        JLabel milestoneNameLabel = new JLabel("Milestone name:      ");
        //Create milestone name text field
        milestoneNameText = new JTextField();
        milestoneNameText.setDocument(new JTextFieldLimit(25));
        milestoneNameText.setBorder(new LineBorder(Color.GRAY));
        //Set the text
        milestoneNameText.setText(milestoneName);
        
        //Set constraints for milestone name label and add
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridy = 0;
        gbc.gridx = 0;
        formPanel.add(milestoneNameLabel, gbc);
        //Set constraints for milestone name text field and add
        gbc.gridx = 1;
        formPanel.add(milestoneNameText, gbc);

        //Create milestone due date label
        JLabel milestoneDateLabel = new JLabel("Milestone due date:");
        //Create milestone due date picker
        milestoneDateText = new JXDatePicker();
        //Get the due date of the milestone and set it
        try {

            milestoneDateText.setDate(MilestoneController.
                                    getDeadline(assignmentName, milestoneName));
        } catch (ItemNotFoundException | NullInputException | 
                           EmptyInputException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), 
                                            "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(UpdateMilestoneForm.class.getName()).
                                                    log(Level.SEVERE, null, ex);

        }
        milestoneDateText.getEditor().setEditable(false);
        milestoneDateText.setPreferredSize(new Dimension(180, 
                                  milestoneDateText.getPreferredSize().height));
        milestoneNameText.setPreferredSize(new Dimension(180, 
                                  milestoneDateText.getPreferredSize().height));
        
        //Set the constraints for the milestone date label and add
        gbc.gridy = 1;
        gbc.gridx = 0;
        formPanel.add(milestoneDateLabel, gbc);
        //Set the constraints for the milestone date picker and add
        gbc.gridx = 1;
        formPanel.add(milestoneDateText, gbc);

        //Add form to its hold panel and add it to the main form
        formHoldPanel.add(formPanel);
        add(formHoldPanel, BorderLayout.CENTER);
    }
    
    /**
     * A method to create "Update" and "Cancel" buttons, add action listeners
     * to them and add the at the bottom of the main panel
     */
    private void createButtons(){
        //Create a panel which will store all the buttons for this form
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        //Create GridBagConstraints for this panel
        gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 10, 15, 10);
        
        //Create update button and add it
        updateButton = new JButton("Update");
        updateButton.addActionListener(updateAction);
        updateButton.setPreferredSize(new Dimension(110, 25));
        buttonPanel.add(updateButton, gbc);

        //Create cancel button and add it
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(cancelAction);
        cancelButton.setPreferredSize(new Dimension(110, 25));
        buttonPanel.add(cancelButton, gbc);
        
        //Add the button panel with buttons to the main panel
        this.add(buttonPanel, BorderLayout.PAGE_END);
    }
    
    /**
     * Action listener which calls a method in a controller to update a 
     * milestone and disposes this JDialog
     */
    private ActionListener updateAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {

            try {
                MilestoneController.updateMilestone(assignmentName,
                        milestoneName, milestoneNameText.getText(),
                        milestoneDateText.getDate());
                dispose();
            } catch (ItemNotFoundException | NullInputException |
                    DuplicateDataException | EmptyInputException |
                    IllegalDateException | IllegalFileException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(),
                        "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(UpdateMilestoneForm.class.getName()).
                        log(Level.SEVERE, null, ex);
            }

        }
    };

    /**
     * An action listener to cancel this form, which will dispose this form
     */
    private ActionListener cancelAction = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            dispose();
        }
    };

}
