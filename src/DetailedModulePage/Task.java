package DetailedModulePage;

//----------IMPORTS----------
import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.ArrayList;
import javax.swing.JLabel;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

public class Task extends JPanel {

    //-------------------CLASS VARIABLES-------------------
    private ArrayList<String> taskInformation;
    
    private String name;
    private String dueDate;
    private String type;
    private String goal;
    
    private JProgressBar progressBar;
    
    private String assignmentName;
    private String milestoneName=null;
    
    private JLabel nameLabel;
    private JLabel dueDateLabel;
    //-----------------------------------------------------
    
    
    /**
     * A constructor which takes another Task as a parameter, to make a copy
     * of another Task.
     * @param taskToCopy Task to copy
     */
    public Task(Task taskToCopy) {
        this.assignmentName=taskToCopy.getTaskAssignmentName();
        this.name = taskToCopy.getTaskName();
        this.dueDate = taskToCopy.getTaskDueDate();
        this.goal = taskToCopy.getTaskGoal();
        
        //Initialises the whole form
        initialise();
    }
    
    
    
    
    /**
     * A constructor to create JPanel with task information
     *
     * @param taskInformation   ArrayList containing information about task
     * @param assignmentName    The name of the assignment this task is in
     * @param milestoneNames    The name of the milestone this task is in
     *                          ("null" if none"
     */
    public Task(ArrayList<String> taskInformation, String assignmentName, String milestoneNames) {
        this.milestoneName=milestoneNames;
        this.assignmentName = assignmentName;
        
        this.taskInformation = taskInformation;
        this.name = taskInformation.get(0);
        this.dueDate = taskInformation.get(1);
        this.goal = taskInformation.get(3);

        //Initialises the whole form
        initialise();
    }
    
    
    /**
     * A method to initialise the whole form and to call appropriate methods
     * to create this Task
     */
    private void initialise(){
        //Set layout for this JPanel
        this.setLayout(new GridLayout(0,3));
        //Create and add constraints for task informaiton Labels
        GridBagConstraints taskJPanelGBC = new GridBagConstraints();
        taskJPanelGBC.anchor = GridBagConstraints.FIRST_LINE_START;
        taskJPanelGBC.weighty = 0;
        taskJPanelGBC.weightx = 1;
        taskJPanelGBC.gridy = 0;
        taskJPanelGBC.gridwidth = 1;
        taskJPanelGBC.insets = new Insets(0, 8, 10, 0);

        //Add each information JLabel to this JPanel
        taskJPanelGBC.gridx=0;
        nameLabel = new JLabel(name);
        this.add(nameLabel, taskJPanelGBC);
        
        taskJPanelGBC.gridx=1;
        dueDateLabel = new JLabel(dueDate);
        this.add(dueDateLabel, taskJPanelGBC);
        
        //Add progress bar to this JPanel
        taskJPanelGBC.gridx=2;
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(Integer.parseInt(goal));
        progressBar.setStringPainted(true);
        this.add(progressBar, taskJPanelGBC);
        
        //Set empty border, so on hover, it wont change its size
        this.setBorder(new EmptyBorder(2,2,2,2));
        
        //Add mouse listeners to this JPanel
        createMouseListeners();
    }
    
    /**
     * Method to add mouse listeners to this JPanel, when the JPanel is hovered
     * it will change its border and its cursor, when it exits it goes back to
     * how it was. When the JPanel is clicked it will open TaskInfoForm
     */
    private void createMouseListeners() {
        this.addMouseListener(new MouseAdapter() {
            //On mouse click
            @Override
            public void mousePressed(MouseEvent e) {
                new TaskInfoForm(assignmentName, name, milestoneName);
            }
            //When mouse enters the JPanel, set the border
            @Override
            public void mouseEntered(MouseEvent e) {
                setBorder();
                revalidate();
                repaint();
            }
            //When mouse exits the JPanel, remoev the border
            @Override
            public void mouseExited(MouseEvent e) {
                removeBorder();
                revalidate();
                repaint();
            }
        });
    }
    
    
    /**
     * Method to set the border with color and to set the cursor
     * to show that this JPanel is clickable
     */
    private void setBorder() {
        //On mouse enter the the border to be colored
        this.setBorder(new MatteBorder(2, 2, 2, 2, Color.LIGHT_GRAY));
        //Set the cursos, to show that its clickable
        this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }
    
    /**
     * Method to set back empty border to this JPanel 
     * and change the cursor to default
     */
    private void removeBorder() {
        //On mouse exit, remove the color from the border
        this.setBorder(new EmptyBorder(2, 2, 2, 2));
        //Set the default cursor
        this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
    
    
    /**
     * Setter method, to set the milestone of this task
     * @param milestoneName   milestone name to be set for this task
     */
    public void setMilestone(String milestoneName){
        this.milestoneName=milestoneName;
    }
    
    
    /**
     * An accessor method to return the name of this task
     * @return Name of this task
     */
    public String getTaskName(){
        return name;
    }
    
    
    
    /**
     * An accessor method to return the due date of this task
     * @return Due date of this task
     */
    public String getTaskDueDate(){
        return dueDate;
    }
    
    
    /**
     * An accessor method to return the type of this task
     * @return type of this task
     */
    public String getTaskType(){
        return type;
    }
    
    
    
    /**
     * An accessor method to return the goal of this task
     * @return goal of this task
     */
    public String getTaskGoal(){
        return goal;
    }
    
    
    
    
    /**
     * An accessor method to return ArrayList with information of this task
     * @return ArrayList with information of this task
     */
    public ArrayList<String> getTaskInformation(){
        return taskInformation;
    }
    
    
    /**
     * Setter method to set the name of the task
     * @param name name to be set for this task
     */
    public void setTaskName(String name){
        this.name=name;
    }
    
    /**
     * setter method to set the due date of this task
     * @param dueDate due date to be set for this task
     */
    public void setTaskDueDate(String dueDate){
        this.dueDate=dueDate;
    }
    
    /**
     * Accessor method to get the name of the assignment this task is in
     * @return the name of the assignment
     */
    public String getTaskAssignmentName(){
        return this.assignmentName;
    }
    
    
    /**
     * A method to update the task
     * @param newName   new name for this task
     * @param newDate   new due date for this task
     */
    public void updateTask(String newName, String newDate){
        this.name = newName;
        this.dueDate = newDate;
        JLabel nameL = (JLabel) this.getComponent(0);
        nameL.setText(newName);
        JLabel dueDateL = (JLabel) this.getComponent(1);
        dueDateL.setText(newDate);
        this.revalidate();
        this.repaint();
    }
    
    
    /**
     * A method to update the task bar
     * @param progressValue value to which the progres bar will be set
     */
    public void updateTaskBar(int progressValue){
        this.progressBar.setValue(progressValue);
        this.progressBar.revalidate();
        this.progressBar.repaint();
    }
    
    
    /**
     * Accessor method to get the progress of the progress bar
     * @return progress of the task
     */
    public int getProgress(){
        return progressBar.getValue();
    }
    
}
