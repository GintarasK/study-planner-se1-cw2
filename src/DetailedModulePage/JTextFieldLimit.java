/*
    Taken from http://stackoverflow.com/questions/3519151/how-to-limit-the-number-of-characters-in-jtextfield
*/
package DetailedModulePage;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class JTextFieldLimit extends PlainDocument {
  private int limit;

  /**
   * to set the limit of the text field
   * @param limit set the limit of JTextField
   */
  JTextFieldLimit(int limit) {
   super();
   this.limit = limit;
   }

  
  @Override
  public void insertString( int offset, String  str, AttributeSet attr ) throws BadLocationException {
    if (str == null) return;

    if ((getLength() + str.length()) <= limit) {
      super.insertString(offset, str, attr);
    }
  }
}