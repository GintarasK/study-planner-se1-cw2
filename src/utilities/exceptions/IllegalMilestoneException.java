package utilities.exceptions;

/**
 * An exception class to simulate exceptions when user tries to remove 
 * a Milestone object which contains uncompleted Tasks.
 */
public class IllegalMilestoneException extends Exception{
    public IllegalMilestoneException(){
        super("The milestone that you are"
                    + " trying to delete contains tasks. Please, remove all"
                    + " the tasks first. If error occurs again, please,"
                    + " contact the system administrator.");
    } 
}
