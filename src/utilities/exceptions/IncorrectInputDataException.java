package utilities.exceptions;

/**
 * An exception class, to simulate exceptions for illegal arguments.
 * Used by classes where user input is required. This type of exception
 * is thrown if the user input is in incorrect format or provided data
 * is erroneous.
 */

public class IncorrectInputDataException extends Exception {
    public IncorrectInputDataException(String message) {
        super(message);
    }       
}
