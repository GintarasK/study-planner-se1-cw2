package utilities.exceptions;

/**
 * An exception class, to simulate exceptions for illegal module codes.
 * Used by Module class, throws exceptions if code pattern doesn't match
 * regex pattern " [A-Z]{3}\-\d{4}[ABY]".
 */
public class IllegalCodeException extends Exception {

    public IllegalCodeException() {
        super("Incorrect module code was"
                + " detected. Please reupload semester data file."
                + " If error occurs again, please check your semester"
                + " data file or contact system administrator.");
    }      
    
}
