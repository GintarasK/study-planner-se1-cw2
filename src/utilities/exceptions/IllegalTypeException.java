package utilities.exceptions;

/**
 * An exception class, to simulate exceptions for illegal types.
 * Used by classes, where type error checking is used. This type of
 * exception is thrown if type is incorrect format or does not 
 * go past error checking.
 */
public class IllegalTypeException extends Exception {
    public IllegalTypeException(String message) {
        super(message);
    }       
}
