package utilities.exceptions;

/**
 * An exception class, to simulate exceptions when change/deletion of a task
 * is not possible due to conflicts with its dependency.
 */
public class IllegalDependencyException extends Exception{
    public IllegalDependencyException(String message) {
        super(message);            
    }    

}
