package utilities.exceptions;

/**
 * An exception class, to simulate exceptions for illegal lists of Module
 * Assignments, when overall Assignments weighting value is not equal to 100.
 */
public class IllegalAssignmentsWeightingException extends Exception {
    public IllegalAssignmentsWeightingException() {
        super("Total "
                + "assignments value for one of the modules "
                + "is not 100. Please check your data file "
                + "and if the problem persists again, please"
                + " contact your HUB/system admistrator.");
    }    
}
