package utilities.exceptions;

/**
 * An exception class to simulate exceptions when provided values (either 
 * user input or JSON file) do not go past error checks.
 */

public class IllegalValueException extends Exception {
    public IllegalValueException() {
        super("A field that was supposed to be"
                + " filled with a number did not get the right value."
                + " Please, check your input (data file/form)"
                + " and resubmit the form. If the error occurs again,"
                + " contact your HUB/system administrator.");
    }
}
