package utilities.exceptions;

/**
 * An exception class, to simulate exceptions, where user tries to 
 * attempt restricted data manipulation actions.
 * Used by Activity class, where conflicts occur with Task references
 * manipulation, when user tries to delete last entry of associated Task
 * references, since Activity should belong at least to one Task.
 */
public class IllegalDataManipulationException extends Exception {

    public IllegalDataManipulationException() {
        super("Cannot remove the task from the"
                + " reference list, since it is the last task that is"
                + " associated with this activity.");
    }       
    
}
