package utilities.exceptions;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * An exception class to simulate exception for Tasks/Activities/Milestones
 * start dates input, to ensure that they are no earlier than current machine
 * date - 1 day.
 */
public class CurrentDateException extends Exception{
    public CurrentDateException(){
        super("Given start date is earlier than: " 
                + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) +
                ". Please correct your input and try again.");
    }
}
