package utilities.exceptions;

/**
 * An exception class, to simulate exceptions when when logs files 
 * are to be read or exported by the program.
 */
public class IllegalFileException extends Exception{
    public IllegalFileException() {
        super("The program failed to read/write the"
                + " file. Please, check the path to the file and its"
                + " format. If error"
                + " occurs again, please check your semester data file"
                + " or contact HUB/system administrator.");
    } 
}
