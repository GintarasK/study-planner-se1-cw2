/*
 * An exception class to simulate messages, when null value error checking is
 * done.
 */
package utilities.exceptions;

public class NullInputException extends Exception{
    public NullInputException() {
        super("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the"
                + " error persists, please contact your HUB/system "
                + "administrator.");
    }  
}
