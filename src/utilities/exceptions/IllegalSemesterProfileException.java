package utilities.exceptions;

/**
 * An exception class to simulate exceptions for SemesterProfile controller
 * when user tries to get headers of SemesterProfile objects, but none are 
 * initialised.
 */
public class IllegalSemesterProfileException extends Exception{
    public IllegalSemesterProfileException(){
        super("The list of Semester"
                + " profiles is empty. Please, add a Semester profile first."
                + " If error occurs again, please, contact the system"
                + " administrator.");
    } 
}
