package utilities.exceptions;

/**
 * An exception class, to simulate exceptions for illegal dates.
 * Used by classes, when dates are provided by user input or semester
 * data file. This type of exception is thrown if date is in incorrect format
 * and/or does not pass error checking.
 */
public class IllegalDateException extends Exception {
    public IllegalDateException() {
        super("Invalid start and/or end"
                + " date format detected or dates are clashing(start date"
                + " is after end date/end date is before start date).");
    }    
}
