package utilities.exceptions;

/**
 * An exception class, to simulate empty input exceptions. Used by classes
 * when user submits a form, where a field cannot be empty.
 */
public class EmptyInputException extends Exception {
    public EmptyInputException() {
        super("Empty or incorrect field was"
                + " provided. Please, check your input (data file/form)"
                + " and try again. If the error persists, please"
                + " contact your HUB/system administrator.");
    }    
}
