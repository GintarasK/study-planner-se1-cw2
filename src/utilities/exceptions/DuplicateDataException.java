/*
 * An exception class to emalute exceptions, when given data (object) is 
 * already present in a given list or collection of objects.
 */
package utilities.exceptions;

public class DuplicateDataException extends Exception{
    public DuplicateDataException(String message) {
        super(message);
    }     
}
