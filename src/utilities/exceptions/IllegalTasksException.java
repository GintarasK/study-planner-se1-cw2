package utilities.exceptions;

/**
 * An exception class, to simulate exceptions for illegal tasks.
 * Used by classes when empty lists of tasks are provided
 * in the constructor or setter methods.
 */

public class IllegalTasksException extends Exception {
    public IllegalTasksException() {
        super("Please, make sure"
                + " that the deadline of the task that"
                + " you are trying to create/update"
                + " is after"
                + " the deadline of the task that you"
                + " are trying to set dependency on.\n"
                + " If error occurs again, please,"
                + " contact the system administrator.");
    }    
}
