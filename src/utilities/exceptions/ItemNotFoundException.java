package utilities.exceptions;

/**
 * An exception class, to simulate exceptions when searches are commenced,
 * but required objects are not found.
 */
public class ItemNotFoundException extends Exception {
    public ItemNotFoundException(String message) {
        super(message + " - specified item could be found in the system."
                + " Please check "
                + "your input and try again.");            
    }     
}
