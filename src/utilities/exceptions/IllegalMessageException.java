package utilities.exceptions;

/**
 * An exception class to simulate exceptions when the program fails to send 
 * a bug report to the administrators.
 */
public class IllegalMessageException extends Exception{
    public IllegalMessageException(){
        super("The system failed to send the"
                + " message. Please, check the Internet connection and try"
                + " again. If error occurs again, please, contact"
                + " system administrator.");
    } 
}
