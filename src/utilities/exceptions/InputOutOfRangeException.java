package utilities.exceptions;

/**
 * An exception class, to simulate exceptions when provided integer input is
 * not within specified range.
 */

public class InputOutOfRangeException extends Exception {
    public InputOutOfRangeException(String message) {
        super(message);
    }     
}
