package utilities.ErrorChecking;

import utilities.exceptions.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import studyplanner.model.*;

/**
 * A class to check input data for model classes.
 */
public class ErrorChecking {
    
    /**
     * A method to check whether or not a string is a number. 
     * 
     * @param string                    string to check
     * 
     * @return                          true if the string is a number,
     *                                  false otherwise
     * 
     * @throws IllegalValueException    if the string is not a number.
     */
    public static boolean isInteger(String string) 
            throws IllegalValueException {
        try {
            Integer.parseInt(string);
        } catch (NumberFormatException | NullPointerException e) {
            throw new IllegalValueException();
        }
        return true;
    }
    
    /**
     * A error checking method, that ensures that any string input from 
     * a data file or any form is in correct format (not a null object or 
     * an empty string).
     * 
     * @param input                 string input to be checked.
     * 
     * @throws NullInputException   an exception to be thrown if 
     *                              provided input is a null object.
     * 
     * @throws EmptyInputException  an exception to be thrown if provided
     *                              input is an empty string.
     */
    public static void checkStringInput(String input) 
                               throws NullInputException, EmptyInputException{        
        checkIfNull(input);
        if(input.isEmpty() == true)
            throw new EmptyInputException();
    }
    
    /**
     * A error checking method that is used to check if a given Milestone or 
     * Task (both are searched by their names) already exist within their 
     * respective lists of Tasks/Milestones that are associated with this 
     * Assignment.
     * 
     * @param assignment                    Assignment to be searched in
     * @param name                          name of Task/Milestone
     * 
     * @throws NullInputException           an exception to be thrown if 
     *                                      any of given parameters are null
     * 
     * @throws EmptyInputException          an exception to be thrown if name
     *                                      input is an empty string
     * 
     * @throws DuplicateDataException   `   an exception to be thrown if 
     *                                      given Task/Milestone already exist
     */
    public static void checkForDuplicateName(Assignment assignment,
            String name) throws NullInputException, EmptyInputException,
            DuplicateDataException{
        
        checkIfNull(assignment);
        checkStringInput(name);
        
        for(Milestone m : assignment.getMilestoneList()){
            if(m.getName().equalsIgnoreCase(name)){
                throw new DuplicateDataException("An item with the provided"
                        + " name already exists in this assignment.");
            }
        }
        
        for(Task t : assignment.getTaskList()){
            if(t.getName().equalsIgnoreCase(name)){
                throw new DuplicateDataException("An item with the provided"
                        + " name already exists in the assignment.");
            }
        }
    } 
        
    /**
     * A error checking method, that ensures that any provided object input
     * is not a null object.
     * 
     * @param o                     object to be checked.
     * 
     * @throws NullInputException   an exception to be thrown if provided 
     *                              object is null.
     */
    public static void checkIfNull(Object o) 
            throws NullInputException{
        if(o == null)
            throw new NullInputException();
    }
    
    /**
     * A error checking method that checks if given sum of all Assignments 
     * weightings within a Module is equal to a 100.
     * 
     * @param totalWeighting sum of all Assignments weightings.
     * 
     * @throws IllegalAssignmentsWeightingException an exception to be thrown
     * if the overall sum of all Assignments weightings is not equal to a 100.
     */
    public static void checkModuleAssignmentsWeighting(int totalWeighting)
            throws IllegalAssignmentsWeightingException{
        if(totalWeighting != 100)
            throw new IllegalAssignmentsWeightingException();
    }
    /**
     * A error checking method that ensures that given list input is not a
     * null object at has at least one entry.
     * 
     * @param givenList     ArrayList to be checked.
     * 
     * @throws EmptyInputException an exception to be thrown is provided list
     *                             has no entries.
     * 
     * @throws NullInputException  an exception to be thrown if
     *                             provided list has no entries or
     *                             is a null.
     */
    public static void checkListInput(ArrayList<?> givenList) 
            throws EmptyInputException, NullInputException{            
        checkIfNull(givenList);
        if(givenList.isEmpty() == true)
            throw new EmptyInputException();
    }
    
    /**
     * A error checking method that ensures that given integer input is given
     * given bounds.
     * 
     * @param input         input to be checked.
     * @param lowerBound    lower boundary of given range.
     * @param upperBound    upper boundary of given range.
     * 
     * @throws InputOutOfRangeException  an exception to be thrown if
     *                                      given input is not within given
     *                                      given boundaries.
     */
    public static void checkIntInput(int input, int lowerBound, int upperBound) 
                                     throws InputOutOfRangeException{
        if(input < lowerBound || input > upperBound)
            throw new InputOutOfRangeException("Incorrect ammount was"
                    + " detected. Please check your input (should be between"
                    + " " + lowerBound + " and " + upperBound + ") and"
                    + " resubmit the form. If the error persists, please"
                    + " contact your HUB/system administrator.");  
    }

    /**
     * A error checking method that checks that the given date is in correct
     * format and is within acceptable range (date should be in range between:
     * the year of current academic year -1 and the year of current academic 
     * year +1.
     * 
     * @param date  Date object to be checked.
     * 
     * @throws IllegalDateException         an exception to be thrown if given
     *                                      is in wrong format or not within
     *                                      acceptable range.
     * 
     * @throws NullInputException           an exception to be thrown if null
     *                                      is passed as a parameter.
     */
    public static void checkDate(Date date) throws IllegalDateException,
                                                   NullInputException{
        ErrorChecking.checkIfNull(date);
        //Date format to test if the assigment date year, matches
        //the one of the local machine
        DateFormat yearTest = new SimpleDateFormat("yyyy");
        
        Date localDate = new Date();     //Initialises local date
        
        if(
          (Integer.parseInt(yearTest.format(date)) >
                (Integer.parseInt(yearTest.format(localDate)) + 1)) ||

          (Integer.parseInt(yearTest.format(date)) <
                Integer.parseInt(yearTest.format(localDate)) - 1)  
          )
            throw new IllegalDateException();
    }
    
    /**
     * A error checking method that ensures that given dates are not clashing
     * with each other. The first date should not be earlier than the second 
     * one and both of the dates have to be in acceptable format (should not 
     * be earlier or later more than 1 year in comparison to the current 
     * academic year (denoted by local machine time).
     * 
     * @param firstDate     date that should be chronologically earlier.
     * 
     * @param lastDate      date that should be chronologically later.
     * 
     * @throws IllegalDateException an exception to be thrown if any of the
     *                              given dates are in wrong format or not
     *                              within acceptable range.
     * 
     * @throws NullInputException   an exception to be thrown if any of the 
     *                              required parameters are passed as a null.
     */
    public static void checkMultiDates(Date firstDate, Date lastDate) 
            throws IllegalDateException, NullInputException{
        ErrorChecking.checkDate(firstDate);
        ErrorChecking.checkDate(lastDate);
        if(firstDate.after(lastDate))
            throw new IllegalDateException();
    }
    
    /*
     * A error checking method that ensures that given dates for Assignment 
     * are not clashing with each other. The first date should not be earlier
     * than the second one.
     * 
     * @param firstDate     date that should be chronologically earlier.
     * 
     * @param lastDate      date that should be chronologically later.
     * 
     * @throws IllegalDateException an exception to be thrown if any of the
     *                              given dates are in wrong format or not
     *                              within acceptable range.
     * 
     * @throws NullInputException   an exception to be thrown if any of the 
     *                              required parameters are passed as a null.
     */
    public static void checkAssignmentsMultiDates(Date firstDate, Date lastDate)
            throws IllegalDateException, NullInputException{
        ErrorChecking.checkIfNull(firstDate);
        ErrorChecking.checkIfNull(lastDate);
        if(firstDate.after(lastDate))
            throw new IllegalDateException();
    }
    
    /**
     * A error checking method that ensures that a given Date is no earlier 
     * before current local machine date - 1 day. Calculations are based on
     * getTime() method from Date API, which returns the Date as
     * milliseconds. The "(1000*60*60*24)" represents a single day in 
     * milliseconds.
     * 
     * @param startDate date to be checked.
     * 
     * @throws IllegalDateException an exception to be thrown if user provides
     * wrong date.
     * 
     * @throws NullInputException an exception to be thrown if user provides
     * null input.
     * 
     * @throws CurrentDateException an exception to be thrown if user provides
     * date that is earlier than current machine time by more than one day.
     */
    public static void checkStartDate(Date startDate)
            throws IllegalDateException, NullInputException,
            CurrentDateException{
        ErrorChecking.checkDate(startDate);
        if(startDate.getTime() < new Date().getTime() - (1000*60*60*24))
            throw new CurrentDateException();
    }
    
    /**
     * A error checking method to check if deadlines for year long modules are
     * in valid format and with valid values.
     * 
     * @param deadline                  deadline of year long module assignment
     *                                  to check
     * 
     * @throws NullInputException       an exception to be thrown if null 
     *                                  input is detected
     * 
     * @throws IllegalDateException     an exception to be thrown if illegal
     *                                  date format or value is detected
     */
    public static void checkYearLongDeadline(Date deadline)
            throws NullInputException, IllegalDateException{
        ErrorChecking.checkIfNull(deadline);
        //Date format to test if the assigment date year, matches
        //the one of the local machine
        DateFormat yearTest = new SimpleDateFormat("yyyy");
        
        Date localDate = new Date();     //Initialises local date
        
        if(
          (Integer.parseInt(yearTest.format(deadline)) >
                (Integer.parseInt(yearTest.format(localDate)) + 1)) ||

          (Integer.parseInt(yearTest.format(deadline)) <
                Integer.parseInt(yearTest.format(localDate)) - 1)  
          )
            throw new IllegalDateException();
    }
    
    
    /**
     * A error checking method, that checks if Assignments deadlines are 
     * compatible with given Module type (A,B,Y)
     * 
     * @param type                  type of the Module
     * @param assignmentList        Assignments list of that Module 
     * 
     * @throws NullInputException       an exception to be thrown if any of
     *                                  input parameters are null
     * 
     * @throws IllegalDateException     an exception to be thrown if any of
     *                                  assignments deadlines are wrong
     */
    public static void checkAssignmentsDeadlines(Module.Type type,
            ArrayList<Assignment> assignmentList)
            throws NullInputException, IllegalDateException{
        ErrorChecking.checkIfNull(type);
        
        //Iterating through and error checking for 1 semester modules
        if(type == Module.Type.A || type == Module.Type.B)
            for(Assignment a : assignmentList)
                ErrorChecking.checkDate(a.getDeadline());
        
        //Error checking for year long modules
        else
            for(Assignment a : assignmentList)
                ErrorChecking.checkYearLongDeadline(a.getDeadline());
    }
    
    /**
     * A error checking method that ensures that given Note object is not 
     * already in given Note ArrayList, be comparing the contents of the Note
     * objects.
     * 
     * @param note      Note to be checked.
     * 
     * @param noteList  list of Note objects to check against.
     * 
     * @throws DuplicateDataException   an exception to be thrown if duplicate
     *                                  Note is found.
     */
    public static void checkForDuplicateNote(Note note,
                                             ArrayList<Note> noteList)
            throws DuplicateDataException{
        for(Note n : noteList){
            if(n.getContent().equalsIgnoreCase(note.getContent()))
                throw new DuplicateDataException("Duplicate note"
                        + " was detected. Please check your input "
                        + " and resubmit the form. If the error"
                        + " persists, please contact system"
                        + " administrator.");  
        }
    }
    
    /**
     * A error checking method, that ensures that given task type is 
     * compatible with the given list of tasks.
     * 
     * @param type      type of task that is being checked.
     * 
     * @param task      a list tasks to be compared against.
     * 
     * @throws IllegalTypeException     an exception to be thrown if there is a
     *                                  type miss match between given task type 
     *                                  and given task list.
     */
    public static void checkActivityType(Task.Type type, Task task) 
                                                throws IllegalTypeException{
        if(type != task.getType())
                    throw new IllegalTypeException("Task with incompatible type"
                            + " provided. Please ensure, that the task given"
                            + " is compatable with this activity."
                            + " If error occurs again, please check entered"
                            + " details or contact system administrator.");
    }
    
    /**
     * A error checking method, that ensures that given task type is 
     * compatible with the given list of tasks.
     * 
     * @param type      type of task that is being checked.
     * 
     * @param taskList  a list tasks to be compared against.
     * 
     * @throws IllegalTypeException     an exception to be thrown if there is a
     *                                  type miss match between given task type 
     *                                  and given task list.
     */
    public static void checkTypeInput(Task.Type type, ArrayList<Task> taskList) 
                                                throws IllegalTypeException{
        for(Task reference : taskList){
                if(type != reference.getType())
                    throw new IllegalTypeException("Incompatible types"
                            + " were detected. Please check your input "
                            + " and resubmit the form. If the error"
                            + " persists, please contact your HUB/system"
                            + " administrator.");
        }
    }
    
    /**
     * A error checking method, to ensure that a given Activity is compatible
     * with a given Task, by comparing their types.
     * 
     * @param activity  Activity to be checked.
     * 
     * @param task      Task to be checked.
     * 
     * @throws IllegalTypeException an exception to be thrown if given Activity
     *                              is incompatible with a given Task.
     */
    public static void checkTaskType(Activity activity, Task task)
                                                throws IllegalTypeException{
        if(activity.getType() != task.getType())
                    throw new IllegalTypeException("Task with incompatible type"
                            + " provided. Please ensure, that the task given"
                            + " is compatable with this activity."
                            + " If error occurs again, please check entered"
                            + " details or contact system administrator.");
    }
    
    /**
     * A error method checking method, that ensures that given list of Tasks
     * can be manipulated (deletion of an entry).
     * 
     * @param referenceList           a list of Tasks to be checked.
     * 
     * @throws IllegalDataManipulationException an exception to be thrown if 
     *                                          provided list of Tasks only 
     *                                          has a single entry.
     */
    public static void checkReferenceList(ArrayList<Task> referenceList) 
                                throws IllegalDataManipulationException{
        if(referenceList.size() == 1)
            throw new IllegalDataManipulationException();
    }
    
    
    /**
     * A error checking method that checks if the given Task already exists
     * within the list of Tasks of provided Activity object.
     * 
     * @param activity      Activity to check for duplicate Tasks.
     * 
     * @param task          Task that will be searched for.
     * 
     * @throws DuplicateDataException   an exception to be thrown if 
     *                                  given Activity already has given Task.
     */
    public static void checkForDuplicateTask(Activity activity, Task task) 
            throws DuplicateDataException{
        for(Task t : activity.getReferenceList()){
            if(t.getName().equalsIgnoreCase(task.getName())){
                throw new DuplicateDataException("Following task already"
                        + " exists. Please check your input "
                        + " and resubmit the form. If error occurs"
                        + " again, please contact system administrator."); 
            }
        }
    }
    
    /**
     * A error checking method that checks if the given Task already exists
     * within the list of Tasks of provided Assignment object.
     * 
     * @param assignment    Assignment to check for duplicate Tasks.
     * 
     * @param task          Task that will be searched for.
     * 
     * @throws DuplicateDataException   an exception to be thrown if 
     *                                  given Assignment already has given Task.
     */
    public static void checkForDuplicateTask(Assignment assignment, Task task) 
            throws DuplicateDataException{
        for(Task t : assignment.getTaskList()){
            if(t.getName().equalsIgnoreCase(task.getName())){
                throw new DuplicateDataException("Following task already"
                        + " exists. Please check your input "
                        + " and resubmit the form. If error occurs"
                        + " again, please contact system administrator."); 
            }
        }
    }
    
    /**
     * A error checking method that checks if the given Task already exists
     * within the list of Tasks of provided Milestone object.
     * 
     * @param milestone     Milestone to check for duplicate Tasks.
     * 
     * @param task          Task that will be searched for.
     * 
     * @throws DuplicateDataException   an exception to be thrown if 
     *                                  given Assignment already has given 
     *                                  Milestone.
     */
    public static void checkForDuplicateTask(Milestone milestone, Task task) 
            throws DuplicateDataException{
        for(Task t : milestone.getTaskList()){
            if(t.getName().equalsIgnoreCase(task.getName())){
                throw new DuplicateDataException("Following task already"
                        + " exists. Please check your input "
                        + " and resubmit the form. If error occurs"
                        + " again, please contact system administrator."); 
            }
        }
    }
    
    /**
     * A error checking method that checks if the given Milestone exists in the
     * list of Milestones contained in a given Assignment.
     * 
     * @param assignment Assignment that contains the list of Milestones, that
     * will be searched through.
     * 
     * @param milestone Milestone to be searched for.
     * 
     * @throws DuplicateDataException an exception to be thrown if given 
     * Milestone already exists in the list of Milestones of given Assignment.
     */
    public static void checkForDuplicateMilestone(Assignment assignment,
            Milestone milestone) throws DuplicateDataException{
        for(Milestone m : assignment.getMilestoneList()){
            if (m.getName().equalsIgnoreCase(milestone.getName()))
                throw new DuplicateDataException("The milestone that"
                        + " you are trying to add already exists."
                        + " Please, try again. If error occurs"
                        + " again, please, contact the system"
                        + " administrator.");
        }
    }
    
    /**
     * A error checking method that checks if the given Milestone exists in the
     * list of Milestones contained in a given Task.
     * 
     * @param task Task that contains the list of Milestones, that
     * will be searched through.
     * 
     * @param milestone Milestone to be searched for.
     * 
     * @throws DuplicateDataException an exception to be thrown if given 
     * Milestone already exists in the list of Milestones of given Assignment.
     */
    public static void checkForDuplicateMilestone(Task task,
            Milestone milestone)
            throws DuplicateDataException{
        for(Milestone m : task.getMilestoneList()){
            if (m.getName().equalsIgnoreCase(milestone.getName()))
                throw new DuplicateDataException("The milestone that"
                        + " you are trying to add already exists."
                        + " Please, try again. If error occurs"
                        + " again, please, contact the system"
                        + " administrator.");
        }
    }
    
    /**
     * A error checking method that checks if provided Module code is in right
     * format.
     * 
     * @param code Module code to be checked.
     * 
     * @throws IllegalCodeException an exception to be thrown if provided 
     * Module code is in wrong format.
     * 
     * @throws EmptyInputException an exception to be thrown if empty string
     * is provided as input parameter.
     * 
     * @throws NullInputException an exception to be thrown if provided 
     * parameter is null.
     */
    public static void checkModuleCode(String code) throws IllegalCodeException,
                                    EmptyInputException, NullInputException{
        ErrorChecking.checkStringInput(code);
        if(!code.matches("[A-Z]{3}\\-\\d{4}[ABY]"))
            throw new IllegalCodeException(); 
    }
    
    /**
     * A error checking method that checks if given Activity already exist 
     * within the list of Activity objects in given task.  
     * 
     * @param task          Task to check for duplicates.
     * 
     * @param activity      Activity that will be searched for.
     * 
     * @throws DuplicateDataException   an exception to be thrown if given 
     *                                  Activity already exists in the list
     *                                  of Activities in a given Task.
     */
    public static void checkForDuplicateActivity(Task task, Activity activity) 
                                                throws DuplicateDataException{
       for(Activity a : task.getActivityList())
            if(a.getName().equals(activity.getName()))
                throw new DuplicateDataException("Given activity already"
                        + " exists in the list. Please check your input and"
                        + " try again. If error perists"
                        + " please, contact the system"
                        + " administrator.");
    }
    
    /**
     * Check given types for compatability.
     * 
     * @param type                              type of a Semester Profile
     * @param moduleType                        type of a Module
     * 
     * @throws IllegalTypeException             if types are incompatible
     */
    public static void checkTypes(SemesterProfile.Type type, 
                Module.Type moduleType) throws IllegalTypeException {
        
        if ((moduleType == Module.Type.B
                && type == SemesterProfile.Type.AUTUMN)
                || (moduleType == Module.Type.A
                && type == SemesterProfile.Type.SPRING)) {
            throw new IllegalTypeException("Incompatible"
                    + " Semester Profile and Module types were"
                    + " detected. Please, check the data file and"
                    + " try again. If error occurs again, please"
                    + " contact the HUB/system administrator.");
        }
    }
    
    /**
     * A method to check whether or not the ArrayList already has a Module with
     * the given module code.
     * 
     * @param modules                           ArrayList of Modules
     * @param code                              module code
     * 
     * @throws DuplicateDataException           if the ArrayList already has
     *                                          a Module with the given code
     */
    public static void duplicateModules(ArrayList<Module> modules, 
                           String code) throws DuplicateDataException {
        
        if (!modules.isEmpty()) {
            for (Module m : modules) {
                if (m.getCode().equalsIgnoreCase(code)) {
                    throw new DuplicateDataException("The data"
                            + " file has duplicate modules. Please, check"
                            + " the data file and try again. If error"
                            + " occurs again, please, contact the HUB/"
                            + "system administrator.");
                }
            }
        }
    }
    
    /**
     * A method to check whether or not an Assignment has an Activity with the 
     * given name.
     * 
     * @param assignment                Assignment that Activity belongs to
     * 
     * @param activityName              potential Activity name
     * 
     * @throws EmptyInputException      if some field is empty
     * 
     * @throws DuplicateDataException if the Assignment already has an
     *                                  Activity with the given name
     */
    public static void duplicateActivityName(Assignment assignment, 
            String activityName) throws EmptyInputException,
                                        DuplicateDataException {
        
        if(activityName == null || activityName.isEmpty()){
            throw new EmptyInputException();
        }
        
        if(assignment != null && !assignment.getTaskList().isEmpty()){
            for(Task t : assignment.getTaskList()){
                if(!t.getActivityList().isEmpty()){
                    for(Activity a : t.getActivityList()){
                        if(a.getName().equalsIgnoreCase(activityName)){
                            throw new DuplicateDataException("The assignment"
                                    + " already has an activity with the"
                                    + " specified name. Please, enter another"
                                    + " name and try again. If error occurs"
                                    + " again, please, contact the system"
                                    + " administrator.");
                        }
                    }
                }
            }
        }
    }
}
