package studyplanner;


import SemesterProfilePage.MainPageGUI;
import studyplanner.controller.FileController;
import studyplanner.controller.SemesterProfileController;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalFileException;
import utilities.exceptions.IllegalMessageException;
import utilities.exceptions.IllegalSemesterProfileException;
import utilities.exceptions.IllegalTypeException;
import utilities.exceptions.ItemNotFoundException;


public class StudyPlanner {

    private static MainPageGUI GUI;
    
    public static void main(String[] args) throws IllegalFileException, 
            EmptyInputException, IllegalMessageException, 
            IllegalTypeException, ItemNotFoundException, IllegalSemesterProfileException{

        //start the program
        FileController.readLogFile();
        GUI = new MainPageGUI(SemesterProfileController.isEmpty());
        GUI.setVisible(true);
        
    }
    
    public static void reloadMainPage() throws IllegalSemesterProfileException, ItemNotFoundException{
        GUI.reloadPage(SemesterProfileController.isEmpty());
    }
    
    public static MainPageGUI getMainPageGUI(){
        return GUI;
    }
}
