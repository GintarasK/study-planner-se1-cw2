package studyplanner.model;

import utilities.ErrorChecking.ErrorChecking;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import studyplanner.controller.SemesterProfileController;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.IllegalTypeException;
import utilities.exceptions.IncorrectInputDataException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate semester profile objects.
 */

public class SemesterProfile implements Serializable{
    
    //----------------------VARIABLE_DECLARATIONS------------------------//
    private static final long serialVersionUID = 101;
    
    private int ID;                //Represents unique ID of this 
                                                //SemesterProfile object
    
    private Date startDate;        //Represents start date of
                                                //this semester
    
    private Date endDate;          //Represents end date of 
                                                //this semester
    
    private Date holidaysStart;     //holidays start date of this semester
                                    //used only for Spring semesters
    
    private Date holidaysEnd;       //holidays end date of this semester
                                    //used only for Spring semesters
    
    public enum Type{              //Enum type variable to represent 
        AUTUMN, SPRING             //the type of this semester
    }   
    
    private Type type;  //Represents type of this semester (Autumn, Spring,
                        //Summer)
    
    //Represents modules contained in this semester   
    private ArrayList<Module> moduleList;   
    
    //----------------END_OF_VARIABLE_DECLARATIONS------------------------//
    
    
    //-----------------METHODS_DEFINED_IN_CLASS_DIAGRAM-------------------//
    
    /**
     * Constructor for a new SemesterProfile that has four parameters to 
     * represents it's start date, end date, type and modules that will be
     * taken in that semester.
     * 
     * 
     * @param startDate         start date of this semester
     * @param endDate           end date of this semester
     * @param type              type of this semester(autumn or spring)
     * @param moduleList        list of modules that will be taken during this
     *                          semester
     * @param holidaysStartDate start date of holidays of this semester
     * @param holidaysEndDate   start date of holidays of this semester
     * 
     * @throws IllegalDateException     an exception to be thrown if illegal
     *                                  date format is specified
     * 
     * @throws IllegalTypeException     an exception to be thrown if illegal
     *                                  semester type is provided
     * 
     * @throws NullInputException       an exception to be thrown if any of the
     *                                  input parameters are passed as null
     * 
     * @throws EmptyInputException      an exception to be thrown if any of
     *                                  string input is passed as an empty 
     *                                  string
     */
    public SemesterProfile(Date startDate, Date endDate, Type type,
            ArrayList<Module> moduleList, 
            Date holidaysStartDate, Date holidaysEndDate) 
            throws IllegalDateException, NullInputException, 
            EmptyInputException{
       
        //Following if statement checks for the following errors in
        //startDate and endDate input: if semester startDate is not after
        //semester endDate; if startDate and endDate is of current year + 1
        //(compares years with the ones of local machine)
        ErrorChecking.checkMultiDates(startDate, endDate);
        ErrorChecking.checkListInput(moduleList);
        ErrorChecking.checkIfNull(type);
        
        //Checking that semester profiles from previous years, could not be 
        //initialised 
        for(Module m : moduleList){
            for(Assignment a : m.getAssignmentList()){
                if(a.getName().equalsIgnoreCase("Exam") ||
                        a.getName().equalsIgnoreCase("Course test")){
                    
                    Exam exam = (Exam) a;
                    ErrorChecking.checkAssignmentsMultiDates(startDate, 
                            exam.getStartTime());
                    
                }else if(m.getType() != Module.Type.Y){
                    Coursework coursework = (Coursework) a;
                    ErrorChecking.checkAssignmentsMultiDates(startDate,
                            coursework.getSetDate());
                    
                    ErrorChecking.checkAssignmentsMultiDates(
                            coursework.getSetDate(), endDate);
                }
            }
        }
                
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.ID = startDate.hashCode() + endDate.hashCode();
        this.holidaysStart = holidaysStartDate;
        this.holidaysEnd = holidaysEndDate;
        if(moduleList == null){
            this.moduleList = new ArrayList<>();
        }else{
            this.moduleList = moduleList;
        }
    }
    
    /**
     * An accessor method to return this SemesterProfile's start date.
     * 
     * @return SemesterProfile's start date.
     */
    public Date getStartDate(){
        return this.startDate;
    }
    
    /**
     * An accessor method to return this SemesterProfile's end date.
     * 
     * @return SemesterProfile's end date.
     */    
    public Date getEndDate(){
        return this.endDate;
    }
    
    /**
     * An accessor method to return this SemesterProfile's type.
     * Can be either AUTUMN or SPRING.
     * 
     * @return this SemesterProfile's type.
     */  
    public Type getType(){
        return this.type;
    }
    
    /**
     * An accessor method to return this SemesterProfile's modules.
     * 
     * @return this SemesterProfile's modules list.
     */  
    public ArrayList<Module> getModuleList(){
        return this.moduleList;
    }
    
    /**
     * An accessor method to return this SemesterProfile's ID.
     * 
     * @return this SemesterProfile's ID.
     */
    public int getID(){
        return this.ID;
    }
    
    /**
     * A mutator method to set this SemesterProfile's ID.
     * 
     * @param ID new ID for this SemesterProfile.
     * 
     * @throws IncorrectInputDataException if the passed argument is invalid.
     * 
     * @throws InputOutOfRangeException if the passed argument is out of range.
     */
    public void setID(int ID) throws IncorrectInputDataException,
            InputOutOfRangeException{
        ErrorChecking.checkIntInput(ID, 0, 999);
        for(SemesterProfile p : SemesterProfileController.getSemProfiles())
            if(p.getID() == ID)
                throw new IncorrectInputDataException("Entered ID already "
                        + "belongs to another module. Please check your input "
                        + "and try again. If error persists, contact "
                        + "system administrator."); 
        this.ID = ID;
    }
    
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder("");

        if(this.startDate.getDate() < 10){
            str.append("0" + this.startDate.getDate());
        }else{
            str.append(this.startDate.getDate());
        }
        
        if(this.startDate.getMonth() + 1 < 10){
            str.append("/0" + (this.startDate.getMonth() + 1));
        }else{
            str.append("/" + (this.startDate.getMonth() + 1));
        }
        
        str.append("/" + (this.startDate.getYear() + 1900) + " - ");
        
        
        if(this.endDate.getDate() < 10){
            str.append("0" + this.endDate.getDate());
        }else{
            str.append(this.endDate.getDate());
        }
        
        if(this.endDate.getMonth() + 1 < 10){
            str.append("/0" + (this.endDate.getMonth() + 1));
        }else{
            str.append("/" + (this.endDate.getMonth() + 1));
        }
        
        str.append("/" + (this.endDate.getYear() + 1900));
        str.append(" : " + this.type.toString() + " SEMESTER");
        
        return str.toString();
    }
    
    //---------------END_OF_METHODS_DEFINED_IN_CLASS_DIAGRAM----------------//
    
    //-------------ADDITIONAL_METHODS_NOT_DEFINED_IN_CLASS_DIAGRAM----------//
    
    /**
     * A method which gets called when serialisation process happens on this
     * Semester Profile
     * 
     * @param out                       ObjectOutputStream
     * 
     * @throws IOException              if I/O operations are failed or 
     *                                  interrupted
     * 
     * @throws ClassNotFoundException   if the program tries to load in a class 
     *                                  through its string name but no 
     *                                  definition for the class with the 
     *                                  specified name could be found
     */
    public void writeObject(ObjectOutputStream out)
            throws IOException, ClassNotFoundException {
        out.writeObject(this);
    }
    
    /**
     * A set method to update the date which represents the start date of
     * holidays of this Semester profile.
     * 
     * @param d         new date
     * 
     * @throws IllegalDateException if passed Date input is in wrong format.
     * 
     * @throws NullInputException if any of the input parameters are null.
     */
    public void setHolidaysStart(Date d) throws IllegalDateException,
                                                NullInputException {
        ErrorChecking.checkDate(d);
        this.holidaysStart = d;
    }

    /**
     * A set method to update the date which represents the end date of
     * holidays of this Semester profile.
     * 
     * @param d         new date
     */
    public void setHolidaysEnd(Date d) {
        this.holidaysEnd = d;
    }
    
    /**
     * An accessor method to get the date which represents the start date of
     * holidays of this Semester profile.
     * 
     * @return          start date
     */
    public Date getHolidaysStart(){
        return this.holidaysStart;
    }
    
    /**
     * An accessor method to get the date which represents the end date of
     * holidays of this Semester profile.
     * 
     * @return          end date
     */
    public Date getHolidaysEnd(){
        return this.holidaysEnd;
    }
   
}
