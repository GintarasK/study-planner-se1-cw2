package studyplanner.model;

import utilities.ErrorChecking.ErrorChecking;
import java.io.Serializable;
import java.util.ArrayList;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalCodeException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate module objects.
 */

public class Module implements Serializable{
    
    //----------------------VARIABLE_DECLARATIONS------------------------//
    private static final long serialVersionUID = 102;
    
    private int creditValue;        //credit value of this module
    
    private String name;        //Represents the name of this module
    
    private String code;        //Represents the module code for this module
    
    private String moduleOrganiser;     //Represents the name of the organiser
                                        //of this module
    
    private String description;         //Represents the description for this
                                        //module
    
    public enum Type{           //Enum type variable to represent the
        A, B, Y                 //the type of this module
    }   
    
    private Type type;  //Represents type of this module (1st semester, 
                        //2nd semester, year long)
    
    //Represents modules contained in this semester   
    private ArrayList<Assignment> assignmentList;   
    
    //----------------END_OF_VARIABLE_DECLARATIONS------------------------//
    
    //-----------------METHODS_DEFINED_IN_CLASS_DIAGRAM-------------------//
    
    /**
     * Constructor for a new Module that has seven parameters to 
     * represent it's name, code, credit value, module organiser's name, 
     * module description, module type and assignments list, 
     * that will be given this semester.
     * 
     * @param name              name of this module
     * @param code              module code (Format: SCHOOL-MODULE_ID/TYPE)
     * @param creditValue       credit value of this module
     * @param moduleOrganiser   name of this module organiser
     * @param description       description for this module
     * @param type              type of this module
     * @param assignmentList    list of assignments that will be given during
     *                          this semester
     * 
     * @throws EmptyInputException          an exception to be thrown if
     *                                      no information (empty values) 
     *                                      is provided for string fields: 
     *                                      name/code/moduleOrganiser/
     *                                      description
     * 
     * @throws NullInputException           an exception to be thrown if 
     *                                      any of the required parameters are
     *                                      passed as null
     * 
     * @throws IllegalCodeException         an exception to be thrown if
     *                                      illegal module code is used
     * 
     * @throws InputOutOfRangeException     an exception to be thrown if credit
     *                                      value is out of range
     * 
     * @throws IllegalDateException         an exception to be thrown if any
     *                                      of given Assignments deadlines are
     *                                      incompatible with this Module
     */
    public Module(String name, String code, int creditValue,
            String moduleOrganiser, String description, Type type, 
            ArrayList<Assignment> assignmentList) throws EmptyInputException, 
            NullInputException, IllegalCodeException,
            InputOutOfRangeException, IllegalDateException{
        
        ErrorChecking.checkStringInput(name);
        ErrorChecking.checkModuleCode(code);
        ErrorChecking.checkStringInput(moduleOrganiser);
        ErrorChecking.checkStringInput(description);
        ErrorChecking.checkIfNull(type);
        ErrorChecking.checkIntInput(creditValue, 1, 100);
        ErrorChecking.checkListInput(assignmentList);        
        //Error checking for year-long modules and assignment date clashes
        ErrorChecking.checkAssignmentsDeadlines(type, assignmentList);
        
        this.name = name;
        this.code = code;
        this.moduleOrganiser = moduleOrganiser;
        this.description = description;
        this.type = type;
        this.assignmentList = assignmentList;
        this.creditValue = creditValue;
}
    /**
     * An accessor method to return this Module's name.
     * 
     * @return this Module's name.
     */
    public String getName(){
        return this.name;
    }
    
    /**
     * An accessor method to return this Module's code.
     * 
     * @return this Module's code.
     */
    public String getCode(){
        return this.code;
    }
    
    /**
     * An accessor method to return this Module's organiser name.
     * 
     * @return this Module's organiser name.
     */
    public String getOrganiser(){
        return this.moduleOrganiser;
    }
    
    /**
     * An accessor method to return this Module's type (1st semester(A),
     * 2nd semester(B) or year long(Y)).
     * 
     * @return type of this module.
     */
    public Type getType(){
        return this.type;
    }
    
    /**
     * A mutator method to set the Assignment list for this Module.
     * 
     * @param list Assignment list to be set.
     * 
     * @throws EmptyInputException an exception to be thrown if provided list
     * does not contain any entries.
     * 
     * @throws NullInputException an exception to be thrown 
     * list parameter is passed as null.
     */
    public void setAssignmentList(ArrayList<Assignment> list) throws 
            EmptyInputException, NullInputException{
        ErrorChecking.checkListInput(list);
        this.assignmentList = list;
    }
    
    /**
     * An accessor method to return this Module's credit value
     * 
     * @return  credit value of this module.
     */
    public int getCreditValue(){
        return this.creditValue;
    }
    
    /**
     * An accessor method to return this Module's list of assignments, that
     * will be taken in this Module.
     * 
     * @return assignments list of this Module.
     */
    public ArrayList<Assignment> getAssignmentList(){
        return this.assignmentList;
    }

  //---------------END_OF_METHODS_DEFINED_IN_CLASS_DIAGRAM----------------//
    /**
     * An accessor method to return this Module's description.
     * 
     * @return this Module's description.
     */
    public String getDescription(){
        return this.description;
    }    
       
}
