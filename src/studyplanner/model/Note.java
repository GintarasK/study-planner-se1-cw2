package studyplanner.model;

import utilities.ErrorChecking.ErrorChecking;
import java.io.Serializable;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate note objects.
 */

public class Note implements Serializable{

    //----------------------VARIABLE_DECLARATIONS------------------------//
    private static final long serialVersionUID = 107;
    
    private String content;         //Represents the text within the note
    
    //----------------END_OF_VARIABLE_DECLARATIONS------------------------//
    
    //-----------------METHODS_DEFINED_IN_CLASS_DIAGRAM-------------------//
    
    /**
     * A constructor for Note objects, that has one field: content - represents
     * text within the note.
     * 
     * @param content                   text within the note.
     * 
     * @throws EmptyInputException      an exception to be thrown if user
     *                                  submits a form with empty input 
     *                                  variable(s).
     * 
     * @throws NullInputException       an exception to be thrown if user
     *                                  provides null variables parameters.
     * 
     */
    public Note(String content) throws EmptyInputException,
            NullInputException{
        ErrorChecking.checkStringInput(content);
        this.content = content;

    }
    
    
    /**
     * An accessor method to return text that is contained within this Note.
     * 
     * @return                  text within this Note.
     */
    public String getContent(){
        return this.content;
    }
    
    //NOTE: changed method to boolean!!!!
    /**
     * A setter method to change the textual content of this Note.
     * 
     * @param content       text to be changed in this Note.
     * 
     * @throws EmptyInputException  an exception to be thrown if empty
     *                              input fields are detected.
     * 
     * @throws NullInputException   an exception to be thrown if user
     *                              provides null parameters.
     */
    public void setContent(String content) throws EmptyInputException,
            NullInputException{
        ErrorChecking.checkStringInput(content);
        this.content = content;
    }
    
    //---------------END_OF_METHODS_DEFINED_IN_CLASS_DIAGRAM----------------//
    
}
