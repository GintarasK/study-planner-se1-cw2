package studyplanner.model;

import utilities.ErrorChecking.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * An abstract lass to simulate module assignment objects.
 */

public abstract class Assignment implements Serializable{
    //----------------------VARIABLE_DECLARATIONS------------------------//
    
    protected String name;          //Represents the name of this assignment
    
    protected int result;           //Represents the result of this assignment
    
    protected int weighting;        //Represents the weight of the assigment
                                    //(E.G. 20% of the module)
                              
    
    protected Date deadline;    //Represents the dealine for COURSEWORK
                                //subsimission or end time of EXAM
    
    //Represents tasks, that are associated with this assigment
    protected ArrayList<Task> taskList;   
    
    //Represents milestone, that are associated with this assigment  
    protected ArrayList<Milestone> milestoneList;   
    
    //----------------END_OF_VARIABLE_DECLARATIONS------------------------//
    
    //-----------------METHODS_DEFINED_IN_CLASS_DIAGRAM-------------------//
    
    /**
     * Constructor for a new Assignment that has five parameters to 
     * represents it's name, weighting, deadline (or 
     * commence time).
     * 
     * @param name          name of the assignment
     * @param weighting     weighting of the assignment
     * @param deadline      deadline of the assignment
     * 
     * @throws NullInputException       an exception to be thrown if null 
     *                                  is passed as a parameter
     * 
     * @throws IllegalDateException     an exception to be thrown if
     *                                  the deadline is in incorrect format
     * 
     * 
     * @throws EmptyInputException      an exception to be thrown if empty
     *                                  field for assignment name was provided
     * 
     * @throws InputOutOfRangeException an exception to be thrown if weighting
     *                                  input is not within range (0-100)
     * 
     */
    public Assignment(String name, int weighting, Date deadline) 
            throws NullInputException, IllegalDateException,
            EmptyInputException, InputOutOfRangeException{
       
        //Error checking for deadline input. Ensures, that the date is either
        //a in range of current local machine date +- 1 year
        ErrorChecking.checkDate(deadline);
        
        //Checks that the name parameter is not null or empty
        ErrorChecking.checkStringInput(name);
        
        //Checks that the weighting input is in the range from 0 to 100
        ErrorChecking.checkIntInput(weighting, 0, 100);
        
        
        //Creating empty placeholders for task and milestonse lists.       
        this.taskList = new ArrayList<>();
        this.milestoneList = new ArrayList<>();
        this.name = name;
        this.weighting = weighting;
        this.deadline = deadline;        
    }
    
    /**
     * An accessor method to return the name of this Assignment.
     * 
     * @return name of this Assignment.
     */
    public String getName(){
        return this.name;
    }
    
    /**
     * An accesor method to return the weighting of this Assignment.
     * (Overall percentage for the module, e.g. 20%)
     * 
     * @return weighting of this Assignment.
     */
    public int getWeighting(){
        return this.weighting;
    }
    
    /**
     * An accessor method to return the result of this Assignment.
     * (How much the student has scored in this Assignment (0%-100%))
     * 
     * @return result of this Assignment.
     */
    public int getResult(){
        return this.result;
    }
    
    
    /**
     * An accessor method to return the deadline for this Assignment.
     * (Applies to coursework only)
     * 
     * @return the deadline for this Assignment.
     */
    public Date getDeadline(){
        return this.deadline;
    }
    
    /**
     * An accessor method to return the list of tasks that are associated with
     * this Assignment.
     * 
     * @return list of tasks that are associated with this Assignment.
     */
    public ArrayList<Task> getTaskList(){
        return this.taskList;
    }
    
    /**
     * An accessor method to return the list of milestones that are associated 
     * with this Assignment.
     * 
     * @return list of milestones that are associated with this Assignment.
     */
    public ArrayList<Milestone> getMilestoneList(){
        return this.milestoneList;
    }
    
    /**
     * A setter method to set the deadline for this Assignment.
     * 
     * @param deadline deadline date to be set for this Assignment.
     * 
     * @throws IllegalDateException an exception to be thrown if the deadline
     * given is in incorrect format or if erroneous data is provided.
     * 
     * @throws NullInputException an exception to be thrown if deadline 
     * parameter is passed as null.
     */
    public void setDeadline(Date deadline) throws IllegalDateException,
                                                     NullInputException{
        ErrorChecking.checkDate(deadline);
        this.deadline = deadline;
    }
    
    
    /**
     * A setter method to add a task for this Assignment.
     * 
     * @param task task to be added for this Assignment.
     * 
     * @throws NullInputException   an exception to be thrown if any of the 
     *                              required parameters are passed as null.
     * 
     * @throws IllegalDateException an exception to be thrown if any of 
     *                              the provided dates are in illegal
     *                              format 
     * 
     * @throws DuplicateDataException   an exception to be thrown if given Task
     *                                  already exists in the list of Tasks 
     *                                  of this Assignment.
     */
    public void addTask(Task task) throws
            NullInputException, IllegalDateException, DuplicateDataException{
        ErrorChecking.checkIfNull(task);
        ErrorChecking.checkForDuplicateTask(this, task);
        ErrorChecking.checkMultiDates(task.getDeadline(), this.deadline);
        taskList.add(task);
        }
    
    /**
     * A setter method to add a milestone for this Assignment.
     * 
     * @param milestone milestone to be added for this Assignment.
     * 
     * @throws NullInputException an exception to be thrown if milestone 
     * parameter is passed as a null
     * 
     * @throws DuplicateDataException an exception to be thrown if given 
     * Milestone already exist in the list of Milestones of this Assignment.
     * 
     */
    public void addMilestone(Milestone milestone) throws NullInputException,
            DuplicateDataException{
        ErrorChecking.checkIfNull(milestone);
        ErrorChecking.checkForDuplicateMilestone(this, milestone);
        milestoneList.add(milestone);
    }
    
    /**
     * A setter method to remove a task for this Assignment.
     * 
     * @param task task to be removed for this Assignment.
     * 
     * @throws NullInputException an exception to be thrown if Task parameter
     * is passed as null.
     * 
     * @throws ItemNotFoundException an exception to be thrown if specified
     * Task is not found.
     */
    public void removeTask(Task task) throws NullInputException,
                                                    ItemNotFoundException{
        ErrorChecking.checkIfNull(task);
        if(!this.taskList.remove(task)){
            throw new ItemNotFoundException("Error on given task -");
        }
    }
    
    /**
     * A setter method to remove a milestone for this Assignment.
     * 
     * @param milestone milestone to be removed for this Assignment.
     * 
     * @throws NullInputException an exception to be thrown if Milestone 
     * parameter is passed as null.
     * 
     * @throws ItemNotFoundException an exception to be thrown if given 
     * Milestone is not found in the list.
     */
    public void removeMilestone(Milestone milestone) 
                                                    throws NullInputException,
                                                        ItemNotFoundException{
        ErrorChecking.checkIfNull(milestone);
        if(!this.milestoneList.remove(milestone)){
            throw new ItemNotFoundException("Error on given milestone -");
        }
    }
    
    //---------------END_OF_METHODS_DEFINED_IN_CLASS_DIAGRAM----------------//
    
    //-------------ADDITIONAL_METHODS_NOT_DEFINED_IN_CLASS_DIAGRAM----------//
    
    /**
     * A mutator method to set the result for this Assignment.
     * 
     * @param result result to change to.
     * 
     * @throws InputOutOfRangeException an exception to be thrown if given
     * result is not within range (0-100).
     */
    public void setResult(int result) throws InputOutOfRangeException{
            ErrorChecking.checkIntInput(result, 0, 100);
            this.result = result;
    }
    
}
