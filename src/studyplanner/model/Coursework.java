package studyplanner.model;

import utilities.ErrorChecking.ErrorChecking;
import java.io.Serializable;
import java.util.Date;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate coursework objects.
 */

public class Coursework extends Assignment implements Serializable{
    
    //----------------------VARIABLE_DECLARATIONS------------------------//
    private static final long serialVersionUID = 103;
    
    private Date setDate;     //Represents the date when the coursework was set.
   
    //----------------END_OF_VARIABLE_DECLARATIONS------------------------//
    
    //-----------------METHODS_DEFINED_IN_CLASS_DIAGRAM-------------------//
    
    /**
     * Constructor for a new Coursework that has five parameters to 
     * represents it's name, weighting, assignment type, set date.
     * 
     * @param name          name of the coursework
     * @param weighting     weighting of the coursework
     * @param deadline      submission deadline for this coursework
     * @param setDate       date when the coursework was set
     * 
     * @throws IllegalDateException     an exception to be thrown if
     *                                  the deadline is in incorrect format
     * 
     * @throws NullInputException       an exception to be thrown if any of
     *                                  the required parameters are passed as
     *                                  null
     * 
     * @throws InputOutOfRangeException an exception to be thrown if given
     *                                  weighting is not in range (0-100).
     * 
     * @throws EmptyInputException      an exception to be thrown if empty
     *                                  field for coursework name was provided
     *     
     */
    public Coursework(String name, int weighting, Date deadline, Date setDate) 
                      throws IllegalDateException, NullInputException,
                             EmptyInputException, InputOutOfRangeException {
        super(name, weighting, deadline);
        ErrorChecking.checkAssignmentsMultiDates(setDate, deadline);                
        this.setDate = setDate;
    }
    
    /**
     * An accessor method to return this Coursework's set date.
     * 
     * @return this Coursework's set date.
     */
    public Date getSetDate(){
        return this.setDate;
    }
    
    //---------------END_OF_METHODS_DEFINED_IN_CLASS_DIAGRAM----------------//
    
    //-------------ADDITIONAL_METHODS_NOT_DEFINED_IN_CLASS_DIAGRAM----------//

    
}
