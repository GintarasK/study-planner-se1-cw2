package studyplanner.model;

import utilities.ErrorChecking.ErrorChecking;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalTypeException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate Activity objects.
 */

public class Activity implements Serializable{
    
    //----------------------VARIABLE_DECLARATIONS------------------------//

    private static final long serialVersionUID = 105;    //SerializationID
    
    private String name;            //Represents the name of the activity
    
    private int timeTaken;          //Represents the ammount of hours spent
                                    //on this activity
    
    //Type is extracted from Task.Type and should be the same.
    private Task.Type type;         //Represents type of this activity 
                                    //(reading/programming)
    
    private int contribution;       //A field to evaluate activity contribution
    
    //Represents a list of tasks that are associated with this activity
    private ArrayList<Task> referenceList;   
    
    //Represents notes associated with activity   
    private ArrayList<Note> noteList;   
    
    private Date setDate;            //Represents set date for this activity
    
    //----------------END_OF_VARIABLE_DECLARATIONS------------------------//
    
    //-----------------METHODS_DEFINED_IN_CLASS_DIAGRAM-------------------//
    
    /**
     * Constructor for a new Activity that has six parameters to 
     * represent it's start name, type(programming, etc), contribution
     * (time spent on it by individual student) a list of task that belong
     * to this activity, time taken (overall) for this activity and a list of
     * notes that are associated with this activity.
     * 
     * @param name              Represents the name of activity
     * @param type              Represents the type of activity
     * @param contribution      Represents individual contribution
     *                          of a student for this task
     * 
     * @param reference         Represents a list of tasks that are associated
     *                          with this activity
     * 
     * @param noteList          Represents a list of notes that are associated
     *                          with this activity
     * 
     * @param timeTaken         Represents the time taken for this in
     *                          hours
     * 
     * @throws IllegalTypeException     an exception to be thrown if 
     *                                  erroneous data is provided for the
     *                                  activity type
     * 
     * @throws EmptyInputException      an exception to be thrown if an empty
     *                                  activity name is provided
     * 
     * @throws InputOutOfRangeException     an exception to be thrown if
     *                                      the data provided is erroneous 
     *                                      (contribution <0 or contribution>
     *                                      10000).
     * 
     * @throws NullInputException           an exception to be thrown if user
     *                                      provides a null input variable
     * 
     */
    public Activity(String name, Task.Type type, 
                    Task reference, ArrayList<Note> noteList,
                    int timeTaken, int contribution)
                    throws IllegalTypeException, NullInputException,
                           EmptyInputException, InputOutOfRangeException{
        
        //Error checking for empty/null strings on Activity name
        ErrorChecking.checkStringInput(name);

        //Error checking for types. Activity type should match it's reference
        //list(task list) type: checks the whole task list and check if each
        //task matches the one of this activity.
        ErrorChecking.checkIfNull(type);
        
        ErrorChecking.checkIfNull(reference);
        ErrorChecking.checkActivityType(type, reference);
        
        //Checking contribution. As we assume that the Activity is added once
        //it is already done, user should specify how much it has taken him
        //to complete it once he submits this form.
        ErrorChecking.checkIntInput(contribution, 1, 10000);
        
        ErrorChecking.checkIntInput(timeTaken, 1, 24);
        
        
        this.timeTaken = timeTaken;
        this.setDate = new Date();        
        this.name = name;         
        this.type = type;         
        this.contribution = contribution; 
        
        //Create and initialise a new reference list of Tasks, then
        //add the reference passed in the constructor (upon Activity creation,
        //user is allowed to select one Task reference only)
        this.referenceList = new ArrayList<Task>();
        referenceList.add(reference); 
        
        //Error checking for Note list input. If nothing is provided for 
        //the Note list (Null is passed or the list is empty, we siimply 
        //initialise a new Note list).
        if(noteList == null)
            this.noteList = new ArrayList<Note>();
        else
            this.noteList = noteList;
    }   
    
    /**
     * A setter method to add a note to this Activity.
     * 
     * @param note      Note to be added.
     * 
     * @throws DuplicateDataException       an exception to be thrown if
     *                                      duplicate note is provided.
     * 
     * @throws NullInputException           an exception to be thrown if user
     *                                      provides a null input variable.
     */
    public void addNote(Note note) throws DuplicateDataException,
                                             NullInputException{
        ErrorChecking.checkIfNull(note);
        ErrorChecking.checkForDuplicateNote(note, noteList);
        this.noteList.add(note);
    }
    
    
    /**
     * A setter method to set contribution(individual) for this Activity.
     * 
     * @param contribution      amount of hours to be set (contribution) 
     *                          for this activity.
     * 
     * @throws InputOutOfRangeException     an exception to be thrown if
     *                                      the data provided is erroneous
     *                                      (contribution < 0 or contribution >
     *                                      999).
     */
    public void setContribution(int contribution) 
                                               throws InputOutOfRangeException{
        ErrorChecking.checkIntInput(contribution, 1, 10000);
        this.contribution = contribution;
    }
    
    /**
     * A setter method to assign a task to this Activity.
     * 
     * @param task      Task to be set to this Activity.
     * 
     * @throws IllegalTypeException     an exception to be thrown if illegal 
     *                                  Task is to be added to the Task 
     *                                  reference list.
     * 
     * @throws NullInputException           an exception to be thrown if user
     *                                      provides a null input variable.
     * 
     * @throws DuplicateDataException       an exception to be thrown if
     *                                      provided Task already exists
     *                                      in the list of Tasks of this 
     *                                      Activity.
     */
    public void assignTask(Task task) throws IllegalTypeException,
                                                NullInputException,
                                                DuplicateDataException{
        //Error checking for null input
        ErrorChecking.checkIfNull(task);
        
        //Check to ensure that the given task's type matches that of current
        //Tasks associated with this Activity
        ErrorChecking.checkTaskType(this, task);        
        
        //Checking if given Task is not already in the reference list
        ErrorChecking.checkForDuplicateTask(this, task);
        
       referenceList.add(task);        
    }
    
    /**
     * A mutator method to set the amount of time taken for this Activity.
     * 
     * @param hours       amount of time taken for this Activity.
     * 
     * @throws InputOutOfRangeException     an exception to be thrown if 
     *                                      illegal amount of time taken is
     *                                      provided.
     */
    public void setTimeTaken(int hours) 
            throws InputOutOfRangeException {   
        ErrorChecking.checkIntInput(hours, 1, 24);
        this.timeTaken = hours;
    }
    
    /**
     * A mutator method to remove all of the Note objects that are associated
     * with this Activity.
     */
    public void removeNotes(){
        while(!this.noteList.isEmpty()){
            noteList.remove(0);
        }
    }
    
    /**
     * A setter method to set the name for this Activity.
     * 
     * @param name      name to be set for this Activity.
     * 
     * @throws EmptyInputException  an exception to be thrown if empty name for 
     *                              the Activity has been provided.
     * 

     * @throws NullInputException   an exception to be thrown if user
     *                              provides a null input variable
     */
    public void setName(String name) throws EmptyInputException,                                              
                                               NullInputException{
        ErrorChecking.checkStringInput(name);
        this.name = name;
    }    
    
    /**
     * An accessor method to return the name of this Activity.
     * 
     * @return name of this Activity
     */
    public String getName(){
        return this.name;
    }
    
    /**
     * An accessor method to return the type of this Activity.
     * 
     * @return type of this Activity
     */
    public Task.Type getType(){
        return this.type;
    }
    
    /**
     * An accessor method to return the contribution for this Activity.
     * 
     * @return contribution for this activity
     */
    public int getContribution(){
        return this.contribution;
    }
    
    /**
     * An accessor method to return the list of tasks that are associated with
     * this Activity.
     * 
     * @return a list of Tasks that are associated with this Activity
     */
    public ArrayList<Task> getReferenceList(){
        return this.referenceList;
    }

    /**
     * An accessor method to return the amount of time taken for this Activity.
     * 
     * @return amount of time taken for this activity
     */
    public int getTimeTaken(){
        return this.timeTaken;
    }
    
    /**
     * An accessor method to return the list of notes that are associated with
     * this Activity.
     * 
     * @return a list of Note objects that are associated with this Activity
     */
    public ArrayList<Note> getNoteList(){
        return this.noteList;
    }
    
    /**
     * Method to return set date for this Activity.
     * 
     * @return set date for this Activity.
     */
    public Date getSetDate(){
        return this.setDate;
    }
    
    /**
     * A mutator method to remove a Note for this activity.
     * 
     * @param note  Note to be removed
     * 
     * @throws EmptyInputException   an exception to be thrown if user provides
     *                               incorrect input data, such as null objects
     * 
     * @throws NullInputException    an exception to be thrown if user
     *                               provides a null input variable
     * 
     * @throws ItemNotFoundException an exception to be thrown if given note
     *                               is not found in the note list
     */
    public void removeNote(Note note) throws EmptyInputException,
                                    NullInputException, ItemNotFoundException{
        ErrorChecking.checkIfNull(note);
        ErrorChecking.checkStringInput(note.getContent());
        if(!this.noteList.remove(note)){
            throw new ItemNotFoundException("Error on given note -");

        }
    }    

    /**
     * A mutator method to remove a task for this Activity.
     * 
     * @param task      Task to be removed
     * 
     * @throws NullInputException an exception to be thrown if user provides a
     * null input variable
     * 
     * @throws ItemNotFoundException an exception to be thrown if given 
     * Task is not found
     */
    public void removeTask(Task task) throws NullInputException, 
                                                         ItemNotFoundException{
        ErrorChecking.checkIfNull(task);

        if(!this.referenceList.remove(task)){
            throw new ItemNotFoundException("Error on given task -");
        }
    }    
   
    //---------------END_OF_METHODS_DEFINED_IN_CLASS_DIAGRAM----------------//
}
