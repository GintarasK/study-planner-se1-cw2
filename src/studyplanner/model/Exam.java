package studyplanner.model;

import utilities.ErrorChecking.ErrorChecking;
import java.io.Serializable;
import java.util.Date;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate exam objects.
 */

public class Exam extends Assignment implements Serializable{
    
    //----------------------VARIABLE_DECLARATIONS------------------------//
    private static final long serialVersionUID = 104; //Serialization ID
    
    private String location;    //Represents the location (room) of this exam.
    private Date startTime;     //represents the start time of the exam.
    
    //----------------END_OF_VARIABLE_DECLARATIONS------------------------//
    
    //-----------------METHODS_DEFINED_IN_CLASS_DIAGRAM-------------------//
    
    //NOTE: Added startTime
    /**
     * Constructor for a new Exam that has six parameters to 
     * represents it's name, weighting, assignment type, commence time,
     * location.
     * 
     * @param name          name of the exam
     * @param weighting     weighting of the exam
     * @param deadline      end time of the exam
     * @param location      location for this exam
     * @param startTime     start time of the exam
     * 
     * @throws IllegalDateException     an exception to be thrown if
     *                                  the deadline is in incorrect format
     * 
     * @throws NullInputException       an exception to be thrown if any of 
     *                                  the required parameters are passed as
     *                                  null
     * 
     * @throws InputOutOfRangeException an exception to be thrown if weighting
     *                                  is not in required range (0-100).
     * 
     * @throws EmptyInputException      an exception to be thrown if empty
     *                                  field for assignment name was provided
     */
    public Exam(String name, int weighting, Date deadline,
            String location, Date startTime) 
            throws IllegalDateException, NullInputException,
            EmptyInputException, InputOutOfRangeException {
        super(name, weighting, deadline);

        ErrorChecking.checkStringInput(location);
        ErrorChecking.checkMultiDates(startTime, deadline);

        this.location = location;
        this.startTime = startTime;
        
    }
    
    
    /**
     * An accessor method to return this Exam's location.
     * 
     * @return this Exam's location.
     */
    public String getLocation(){
        return this.location;
    }
    
    /**
     * An accessor method to return this Exam's start date and time.
     * 
     * @return this Exam's start date and time.
     */
    public Date getStartTime(){
        return this.startTime;
    }
    
    /**
     * A setter method to change the location for this Exam.
     * 
     * @param location location to be set for this Exam;
     * 
     * @throws EmptyInputException an exception to be thrown if no data in
     * input field is detected.
     * 
     * @throws NullInputException an exception to be thrown if 
     * provided input null.
     */
    public void setLocation(String location) throws EmptyInputException,
                                                       NullInputException{
        ErrorChecking.checkStringInput(location);        
        this.location = location;
    }
    
    //---------------END_OF_METHODS_DEFINED_IN_CLASS_DIAGRAM----------------//
    
}
