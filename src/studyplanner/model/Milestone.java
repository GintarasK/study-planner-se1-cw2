package studyplanner.model;

import utilities.ErrorChecking.ErrorChecking;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate milestone objects.
 */

public class Milestone implements Serializable{
    
    //----------------------VARIABLE_DECLARATIONS------------------------//
    private static final long serialVersionUID = 106;
    
    private String name;            //Represents the name of this milestone
    private Date deadline;          //Represents the deadline for
                                    //this milestone
    
    //Represents the list of tasks that are associated with this milestone
    private ArrayList<Task> taskList;
    
    
    //----------------END_OF_VARIABLE_DECLARATIONS------------------------//
    
    //-----------------METHODS_DEFINED_IN_CLASS_DIAGRAM-------------------//
    
    /**
     * A constructor for Milestone objects that has five fields to represent
     * its name, set date, deadline, assignment that this milestone attach to
     * and a list of tasks that are associated with this Milestone.
     * 
     * 
     * @param name          name of this Milestone
     * 
     * @param deadline      deadline of this Milestone
     * 
     * 
     * @param taskList      a list of Task objects that are associated with
     *                      this Milestone
     * 
     * 
     * @throws EmptyInputException      an exception to be thrown if
     *                                  no information for name is provided
     * 
     * @throws NullInputException       an exception to be thrown if any 
     *                                  of the required parameters are passed
     *                                  as null.
     * 
     * @throws IllegalDateException     an exception to be thrown if illegal
     *                                  date format is specified
     */
    public Milestone(String name, Date deadline, ArrayList<Task> taskList) 
                            throws EmptyInputException, NullInputException, 
                                                        IllegalDateException{
        
        ErrorChecking.checkStringInput(name);
        ErrorChecking.checkDate(deadline);
        ErrorChecking.checkMultiDates(new Date(), deadline);
        
        this.name = name;
        this.deadline = deadline;
        
        if(taskList == null){
            this.taskList = new ArrayList<>();
        }else{
            this.taskList = taskList;
        }
    }
    
    /**
     * A setter method to set the name for this Milestone.
     * 
     * @param name name to be set for this Milestone.
     * 
     * @throws EmptyInputException an exception to be thrown if
     * empty name string variable is passed.
     * 
     * @throws NullInputException an exception to be thrown if
     * name string variable is passed as a null.
     */
    public void setName(String name) throws EmptyInputException,
            NullInputException{
        ErrorChecking.checkStringInput(name);
        this.name = name;
    }
    
    /**
     * A mutator method that changes the deadline Date for this Milestone.
     * 
     * @param deadline deadline Date to be changed to.
     * 
     * @throws IllegalDateException an exception to be thrown if given
     * Date is invalid.
     * 
     * @throws NullInputException an exception to be thrown if any of the 
     * required parameters are passed as null.
     */
    public void setDeadline(Date deadline) throws IllegalDateException,
            NullInputException{
        ErrorChecking.checkDate(deadline);        
        this.deadline = deadline;
    }
    
    /**
     * A method that adds a specified Task object to the list of Task
     * objects that are associated with this Milestone.
     * 
     * @param task Task object to be added to the list of Task objects that are
     * associated with this Milestone.
     * 
     * @throws NullInputException an exception to be thrown if user provides 
     * a null input variable
     * 
     * @throws DuplicateDataException an exception to be thrown if provided
     * Task already exists in the list of Tasks within this Milestone
     */
    public void addTask(Task task) throws NullInputException,
                                             DuplicateDataException{
        ErrorChecking.checkIfNull(task);
        ErrorChecking.checkForDuplicateTask(this, task);
        this.taskList.add(task);
    }
    
    /**
     * A mutator method that removes a specified Task object from the list of
     * Task objects that are associated with this Milestone.
     * 
     * @param task Task object to be removed from the list of Task objects that 
     * are associated with this Milestone.
     * 
     * @throws NullInputException an exception to be thrown if user provides a
     * null input variable.
     * 
     * @throws ItemNotFoundException an exception to be thrown if given task
     * is not found in the note list
     */
    public void removeTask(Task task) throws NullInputException,
                                                        ItemNotFoundException{
        ErrorChecking.checkIfNull(task);
        if(!this.taskList.remove(task)){
            throw new ItemNotFoundException("Error on given task -");
        }
    }
    
    /**
     * An accessor method that returns the deadline Date of this Milestone.
     * 
     * @return deadline Date of this Milestone.
     */
    public Date getDeadline(){
        return this.deadline;
    }
    
    /**
     * An accessor method that returns the name of this Milestone.
     * 
     * @return name of this Milestone.
     */
    public String getName(){
        return this.name;
    }
    
    /**
     * An accessor method that returns the Task objects list that are 
     * associated with this Milestone.
     * 
     * @return list of Task objects that are associated with this Milestone.
     */
    public ArrayList<Task> getTaskList(){
        return this.taskList;
    }  
    
    //---------------END_OF_METHODS_DEFINED_IN_CLASS_DIAGRAM----------------//
}
