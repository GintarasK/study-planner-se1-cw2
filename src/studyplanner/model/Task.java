package studyplanner.model;

import utilities.ErrorChecking.ErrorChecking;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import utilities.exceptions.CurrentDateException;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.IllegalTypeException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate task objects.
 */

public class Task implements Serializable{
    
    //----------------------VARIABLE_DECLARATIONS------------------------//
    private static final long serialVersionUID = 108;
    
    //Represents the deadline for this task
    private Date deadline;          
    
    //Represents the name of this task
    private String name;            

    //Enum type variable to represent 
    //the type of this task
    public enum Type{                 
        READING, PROGRAMMING, WRITING   
    }
    
    //Represents type of this task
    private Type type;
    
    //is task critical or not ? true for yes, false for no
    private boolean critical;
    
    //Represents the goal for this task
    private int goal;               
    
    //Represents activities associated with this task
    private ArrayList<Activity> activityList;   
    
    //Represents milestones associated with this task
    private ArrayList<Milestone> milestoneList;   
    
    //Represents notes associated with this task
    private ArrayList<Note> noteList;   
    
    //when this Task was set by a user
    private Date startDate;
    
    //Represents a Task that is dependant on this Task
    private Task dependency;
    
    //represents a Task on which this Task is dependant on
    private Task reference;
    
    //----------------END_OF_VARIABLE_DECLARATIONS------------------------//

    //-----------------METHODS_DEFINED_IN_CLASS_DIAGRAM-------------------//
    
    /**
     * A constructor for Task objects that has five fields to represent
     * its set date, deadline, assignment that this task attached to, goal 
     * target and type.
     * 
     * @param startDate     start date for this task
     * @param deadline      deadline for this task
     * @param type          type of the task
     * @param goal          goal of the task
     * @param name          name of the task
     * @param critical      is task critical or not
     * 
     * @throws IllegalDateException     an exception to be thrown if illegal
     *                                  date format is specified
     * 
     * @throws NullInputException       an exception to be thrown if user
     *                                  uses null as any of the parameters
     * 
     * @throws InputOutOfRangeException an exception to be thrown if user
     *                                  provides goal that is out of range
     * 
     * @throws EmptyInputException      an exception to be thrown if empty
     *                                  input is provided
     * 
     * @throws CurrentDateException     an exception to be thrown if user 
     *                                  provides date that is earlier than 
     *                                  current machine time by more than
     *                                  one day.
     */
    public Task(Date startDate, Date deadline, Type type,
                int goal, String name, boolean critical) throws
            IllegalDateException, NullInputException, InputOutOfRangeException, 
            EmptyInputException, CurrentDateException{
        
        ErrorChecking.checkStartDate(startDate);
        ErrorChecking.checkDate(deadline);
        ErrorChecking.checkIntInput(goal, 1, 999);
        ErrorChecking.checkStringInput(name);
        ErrorChecking.checkMultiDates(startDate, deadline);
        ErrorChecking.checkIfNull(type);
        
        this.startDate = startDate;
        this.deadline = deadline;
        this.type = type;
        this.goal = goal;     
        this.name = name;
        this.critical = critical;
        this.activityList = new ArrayList<>();
        this.milestoneList = new ArrayList<>();
        this.noteList = new ArrayList<>();
    }
    
    /**
     * A mutator method to add a note to a list of notes associated with this
     * task.
     * 
     * @param note note to be added to the notes list.
     * 
     * @throws NullInputException an exception to be thrown if null is being
     * used as a parameter.
     * 
     * @throws DuplicateDataException an exception to be thrown if provided
     * Note already exists.
     */
    public void addNote(Note note) throws NullInputException,
            DuplicateDataException{
        ErrorChecking.checkIfNull(note);
        ErrorChecking.checkForDuplicateNote(note, noteList);
        this.noteList.add(note);
    }
    
    /**
     * A mutator method to change the deadline for this task.
     * 
     * @param deadline deadline date to be change to.
     * 
     * @throws IllegalDateException an exception to be thrown if incorrect
     * date input is provided.
     * 
     * @throws NullInputException an exception to be thrown if null is used
     * as parameter.
     */
    public void setDeadline(Date deadline) throws IllegalDateException,
                                              NullInputException{
        ErrorChecking.checkDate(deadline);
        ErrorChecking.checkMultiDates(this.startDate, deadline);
        this.deadline = deadline;
    }
    
    /**
     * A mutator method to set critical of this Task.
     * 
     * @param critical      critical to be set.
     */
    public void setCritical(boolean critical) {
        this.critical = critical;
    }
    
    /**
     * A mutator method to change the name for this task.
     * 
     * @param name name to be changed to.
     * 
     * @throws EmptyInputException an exception to be thrown if empty fields
     * were provided.
     * 
     * @throws NullInputException an exception to be thrown if
     * null name parameter is found.
     */
    public void setName(String name) throws EmptyInputException,
            NullInputException{
        ErrorChecking.checkStringInput(name);
        this.name = name;
    }
    
    /**
     * An accessor method to return the number of minutes this Task has taken.
     * 
     * @return number of minutes this Task has taken.
     */
    public int getTimeTaken(){
        if(this.activityList.isEmpty()){
            return 0;
        }else{
            int sum = 0;
            for(Activity a : this.activityList){
                sum += a.getTimeTaken();
            }            
            return sum;
        }
    }
    
    /**
     * A mutator method to change the goal for this task.
     * 
     * @param goal goal to be changed to.
     * 
     * @throws InputOutOfRangeException an exception to be thrown if
     * provided goal is out of range
     */
    public void setGoal(int goal) throws InputOutOfRangeException{
        ErrorChecking.checkIntInput(goal, 0, 999);
        this.goal = goal;
    }
    
    /**
     * An accessor method to return the value of critical of this Task.
     * 
     * @return  critical value of this Task.
     */
    public boolean getCritical(){
        return this.critical;
    }
    
    /**
     * A mutator method to add an activity to the list of activities this task
     * is associated with.
     * 
     * @param activity activity to add to the activity list.
     * 
     * @throws NullInputException an exception to be thrown if
     * null is passed as a parameter.
     * 
     * @throws DuplicateDataException an exception to be thrown if provided
     * activity already exists.
     */
    public void addActivity(Activity activity) 
            throws NullInputException, DuplicateDataException,
            IllegalTypeException{
        ErrorChecking.checkIfNull(activity);
        ErrorChecking.checkTaskType(activity, this);
        ErrorChecking.checkForDuplicateActivity(this, activity);
        this.activityList.add(activity);
    }
    
    /**
     * A mutator method to add an milestone to the list of milestones this task
     * is associated with.
     * 
     * @param milestone milestone to add to the activity list.
     * 
     * @throws DuplicateDataException an exception to be thrown if given
     * Milestone already exists.
     * 
     * @throws NullInputException an exception to be thrown if
     * null is passed as a parameter.
     */    
    public void assignMilestone(Milestone milestone) 
            throws NullInputException, DuplicateDataException{
        ErrorChecking.checkIfNull(milestone);
        ErrorChecking.checkForDuplicateMilestone(this, milestone);
        this.milestoneList.add(milestone);
    }
    
    /**
     * A mutator method to set a dependency for this Task.
     * 
     * @param task new dependency to be set.
     * 
     * @throws NullInputException an exception to be thrown if null is passed
     * as parameter.
     */  
    public void setDependency(Task task) throws NullInputException{
        ErrorChecking.checkIfNull(task);
        this.dependency = task;
    }
    
    /**
     * A method to remove a dependency this task is associated with.
     */  
    public void removeDependency(){
        this.dependency = null;
    }
    
    /**
     * A mutator method to remove an milestone from the list of milestones 
     * this task is associated with.
     * 
     * @param milestone milestone to remove from the activity list.
     * 
     * @throws NullInputException an exception to be thrown if 
     * null input is detected.
     * 
     * @throws ItemNotFoundException an exception to be thrown if given 
     * Task is not found
     */    
    public void removeMilestone(Milestone milestone) 
            throws  NullInputException, ItemNotFoundException{
        ErrorChecking.checkIfNull(milestone);
        if(!this.milestoneList.remove(milestone)){
            throw new ItemNotFoundException("Error on given milestone");
        }
    }
    
    /**
     * A mutator method to remove a note from the list of notes associated
     * with this task.
     * 
     * @param note note to be removed to the notes list.
     * 
     * @throws NullInputException an exception to be thrown if
     * null input is detected
     * 
     * @throws ItemNotFoundException an exception to be thrown if given 
     * Task is not found
     */
    public void removeNote(Note note) throws NullInputException, 
            ItemNotFoundException{
        ErrorChecking.checkIfNull(note);
        if(!this.noteList.remove(note)){
            throw new ItemNotFoundException("Error on given note");
        }
    }
    
    /**
     * A mutator method that removes all the Note objects that are associated 
     * with this Task.
     */
    public void removeNotes(){
        while(!this.noteList.isEmpty()){
            noteList.remove(0);
        }
    }
    
    /**
     * A method to remove an Activity from the Activities list associated with
     * this Task.
     * 
     * @param activity  activity to be removed from the activity list.
     * 
     * @throws NullInputException an exception to be thrown if
     * null input is detected
     * 
     * @throws ItemNotFoundException an exception to be thrown if given 
     * Task is not found
     */
    public void removeActivity(Activity activity) throws NullInputException,
            ItemNotFoundException{
        ErrorChecking.checkIfNull(activity);
        if(!this.activityList.remove(activity)){
            throw new ItemNotFoundException("Error on given activity");
        }
    }
    
    /**
     * An accessor method to return the list of notes that are associated
     * with this task.
     * 
     * @return list of notes that are associated with this task.
     */
    public ArrayList<Note> getNoteList(){
        return this.noteList;
    }
    
    /**
     * An accessor method to return the dependency of this Task.
     * 
     * @return the dependency of this Task.
     */
    public Task getDependency(){
        return this.dependency;
    }
    
    /**
     * An accessor method to return the start date of this task.
     * 
     * @return start date of this task.
     */
    public Date getStartDate(){
        return this.startDate;
    }
    
    /**
     * An accessor method to return deadline date of this task.
     * 
     * @return deadline date of this task.
     */
    public Date getDeadline(){
        return this.deadline;
    }
    
    /**
     * An accessor method to return the name of this task.
     * 
     * @return name of this task.
     */
    public String getName(){
        return this.name;
    }
    
    /**
     * An accessor method to return the type of this task.
     * 
     * @return type of this task.
     */
    public Type getType(){
        return this.type;
    }
    
    /**
     * An accessor method to return the goal target of this task.
     * 
     * @return goal target of this task.
     */
    public int getGoal(){
        return this.goal;
    }
    
    /**
     * An accessor method to return the list of activities that are associated
     * with this task.
     * 
     * @return list of activities that are associated with this task.
     */
    public ArrayList<Activity> getActivityList(){
        return this.activityList;
    }
    
    /**
     * An accessor method to return the list of milestones that are associated 
     * with this task.
     * 
     * @return list of milestones that are associated with this task.
     */
    public ArrayList<Milestone> getMilestoneList(){
        return this.milestoneList;
    }

    /**
     * An accessor method to check if this task has been completed.
     * 
     * @return true if task has been completed, false otherwise.
     */
    public boolean isCompleted(){
        int contribution = 0;
        for(Activity a : activityList){
            contribution += a.getContribution();
        }
        if(contribution >= this.goal){
            return true;
        }
        return false;
    }

    /**
     * An accessor method to evaluate and return current completion percentage
     * of this Task.
     * 
     * @return percentage of this Task completion.
     */
    public int getProgressValue() {
        int howMuch = 0;
        if (!this.getActivityList().isEmpty()) {
            for (Activity ac : this.getActivityList()) {
                howMuch += ac.getContribution();
            }
        }
        double percentage = ((double)howMuch / goal) * 100;
        
        //round since we don't need to be that precise
        if((int) percentage > 100){
            return 100;
        }
        
        return (int) percentage;
    }

    /**
     * An accessor method to return a Task, on which this Task is dependent.
     * 
     * @return 
     */
    public Task getReference(){
        return this.reference;
    }
    
    /**
     * A mutator method to change the reference Task for this Task.
     * 
     * @param reference dependency to change to.
     * 
     * @throws NullInputException an exception to be thrown if null value is
     * used.
     */
    public void setReference(Task reference) 
                        throws NullInputException{
        ErrorChecking.checkIfNull(reference);
        this.reference = reference;
    }
    
    /**
     * A mutator method to set this Tasks dependency to null.
     */
    public void removeReference(){
        this.reference = null;
    }
    
}
