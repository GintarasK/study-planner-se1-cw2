package studyplanner.controller;

import java.util.ArrayList;
import DetailedModulePage.MainFrame;
import studyplanner.StudyPlanner;
import studyplanner.model.Module;
import utilities.ErrorChecking.ErrorChecking;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate ModuleController.
 */

public class ModuleController {
    
    private static MainFrame activePage;
    
    /**
     * A method to provide information for a Detailed module page and open it.
     * 
     * @param header                            header info of a Module
     *
     * @throws ItemNotFoundException            if get methods does not find
     *                                          required items
     * 
     * @throws EmptyInputException              if an empty string is used
     *                                          as parameter
     * 
     * @throws NullInputException               if null is used as any of the
     *                                          parameters
     */
    public static void displayModule(String header)
            throws ItemNotFoundException, EmptyInputException, 
            NullInputException {

        String code = header.substring(0, 9);

        Module tempModule = getModule(code);
        ArrayList<String> moduleInfo = getModuleInfo(tempModule);
        ArrayList<String> assignmentInfo
                = AssignmentController.getAssignmentsInfo(tempModule);
        moduleInfo.addAll(assignmentInfo);
        
        
        //update the view
        StudyPlanner.getMainPageGUI().displayDetailedModulePage(moduleInfo);
    }
    
    /**
     * A method to return textual representation of the passed Module.
     * 
     * @param module            Module object
     * 
     * @return                  textual representation of Module
     */
    private static ArrayList<String> getModuleInfo(Module module) {
        ArrayList<String> moduleInfo = new ArrayList<>();

        moduleInfo.add(module.getName());
        moduleInfo.add(module.getDescription());
        moduleInfo.add(module.getCode());
        moduleInfo.add(module.getOrganiser());
        moduleInfo.add(String.valueOf(module.getCreditValue()));

        return moduleInfo;
    }
    
    /**
     * A method to return the total number of Modules.
     * 
     * @return                                  the total number of Module
     *                                          objects
     */
    public static int size(){
        return getModules().size();
    }
    
    /**
     * A method to return an ArrayList of all Module objects that are in the 
     * system.
     * 
     * @return                                  ArrayList of Module objects that
     *                                          are in the system
     */
    public static ArrayList<Module> getModules(){
        return SemesterProfileController.getModules();
    }
    
    /**
     * A method to return a Module (if exists) with the given code.
     * 
     * @param moduleCode                        module code
     * 
     * @return                                  Module object with the given 
     *                                          code
     * 
     * @throws ItemNotFoundException            if get methods does not find
     *                                          required items
     * 
     * @throws EmptyInputException              if an empty string is used
     *                                          as parameter
     * 
     * @throws NullInputException               if null is used as any of the
     *                                          parameters
     */
    public static Module getModule(String moduleCode) throws 
            EmptyInputException, NullInputException, ItemNotFoundException{
        ErrorChecking.checkStringInput(moduleCode);
        
        if(!ModuleController.isEmpty()){
            for(Module m : ModuleController.getModules()){
                if(m.getCode().equalsIgnoreCase(moduleCode)){
                    return m;
                }
            }
            throw new ItemNotFoundException("MODULE");
        }else{
            throw new ItemNotFoundException("MODULE");
        }
    }
    
    /**
     * A method to tell whether or not there are any Modules in the 
     * program.
     * 
     * @return      true if 0
     *              false if >0
     */
    public static boolean isEmpty(){
        return SemesterProfileController.getModules().isEmpty();
    }
    
    /**
     * An accessor method to return the active DetailedModulePage.
     * 
     * @return              active detailed module page 
     */
    public static MainFrame getActiveModulePage(){
        return StudyPlanner.getMainPageGUI().getDetailedModulePage();
    }
}
