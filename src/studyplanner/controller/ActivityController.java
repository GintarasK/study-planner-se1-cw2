package studyplanner.controller;

import java.util.ArrayList;
import studyplanner.model.Activity;
import studyplanner.model.Assignment;
import studyplanner.model.Module;
import studyplanner.model.Note;
import studyplanner.model.Task;
import utilities.ErrorChecking.ErrorChecking;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.IllegalFileException;
import utilities.exceptions.IllegalTypeException;
import utilities.exceptions.IllegalValueException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate ActivityController.
 */

public class ActivityController {

    /**
     * A method to create an Activity with the passed arguments from 
     * an ActivityForm.
     * 
     * @param activityName                      the name of the Activity
     * @param timeTakenp                        time taken for the Activity
     * @param contributionp                     Activity's contribution
     * 
     * @param notes                             ArrayList of notes for 
     *                                          the Activity
     * 
     * @param taskName                          Task associated with 
     *                                          the Activity
     * 
     * @param assignmentName                    name of the Assignment that the
     *                                          Activity belongs to
     * 
     * @throws IllegalValueException        if provided values (either 
     *                                      user input or JSON file) do not go 
     *                                      past error checks
     * 
     * @throws EmptyInputException          if a field cannot be empty
     * 
     * @throws DuplicateDataException
     * 
     * @throws IllegalTypeException         if the passed String did not match 
     *                                      any possible types
     * 
     * @throws NullInputException       if empty lists of tasks are provided
     *                                  in the constructor or setter methods
     * 
     * @throws IllegalFileException         if the program fails to 
     *                                      open/read the log file
     * 
     * @throws InputOutOfRangeException     an exception to be thrown if
     *                                      illegal contribution input is 
     *                                      detected (out of range)
     * 
     * @throws ItemNotFoundException        if the system fails to find
     *                                      an item in the list
     */
    public static void newActivity(String activityName, String timeTakenp, 
            String contributionp, ArrayList<String> notes, 
            String taskName, String assignmentName) 
            throws IllegalValueException,
            EmptyInputException, DuplicateDataException, NullInputException,
            IllegalTypeException, InputOutOfRangeException, 
            IllegalFileException, ItemNotFoundException {

        
        //get contribution
        ErrorChecking.isInteger(contributionp);
        int contribution = Integer.valueOf(contributionp);
        
        //get timeTaken
        ErrorChecking.isInteger(timeTakenp);
        int minutes = Integer.valueOf(timeTakenp);
        
        //get module
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        //get assignment
        Assignment assignment = AssignmentController.getAssignment(module, 
                                                                assignmentName);
        
        //check for duplicate name
        ErrorChecking.duplicateActivityName(assignment, activityName);
        
        //get task
        Task task = TaskController.getTask(assignment, taskName);

        //notes        
        ArrayList<Note> noteList = new ArrayList<>();
        if(notes != null){
            for(String s : notes){
                noteList.add(new Note(s));
            }
        }
        
        Activity activity = new Activity(activityName, task.getType(), 
                                          task, noteList, minutes, contribution);
        
        //assign activity to the task
        task.addActivity(activity);
        
        //update the log file
        FileController.updateLogFile();
        
        //update the view
        ModuleController.getActiveModulePage().updateTaskBar
                            (assignmentName, taskName, task.getProgressValue());
    }
    
    /**
     * A method to update an Activity with the passed arguments from the 
     * update Activity form.
     * 
     * @param assignmentName                    name of the Assignment the
     *                                          Activity belongs to
     * 
     * @param oldName                           old name of the Activity
     * 
     * @param newName                           name of the Activity to be set
     * 
     * @param timeTaken                         time taken of the Activity to
     *                                          be set
     * 
     * @param contribution                      contribution of the Activity
     *                                          to be set
     * 
     * @param notes                             note list of the Activity to
     *                                          be set
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws ItemNotFoundException            if Activity is not found on the
     *                                          system
     * 
     * @throws NullInputException               if null input is provided
     * 
     * @throws IllegalValueException            if instead of integers strings
     *                                          were provided
     * 
     * @throws InputOutOfRangeException         if integer input is out of range
     * 
     * @throws DuplicateDataException           if given data is a duplicate
     *                                          of already existing 
     * 
     * @throws IllegalFileException             if the program fails to 
     *                                          open/read the log file
     */
    public static void updateActivity(String assignmentName, String oldName, 
            String newName, String timeTaken, String contribution, 
            ArrayList<String> notes) throws 
            EmptyInputException,
            NullInputException, ItemNotFoundException,
            IllegalValueException, InputOutOfRangeException,
            DuplicateDataException, IllegalFileException{
        
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment
                                                    (module, assignmentName);
        
        Activity activity = getActivity(assignment, oldName);
        
        activity.setName(newName);
        ErrorChecking.isInteger(timeTaken);
        activity.setTimeTaken(Integer.parseInt(timeTaken));
        ErrorChecking.isInteger(contribution);
        activity.setContribution(Integer.parseInt(contribution));
        
        //remove notes
        activity.removeNotes();
        
        //reassign
        if(notes != null){
            for(String s : notes){
                activity.addNote(new Note(s));
            }
        }
        
        //update the log file
        FileController.updateLogFile();
        
        //update the view
        for(Task task : activity.getReferenceList()){
            ModuleController.getActiveModulePage().updateTaskBar
                            (assignmentName, task.getName(), task.getProgressValue());
        }
        
    }
    
    /**
     * A method to remove an Activity with the specified arguments provided by
     * the remove Activity form.
     * 
     * @param assignmentName                    name of the Assignment the
     *                                          Activity belongs to
     * 
     * @param taskName                          name of the Task the
     *                                          Activity belongs to
     * 
     * @param activityName                      name of the Activity that has
     *                                          to be deleted
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws ItemNotFoundException            if Activity is not found on the
     *                                          system
     * 
     * @throws NullInputException               if any of required data is 
     *                                          passed as a null.
     * 
     * @throws IllegalFileException             if the program fails to 
     *                                          open/read the log file
     */
    public static void removeActivity(String assignmentName, String taskName, 
            String activityName) throws EmptyInputException, 
            ItemNotFoundException, NullInputException, IllegalFileException {

        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment
                                                    (module, assignmentName);
        
        Task task = TaskController.getTask(assignment, taskName);
        
        Activity activity = getActivity(assignment, activityName);
        
        //remove activity from the task
        task.removeActivity(activity);
        
        //remove task from the reference list
        activity.removeTask(task);

        //update the log file
        FileController.updateLogFile();
        
        //update the view
        ModuleController.getActiveModulePage().updateTaskBar
                            (assignmentName, taskName, task.getProgressValue());
    }
    
    /**
     * A method to return an Activity (if exists) that has the given name and is
     * associated with the given Assignment.
     * 
     * @param assignment                    Assignment object that the Activity
     *                                      belongs to
     * 
     * @param activityName                  name of the Activity
     * 
     * @return                              object if exists
     * 
     * @throws NullInputException           if a parameter is passed as null
     * 
     * @throws EmptyInputException          if a field cannot be empty
     * 
     * @throws ItemNotFoundException        if Activity is not found on the
     *                                      system
     */
    public static Activity getActivity(Assignment assignment, String activityName) 
            throws NullInputException, EmptyInputException,
            ItemNotFoundException{
        
        ErrorChecking.checkStringInput(activityName);
        
        ErrorChecking.checkIfNull(assignment);
        
        ErrorChecking.checkListInput(assignment.getTaskList());
        
        for(Task t : assignment.getTaskList()){
            if(!t.getActivityList().isEmpty()){
                for(Activity a : t.getActivityList()){
                    if(a.getName().equalsIgnoreCase(activityName)){
                        return a;
                    }
                }
            }
        }

        throw new ItemNotFoundException("ACTIVITY");
    }
    
    /**
     * A method to attach an Activity to a Task.
     * 
     * @param assignmentName            name of Assignment that Task and
     *                                  Activity belong to
     * 
     * @param taskName                  name of the Task
     * 
     * @param activityName              name of the Activity
     * 
     * @throws EmptyInputException      if a field cannot be empty
     * 
     * @throws ItemNotFoundException    if Activity is not found on the
     *                                  system
     * 
     * @throws NullInputException       if any of required data is 
     *                                  passed as a null.
     * 
     * @throws IllegalDateException     if dates clash
     * 
     * @throws DuplicateDataException   if the Task already contains 
     *                                  the Activity
     * 
     * @throws IllegalFileException     if the program fails to 
     *                                  open/read the log file
     * 
     * @throws IllegalTypeException     if given Activity
     *                                  is incompatible with a given Task
     */
    public static void attachActivity(String assignmentName, String 
            taskName, String activityName) throws EmptyInputException,
            NullInputException, ItemNotFoundException, IllegalDateException, 
            DuplicateDataException, IllegalFileException, IllegalTypeException{
        
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment(module, assignmentName);
        
        Task task = TaskController.getTask(assignment, taskName);
        
        Activity activity = ActivityController.getActivity(assignment, activityName);
        
        ErrorChecking.checkMultiDates(activity.getSetDate(), task.getDeadline());
        
        task.addActivity(activity);
        activity.assignTask(task);
        
        FileController.updateLogFile();
        
        ModuleController.getActiveModulePage().updateTaskBar
                            (assignmentName, taskName, task.getProgressValue());
    }
    
    /**
     * A method to return textual representation of Activity for a specified
     * Assignment.
     * 
     * @param assignmentName                    the Assignment name
     * 
     * @param activityName                      the Activity name
     * 
     * @return                                  list of Strings that 
     *                                          represent the Activity
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws ItemNotFoundException            if the Module does not have any
     *                                          Assignments or does not have an
     *                                          Assignment with the given name           
     * 
     * @throws NullInputException               if any of the passed parameters
     *                                          is null
     */
    public static ArrayList<String> getFormInfo(String assignmentName,
            String activityName) throws EmptyInputException,
            ItemNotFoundException, NullInputException{
        
        ArrayList<String> result = new ArrayList<>();
        
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment(module,
                assignmentName);
        
        Activity activity = ActivityController.getActivity
                                                (assignment, activityName);
        
        result.add(activity.getName());
        result.add(activity.getSetDate().toString().substring(0, 10));
        result.add(String.valueOf(activity.getTimeTaken()));
        result.add(String.valueOf(activity.getContribution()));
        result.add(String.valueOf(activity.getNoteList().size()));
        
        //notes
        for(Note n : activity.getNoteList()){
            result.add(n.getContent());
        }
        
        return result;
    }
    
    /**
     * A method to get a list of Activity names that can be attached to
     * the specified Task.
     * 
     * @param assignmentName            name of Assignment that 
     *                                  the Task belongs to
     * 
     * @param taskName                  name of the Task
     * 
     * @return                          list of Activity names
     * 
     * @throws EmptyInputException      if a field cannot be empty
     * 
     * @throws ItemNotFoundException    if the Module does not have any
     *                                  Assignments or does not have an
     *                                  Assignment with the given name           
     * 
     * @throws NullInputException       if any of the passed parameters
     *                                  is null
     */
    public static ArrayList<String> getActivities(String assignmentName, 
            String taskName) throws EmptyInputException, NullInputException, 
            ItemNotFoundException{
        
        ArrayList<String> result = new ArrayList<>();
        
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment
                                                    (module, assignmentName);
        
        Task task = TaskController.getTask(assignment, taskName);
        
        for(Task t : assignment.getTaskList()){
            if(!t.equals(task)){
                for(Activity a : t.getActivityList()){
                    if(!result.contains(a.getName()) && 
                            !task.getActivityList().contains(a)){
                        result.add(a.getName());
                    }
                }
            }
        }
        
        return result;
    }
    
    
}
