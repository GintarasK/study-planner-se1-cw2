package studyplanner.controller;

import java.util.ArrayList;
import java.util.Date;
import studyplanner.model.Activity;
import studyplanner.model.Assignment;
import studyplanner.model.Milestone;
import studyplanner.model.Module;
import studyplanner.model.Note;
import studyplanner.model.Task;
import utilities.ErrorChecking.ErrorChecking;
import utilities.exceptions.CurrentDateException;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.IllegalDependencyException;
import utilities.exceptions.IllegalFileException;
import utilities.exceptions.IllegalTasksException;
import utilities.exceptions.IllegalTypeException;
import utilities.exceptions.IllegalValueException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate TaskController.
 */

public class TaskController {
    
    /**
     * A method to create a Task with the passed arguments from a TaskForm.
     * 
     * @param taskName                      the name of the Task
     * @param startDate                     the start date of the Task
     * @param deadline                      the deadline of the Task
     * @param taskType                      the type of the Task
     * @param taskGoal                      the goal of the Task
     * @param notes                         ArrayList of notes of the Task
     * 
     * @param dependencyName                name of dependency(if any) for 
     *                                      the Task
     * 
     * @param assignmentName                name of an Assignment which the Task
     *                                      is associated with
     * 
     * @param critical                      to tell whether or not the Task is
     *                                      critical to the Assignment
     * 
     * @throws IllegalDateException         if date is in incorrect format
     *                                      and/or does not pass error checking
     * 
     * @throws EmptyInputException          if a field cannot be empty
     * 
     * @throws IllegalValueException        if provided values (either 
     *                                      user input or JSON file) do not go 
     *                                      past error checks
     * 
     * @throws IllegalFileException         if the program fails to 
     *                                      open/read the file
     * 
     * @throws IllegalTasksException        if empty lists of tasks are provided
     *                                      in the constructor or setter methods
     * 
     * @throws NullInputException           if parameters are passed as null
     * 
     * @throws IllegalTypeException         if the passed String did not match 
     *                                      any possible types
     * 
     * @throws InputOutOfRangeException         if integer input is not in range
     * 
     * @throws DuplicateDataException           if provided data would produce
     *                                          duplicate entries
     * 
     * @throws CurrentDateException     an exception to be thrown if user 
     *                                  provides date that is earlier than 
     *                                  current machine time by more than
     *                                  one day.
     * 
     * @throws ItemNotFoundException    when searches are commenced,
     *                                  but required objects are not found
     */
    public static void newTask(String taskName, Date startDate, Date deadline, String taskType, 
            String taskGoal, ArrayList<String> notes, String dependencyName, 
            String assignmentName, String critical) throws IllegalDateException,
            IllegalValueException, IllegalTypeException, EmptyInputException,
            NullInputException, IllegalTasksException,
            InputOutOfRangeException, CurrentDateException,
            IllegalFileException, DuplicateDataException,
            ItemNotFoundException {
        
        //get goal
        ErrorChecking.isInteger(taskGoal);
        int goal = Integer.valueOf(taskGoal);
        
        //get type
        Task.Type type = taskType(taskType);

        //get module
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        //get assignment
        Assignment assignment = AssignmentController.getAssignment(module, 
                                                                assignmentName);
        
        //check for duplicate name
        ErrorChecking.checkForDuplicateName(assignment, taskName);
        
        //check for dat clashes with the assignment
        ErrorChecking.checkMultiDates(startDate, assignment.getDeadline());
        ErrorChecking.checkMultiDates(deadline, assignment.getDeadline());

        //critical
        boolean crit;
        if(critical.equalsIgnoreCase("yes")){
            crit = true;
        }else{
            crit = false;
        }
        
        Task task = new Task(startDate, deadline, type, goal, taskName, crit);
        
        assignDependency(assignment, task, dependencyName);

        addNotes(task, notes);

        assignment.addTask(task);
        
        //update the view
        ModuleController.getActiveModulePage().newTask
                                (assignmentName, taskName, 
                                        deadline.toString().substring(0, 10));
        
        FileController.updateLogFile();
    }
    
    /**
     * A method to update a Task with the specified parameters from the update
     * Task form.
     * 
     * @param assignmentName                    name of Assignment the Task
     *                                          belongs to
     * 
     * @param oldTaskName                       old name of the Task
     * 
     * @param newTaskName                       name of the Task to be set
     * 
     * @param deadline                          deadline of the Task to be set
     * 
     * @param goal                              goal of the Task to be set
     * 
     * @param referenceName
     * 
     * @param noteList                          list of notes of the 
     *                                          Task to be set
     * 
     * @param critical                          to tell whether or not the Task
     *                                          is critical to the Assignment
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws IllegalTasksException            if the program fails to find the
     *                                          specified Task
     * 
     * @throws IllegalDateException             if Date input is in wrong 
     *                                          format
     * 
     * @throws IllegalFileException             if the program fails to 
     *                                          open/read the log file
     * 
     * @throws IllegalValueException            if an integer field was filled
     *                                          with a string
     * 
     * @throws NullInputException               if any of input parameters 
     *                                          are null
     * 
     * @throws InputOutOfRangeException         if integer input is not 
     *                                          in range
     * 
     * @throws DuplicateDataException           if constructed data would be
     *                                          duplicates
     * 
     * @throws ItemNotFoundException            when searches are commenced,
     *                                          but required objects 
     *                                          are not found
     */
    public static void updateTask(String assignmentName, String oldTaskName, 
            String newTaskName, Date deadline, String goal, 
            String referenceName, ArrayList<String> noteList, String critical) throws 
            EmptyInputException,  IllegalTasksException, 
            IllegalDateException, IllegalValueException, IllegalFileException,
            NullInputException, InputOutOfRangeException, DuplicateDataException,
            ItemNotFoundException{
        
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment
                                                    (module, assignmentName);
        
        if(!oldTaskName.equals(newTaskName)){
            ErrorChecking.checkForDuplicateName(assignment, newTaskName);
        }
        
        Task task = TaskController.getTask(assignment, oldTaskName);
        
        //check clashes with dates
        ErrorChecking.checkMultiDates(task.getStartDate(), deadline);
        //ErrorChecking.checkMultiDates(deadline, assignment.getDeadline());
        
        //check deadline clashes with references
        if(referenceName != null){
            try {
                for (Task t : assignment.getTaskList()) {
                    if (t.getName().equalsIgnoreCase(referenceName)) {
                        ErrorChecking.checkMultiDates(t.getDeadline(), deadline);
                    }
                }
            } catch (Exception e) {
                throw new IllegalTasksException();
            }
        }
        
        //check deadline clashes with dependencies
        if(task.getDependency() != null){
            try {
            ErrorChecking.checkMultiDates(deadline, task.getDependency().getDeadline());
        
            } catch (Exception e) {
            throw new IllegalTasksException();
            }
        }
        
        //check deadline clashes with milestones
        for(Milestone m : task.getMilestoneList()){
            ErrorChecking.checkMultiDates(deadline, m.getDeadline());
        }
                        
        //set parameters
        task.setName(newTaskName);
        task.setDeadline(deadline);
        ErrorChecking.isInteger(goal);
        task.setGoal(Integer.parseInt(goal));
        if(critical.equalsIgnoreCase("Yes")){
            task.setCritical(true);
        }else{
            task.setCritical(false);
        }

        //reassign
        task.removeNotes();
        for (String s : noteList) {
            task.addNote(new Note(s));
        }
        
        if(task.getReference() != null){
            task.getReference().removeDependency();
        }
        
        if(referenceName != null){
            //dependency
            assignDependency(assignment, task, referenceName);
        } else {
            task.removeReference();
        }
        
        //update the log file
        FileController.updateLogFile();
        
        //update the view
        ModuleController.getActiveModulePage().updateTask(assignmentName, 
                oldTaskName, newTaskName, deadline.toString().substring(0,10));
    }
    
    /**
     * A method to remove a Task with the specified parameters from the remove
     * Task form.
     * 
     * @param assignmentName                    name of Assignment the Task
     *                                          belongs to
     * 
     * @param milestoneName                     name of Milestone the Task
     *                                          belongs to
     * 
     * @param taskName                          name of the Task
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws IllegalFileException             if the program fails to 
     *                                          open/read the log file
     * 
     * @throws NullInputException               if any of the inputs are null
     * 
     * @throws IllegalDependencyException       when change/deletion of a task
     *                                          is not possible due to conflicts
     *                                          with its dependency
     * 
     * @throws ItemNotFoundException            if get methods do not find
     *                                          an item
     */
    public static void removeTask(String assignmentName, String milestoneName, 
            String taskName) throws EmptyInputException, IllegalFileException,
            NullInputException, ItemNotFoundException, IllegalDependencyException{
        
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment
                                                    (module, assignmentName);

        Milestone milestone = null;
        if(milestoneName != null){
            milestone = MilestoneController.getMilestone
                                                    (assignment, milestoneName);
        }
        
        Task task = TaskController.getTask(assignment, taskName);
        
        if(task.getMilestoneList().isEmpty() && task.getDependency() != null){
            throw new IllegalDependencyException("You cannot delete the task "
                    + "as task \"" + task.getDependency().getName() + "\" is "
                    + "dependant on it. Please, delete the relationship and "
                    + "try again.");
        }
        
        if (milestone != null) {
            milestone.removeTask(task);
            task.removeMilestone(milestone);
        } else {
            if(task.getReference() != null){
                task.getReference().removeDependency();
            }
            
            assignment.removeTask(task);
        }
        
        //update the log file
        FileController.updateLogFile();
        
        //update the view
        ModuleController.getActiveModulePage().removeTask(assignmentName, milestoneName, taskName);
    }
    
    /**
     * A method to get the name of dependency of the specified Task.
     * 
     * @param assignmentName            name of Assignment that the Task
     *                                  belongs to
     * 
     * @param taskName                  name of the Task
     * 
     * @return                          name of dependency of the Task
     * 
     * @throws EmptyInputException      if a field cannot be empty
     * 
     * @throws NullInputException       if any of the inputs are null
     * 
     * @throws ItemNotFoundException    if get methods do not find
     *                                  an item 
     */
    public static String getDependency(String assignmentName, String taskName) 
            throws  EmptyInputException, NullInputException, ItemNotFoundException{
        
        Module module = ModuleController.getModule
        (ModuleController.getActiveModulePage().getInformationPart().getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment(module, assignmentName);
        
        if(TaskController.getTask(assignment, taskName).getReference() == null){
            return null;
        }
        
        return TaskController.getTask(assignment, taskName).getReference().getName();
    }
    
    /**
     * A method to return an ArrayList of Task names for a specific Assignment,
     * ignoring those that are in the specified Milestone.
     * 
     * @param assignmentName    Assignment name
     * @param milestoneName     Milestone name
     * 
     * @return                  ArrayList of Task names
     * 
     * @throws EmptyInputException              if string input is empty string
     * 
     * @throws NullInputException               if input is null
     * 
     * @throws ItemNotFoundException            if get methods do not find
     *                                          an item 
     */
    public static ArrayList<String> getTaskList(String assignmentName, 
            String milestoneName) throws EmptyInputException, 
            NullInputException,ItemNotFoundException{
        
        ArrayList<String> info = new ArrayList<>();
        
        Module module = ModuleController.getModule
        (ModuleController.getActiveModulePage().getInformationPart().
                                                               getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment(module, 
                                                                assignmentName);

        for (Task t : assignment.getTaskList()) {
            if(!MilestoneController.isInTask(t, milestoneName)){
                info.add(t.getName());
            }
        }

        return info;
    }
    
    /**
     * A method to get a list task names for the specified Assignment that don't
     * have dependencies (also exclude the passed one).
     * 
     * @param assignmentName                    name of Assignment
     * 
     * @param taskName                          name of Task to ignore
     * 
     * @return                                  a list task names for the 
     *                                          specified Assignment that don't
     *                                          have dependencies 
     *                                          (also exclude the passed one)
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws NullInputException               if input is passed as null
     * 
     * @throws ItemNotFoundException            if get methods do not find
     *                                          an item 
     */
    public static ArrayList<String> getDependencyList(String assignmentName, 
            String taskName) throws EmptyInputException, NullInputException, 
            ItemNotFoundException{
        
        //get a list of all tasks apart from the sent one
        ArrayList<String> result;
        result = getTaskList(assignmentName, null);
        result.remove(taskName);
        
        Module module = ModuleController.getModule
        (ModuleController.getActiveModulePage().getInformationPart().
                                                               getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment(module, 
                                                                assignmentName);
        
        //get rid of tasks that already have dependencies
        for(Task t : assignment.getTaskList()){
            if(t.getDependency() != null){
                result.remove(t.getName());
            }
        }
        
        return result;
    }
    
    /**
     * A method to attach a Task to a Milestone.
     * 
     * @param assignmentName        Assignment name which Task and Milestone 
     *                              belong to
     * 
     * @param milestoneName         name of the Milestone
     * @param taskName              name of the Task
     * 
     * @throws IllegalFileException if the program fails to 
     *                              open/read the file
     * 
     * @throws IllegalDateException             if there is a date clash
     * 
     * @throws EmptyInputException              if a field was left empty
     * 
     * @throws NullInputException               if nulls are used for parameters
     * 
     * @throws ItemNotFoundException            if get methods do not find 
     *                                          a required element
     * 
     * @throws DuplicateDataException           if constructed data would be 
     *                                          duplicate of already existing 
     *                                          one
     */
    public static void attachTask(String assignmentName,
            String milestoneName, String taskName) throws IllegalFileException, 
            IllegalDateException, EmptyInputException, 
            NullInputException, ItemNotFoundException, DuplicateDataException {
        
        Module tempModule = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        Assignment tempAs = AssignmentController.getAssignment(tempModule,
                assignmentName);
        
        Milestone tempMil = MilestoneController.getMilestone(tempAs, 
                milestoneName);
        
        if (tempMil != null) {
            if (!tempAs.getTaskList().isEmpty()) {
                for (Task t : tempAs.getTaskList()) {
                    if (t.getName().equalsIgnoreCase(taskName)) {
                        
                        //check for deadline clashes
                        if(t.getDeadline().getMonth() == tempMil.getDeadline().getMonth() &&
                                t.getDeadline().getDate() == tempMil.getDeadline().getDate()){
                            throw new IllegalDateException();
                        }
                        
                        ErrorChecking.checkMultiDates(t.getDeadline(), tempMil.getDeadline());
                        
                        //set relationships
                        tempMil.addTask(t);
                        t.assignMilestone(tempMil);
                        
                        //update the view
                        ModuleController.getActiveModulePage().attachTask(
                                assignmentName, taskName, milestoneName);
                        
                        FileController.updateLogFile();
                        
                        break;
                    }
                }
            }
        }
    }
    
    /**
     * A method to provide information to a View class. Is used to get all
     * needed information about a Task in order to show it in a Task Form.
     * 
     * @param assignmentName                    assignment name of the 
     *                                          Assignment that the Task belongs
     *                                          to
     * 
     * @param taskName                          name of the Task
     * 
     * @return                                  ArrayList containing all the 
     *                                          needed information about the
     *                                          Task
     * 
     * @throws EmptyInputException              if string input is passed 
     *                                          as an empty string
     * 
     * @throws NullInputException               if any of the parameters is 
     *                                          null
     * 
     * @throws ItemNotFoundException            if get methods do not find
     *                                          an item 
     */
    public static ArrayList<String> getFormInfo(String assignmentName, 
            String taskName) 
            throws  
            EmptyInputException, NullInputException, ItemNotFoundException{
        
        ArrayList<String> info = new ArrayList<>();
        
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        Assignment assignment = AssignmentController.getAssignment
                                                       (module, assignmentName);
        Task task = getTask(assignment, taskName);
        
        info.add(task.getName());
        info.add(task.getDeadline().toString().substring(0, 10));
        info.add(task.getStartDate().toString().substring(0, 10));
        info.add(task.getType().toString());
        info.add(String.valueOf(task.getGoal()));
        if(task.getCritical()){
            info.add("yes");
        }else{
            info.add("no");
        }
        
        //notes (if any)
        info.add(String.valueOf(task.getNoteList().size()));
        if(!task.getNoteList().isEmpty()){
            for(Note n : task.getNoteList()){
                info.add(n.getContent());
            }
        }
        
        //activites (if any)
        info.add(String.valueOf(task.getActivityList().size()));
        if(!task.getActivityList().isEmpty()){
            for(Activity a : task.getActivityList()){
                info.add(a.getName());
                info.add(a.getSetDate().toString().substring(0,10));
                info.add(String.valueOf(a.getContribution()));
            }
        }
        
        //dependency (if any)
        if(task.getReference()== null){
            info.add("0");
        }else{
            info.add("1");
            info.add(task.getReference().getName());
            info.add(String.valueOf(task.getReference().getProgressValue()));
        }
        
        //value for progress bar
        info.add(String.valueOf(task.getProgressValue()));
        
        return info;
    }
    
    /**
     * A method to get a deadline of the specified Task.
     * 
     * @param assignmentName                    name of Assignment the Task
     *                                          belongs to
     * 
     * @param taskName                          name of the Task
     * 
     * @return                                  deadline of the Task
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws ItemNotFoundException            if get methods do not find
     *                                          an item 
     * 
     * @throws NullInputException               if input data is passed as null
     */
    public static Date getDeadline(String assignmentName, String taskName)
            throws  EmptyInputException, NullInputException, 
            ItemNotFoundException {

        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());

        Assignment assignment = AssignmentController.getAssignment
                                                    (module, assignmentName);

        return TaskController.getTask(assignment, taskName).getDeadline();
    }
    
    public static Date getStartDate(String assignmentName, String taskName) 
            throws ItemNotFoundException, EmptyInputException,
            NullInputException {
        
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());

        Assignment assignment = AssignmentController.getAssignment
                                                    (module, assignmentName);

        return TaskController.getTask(assignment, taskName).getStartDate();
    }
    
    /**
     * A method to tell how many tasks without milestones are in the specified
     * Assignment and give the textual representation of them.
     * 
     * @param a             Assignment
     * 
     * @return              number of tasks at index 0
     *                      then task info if there were found any
     */
    public static ArrayList<String> tasksWithoutMilestonesInfo(Assignment a) {
        ArrayList<String> info = new ArrayList<>();

        //how many tasks
        int sum = 0;

        //actual tasks info
        if (!a.getTaskList().isEmpty()) {
            for (Task t : a.getTaskList()) {
                if (t.getMilestoneList().isEmpty()) {
                    info.add(t.getName());
                    info.add(t.getDeadline().toString().substring(0, 10));
                    info.add(t.getStartDate().toString());

                    //how much completed
                    info.add(String.valueOf(t.getProgressValue()));
                    
                    sum++;
                }
            }
        }

        info.add(0, String.valueOf(sum));

        return info;
    }
    
    /**
     * A method to tell how many tasks without milestones are in the specified
     * Assignment and give the textual representation of them.
     * 
     * @param moduleCode        module code of a Module which the Assignment 
     *                          belongs to
     * 
     * @param assignmentName    name of the Assignment
     * 
     * @return                  number of tasks at index 0
     *                          then task info if there were found any
     * 
     * @throws EmptyInputException              if string input is passed as       
     *                                          an empty string
     * 
     * @throws NullInputException               if any of the input parameters
     *                                          are passed as null
     * 
     * @throws ItemNotFoundException            if get methods do not find
     *                                          an item 
     */
    public static ArrayList<String> tasksWithoutMilestonesInfo
        (String moduleCode, String assignmentName) throws EmptyInputException, 
                NullInputException, ItemNotFoundException{
                                     
        Module tempModule = ModuleController.getModule(moduleCode);
        Assignment tempAs = AssignmentController.getAssignment(tempModule, 
                                                                assignmentName);
        return tasksWithoutMilestonesInfo(tempAs);
    }
    
    /**
     * A method to tell how many tasks are in the specified Milestone
     * and give the textual representation of them.
     * 
     * @param m             Milestone
     * 
     * @return              number of tasks at index 0
     *                      then task info if there were found any
     */
    public static ArrayList<String> tasksInfo(Milestone m) {
        ArrayList<String> info = new ArrayList<>();

        //how many tasks 
        int sum = 0;

        //actual tasks info
        if (m != null && !m.getTaskList().isEmpty()) {
            for (Task t : m.getTaskList()) {
                info.add(t.getName());
                info.add(t.getDeadline().toString().substring(0, 10));
                info.add(t.getStartDate().toString());

                //percentage how much completed
                info.add(String.valueOf(t.getProgressValue()));
                
                sum++;
            }
        }

        info.add(0, String.valueOf(sum));

        return info;
    }
    
    /**
     * A method to return a list of all possible Types of Tasks.
     * 
     * @return  ArrayList of all possible Types of Tasks
     */
    public static ArrayList<String> getTypes(){
        ArrayList<String> list = new ArrayList<>();
        for(Task.Type t : Task.Type.values()){
            list.add(t.toString());
        }
        return list;
    }
    
    /**
     * A method to return a Task type based on the passed String.
     * 
     * @param taskType                  textual representation of a type, String
     * 
     * @return                          type, Task.Type object
     * 
     * @throws IllegalTypeException     if the passed String did not match any
     *                                  possible types
     */
    private static Task.Type taskType(String taskType) 
                                                    throws IllegalTypeException{
        
        if(taskType.equalsIgnoreCase("PROGRAMMING")){
            return Task.Type.PROGRAMMING;
        }else if(taskType.equalsIgnoreCase("WRITING")){
            return Task.Type.WRITING;
        }else if(taskType.equalsIgnoreCase("READING")){
            return Task.Type.READING;
        }
        
        throw new IllegalTypeException("Incorrect task type was"
                + " detected. Please, check the type and try again."
                + " If error occurs again, please check your semester"
                + " data file or contact system administrator.");
    }

    /**
     * A method to assign dependency. 
     * Task will become dependant on referenceName
     * 
     * @param assignment                Assignment which both tasks belong to
     * 
     * @param task                      Task that needs dependency to be 
     *                                  assigned to
     * 
     * @param referenceName             name of a Task that represents 
     *                                  dependency
     * 
     * @throws IllegalTasksException    if there is a conflict between deadlines
     */
    private static void assignDependency(Assignment assignment, Task task,
            String referenceName) throws IllegalTasksException, 
            IllegalFileException, NullInputException, ItemNotFoundException {

        if (referenceName != null && !referenceName.isEmpty()) {
            if (!assignment.getTaskList().isEmpty()) {
                for (Task t : assignment.getTaskList()) {
                    if (t.getName().equalsIgnoreCase(referenceName)) {
                        if (t.getDeadline().before(task.getDeadline())) {
                            t.setDependency(task);
                            task.setReference(t);
                            FileController.updateLogFile();
                            return;
                        } else {
                            throw new IllegalTasksException();
                        }
                    }
                }
                
                throw new ItemNotFoundException("DEPENDENCY");
            }
        }
    }
    
    /**
     * A method to create notes from the passed ArrayList and add them to the
     * passed Task.
     * 
     * @param task          Task object
     * 
     * @param notes         ArrayList of Strings where each String represents
     *                      single Note's content
     */
    private static void addNotes(Task task, ArrayList<String> notes) 
            throws EmptyInputException, NullInputException,
            DuplicateDataException {
        
        if(notes != null && !notes.isEmpty()){
            for(String s : notes){
                Note n = new Note(s);
                task.addNote(n);
            }
        }
    }
    
    /**
     * A method to find a Task in the given Assignment by specified task name.
     * 
     * @param assignment                    Assignment that the Task belongs to
     * @param taskName                      name of the Task
     * 
     * @return                              Task object
     * 
     * @throws EmptyInputException          if any of the string input data is
     *                                      an empty string
     * 
     * @throws NullInputException           if any of the input parameters are
     *                                      null
     * 
     * @throws ItemNotFoundException        if the task could not be found
     */
    public static Task getTask(Assignment assignment, String taskName)
            throws EmptyInputException, NullInputException, ItemNotFoundException {

        ErrorChecking.checkStringInput(taskName);
        ErrorChecking.checkIfNull(assignment);
        ErrorChecking.checkListInput(assignment.getTaskList());

        for (Task t : assignment.getTaskList()) {
            if (t.getName().equalsIgnoreCase(taskName)) {
                return t;
            }
        }

        throw new ItemNotFoundException("");
    }
    
}
