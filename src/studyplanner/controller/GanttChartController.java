package studyplanner.controller;

import java.util.ArrayList;
import java.util.Date;
import studyplanner.model.Assignment;
import studyplanner.model.Milestone;
import studyplanner.model.Module;
import studyplanner.model.Task;
import utilities.ErrorChecking.ErrorChecking;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

public class GanttChartController {

    /**
     * A method to get information that is necessary to build a Gantt chart
     * for the active module page.
     * 
     * @return                          information about Assignments, Tasks 
     *                                  and Milestones in the active Module
     * 
     * @throws EmptyInputException      when a required field cannot be empty
     * 
     * @throws NullInputException       when null value error checking is done
     *     
     * @throws ItemNotFoundException    when searches are commenced,
     *                                  but required objects are not found
     */
    public static ArrayList<String> getGanttChartInfo()
            throws EmptyInputException, NullInputException,
            ItemNotFoundException {

        ArrayList<String> result = new ArrayList<>();

        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());

        //how many assignments
        result.add(String.valueOf(module.getAssignmentList().size()));
        
        for(Assignment assignment : module.getAssignmentList()){
            //assignment details
            result.addAll(getAssignmentInfo(assignment));
        }

        return result;
    }

    /**
     * A method to get information that is necessary to build a Gantt chart
     * for the specified Assignment.
     * 
     * @param assignment                Assignment object
     * 
     * @return                          information about Tasks and Milestones
     *                                  in the specified Assignment
     * 
     * @throws NullInputException       when null value error checking is done
     */
    private static ArrayList<String> getAssignmentInfo(Assignment assignment)
            throws NullInputException {

        ErrorChecking.checkIfNull(assignment);

        ArrayList<String> result = new ArrayList<>();
        ArrayList<Task> addedTasks = new ArrayList<>();

        //name
        result.add(assignment.getName());

        //earliest date
        Date earliestDate = new Date();
        for (Task t : assignment.getTaskList()) {
            if (earliestDate.after(t.getStartDate())) {
                earliestDate = t.getStartDate();
            }
        }

        //add earliest date
        result.add(String.valueOf((earliestDate.getDate()) - 1) + "/"
                + String.valueOf((earliestDate.getMonth()) + 1));

        //latest date
        Date latestDate = new Date();
        for (Task t : assignment.getTaskList()) {
            if (latestDate.before(t.getDeadline())) {
                latestDate = t.getDeadline();
            }
        }

        for (Milestone m : assignment.getMilestoneList()) {
            if (latestDate.before(m.getDeadline())) {
                latestDate = m.getDeadline();
            }
        }

        //duration in days
        Date d = new Date(latestDate.getTime() - earliestDate.getTime());
        int days = (int) Math.floor(d.getTime() / 86400000);
        result.add(String.valueOf(days + 2));

        //how many tasks in the assignment
        result.add(String.valueOf(assignment.getTaskList().size()));
        
        //tasks that don't have any relationships
        for(Task task : assignment.getTaskList()){
            if(task.getReference() == null && task.getDependency() == null){
                result.addAll(getTaskInfo(task));
                addedTasks.add(task);
            }
        }
        
        //task
        for(Task task : assignment.getTaskList()){
            if(!addedTasks.contains(task) && task.getReference() == null){
                result.addAll(getTaskInfo(task));
                addedTasks.add(task);
            }
        }
        
        //how many milestones in the assignment
        result.add(String.valueOf(assignment.getMilestoneList().size()));

        //sort milestones by deadlines
        //bubble sort
        for(int i = assignment.getMilestoneList().size() - 1; i >= 0; i--){
            for(int j = 1; j <= i; j++){
                if(assignment.getMilestoneList().get(j - 1).getDeadline().after(assignment.getMilestoneList().get(j).getDeadline())){
                    Milestone temp = assignment.getMilestoneList().get(j - 1);
                    assignment.getMilestoneList().set((j - 1), assignment.getMilestoneList().get(j));
                    assignment.getMilestoneList().set(j, temp);
                }
            }
        }
        
        //add milestones' info
        for (Milestone m : assignment.getMilestoneList()) {
            //milestone details
            result.addAll(getMilestoneInfo(m));
        }

        return result;
    }

    /**
     * A method to get information that is necessary to build a Gantt chart
     * for the specified Milestone.
     * 
     * @param milestone                 Milestone object
     * 
     * @return                          information about Tasks in the
     *                                  specified Milestone
     * 
     * @throws NullInputException       when null value error checking is done
     */
    private static ArrayList<String> getMilestoneInfo(Milestone milestone)
            throws NullInputException {

        ErrorChecking.checkIfNull(milestone);

        ArrayList<String> result = new ArrayList<>();

        //name
        result.add(milestone.getName());

        //deadline
        result.add(milestone.getDeadline().toString().substring(0, 10));

        return result;
    }

    /**
     * A method to get information that is necessary to build a Gantt chart
     * for the specified Task.
     * 
     * @param milestone             Milestone object that the specified 
     *                              Tasks belongs to
     * 
     * @param task                  Task object
     * 
     * @return                      information about the Task
     * 
     * @throws NullInputException   when null value error checking is done
     */
    private static ArrayList<String> getTaskInfo(Task task)
            throws NullInputException {

        ArrayList<String> result = new ArrayList<>();

        ErrorChecking.checkIfNull(task);

        //name
        result.add(task.getName());

        //startDate
        result.add(task.getStartDate().toString().substring(0, 10));

        //deadline
        result.add(task.getDeadline().toString().substring(0, 10));
        
        //critical
        if(task.getCritical()){
            result.add("yes");
        }else{
            result.add("no");
        }

        //duration in days
        Date d = new Date(task.getDeadline().getTime() - task.getStartDate().getTime());
        int days = (int) Math.floor(d.getTime() / 86400000);
        result.add(String.valueOf(days));

        //how much completed
        result.add(String.valueOf(task.getProgressValue()));
        
        //reference details
        if (task.getReference() != null) {
            result.add(task.getReference().getName());
        } else {
            result.add(null);
        }

        //dependency details
        if (task.getDependency() != null) {
            result.add(task.getDependency().getName());
        } else {
            result.add(null);
        }

        //milestone details
        
        //how many milestones
        result.add(String.valueOf(task.getMilestoneList().size()));
        
        for(Milestone milestone : task.getMilestoneList()){
            result.add(milestone.getName());
        }

        if(task.getDependency() != null){
            result.addAll(getTaskInfo(task.getDependency()));
        }
        
        return result;
    }
}
