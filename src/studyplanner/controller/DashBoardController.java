package studyplanner.controller;

import java.util.ArrayList;
import java.util.Date;
import studyplanner.model.Activity;
import studyplanner.model.Assignment;
import studyplanner.model.Coursework;
import studyplanner.model.Exam;
import studyplanner.model.Milestone;
import studyplanner.model.Module;
import studyplanner.model.SemesterProfile;
import studyplanner.model.Task;

public class DashBoardController {

    /**
     * A method to get all upcoming deadlines that are in the given Date range 
     * for the current Semester Profile.
     * 
     * @param earliestDate      start date of the date range
     * 
     * @param latestDate        end date of the date range
     * 
     * @return                  textual representation of the upcoming deadlines
     */
    public static ArrayList<String> getUpcomingDeadlines(Date earliestDate,
            Date latestDate) {

        Date localDate = new Date();
        ArrayList<String> result = new ArrayList<>();

        if (!SemesterProfileController.isEmpty()) {
            for (SemesterProfile semP : SemesterProfileController.getSemProfiles()) {
                if (localDate.before(semP.getEndDate())
                        && localDate.after(semP.getStartDate())) {

                    //how many modules
                    result.add(String.valueOf(semP.getModuleList().size()));
                    for (Module module : semP.getModuleList()) {
                        //add module info
                        result.addAll(getModuleInfo(module, earliestDate, latestDate));
                    }

                    return result;
                }
            }
        }

        return result;
    }
    
    /**
     * A method to get a list of modules which contains module name and hours
     * spent on it between earliestDate and latestDate.
     * 
     * @param earliestDate      start date
     * 
     * @param latestDate        finish date
     * 
     * @return                  a list of modules which contains module name 
     *                          and hours spent on it between 
     *                          earliestDate and latestDate
     */
    public static ArrayList<String> getHoursSpent(Date earliestDate, Date latestDate){
        
        Date localDate = new Date();
        ArrayList<String> result = new ArrayList<>();
        
        if (!SemesterProfileController.isEmpty()) {
            for (SemesterProfile semP : SemesterProfileController.getSemProfiles()) {
                if (localDate.before(semP.getEndDate())
                        && localDate.after(semP.getStartDate())) {
                    
                    for (Module module : semP.getModuleList()) {
                        //add module name
                        result.add(module.getName());
                        
                        //add hours spent
                        int hours = 0;
                        for(Assignment assignment : module.getAssignmentList()){
                            ArrayList<Activity> activityList = new ArrayList<>();
                            for(Task task : assignment.getTaskList()){
                                for(Activity activity : task.getActivityList()){
                                    if(!activityList.contains(activity) && 
                                            activity.getSetDate().
                                                    after(earliestDate) && 
                                            activity.getSetDate().
                                                    before(latestDate)){
                                        hours += activity.getTimeTaken();
                                    }
                                }
                            }
                        }
                        result.add(String.valueOf(hours));
                    }
                }
            }   
        }
        
        return result;
    }
    
    /**
     * A method to provide Assignment/Milestone/Task information for the 
     * Dashboard for a specified day.
     * 
     * @param date      specified day
     * 
     * @return          Assignment/Milestone/Task information
     */
    public static ArrayList<String> getDayInfo(String date){
        ArrayList<String> milestoneList = new ArrayList<>();
        ArrayList<String> taskList = new ArrayList<>();
        ArrayList<String> assignmentList = new ArrayList<>();
        ArrayList<String> result = new ArrayList<>();
        
        int day = Integer.parseInt(date.substring(0, 2));
        int month = Integer.parseInt(date.substring(3, date.length()));
        
        for (Module module : SemesterProfileController.getModules()) {
            for (Assignment assignment : module.getAssignmentList()) {

                if (assignment.getDeadline().getDate() == day && assignment.
                        getDeadline().getMonth() == (month - 1)) {

                    //add assignment name
                    assignmentList.add(assignment.getName());

                    //add assignment deadline/start date
                    if (assignment instanceof Exam) {
                        Exam ex = (Exam) assignment;
                        assignmentList.add(ex.getStartTime().toString().
                                substring(11, 16));
                    } else {
                        Coursework cw = (Coursework) assignment;
                        assignmentList.add(cw.getDeadline().toString().
                                substring(11, 16));
                    }
                }

                //loop for milestones
                for (Milestone milestone : assignment.getMilestoneList()) {
                    if (milestone.getDeadline().getDate() == day && milestone.
                            getDeadline().getMonth() == (month - 1)) {
                        //add milestone name
                        milestoneList.add(milestone.getName());
                    }
                }

                //loop for tasks
                for (Task task : assignment.getTaskList()) {
                    if (task.getDeadline().getDate() == day && 
                            task.getDeadline().getMonth() == (month - 1)) {
                        //add task name
                        taskList.add(task.getName());

                        //add progress value
                        taskList.add(String.valueOf(task.getProgressValue()));
                    }
                }

            }

        }
        
        //add the number of assignments
        assignmentList.add(0, String.valueOf(assignmentList.size() / 2));
        
        //add the number of milestones
        milestoneList.add(0, String.valueOf(milestoneList.size()));

        //add the number of tasks
        taskList.add(0, String.valueOf(taskList.size() / 2));
        
        //merge lists and return
        result.addAll(assignmentList);
        result.addAll(milestoneList);
        result.addAll(taskList);
        return result;
    }

    /**
     * A method to get all upcoming deadlines that are in the given Date range
     * for the specified Module.
     * 
     * @param module        Module object
     * 
     * @param earliestDate  start date of the date range
     * 
     * @param latestDate    end date of the date range
     * 
     * @return              textual representation of the upcoming deadlines
     */
    private static ArrayList<String> getModuleInfo(Module module, 
            Date earliestDate, Date latestDate) {
        ArrayList<String> result = new ArrayList<>();

        result.add(module.getName());

        //how many assignments
        result.add(String.valueOf(module.getAssignmentList().size()));
        for (Assignment assignment : module.getAssignmentList()) {
            //add assignment info
            result.addAll(getAssignmentInfo(assignment, earliestDate, 
                                                                latestDate));
        }

        return result;
    }

    /**
     * A method to get all upcoming deadlines that are in the given Date range
     * for the specified Assignment.
     * 
     * @param assignment    Assignment object
     * 
     * @param earliestDate  start date of the date range
     * 
     * @param latestDate    end date of the date range
     * 
     * @return              textual representation of the upcoming deadlines
     */
    private static ArrayList<String> getAssignmentInfo(Assignment assignment, 
                                        Date earliestDate, Date latestDate) {
        ArrayList<String> result = new ArrayList<>();

        result.add(assignment.getName());

        //how many milestones
        int milestones = 0;
        for (Milestone milestone : assignment.getMilestoneList()) {
            if (milestone.getDeadline().after(earliestDate) && 
                    milestone.getDeadline().before(latestDate)) {
                milestones++;
            }
        }
        result.add(String.valueOf(milestones));

        for (Milestone milestone : assignment.getMilestoneList()) {
            if (milestone.getDeadline().after(earliestDate) && 
                    milestone.getDeadline().before(latestDate)) {
                //add milestone info
                result.addAll(getMilestoneInfo(milestone));
            }
        }

        //how many tasks
        int tasks = 0;
        for (Task task : assignment.getTaskList()) {
            if (task.getDeadline().after(earliestDate) && 
                    task.getDeadline().before(latestDate)) {
                tasks++;
            }
        }
        result.add(String.valueOf(tasks));

        for (Task task : assignment.getTaskList()) {
            if (task.getDeadline().after(earliestDate) && 
                    task.getDeadline().before(latestDate)) {
                //add task info
                result.addAll(getTaskInfo(task));
            }
        }

        return result;
    }

    /**
     * A method to get the textual representation of the given Milestone.
     * 
     * @param milestone     Milestone object
     * 
     * @return              textual representation of the given Milestone
     */
    private static ArrayList<String> getMilestoneInfo(Milestone milestone) {
        ArrayList<String> result = new ArrayList<>();

        result.add(milestone.getName());
        result.add(milestone.getDeadline().toString().substring(0, 10));

        //how many tasks
        result.add(String.valueOf(milestone.getTaskList().size()));

        for (Task task : milestone.getTaskList()) {
            //add task info
            result.addAll(getTaskInfo(task));
        }

        return result;
    }

    /**
     * A method to get the textual representation of the given Task.
     * 
     * @param task          Task object
     * 
     * @return              textual representation of the given Task
     */
    private static ArrayList<String> getTaskInfo(Task task) {
        ArrayList<String> result = new ArrayList<>();

        result.add(task.getName());
        result.add(String.valueOf(task.getProgressValue()));
        result.add(task.getDeadline().toString().substring(0, 10));

        return result;
    }
}
