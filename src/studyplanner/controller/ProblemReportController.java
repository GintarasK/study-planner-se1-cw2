package studyplanner.controller;

import utilities.exceptions.IllegalMessageException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import utilities.ErrorChecking.ErrorChecking;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate ProblemReportController.
 */

public class ProblemReportController {
    //login studyplanneruea
    //pass  ueastudyplanner

    /**
     * A method to send e-mails to studyplanneruea@gmail.com in order to leave
     * feedback or report a bug in the system. Requires connection to the 
     * Internet.
     * 
     * @param from                      sender details
     * @param subject                   subject of an e-mail
     * @param text                      actual message
     * 
     * @return                          true if the message was sent 
     *                                                              successfully
     *                                  false otherwise
     * 
     * @throws EmptyInputException      if one or more required fields were left
     *                                  empty
     * 
     * @throws IllegalMessageException  if the system failed to send an e-mail
     * 
     * @throws NullInputException       if null values were passed
     */
    public static boolean sendReport(String from, String subject, String text) 
                           throws EmptyInputException, IllegalMessageException,
                                                       NullInputException {
        
        ErrorChecking.checkStringInput(from);
        ErrorChecking.checkStringInput(subject);
        ErrorChecking.checkStringInput(text);
        
        //sockets and ports
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        //session details, login, password
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("studyplanneruea@gmail.com", 
                                                            "ueastudyplanner");
            }
        });

        //message properties
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("studyplanneruea@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("studyplanneruea@gmail.com"));
            message.setSubject(subject);
            message.setText(text + "\n\n\nFROM : " + from);

            Transport.send(message);

            return true;

        } catch (MessagingException e) {
            throw new IllegalMessageException();
        }
    }
}
