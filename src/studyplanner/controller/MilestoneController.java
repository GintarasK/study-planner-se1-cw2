package studyplanner.controller;

import java.util.ArrayList;
import java.util.Date;
import studyplanner.model.Assignment;
import studyplanner.model.Milestone;
import studyplanner.model.Module;
import studyplanner.model.Task;
import utilities.ErrorChecking.ErrorChecking;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.IllegalFileException;
import utilities.exceptions.IllegalMilestoneException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate MilestoneController.
 */
public class MilestoneController {

    /**
     * A method to create a Milestone with the passed arguments from a
     * MilestoneForm.
     *
     * @param name                      the name of a Milestone
     * 
     * @param deadline                  the deadline of a Milestone
     * 
     * @param taskList                  an ArrayList with associated 
     *                                  task names of a Milestone
     *
     * @param assignmentName            name of an assignment 
     *                                  associated with the Milestone
     *
     * @throws EmptyInputException      if some field is empty
     *
     * @throws IllegalDateException     if there is an error in dates
     *
     * @throws IllegalFileException     if the program failed to open 
     *                                  or read the file
     * 
     * @throws NullInputException       if nulls are passed as parameters
     * 
     * @throws DuplicateDataException   if duplicate data is found
     * 
     * @throws ItemNotFoundException    if the system fails to find an
     *                                  item in itself
     */
    public static void newMilestone(String name, Date deadline,
            ArrayList<String> taskList, String assignmentName)
            throws EmptyInputException,
            NullInputException, IllegalDateException, DuplicateDataException,
            IllegalFileException, ItemNotFoundException{

        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());

        Assignment assignment = AssignmentController.getAssignment
                                                    (module, assignmentName);

        ErrorChecking.checkMultiDates(deadline, assignment.getDeadline());
        Milestone milestone = new Milestone(name, deadline, null);
        assignment.addMilestone(milestone);

        //update the view
        ModuleController.getActiveModulePage().
                newMilestone(assignmentName, name,
                        deadline.toString().substring(0, 10));

        FileController.updateLogFile();
    }

    /**
     * A method to give the textual representation of Milestones for the
     * specified Assignment.
     *
     * @param a                     Assignment
     *
     * @return                      number of milestones at index 0 then
     *                              milestone info if there were found any
     */
    public static ArrayList<String> milestoneInfo(Assignment a) {
        ArrayList<String> info = new ArrayList<>();

        //how many milestones
        int sum = a.getMilestoneList().size();

        //actual milestone info
        if (!a.getMilestoneList().isEmpty()) {
            for (Milestone m : a.getMilestoneList()) {
                info.add(m.getName());
                info.add(m.getDeadline().toString().substring(0, 10));

                //info about tasks associated with this milestone
                info.addAll(TaskController.tasksInfo(m));
            }
        }

        info.add(0, String.valueOf(sum));

        return info;
    }

    /**
     * A method to update a Milestone with the passed arguments from the 
     * update Milestone form.
     * 
     * @param assignmentName                    name of Assignment the Milestone
     *                                          belongs to
     * 
     * @param oldMilestoneName                  old name of the Milestone
     * 
     * @param newMilestoneName                  name of the Milestone to be set
     * 
     * @param deadline                          deadline of the Milestone to 
     *                                          be set
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws NullInputException               if nulls are passed as 
     *                                          parameters
     * 
     * @throws IllegalFileException             if the program fails to 
     *                                          open/read the log file
     * 
     * @throws IllegalDateException             if date parameters are in wrong
     *                                          format or out of range
     * 
     * @throws ItemNotFoundException            if the system fails to find an
     *                                          item in itself
     * 
     * @throws DuplicateDataException           if duplicate data is found
     */
    public static void updateMilestone(String assignmentName,
            String oldMilestoneName, String newMilestoneName, Date deadline)
            throws EmptyInputException,
            NullInputException, IllegalDateException,
            IllegalFileException, ItemNotFoundException, DuplicateDataException {

        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());

        Assignment assignment = AssignmentController.getAssignment
                                                    (module, assignmentName);
        
        if(!oldMilestoneName.equals(newMilestoneName)){
            if(isInAssignment(assignment, newMilestoneName)){
                throw new DuplicateDataException("The assignment already has"
                        + " a milestone with the provided name. Please, check"
                        + " your input and try again. If error occurs again,"
                        + " please, contact the system administrator.");
            }
        }
        
        Milestone milestone = MilestoneController.getMilestone
                                                (assignment, oldMilestoneName);

        if(!milestone.getDeadline().equals(deadline)){
            ErrorChecking.checkMultiDates(new Date(), deadline);
            ErrorChecking.checkMultiDates(deadline, assignment.getDeadline());
        }
        
        //check for clashes with task deadlines
        for(Task t : milestone.getTaskList()){
            Date tempDate = new Date(t.getDeadline().getTime());
            tempDate.setDate(tempDate.getDate() + 1);
            ErrorChecking.checkMultiDates(tempDate, deadline);
        }
        
        //set parameters
        milestone.setName(newMilestoneName);
        milestone.setDeadline(deadline);
        
        //update the log file
        FileController.updateLogFile();
        
        //update the view
        ModuleController.getActiveModulePage().
                updateMilestone(assignmentName, oldMilestoneName,
                        newMilestoneName, deadline.toString().substring(0, 10));
    }

    /**
     * A method to get a deadline of the specified Milestone.
     * 
     * @param assignmentName                    name of Assignment the Milestone
     *                                          belongs to
     * 
     * @param milestoneName                     name of the Milestone
     * 
     * @return                                  deadline of the Milestone
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws NullInputException               if nulls are passed as 
     *                                          parameters
     * 
     * @throws ItemNotFoundException            if get methods would not find
     *                                          required data
     */
    public static Date getDeadline(String assignmentName, String milestoneName)
            throws EmptyInputException, NullInputException, 
            ItemNotFoundException {

        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());

        Assignment assignment = AssignmentController.getAssignment
                                                    (module, assignmentName);

        return MilestoneController.getMilestone(assignment, milestoneName).
                                                                getDeadline();
    }

    /**
     * A method to tell whether or not the given Task has a Milestone with the
     * given name.
     *
     * @param task                  Task object
     * @param milestoneName         name of a Milestone
     *
     * @return                      true if the Task has a Milestone 
     *                              with the given name, false otherwise
     */
    public static boolean isInTask(Task task, String milestoneName) {
        if (!task.getMilestoneList().isEmpty()) {
            for (Milestone mil : task.getMilestoneList()) {
                if (mil.getName().equalsIgnoreCase(milestoneName)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * A method to tell whether or not the given Assignment has a Milestone with
     * the given name.
     *
     * @param assignment            Assignment object
     * @param milestoneName         name of a Milestone
     *
     * @return                      true if the Assignment has a Milestone 
     *                              with the given name, false otherwise
     */
    public static boolean isInAssignment(Assignment assignment,
            String milestoneName) {

        if (!assignment.getMilestoneList().isEmpty()) {
            for (Milestone mil : assignment.getMilestoneList()) {
                if (mil.getName().equalsIgnoreCase(milestoneName)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * A method to return a Milestone (if exists) that has the given name and is
     * associated with the given Assignment.
     *
     * @param assignment        Assignment that the Milestone is associated with
     * 
     * @param milestoneName     name of the Milestone
     *
     * @return                  Milestone object if exists
     * 
     * @throws ItemNotFoundException    an exception to be thrown if Milestone
     *                                  was not found.
     */
    public static Milestone getMilestone(Assignment assignment,
            String milestoneName) throws ItemNotFoundException {

        if (isInAssignment(assignment, milestoneName)) {
            for (Milestone mil : assignment.getMilestoneList()) {
                if (mil.getName().equalsIgnoreCase(milestoneName)) {
                    return mil;
                }
            }
        }
        throw new ItemNotFoundException("");
    }

    /**
     * A method to remove a Milestone with the passed arguments from the 
     * update Milestone form.
     * 
     * @param assignmentName                    name of Assignment the Milestone
     *                                          belongs to
     * 
     * @param milestoneName                     name of the Milestone
     * 
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws IllegalFileException             if the program fails to 
     *                                          open/read the log file
     * 
     * @throws IllegalMilestoneException        if the Assignment does not have
     *                                          a Milestone with the given name
     * 
     * @throws NullInputException               if nulls are used as parameters
     * 
     * @throws ItemNotFoundException            if get methods does not find
     *                                          required items
     */
    public static void removeMilestone(String assignmentName,
            String milestoneName) throws IllegalMilestoneException, 
            IllegalFileException, EmptyInputException,
            NullInputException, ItemNotFoundException {

        String moduleCode = ModuleController.getActiveModulePage().
                getInformationPart().getModuleCode();

        Module module = ModuleController.getModule(moduleCode);

        Assignment assignment = AssignmentController.
                getAssignment(module, assignmentName);

        Milestone milestone = getMilestone(assignment, milestoneName);

        if (milestone.getTaskList().isEmpty()) {
            assignment.removeMilestone(milestone); 
            ModuleController.getActiveModulePage().
                                removeMilestone(assignmentName, milestoneName);
            
            FileController.updateLogFile();
        } else {
            throw new IllegalMilestoneException();
        }
    }
}
