package studyplanner.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import javax.swing.filechooser.FileSystemView;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import studyplanner.model.Assignment;
import studyplanner.model.Coursework;
import studyplanner.model.Exam;
import studyplanner.model.Module;
import studyplanner.model.SemesterProfile;
import utilities.ErrorChecking.ErrorChecking;
import utilities.exceptions.DuplicateDataException;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalAssignmentsWeightingException;
import utilities.exceptions.IllegalCodeException;
import utilities.exceptions.IllegalDateException;
import utilities.exceptions.IllegalFileException;
import utilities.exceptions.IllegalTypeException;
import utilities.exceptions.IllegalValueException;
import utilities.exceptions.IncorrectInputDataException;
import utilities.exceptions.InputOutOfRangeException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate FileController.
 */

public class FileController {
        
    private static final SimpleDateFormat DEADLINE_FORMAT = 
                                       new SimpleDateFormat("dd.MM.yyyy HH:mm");
    
    private static final SimpleDateFormat ONLY_DATE = 
                                             new SimpleDateFormat("dd.MM.yyyy");
    
    private static final String LOG_FILE_NAME = "log.ser"; //name of the logfile

    private static final String LOG_FILE_LOCATION
            = FileSystemView.getFileSystemView().getDefaultDirectory().getPath()
            + "\\StudyPlanner\\";  //path to the logfile

    /**
     * A method to open a JSON file.
     * 
     * @param filename              the path to a file
     * 
     * @return                      JSON object representation of the provided
     *                              file
     * 
     * @throws IllegalFileException if the program fails to open/read the file
     */
    public static JSONObject openJSON(String filename) throws IllegalFileException{
        if (!filename.substring(filename.length() - 4,
                filename.length()).equals("json")) {
            throw new IllegalFileException();
        }        
        try{
            JSONParser parser = new JSONParser();
            Object obj = null;
            //open the file
            obj = parser.parse(new FileReader(filename));
            JSONObject file = (JSONObject) obj;
            return file;
        }catch(Exception e){
            throw new IllegalFileException();
        } 
    }
    
    /**
     * A method to update deadlines for existing SemesterProfile by providing
     * new Data file.
     * 
     * @param filename                          location of the data file
     * 
     * @throws IllegalFileException             if the program fails to 
     *                                          open/read the file
     * 
     * @throws ParseException                   if JSON syntax is wrong
     * 
     * @throws IllegalDateException             if dates are not specified
     *                                          correctly
     * 
     * @throws EmptyInputException              if a field appears to be empty
     * 
     * @throws NullInputException               if a field appears to be null
     * 
     * @throws IllegalTypeException             if the system fails to assign
     *                                          a type to the semester profile
     * 
     * @throws ItemNotFoundException            if the system fails to find an
     *                                          item in itself
     * 
     * @throws DuplicateDataException           if program fails to find data
     *                                          about assignments
     */
    public static void updateSemesterProfile(String filename) throws 
            IllegalFileException, ParseException, IllegalDateException, 
            EmptyInputException, NullInputException, IllegalTypeException, 
            ItemNotFoundException, DuplicateDataException{
        boolean flag = false;
        DEADLINE_FORMAT.setLenient(false);
        ONLY_DATE.setLenient(false);
        TimeZone.setDefault(TimeZone.getTimeZone("London"));
        try{
            JSONObject file = openJSON(filename);
            
            //get type for the SemesterProfile
            String t = (String) file.get("type");
            SemesterProfile.Type type = semesterType(t);
            
            //check if the profile exists in the system
            boolean found = false;
            for(SemesterProfile semP : SemesterProfileController.getSemProfiles()){
                if(semP.getType() == type){
                    found = true;
                }
            }
            
            if(!found){
                throw new ItemNotFoundException("SEMESTER PROFILE ");
            }
            
            //get a list of modules
            JSONArray moduleList = (JSONArray) file.get("modules");
            
            if(moduleList != null){
                for(Object o : moduleList){
                    JSONObject module = (JSONObject) o;
                    
                    //get a field from modules
                    String code = (String) module.get("code");
                    
                    //get a list of assignments
                    JSONArray assignments = (JSONArray) 
                                                    module.get("assignments");
                    
                    if (assignments != null) {
                        for(Object a : assignments){
                            JSONObject assignment = (JSONObject) a;
                            
                            //get fields for an assignment
                            String aName = (String) assignment.get("name");
                            String d = (String) assignment.get("deadline");
                            ErrorChecking.checkStringInput(d);
                            Date deadline = DEADLINE_FORMAT.parse(d);

                            //update assignments if a match has been found
                            outerloop:
                            for(SemesterProfile semP : 
                                    SemesterProfileController.getSemProfiles()){
                                for(Module m : semP.getModuleList()){                                    
                                    if(m.getCode().equals(code)){                                                                               
                                        for(Assignment as : 
                                                         m.getAssignmentList()){                                            
                                            if(as.getName().
                                                    equalsIgnoreCase(aName) &&
                                                    as.getDeadline().before(deadline)){
                                                as.setDeadline(deadline);
                                                flag = true;
                                                break outerloop;
                                            }
                                        }                                       
                                    }
                                }
                            }
                        }
                    }
                }
                updateLogFile();
            }
        }catch(IllegalFileException | ParseException | IllegalDateException e){
            throw e;
        }
        
        if(!flag){
            throw new DuplicateDataException("No updates were made. "
                    + "Please, make sure that courseworks that you want to "
                    + "update deadlines for exist. If error occurs again, "
                    + "please, contact the system administrator.");
        }
    }
    
    /**
     * A method to extract data from the provided JSON file and create
     * Assignment, Module and SemesterProfile objects.
     *
     * @param filename file's path and name
     * 
     * @throws IncorrectInputDataException      if the user input is in 
     *                                          incorrect format or provided 
     *                                          data is erroneous
     * 
     * @throws IllegalFileException             if the program fails to 
     *                                          open/read the file
     * 
     * @throws IllegalValueException            when provided values (JSON file) 
     *                                          do not go past error checks
     * 
     * @throws ParseException                   if JSON syntax is wrong
     * 
     * @throws IllegalDateException             if dates are not specified
     *                                          correctly
     * 
     * @throws EmptyInputException              if a field appears to be empty
     * 
     * @throws NullInputException               if a field appears to be null
     * 
     * @throws IllegalCodeException             if the given module code
     *                                          does not meet the expected regex
     * 
     * @throws IllegalTypeException             if the system fails to assign
     *                                          a type to the semester profile
     * 
     * @throws ItemNotFoundException            if the system fails to find an
     *                                          item in itself
     * 
     * @throws InputOutOfRangeException         if provided integer input is
     *                                          not within specified range
     * 
     * @throws IllegalAssignmentsWeightingException     if overall Assignments 
     *                                                  weighting value is not 
     *                                                  equal to 100
     * 
     * @throws DuplicateDataException           if program fails to find data
     *                                          about assignments        
     */
    public static void loadSemesterDataFile(String filename) 
            throws IncorrectInputDataException, ItemNotFoundException,
            IllegalValueException, ParseException,  IllegalDateException,
            IllegalTypeException, EmptyInputException, IllegalCodeException,
            IllegalFileException, NullInputException, InputOutOfRangeException,
            IllegalAssignmentsWeightingException, DuplicateDataException {
        
        try {
            DEADLINE_FORMAT.setLenient(false);
            ONLY_DATE.setLenient(false);
            TimeZone.setDefault(TimeZone.getTimeZone("London"));
            JSONObject file = openJSON(filename);

            //get dates for a SemesterProfile
            String sd = (String) file.get("startDate");
            String ed = (String) file.get("endDate");
            ErrorChecking.checkStringInput(sd);
            ErrorChecking.checkStringInput(ed);
            Date startDate = ONLY_DATE.parse(sd);
            Date endDate = ONLY_DATE.parse(ed);
            
            //check if SemesterProfile's dates do not clash
            ErrorChecking.checkMultiDates(startDate, endDate);

            //get type for the SemesterProfile
            String t = (String) file.get("type");
            SemesterProfile.Type type = semesterType(t);
            
            //get holidays dates. Since only Spring semesters have holidays,
            //this needs to be done only for Spring semesters
            Date holidaysStart = null;
            Date holidaysEnd = null;
            if(type == SemesterProfile.Type.SPRING){
                
                String holidaysStartDate = (String) file.get("holidaysStart");
                ErrorChecking.checkStringInput(holidaysStartDate);
                //This might be inefficient as user could be prompted with an
                //error that would say that even tho the semester is spring
                //the data file does not contain holidays date!!!!!!!!!!!!!!
                holidaysStart = ONLY_DATE.parse(holidaysStartDate);
                
                String holidaysEndDate = (String) file.get("holidaysEnd");
                ErrorChecking.checkStringInput(holidaysEndDate);
                //This might be inefficient as user could be prompted with an
                //error that would say that even tho the semester is spring
                //the data file does not contain holidays date!!!!!!!!!!!!!!
                holidaysEnd = ONLY_DATE.parse(holidaysEndDate);
            }

            //get a list of modules
            JSONArray moduleList = (JSONArray) file.get("modules");
            ArrayList<Module> modules = new ArrayList<>();

            if (moduleList != null) {
                //for loop for the list of Modules
                for (Object o : moduleList) {
                    JSONObject module = (JSONObject) o;

                    //get a list of assignments
                    JSONArray assignments = (JSONArray) 
                                                    module.get("assignments");
                    ArrayList<Assignment> assignmentList = new ArrayList<>();
                    
                    if (assignments != null) {
                        int totalWeighting = 0; 
                        //it will be possible to have more than 100 in 
                        //assignment weightings for the module
                        //for loop for the list of Assignments
                        for (Object a : assignments) {

                            //create an Assignment
                            Assignment tempAssignment = createAssignment
                                                   ((JSONObject) a);
                            //add the assignment to the list
                            if (tempAssignment != null) {
                                assignmentList.add(tempAssignment);
                                totalWeighting+=tempAssignment.getWeighting(); 
                            }
                        }
                        ErrorChecking.checkModuleAssignmentsWeighting(totalWeighting);
                    }
                    
                    //create a Module
                    //returns null if the system already has a year-long module
                    //with the same module code. In this case, the reference to 
                    //the existing module will be added to the list instead 
                    Module tempModule = createModule(module, type, modules, assignmentList);
                    
                    //since the reference was added, we can continue
                    if(tempModule == null){
                        continue;
                    }
                    
                    //add the Module to the list
                    modules.add(tempModule);
                }
            }

            //create a SemesterProfile
            SemesterProfile tempSem = new SemesterProfile(startDate, 
                            endDate, type, modules, holidaysStart, holidaysEnd);
            
            //add to the SemesterProfileController and update logs
            SemesterProfileController.addSemesterProfile(tempSem);
            updateLogFile();
        } catch (ParseException | NumberFormatException | IllegalDateException | 
                IllegalTypeException | EmptyInputException | 
                IllegalCodeException | IllegalFileException e) {
              throw e;
        }
    }

    /**
     * A method to update the log file. Saves the current state of all objects
     * in the program, so they can be loaded in the future.
     *
     * @throws IllegalFileException if the program fails to write the object to
     * the log file
     * 
     * @throws NullInputException an exception to be thrown if null
     * input parameter for saveProfile is detected.
     */
    public static void updateLogFile() throws NullInputException,
            IllegalFileException {

        for (SemesterProfile semP : SemesterProfileController.getSemProfiles()){
            saveProfile(semP);
        }
    }
    
    /**
     * A method to remove a log file for a specified Semester Profile.
     * Is used whenever Semester Profile objects are completely deleted from
     * the system.
     * 
     * @param semP SemesterProfile object which log file should be deleted
     * 
     * @return     true if was deleted successfully,
     *             false otherwise
     * 
     * @throws NullInputException           an exception to be thrown if null
     *                                      input parameter is detected
     */
    public static boolean removeLogFile(SemesterProfile semP) throws NullInputException{
        ErrorChecking.checkIfNull(semP);
        File log = new File(LOG_FILE_LOCATION + LOG_FILE_NAME + semP.getID());
        return log.delete();
    }

    /**
     * A method to save a single SemesterProfile object
     *
     * @param semP SemesterProfile          object that needs to be saved
     *
     * @throws IllegalFileException         if the program fails to 
     *                                      write the object to
     *                                      the log file
     * 
     * @throws NullInputException           if input parameter is null
     */
    public static void saveProfile(SemesterProfile semP)
            throws IllegalFileException, NullInputException {
        ErrorChecking.checkIfNull(semP);
        //create a directory
        File dir = new File(LOG_FILE_LOCATION);

        // attempt to create the directory there
        dir.mkdir();

        //save the passed object in the logfile
        FileOutputStream fos;
        try {
            fos = new FileOutputStream
                            (LOG_FILE_LOCATION + LOG_FILE_NAME + semP.getID());
            ObjectOutputStream out = new ObjectOutputStream(fos);
            semP.writeObject(out);
            out.close();
        } catch (IOException | ClassNotFoundException ex) {
            throw new IllegalFileException();
        }
    }

    /**
     * A method to read the log file. Reads the saved state of all objects in
     * the program that were written to the log file before.
     *
     * @throws IllegalFileException if the program fails to read the log file
     * 
     * @throws IllegalTypeException if there is a Semester
     *                              profile with the provided type
     */
    public static void readLogFile() throws IllegalFileException, 
            IllegalTypeException {
        String[] filesArray = new File(LOG_FILE_LOCATION).list();

        if (filesArray != null && filesArray.length != 0) {

            for (int i = 0; i < filesArray.length; i++) {
                SemesterProfile semP = null;
                FileInputStream fis;
                try {
                    fis = new FileInputStream
                                            (LOG_FILE_LOCATION + filesArray[i]);
                    ObjectInputStream in = new ObjectInputStream(fis);
                    semP = (SemesterProfile) in.readObject();
                    in.close();
                } catch (IOException | ClassNotFoundException ex) {
                    throw new IllegalFileException();
                }
                SemesterProfileController.addSemesterProfile(semP);
            }
        }
    }

    /**
     * Scans a string and leaves only single space characters, removing all
     * double/triple/.../billionple spaces and newline chars
     *
     * @param str String that has space characters to be removed from
     *
     * @return String with only single space characters
     * 
     * @throws EmptyInputException              if a field appears to be empty
     * 
     * @throws NullInputException               if a field appears to be null
     */
    private static String removeSpaces(String str) throws 
            NullInputException, EmptyInputException {
        int index = 0;
        
        ErrorChecking.checkStringInput(str);
        while (index < str.length()) {
            //if there are 2 space chars in a row
            if (str.charAt(index) == ' ' && str.charAt(index + 1) == ' ') {

                //get rid of one of the chars
                str = str.substring(0, index)
                        + str.substring(index + 1, str.length());
            } else {
                index++;
            }
        }

        //replace newline chars
        return str.replaceAll("[\n\r]", "");
    }

    /**
     * A method to return a SemesterProfile type based on the passed String.
     * 
     * @param type                      textual representation of a type, String
     * 
     * @return                          type, SemesterProfile.Type object
     * 
     * @throws IllegalTypeException     if the passed String did not match any
     *                                  possible types
     * 
     * @throws EmptyInputException              if a field appears to be empty
     * 
     * @throws NullInputException               if a field appears to be null
     */
    private static SemesterProfile.Type semesterType(String type) 
        throws IllegalTypeException,
                         NullInputException, EmptyInputException {
        ErrorChecking.checkStringInput(type);
        
        if(type.equalsIgnoreCase("autumn")){
            return SemesterProfile.Type.AUTUMN;
        }else if(type.equalsIgnoreCase("spring")){
            return SemesterProfile.Type.SPRING;
        }

        throw new IllegalTypeException("Incorrect semester type was"
                + " detected. Please reupload semester data file."
                + " If error occurs again, please check your semester"
                + " data file or contact system administrator.");
    }
    
    /**
     * A method to check whether or not the system already has a Module with
     * the given code. If yes, then pass the reference to the list and return 
     * true. False otherwise. Works only with Year-long Modules.
     * 
     * @param modules           module list
     * 
     * @param code              module code
     * 
     * @param moduleType        module type
     * 
     * @return                  true if the Module already exists in the system
     *                          and its reference was successfully passed to the
     *                          list, false otherwise
     */
    private static boolean moduleExists(ArrayList<Module> modules, String code,
            Module.Type moduleType) {
        
        if (!SemesterProfileController.isEmpty()
                                              && moduleType == Module.Type.Y) {
            for (SemesterProfile semP
                                : SemesterProfileController.getSemProfiles()) {
                
                for (Module m : semP.getModuleList()) {
                    if (m.getCode().equals(code)) {
                        modules.add(m);
                        return true;
                    }
                }
                
            }
        }
        
        return false;
    }

    /**
     * A method to load a log file from the specified location.
     * 
     * @param location          location of the log file
     * 
     * @throws Exception        if the file was not found or did not meet the 
     *                          expected format
     */
    public static void importLogFile(String location) throws Exception {
        SemesterProfile semP = null;

        FileInputStream fis;
        
        try{
            fis = new FileInputStream(location);
            ObjectInputStream in = new ObjectInputStream(fis);
            semP = (SemesterProfile) in.readObject();
            in.close();

            SemesterProfileController.addSemesterProfile(semP);
            FileController.updateLogFile();
        }catch (Exception e){
            throw e;
        }
    }
    
    /**
     * A method to export a log file. "Converts" the specified semester profile
     * to a log file and saves it in the specified location.
     * 
     * @param semesterProfileHeader             header of a semester profile
     * @param location                          location of the file
     * 
     * @throws ClassNotFoundException           if serialisation failed
     * 
     * @throws EmptyInputException              if a field appears to be empty
     * 
     * @throws NullInputException               if a field appears to be null
     * 
     * @throws IOException                      if the program fails to
     *                                          read/write to the file
     * 
     * @throws ItemNotFoundException            if the system fails to find an
     *                                          item in itself           
     */
    public static void exportLogFile(String semesterProfileHeader, String location)
            throws ClassNotFoundException, NullInputException, EmptyInputException,
            IOException, ItemNotFoundException {

        ErrorChecking.checkStringInput(location);
        ErrorChecking.checkStringInput(semesterProfileHeader);
        SemesterProfile tempSem = SemesterProfileController.
                findSemesterProfile(semesterProfileHeader);

        FileOutputStream fos;

        try {
            fos = new FileOutputStream(location);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            tempSem.writeObject(out);
            out.close();
        } catch (IOException e) {
            throw e;
        }
    }
    
    /**
     * A method to return a SemesterProfile type based on the passed module 
     *                                                                     code.
     * 
     * @param code                  module code
     * 
     * @return                      type, Module.Type object
     * 
     * @throws IllegalTypeException if the passed module code did not match any
     *                              possible types
     * 
     * @throws EmptyInputException              if a field appears to be empty
     * 
     * @throws NullInputException               if a field appears to be null
     */
    private static Module.Type moduleType(String code) 
                  throws IllegalTypeException, NullInputException,
            EmptyInputException {
        ErrorChecking.checkStringInput(code);
        switch (code.charAt(code.length() - 1)) {
            case 'A':
                return Module.Type.A;
            case 'B':
                return Module.Type.B;
            case 'Y':
                return Module.Type.Y;
        }

        throw new IllegalTypeException("Incorrect module type was"
                + " detected. Please reupload semester data file."
                + " If error occurs again, please check your semester"
                + " data file or contact system administrator.");
    }
    
    /**
     * A method to check whether or not a Module with the given code already
     * exists in the system. Works only with Modules that are not year-long.
     * 
     * @param code                              module code
     * @param moduleType                        module type
     * 
     * @throws DuplicateDataException           if a Module with the given
     *                                          module code already exists in
     *                                          the system
     */
    private static void duplicateModules(String code, Module.Type moduleType)
            throws DuplicateDataException {

        if (!ModuleController.isEmpty() && moduleType != Module.Type.Y) {
            for (Module m : ModuleController.getModules()) {
                if (m.getCode().equals(code)) {
                    throw new DuplicateDataException("The data"
                            + " file has modules that already are in"
                            + " the system. Please, check"
                            + " the data file and try again. If error"
                            + " occurs again, please, contact the HUB/"
                            + "system administrator.");
                }
            }
        }
    }
    
    /**
     * A method to create an Assignment from JSONObject.
     * 
     * @param assignment            JSONObject representation of the Assignment
     * 
     * @param tempModule            Module which the Assignment belongs to
     * 
     * @return                      Assignment object derived from the arguments
     * 
     * @throws ParseException               if JSON syntax is wrong
     * 
     * @throws IllegalDateException         if there is an error in dates 
     * 
     * @throws EmptyInputException          if some fields were left empty
     * 
     * @throws IncorrectInputDataException      if the user input is in 
     *                                          incorrect format or provided 
     *                                          data is erroneous
     * 
     * @throws IllegalValueException            when provided values (JSON file) 
     *                                          do not go past error checks
     * 
     * @throws InputOutOfRangeException         if provided integer input is
     *                                          not within specified range
     * 
     * @throws NullInputException               if a field appears to be null
     */
    private static Assignment createAssignment(JSONObject assignment) throws 
            ParseException, IllegalDateException, EmptyInputException,  
            IncorrectInputDataException, IllegalValueException,
            InputOutOfRangeException, NullInputException {
        DEADLINE_FORMAT.setLenient(false);
        ONLY_DATE.setLenient(false);
        //get fields for an assignment
        String aName = (String) assignment.get("name");

        String w = (String) assignment.get("weighting");
        ErrorChecking.isInteger(w);
        int aWeight = Integer.valueOf(w);
        ErrorChecking.checkIntInput(aWeight, 0, 100);

        String d = (String) assignment.get("deadline");
        ErrorChecking.checkStringInput(d);
        Date deadline = DEADLINE_FORMAT.parse(d);

        Date setDate;

        //depending on type, create an assignment
        String tp = (String) assignment.get("type");
        ErrorChecking.checkStringInput(tp);
        if (tp.equalsIgnoreCase("coursework")) {
            String set = (String) assignment.get("setDate");
            ErrorChecking.checkStringInput(set);
            setDate = ONLY_DATE.parse(set);
            //create a coursework
            return new Coursework((aName), aWeight, deadline, setDate);
        } else if (tp.equalsIgnoreCase("formative")) {
            String s = (String) assignment.get("setDate");
            ErrorChecking.checkStringInput(s);
            setDate = ONLY_DATE.parse(s);
            //create a coursework
            return new Coursework((aName), 0, deadline, setDate);
        } else if (tp.equalsIgnoreCase("exam")) {
            String se = (String) assignment.get("startDateTime");
            ErrorChecking.checkStringInput(se);
            setDate = DEADLINE_FORMAT.parse(se);
            String location = (String) assignment.get("location");
            //create an exam
            return new Exam(aName, aWeight, deadline, location, setDate);
        } else {
            throw new IncorrectInputDataException("One of the assignments "
                    + "data is invalid. Please check your data input and try "
                    + "to upload data file again. If error occurs again please "
                    + "contact the system administrator or your HUB.");
        }
    }
    
    /**
     * A method to create a Module from JSONObject.
     * 
     * @param module                            JSONObject representation of
     *                                          the Module
     * 
     * @param type                              SemesterProfile type that the
     *                                          Module belongs to
     *  
     * @param modules                           ArrayList of Modules that the
     *                                          Module belongs to
     * 
     * @return                                  Module object derived from the 
     *                                          arguments
     * 
     * @throws IllegalTypeException         if there is an error in types
     * 
     * @throws EmptyInputException          if some fields were left empty
     * 
     * @throws IllegalCodeException         if code pattern doesn't match
     *                                      regex pattern 
     *                                      "[A-Z]{3}\-\d{4}[ABY]"
     * 
     * @throws NullInputException           if a field appears to be null
     * 
     * @throws InputOutOfRangeException     if provided integer input is
     *                                      not within specified range
     * 
     * @throws DuplicateDataException       when given data (object) is 
     *                                      already present in a given list 
     *                                      or collection of objects
     * 
     * @throws IllegalDateException         if date is in incorrect format
     */
    private static Module createModule(JSONObject module, 
            SemesterProfile.Type type, ArrayList<Module> modules, 
            ArrayList<Assignment> assignmentList) throws IllegalTypeException, 
            EmptyInputException,IllegalCodeException, NullInputException, 
            InputOutOfRangeException, DuplicateDataException, 
            IllegalDateException {

        //get fields for a module
        String name = (String) module.get("name");

        String code = (String) module.get("code");
        
        String cv = (String) module.get("credits");
        ErrorChecking.checkStringInput(cv);
        
        int creditValue = Integer.parseInt(cv);

        //get module type
        Module.Type moduleType = moduleType(code);

        //check if semester profile and module types are compatible
        ErrorChecking.checkTypes(type, moduleType);

        //check for duplicate modules in the list
        ErrorChecking.duplicateModules(modules, code);

        //another check for duplicate modules in the system
        //(for not year-long)
        duplicateModules(code, moduleType);

        //check if a year-long module with the code already is in 
        //the system. if yes, then pass the reference and skip 
        //this module
        if (moduleExists(modules, code, moduleType)) {
            return null;
        }

        String organiser = (String) module.get("organiser");

        String desc = (String) module.get("description");
        
        ErrorChecking.checkStringInput(desc);
        desc = removeSpaces(desc);

        //create a module
        return new Module(name, code, creditValue,
                organiser, desc, moduleType, assignmentList);
    }
}