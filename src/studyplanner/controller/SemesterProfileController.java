package studyplanner.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import studyplanner.model.Module;
import studyplanner.model.SemesterProfile;
import utilities.ErrorChecking.ErrorChecking;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.IllegalSemesterProfileException;
import utilities.exceptions.IllegalTypeException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate SemesterProfileController.
 */

public class SemesterProfileController {

    private static ArrayList<SemesterProfile> semesterProfileModels 
                                                            = new ArrayList<>();

    /**
     * A method to return information about a Semester Profile.
     * 
     * @param index                             index of a Semester Profile
     * 
     * @return                                  textual representation of the
     *                                          Semester Profile
     * 
     * @throws ItemNotFoundException            if there is no such 
     *                                                          Semester Profile
     */
    public static ArrayList<String> getSemesterProfileInfo(int index) 
                                        throws ItemNotFoundException{
        ArrayList<String> tempList = new ArrayList<>();
        
        if(!isEmpty() && semesterProfileModels.size() > index){
            tempList.add(semesterProfileModels.get(index).toString());
            for(Module m : semesterProfileModels.get(index).getModuleList()){
                tempList.add(m.getCode() + "  "+ m.getName());
            }
            
            return tempList;
        }
        
        throw new ItemNotFoundException("Invalid Semester index"
                + " was provided.");
    }
    
    /**
     * A method to return information about all Semester Profiles.
     * 
     * @return                                  ArrayList where every String
     *                                          represents information
     *                                          about a Semester Profile
     * 
     * @throws IllegalSemesterProfileException  if there are no Semester
     *                                          Profiles in the program
     */
    public static ArrayList<String> getSemesterProfileHeaders() 
                                        throws IllegalSemesterProfileException{
        ArrayList<String> tempList = new ArrayList<>();
        if(!isEmpty()){
            for(SemesterProfile semP : semesterProfileModels){
                tempList.add(semP.toString());
            }
            return tempList;
        }
        
        throw new IllegalSemesterProfileException();
    }
    
    /**
     * A method to add a SemesterProfile.
     * 
     * @param profile           SemesterProfile that needs to be added.
     * 
     * @return                  true if was added successfully
     *                          false otherwise
     * 
     * @throws IllegalTypeException     if the program already has a semester
     *                                  profile initialised with the given type
     */
    public static boolean addSemesterProfile(SemesterProfile profile) 
                                                    throws IllegalTypeException{
        if(isEmpty() || !semesterProfileModels.contains(profile)){
            for(SemesterProfile semP : semesterProfileModels){
                if(profile.getType() == semP.getType()){
                    throw new IllegalTypeException("The program already has a "
                            + "Semester profile initialised with the given type"
                            + ".\nPlease, check the data file and make sure that"
                            + " you deleted all the Semester profiles with the"
                            + " type " + profile.getType() + ".\nIf error"
                            + " occurs again, please, contact the system admi"
                            + "nistrator.");
                }
            }
            semesterProfileModels.add(profile);
            return true;
        }
        return false;
    }
    
    /**
     * A method to remove a SemesterProfile.
     * 
     * @param header            String that represents information about a 
     *                          Semester Profile
     * 
     * @return                  true if was successfully removed
     *                          false otherwise
     * 
     * @throws ItemNotFoundException            if get methods does not find
     *                                          required items
     * 
     * @throws EmptyInputException              if an empty string is used
     *                                          as parameter
     * 
     * @throws NullInputException               if null is used as any of the
     *                                          parameters                      
     */
    public static boolean removeSemesterProfile(String header) 
            throws EmptyInputException, NullInputException, ItemNotFoundException{
        
        SemesterProfile semP = findSemesterProfile(header);
        removeSemesterProfile(semP);
        return true;
    }
    
    /**
     * A method to find a SemesterProfile for the specified header.
     * 
     * @param header            String that represents information about a 
     *                          Semester Profile
     * 
     * @return                  SemesterProfile object that responds to the 
     *                          given header
     * 
     * @throws ItemNotFoundException            if get methods does not find
     *                                          required items
     * 
     * @throws EmptyInputException              if an empty string is used
     *                                          as parameter
     * 
     * @throws NullInputException               if null is used as any of the
     *                                          parameters
     */
    public static SemesterProfile findSemesterProfile(String header) 
            throws 
            EmptyInputException, NullInputException, ItemNotFoundException{
        
        SemesterProfile.Type type = null;
        ErrorChecking.checkStringInput(header);
        
        if(header.contains("AUTUMN")){
            type = SemesterProfile.Type.AUTUMN;
        }else if(header.contains("SPRING")){
            type = SemesterProfile.Type.SPRING;
        }
        
        if(type != null){
            for(SemesterProfile semP : semesterProfileModels){
                if(semP.getType() == type){
                    return semP;
                }
            }
        }
        
        throw new ItemNotFoundException("");
    }
    
    /**
     * A method to remove a SemesterProfile.
     * 
     * @param profile           Semester Profile that needs to be removed
     * 
     * @return                  true if was removed successfully
     *                          false otherwise
     * 
     * @throws NullInputException  an exception to be thrown if 
     * null parameter is provided
     */
    public static boolean removeSemesterProfile(SemesterProfile profile)
            throws NullInputException{
        if(!isEmpty() && semesterProfileModels.contains(profile)){
            semesterProfileModels.remove(profile);
            FileController.removeLogFile(profile);
            return true;
        }
        return false;
    }
    
    /**
     * A method to tell whether or not there are any Semester Profiles in the 
     * program.
     * 
     * @return      true if 0
     *              false if >0
     */
    public static boolean isEmpty(){
        return semesterProfileModels.isEmpty();
    }
    
    /**
     * A method to tell how many Semester Profiles there are in the program.
     * 
     * @return      the number of Semester Profile objects
     */
    public static int size(){
        return semesterProfileModels.size();
    }
    
    /**
     * A method to return a list of SemesterProfile objects.
     * 
     * @return      ArrayList populated with Semester Profile objects 
     */
    public static ArrayList<SemesterProfile> getSemProfiles(){
        return semesterProfileModels;
    }
    
    /**
     * A method to return a list of all Module objects.
     * 
     * @return      ArrayList populated with Module objects
     */
    public static ArrayList<Module> getModules(){
        ArrayList<Module> tempList = new ArrayList<>();
        
        if(!isEmpty()){
            for(SemesterProfile semP : semesterProfileModels){
                for(Module m : semP.getModuleList()){
                    tempList.add(m);
                }
            }
        }
        
        return tempList;
    }
    
    /**
     * A method to return the current week's number.
     * 
     * @return                      the current week's number
     */
    public static String getWeekNo(){
        Date localDate = new Date();
        
        if(!isEmpty()){
            for(SemesterProfile semP : semesterProfileModels){
                if(semP.getType() == SemesterProfile.Type.SPRING
                                       && localDate.before(semP.getEndDate()) 
                                       && localDate.after(semP.getStartDate())){
                    
                    if(localDate.after(semP.getHolidaysStart()) && 
                                       localDate.before(semP.getHolidaysEnd())){
                        return "HOLIDAYS";
                    }
                    
                    if(localDate.after(semP.getHolidaysEnd())){
                        return String.valueOf(Math.abs(getWeeksBetween
                                        (localDate, semP.getStartDate())) - 4);
                    }
                    
                    return String.valueOf(Math.abs(getWeeksBetween(localDate, 
                                                     semP.getStartDate())));
                }else if(localDate.before(semP.getEndDate()) 
                                       && localDate.after(semP.getStartDate())){
                    return String.valueOf(Math.abs(getWeeksBetween(localDate, 
                                                     semP.getStartDate())));
                }
            }
        }
        
        return null;
    }
    
    /**
     * A method to take difference in weeks between two dates.
     * Taken from http://stackoverflow.com/questions/9963147/
     *                                 get-the-number-of-weeks-between-two-dates
     * 
     * @param a                         the first date
     * @param b                         the second date
     * 
     * @return                          the difference in weeks
     */
    private static int getWeeksBetween(Date a, Date b) {

        if (b.before(a)) {
            return -getWeeksBetween(b, a);
        }
        a = resetTime(a);
        b = resetTime(b);

        Calendar cal = new GregorianCalendar();
        cal.setTime(a);
        int weeks = 0;
        while (cal.getTime().before(b)) {
            // add another week
            cal.add(Calendar.WEEK_OF_YEAR, 1);
            weeks++;
        }
        return weeks;
    }
    
    /**
     * A method to reset time of a Date.
     * Taken from http://stackoverflow.com/questions/9963147/
     *                                 get-the-number-of-weeks-between-two-dates
     * 
     * @param d                         date
     * @return                          date with reseted time
     */
    private static Date resetTime(Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

}
