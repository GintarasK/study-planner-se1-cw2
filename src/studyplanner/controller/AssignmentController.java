package studyplanner.controller;

import java.util.ArrayList;
import studyplanner.model.Assignment;
import studyplanner.model.Coursework;
import studyplanner.model.Exam;
import studyplanner.model.Module;
import utilities.ErrorChecking.ErrorChecking;
import utilities.exceptions.EmptyInputException;
import utilities.exceptions.ItemNotFoundException;
import utilities.exceptions.NullInputException;

/**
 * Class to simulate AssignmentController.
 */

public class AssignmentController {
    
    /**
     * A method to return textual representation of Assignments for a specified
     * Module.
     *
     * @param module            Module object
     *
     * @return                  list of Strings that represent the Assignment
     */
    public static ArrayList<String> getAssignmentsInfo(Module module) {
        ArrayList<String> assignmentInfo = new ArrayList<>();

        for (Assignment a : module.getAssignmentList()) {
            //assignment information
            assignmentInfo.add(a.getName());
            assignmentInfo.add(String.valueOf(a.getWeighting()));

            if (a.getName().equalsIgnoreCase("Exam")
                    || a.getName().equalsIgnoreCase("Course test")) {
                Exam b = (Exam) a;
                //start time
                assignmentInfo.add(b.getStartTime().toString().substring(0, 16));
                
                //duration
                assignmentInfo.add(String.valueOf(b.getDeadline().
                            getHours() - b.getStartTime().getHours()) + "h");
                
                //location duh
                assignmentInfo.add(b.getLocation());
            } else {
                Coursework b = (Coursework) a;
                
                //set date
                assignmentInfo.add(b.getSetDate().toString().substring(0, 10));
                
                //deadline
                assignmentInfo.add(b.getDeadline().toString().substring(0, 16));
            }
            
            //tasks without milestones information
            assignmentInfo.addAll(TaskController.tasksWithoutMilestonesInfo(a));

            //milestones and their tasks information 
            assignmentInfo.addAll(MilestoneController.milestoneInfo(a));

        }
        return assignmentInfo;
    }

    /**
     * A method to get an Assignment from a provided Module with the given
     * assignment name.
     * 
     * @param module                        Module object
     * @param assignmentName                name of the Assignment
     * 
     * @return                              Assignment with the given name
     *                                      from the given Module
     * 
     * @throws NullInputException           if the Module object appears to
     *                                      be null
     * 
     * @throws ItemNotFoundException        if the Module does not have any
     *                                      Assignments or does not have an
     *                                      Assignment with the given name
     * 
     * @throws EmptyInputException          if assignmentName is passed as
     *                                      an empty string
     */
    static Assignment getAssignment(Module module, String assignmentName) 
            throws NullInputException,
            EmptyInputException, ItemNotFoundException{

        ErrorChecking.checkStringInput(assignmentName);
        
        ErrorChecking.checkIfNull(module);
        
        ErrorChecking.checkListInput(module.getAssignmentList());

        for(Assignment a : module.getAssignmentList()){
            if(a.getName().equalsIgnoreCase(assignmentName)){
                return a;
            }
        }

        throw new ItemNotFoundException("");
    }
    
    /**
     * A method to get a list of Assignment names for the active detailed
     * Module page.
     * 
     * @return                                  list of Strings with Assignment
     *                                          names
     * 
     * @throws EmptyInputException              if a field cannot be empty
     * 
     * @throws NullInputException               if a field is null
     * 
     * @throws ItemNotFoundException            if the system fails to find
     *                                          a module
     */
    public static ArrayList<String> getAssignmentNames() 
            throws
            EmptyInputException, NullInputException, ItemNotFoundException{
        
        Module module = ModuleController.getModule(ModuleController.
                getActiveModulePage().getInformationPart().getModuleCode());
        
        ArrayList<String> result = new ArrayList<>();
        
        for(Assignment a : module.getAssignmentList()){
            result.add(a.getName());
        }
        
        return result;
    }
}
