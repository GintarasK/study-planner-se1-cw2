package studyplanner.controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.filechooser.FileSystemView;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import studyplanner.model.*;
import utilities.exceptions.*;

/**
 * A class to test all the methods of controller.FileController class.
 */
public class FileControllerTest {
    
    public FileControllerTest() {
    }
    
    //JUnit Rule to define exceptions expected to be thrown
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    //Setting up variable declarations for testing
    
    //SimpleDateFormat to parse Date objects for Deadlines, with full date
    //format (yyyy.mm.dd HH:mm)
    private static SimpleDateFormat DEADLINE_FORMAT;
    
    //SimpleDateFormat to parse Date object with fimple (yyyy.mm.dd) format
    private static SimpleDateFormat ONLY_DATE;
    
    //Name of the logfile, where serialized objects are kept
    private static String LOG_FILE_NAME;

    //Location of the serialized objects log file
    private static String LOG_FILE_LOCATION;
	
	//Location of test files
	private static String TEST_FILES_LOCATION;
    
    
    @BeforeClass
    public static void setUpClass() {
    DEADLINE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");
    
    ONLY_DATE = new SimpleDateFormat("dd.MM.yyyy");
    
    LOG_FILE_NAME = "log.ser";

    LOG_FILE_LOCATION = 
            FileSystemView.getFileSystemView().getDefaultDirectory().getPath()
            + "\\StudyPlanner\\";
	
	TEST_FILES_LOCATION = System.getProperty("user.dir")
            + "\\data_for_testing\\controller.FileController\\";
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws Exception {
        ArrayList<SemesterProfile> sl = SemesterProfileController
                .getSemProfiles();
        int i = 0;
        while(!SemesterProfileController.getSemProfiles().isEmpty()){
            SemesterProfileController.removeSemesterProfile(sl.get(0));
            i++;
        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of openJSON method, of class FileController.
     */
    @Test
    public void testOpenJSON() throws Exception {
        System.out.println("openJSON");
        String filename = System.getProperty("user.dir") 
                + "\\final1semester.json";
        
        JSONParser parser = new JSONParser();
        Object obj = null;
        obj = parser.parse(new FileReader(filename));
        JSONObject expResult = (JSONObject) obj;
        
        JSONObject result = FileController.openJSON(filename);
        assertEquals(expResult, result);
    }
    
    //-------------------------TEST_CASES_FOR_DATA_LOADING-------------------//
    ///////////////////FILE_PATH/INCORRECT_EXTENSION/INVALID_FILE//////////////
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling for invalid file names (not existing)
     */
    @Test
    public void testLoadSemesterDataFileItemNotFound() throws Exception {
        System.out.println("loadSemesterDataFile with not existing file");
        String filename = "1.json"; //Invalid file name
        thrown.expect(IllegalFileException.class);
        thrown.expectMessage("The program failed to read/write the file. "
                + "Please, check the path to the file and its format. If "
                + "error occurs again, please check your semester data file "
                + "or contact HUB/system administrator."); 
        FileController.loadSemesterDataFile(filename);
    }
    
   /**
     * Test of openJSON method, of class FileController, when wrong file name
     * is specified.
     */
    @Test
    public void testOpenWrongFileNameJSON() throws Exception {
        System.out.println("openJSON with wrong file name");
        String filename = "random.json";
        thrown.expect(IllegalFileException.class);
        thrown.expectMessage("The program failed to read/write the file. "
                + "Please, check the path to the file and its format. If "
                + "error occurs again, please check your semester data file "
                + "or contact HUB/system administrator."); 
        FileController.openJSON(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling for invalid file extensions (should be .JSON)
     */
    @Test
    public void testLoadSemesterDataFileWrongExtension() throws Exception {
        System.out.println("loadSemesterDataFile with wrong extension");
        String filename = TEST_FILES_LOCATION + "updated1semester.random";
        thrown.expect(IllegalFileException.class);
        thrown.expectMessage("The program failed to read/write the file. "
                + "Please, check the path to the file and its format. If "
                + "error occurs again, please check your semester data file "
                + "or contact HUB/system administrator."); 
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when NULL is passed as a parameter
     */
    @Test
    public void testLoadSemesterDataFileNullFile() throws Exception {
        System.out.println("loadSemesterDataFile null as input parameter");
        String filename = null; //Invalid file name
        thrown.expect(NullPointerException.class);
        FileController.loadSemesterDataFile(filename);
    }
    
    //-------------------------------SEMESTER_PROFILE_TYPE------------------//
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when semester profile file has semester type of 'Spring'
     * but does not contain holiday start/end dates.
     */
    @Test
    public void testLoadSemesterDataFileMixedSemesterTypes() throws Exception {
        System.out.println("loadSemesterDataFile with mixed up semester types "
                + "in the data file");
        String filename = TEST_FILES_LOCATION + "semesterTypeMixup.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when semester profile file has semester type of 'Spring'
     * but contains modules from 'Autumn' semester
     */
    @Test
    public void testLoadSemesterDataFileIncompatableModules() throws Exception {
        System.out.println("loadSemesterDataFile where data file contains "
                + "incompatable modules with the semester type ");
        String filename = TEST_FILES_LOCATION + "moduleSemesterMixup.json";
        thrown.expect(IllegalTypeException.class);
        thrown.expectMessage("Incompatible Semester Profile and Module types "
                + "were detected. Please, check the data file and try again. "
                + "If error occurs again, please contact the HUB/system "
                + "administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when semester profile file is a duplicate of already 
     * loaded semester profile
     */
    @Test
    public void testLoadSemesterDataFileWithDuplicateFile() throws Exception {
        System.out.println("loadSemesterDataFile duplicate data file");
        String filename = TEST_FILES_LOCATION + "final1semester.json";
        FileController.loadSemesterDataFile(filename);
        //thrown.expect(IllegalSemesterProfileException.class);
        thrown.expectMessage("The data"
                + " file has modules that already are in"
                + " the system. Please, check"
                + " the data file and try again. If error"
                + " occurs again, please, contact the HUB/"
                + "system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when semester profile types are nulls
     */
    @Test
    public void testLoadSemesterDataFileNullSemesterType() throws Exception {
        System.out.println("loadSemesterDataFile with null module codes");
        String filename = TEST_FILES_LOCATION + "nullSemesterType.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when empty(empty string) semester types are present in
     * the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptySemesterType() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty semester type");
        String filename = TEST_FILES_LOCATION + "emptySemesterType.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong semester types are present in
     * the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongSemesterType() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty semester type");
        String filename = TEST_FILES_LOCATION + "wrongSemesterType.json";
        thrown.expect(IllegalTypeException.class);
        thrown.expectMessage("Incorrect semester type was"
                + " detected. Please reupload semester data file."
                + " If error occurs again, please check your semester"
                + " data file or contact system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
   //--------------------------TESTING_FOR_SEMESTER_DATES-----------------//
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when null semester start dates are present in the JSON
     * file.
     */
    @Test
    public void testLoadSemesterDataFileNullStartDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null semester start "
                + "date");
        String filename = TEST_FILES_LOCATION + "nullSemesterStartDate.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when semester start dates are present as empty string in
     * the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyStartDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty semester start "
                + "date");
        String filename = TEST_FILES_LOCATION + "emptySemesterStartDate.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
     
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(the date is earlier than local machine date by
     * more than 1 year) semester start dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongSemesterStartDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong semester start "
                + "date(it is more than 1 year earlier than local machine's "
                + "date)");
        String filename = TEST_FILES_LOCATION + "wrongSemesterStartDate.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
     
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(the date is later than local machine date by
     * more than 1 year) semester start dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongSemesterStartDate2() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong semester start "
                + "date(it is more than 1 year later than local machine's "
                + "date)");
        String filename = TEST_FILES_LOCATION + "wrongSemesterStartDate2.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(start date is after end date) semester start
     * dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongSemesterStartDate3() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong semester start "
                + "date(later than semester end date)");
        String filename = TEST_FILES_LOCATION + "wrongSemesterStartDate3.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(date is wrong, e.g.: days/months are out of 
     * range - 99.99.2017) semester start dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongSemesterStartDate4() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong semester start "
                + "date(wrong format)");
        String filename = TEST_FILES_LOCATION + "wrongSemesterStartDate4.json";
        thrown.expect(ParseException.class);
        thrown.expectMessage("Unparseable date: " + "\"99.12.2017\"");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when null semester end dates are present in the JSON
     * file.
     */
    @Test
    public void testLoadSemesterDataFileNullEndDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null semester start "
                + "date");
        String filename = TEST_FILES_LOCATION + "nullSemesterEndDate.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when semester end dates are present as empty string in
     * the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyEndDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty semester start "
                + "date");
        String filename = TEST_FILES_LOCATION + "emptySemesterEndDate.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                + " provided. Please, check your input (data file/form)"
                + " and try again. If the error persists, please"
                + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(the date is earlier than local machine date by
     * more than 1 year) semester end dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongSemesterEndDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong semester end "
                + "date(it is more than 1 year earlier than local machine's "
                + "date)");
        String filename = TEST_FILES_LOCATION + "wrongSemesterEndDate.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(the date is later than local machine date by
     * more than 1 year) semester end dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongSemesterEndDate2() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong semester end "
                + "date(it is more than 1 year later than local machine's "
                + "date)");
        String filename = TEST_FILES_LOCATION + "wrongSemesterEndDate2.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(end date is before start date) semester end
     * dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongSemesterEndDate3() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong semester end "
                + "date(before semester start date)");
        String filename = TEST_FILES_LOCATION + "wrongSemesterEndDate3.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(date is wrong, e.g.: days/months are out of 
     * range - 99.99.2017) semester start dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongSemesterEndDate4() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong format");
        String filename = TEST_FILES_LOCATION + "wrongSemesterEndDate4.json";
        thrown.expect(ParseException.class);
        thrown.expectMessage("Unparseable date: " + "\"99.99.2017\"");
        FileController.loadSemesterDataFile(filename);
    }
 
    
    //-------------------MODULES_TESTING------------------------------------//
    //-------------------------MODULE_NAME----------------------------------//
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(null) module names present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileNullModuleName() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null module name field");
        String filename = TEST_FILES_LOCATION + "nullModuleName.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(empty string) module names present in the
     * JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyModuleName() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null module name field");
        String filename = TEST_FILES_LOCATION  + "emptyModuleName.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    //-------------------------MODULE_CODE---------------------------------//
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong Module codes are present in the file
     */
    @Test
    public void testLoadSemesterDataFileWrongModuleCode() throws Exception {
        System.out.println("loadSemesterDataFile with wrong module codes");
        String filename = TEST_FILES_LOCATION  + "wrongModuleCode.json";
        thrown.expect(IllegalCodeException.class);
        thrown.expectMessage("Incorrect module code was"
                    + " detected. Please reupload semester data file."
                    + " If error occurs again, please check your semester"
                    + " data file or contact system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Module codes with wrong types are present in the 
     * file
     */
    @Test
    public void testLoadSemesterDataFileWrongModuleCodeType() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong module code "
                + "types");
        String filename = TEST_FILES_LOCATION + "wrongModuleCodeType.json";
        thrown.expect(IllegalTypeException.class);
        thrown.expectMessage("Incorrect module type was"
                + " detected. Please reupload semester data file."
                + " If error occurs again, please check your semester"
                + " data file or contact system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Module codes are just blank strings
     */
    @Test
    public void testLoadSemesterDataFileEmptyModuleCode() throws Exception {
        System.out.println("loadSemesterDataFile with empty module codes");
        String filename = TEST_FILES_LOCATION + "wrongModuleCodeTypeEmpty.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                + " provided. Please, check your input (data file/form)"
                + " and try again. If the error persists, please"
                + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }

   /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Module codes are nulls
     */
    @Test
    public void testLoadSemesterDataFileNullModuleCode() throws Exception {
        System.out.println("loadSemesterDataFile with empty module codes");
        String filename = TEST_FILES_LOCATION + "nullModuleCode.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    //----------------------MODULE_CREDITS----------------------------------//

   /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Module credit values are nulls
     */
    @Test
    public void testLoadSemesterDataFileNullModuleCreditValue() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null credit value "
                + "field");
        String filename = TEST_FILES_LOCATION + "nullModuleCreditValue.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
   /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Module credit values are empty strings
     */
    @Test
    public void testLoadSemesterDataFileEmptyModuleCreditValue() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty module credit "
                + "value");
        String filename = TEST_FILES_LOCATION + "emptyModuleCreditValue.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                + " provided. Please, check your input (data file/form)"
                + " and try again. If the error persists, please"
                + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
   /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Module credit values are out of range (negative)
     */
    @Test
    public void testLoadSemesterDataFileNegativeModuleCreditValue() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with out of range(negative)"
                + "credit value");
        String filename = TEST_FILES_LOCATION 
                + "negativeModuleCreditValue.json";
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was detected. Please check"
                + " your input (should be between " + 1 + " and " + 100 + ")"
                + " and resubmit the"
                + " form. If the error persists, please contact your"
                + " HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
   /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Module credit values are out of range (positive)
     */
    @Test
    public void testLoadSemesterDataFilePostiveModuleCreditValue() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with out of range(positive) "
                + "credit value");
        String filename = TEST_FILES_LOCATION 
                + "positiveModuleCreditValue.json";
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was detected. Please check"
                + " your input (should be between " + 1 + " and " + 100 + ")"
                + " and resubmit the"
                + " form. If the error persists, please contact your"
                + " HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    //---------------------MODULE_ORGANISER_NAME----------------------------//
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(null) module organiser names present in the 
     * JSON file.
     */
    @Test
    public void testLoadSemesterDataFileNullModuleOrganiserName() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null module name field");
        String filename = TEST_FILES_LOCATION + "nullModuleOrganiserName.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(empty string) module organiser names present
     * in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyModuleOrganiserName() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null module name field");
        String filename = TEST_FILES_LOCATION 
                + "emptyModuleOrganiserName.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    //-------------------------MODULE_DESCRIPTION----------------------------//
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(null) module descriptions present in the 
     * JSON file.
     */
    @Test
    public void testLoadSemesterDataFileNullModuleDescription() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null module description "
                + "field");
        String filename = TEST_FILES_LOCATION + "nullModuleDescription.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(empty string) module descriptions are present
     * in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyModuleDescription() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty module "
                + "description field");
        String filename = TEST_FILES_LOCATION + "emptyModuleDescription.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    //----------------------MODULE_INPUT_TESTING_END------------------------//
    //----------------------ASSIGNMENTS_TESTING-----------------------------//
    //----------------------EXAM_START_DATE---------------------------------//
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(null) types are present in the 
     * JSON file.
     */
    @Test
    public void testLoadSemesterDataFileNullAssignmentType() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null assignment type");
        String filename = TEST_FILES_LOCATION + "nullAssignmentType.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(empty string) assignment types are present
     * in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyAssignmentType() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty assignment type "
                + "field");
        String filename = TEST_FILES_LOCATION + "emptyAssignmentType.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(not in specified range: C/F/E) assignment
     * types are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongAssignmentType() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong assignment type "
                + "field");
        String filename = TEST_FILES_LOCATION + "wrongAssignmentType.json";
        thrown.expect(IncorrectInputDataException.class);
        thrown.expectMessage("One of the assignments "
                    + "data is invalid. Please check your data input and try "
                    + "to upload data file again. If error occurs again "
                    + "please contact the system administrator or your HUB.");
        FileController.loadSemesterDataFile(filename);
    }
    
    //----------------------------ASSINGMENT_NAME---------------------------//
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(null) names are present in the 
     * JSON file.
     */
    @Test
    public void testLoadSemesterDataFileNullAssignmentName() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null assignment name "
                + "field");
        String filename = TEST_FILES_LOCATION + "nullAssignmentName.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(empty string) assignment names are present
     * in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyAssignmentName() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty assignment name "
                + "field");
        String filename = TEST_FILES_LOCATION + "emptyAssignmentName.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                + " provided. Please, check your input (data file/form)"
                + " and try again. If the error persists, please"
                + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    //--------------------------ASSIGNMENT_WEIGHTING_TEST--------------------//
    
   /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Assignment weighting values are nulls
     */
    @Test
    public void testLoadSemesterDataFileNullAssignmentWeighting() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null assignment "
                + "weighting value");
        String filename = TEST_FILES_LOCATION + "nullAssignmentWeighting.json";
        thrown.expect(IllegalValueException.class);
        thrown.expectMessage("A field that was supposed to be filled with a "
                + "number did not get the right value. Please, check your "
                + "input (data file/form) and resubmit the form. If the error "
                + "occurs again, contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
   /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Assignment weighting values are empty strings
     */
    @Test
    public void testLoadSemesterDataFileEmptyAssignmentWeighting() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty assignment "
                + "weighting value");
        String filename = TEST_FILES_LOCATION + "emptyAssignmentWeighting.json";
        thrown.expect(IllegalValueException.class);
        thrown.expectMessage("A field that was supposed to be filled with a "
                + "number did not get the right value. Please, check your "
                + "input (data file/form) and resubmit the form. If the error "
                + "occurs again, contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
   /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Assignment weighting values are out of range 
     * (negative)
     */
    @Test
    public void testLoadSemesterDataFileNegativeAssignmentWeighting()
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with out of range(negative)"
                + "assignment weighting value");
        String filename = TEST_FILES_LOCATION 
                + "negativeAssignmentWeighting.json";
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was detected. Please check"
                + " your input (should be between " + 0 + " and " + 100 + ")"
                + " and resubmit the"
                + " form. If the error persists, please contact your"
                + " HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
   /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when Assignment weighting values are out of range 
     * (positive)
     */
    @Test
    public void testLoadSemesterDataFilePostiveAssignmentWeighting() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with out of range(positive) "
                + "assignment weighting value");
        String filename = TEST_FILES_LOCATION 
                + "positiveAssignmentWeighting.json";
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was detected. Please check"
                + " your input (should be between " + 0 + " and " + 100 + ")"
                + " and resubmit the"
                + " form. If the error persists, please contact your"
                + " HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
   /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when the sum of assignments weightings for a module is
     * over a 100.
     */
    @Test
    public void testLoadSemesterDataFileAssignmentsOverallValue() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile where data file contains "
                + "a list of assignments for a specific module, and their "
                + "sum is over a 100");
        String filename = TEST_FILES_LOCATION + "moduleAssignmentsSum.json";
       // thrown.expect(IllegalAssignmentsException.class);
        thrown.expectMessage("Total assignments value for one of the modules "
                + "is not 100. Please check your data file and if the problem "
                + "persists again, please contact your HUB/system "
                + "admistrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    
    //------------------------ASSIGNMNET_DATES----------------------------//
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when null assignment set dates are present in the JSON
     * file.
     */
    @Test
    public void testLoadSemesterDataFileNullAssignmentSetDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null assignment set "
                + "date");
        String filename = TEST_FILES_LOCATION + "nullAssignmentSetDate.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your "
                + "input (data file/form) and resubmit the form. If the "
                + "error persists, please contact your HUB/system "
                + "administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when assignment set dates are present as empty string in
     * the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyAssignmentSetDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty assignment set "
                + "date");
        String filename = TEST_FILES_LOCATION + "emptyAssignmentSetDate.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
     
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(the date is earlier than local machine date by
     * more than 1 year) assignment set dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongAssignmentSetDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong assignment set "
                + "date(it is more than 1 year earlier than local machine's "
                + "date)");
        String filename = TEST_FILES_LOCATION + "wrongAssignmentSetDate.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
     
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(the date is later than local machine date by
     * more than 1 year) assignment set dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongAssignmentSetDate2() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong assignment set "
                + "date(it is more than 1 year later than local machine's "
                + "date)");
        String filename = TEST_FILES_LOCATION + "wrongAssignmentSetDate2.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(set date is after deadline date) assignment
     * set dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongAssignmentSetDate3() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong assignment set "
                + "date(later than deadline date)");
        String filename = TEST_FILES_LOCATION + "wrongAssignmentSetDate3.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(date is wrong, e.g.: days/months are out of 
     * range - 99.99.2017) assignment set dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongAssignmentSetDate4() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong assignment set "
                + "date(wrong format)");
        String filename = TEST_FILES_LOCATION + "wrongAssignmentSetDate4.json";
        thrown.expect(ParseException.class);
        thrown.expectMessage("Unparseable date: " + "\"99.99.9999\"");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when null assignment deadline dates are present in the 
     * JSON file.
     */
    @Test
    public void testLoadSemesterDataFileNullAssignmentDeadlineDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null assignment deadline "
                + "date");
        String filename = TEST_FILES_LOCATION 
                + "nullAssignmentDeadlineDate.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when assignment deadline dates are present as empty 
     * string in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyAssignmentDeadlineDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty assignment "
                + "deadline date");
        String filename = TEST_FILES_LOCATION 
                + "emptyAssignmentDeadlineDate.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(the date is earlier than local machine date by
     * more than 1 year) assignment deadline dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongAssignmentDeadlineDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong assignment "
                + "deadline date(it is more than 1 year earlier than local "
                + "machine's  date)");
        String filename = TEST_FILES_LOCATION 
                + "wrongAssignmentDeadlineDate.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(the date is later than local machine date by
     * more than 1 year) assignment deadline dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongAssignmentDeadlineDate2() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong assignment "
                + "deadline  date(it is more than 1 year later than local "
                + "machine's date)");
        String filename = TEST_FILES_LOCATION 
                + "wrongAssignmentDeadlineDate2.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(deadline date is before set date) assignment 
     * deadline dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongAssignmentDeadlineDate3() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong assignment deadline "
                + "date(before assignment set date)");
        String filename = TEST_FILES_LOCATION 
                + "wrongAssignmentDeadlineDate3.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(date is wrong, e.g.: days/months are out of 
     * range - 99.99.2017) assignment deadline dates are present in the
     * JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongAssignmentDeadlineDate4() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong assignemnt "
                + "deadline"
                + " date format");
        String filename = TEST_FILES_LOCATION +
                "wrongAssignmentDeadlineDate4.json";
        thrown.expect(ParseException.class);
        thrown.expectMessage("Unparseable date: " + "\"99.11.2017 15:00\"");
        FileController.loadSemesterDataFile(filename);
    }
    
    //----------------------DATES_TESTING_END--------------------------------//
    //----------------------EXAM_LOCATION_TESTING----------------------------//
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(null) exam locations are present in the 
     * JSON file.
     */
    @Test
    public void testLoadSemesterDataFileNullExamLocation() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null exam location "
                + "field");
        String filename = TEST_FILES_LOCATION + "nullExamLocation.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(empty string) exam locations are present
     * in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyExamLocation() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with empty exam location "
                + "field");
        String filename = TEST_FILES_LOCATION + "emptyExamLocation.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    //----------------------------EXAM_START_TIME----------------------------//
    
   
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when null exam start time dates are present in the JSON
     * file.
     */
    @Test
    public void testLoadSemesterDataFileNullExamStartTimeDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with null exam start "
                + "date");
        String filename = TEST_FILES_LOCATION + "nullExamStartTime.json";
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when  exam start time dates are present as empty string 
     * in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileEmptyExamStartTimeDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with exam start "
                + "date");
        String filename = TEST_FILES_LOCATION + "emptyExamStartTime.json";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        FileController.loadSemesterDataFile(filename);
    }
     
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(the date is earlier than local machine date by
     * more than 1 year) exam start time dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongExamStartTimeDate() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong exam start "
                + "date(it is more than 1 year earlier than local machine's "
                + "date)");
        String filename = TEST_FILES_LOCATION + "wrongExamSetTime.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected or "
                + "dates are clashing(start date is after end date/end date is "
                + "before start date)");
        FileController.loadSemesterDataFile(filename);
    }
     
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(the date is later than local machine date by
     * more than 1 year) exam start time dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWronExamStartTimeDate2() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong exam start "
                + "date(it is more than 1 year later than local machine's "
                + "date)");
        String filename = TEST_FILES_LOCATION + "wrongExamSetTime2.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(set date is after deadline date)  exam start
     * time dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongExamStartTimeDate3() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong exam start "
                + "date(later than deadline date)");
        String filename = TEST_FILES_LOCATION + "wrongExamSetTime3.json";
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage("Invalid start and/or end date format detected "
                + "or dates are clashing(start date is after end date/end "
                + "date is before start date)");
        FileController.loadSemesterDataFile(filename);
    }
    
    /**
     * Test of loadSemesterDataFile method, of class FileController, to test
     * error handling when wrong(date is wrong, e.g.: days/months are out of 
     * range - 99.99.2017) exam start time dates are present in the JSON file.
     */
    @Test
    public void testLoadSemesterDataFileWrongExamStartTimeDate4() 
                                                            throws Exception {
        System.out.println("loadSemesterDataFile with wrong exam start "
                + "date(wrong format)");
        String filename = TEST_FILES_LOCATION + "wrongExamSetTime4.json";
        thrown.expect(ParseException.class);
        thrown.expectMessage("Unparseable date: " + "\"99.99.9999 15:00\"");
        FileController.loadSemesterDataFile(filename);
    }
    
    //----------------END_OF_LOAD_SEMESTER_DATA_FILE_TESTING----------------//
    
    //---------------TEST_ON_UPDATE_SEMESTER_PROFILE------------------------//
    
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!DOES NOT GIVE AN ERROR!!!!!!!!!!!!!!!!!!!!!//
    /**
     * Test of updateSemesterProfile method, of class FileController, when 
     * selected JSON file is a copy of original one, hence no updates should
     * be made.
     */
    @Test
    public void testUpdateSemesterProfileNoUpdates() throws Exception {
        System.out.println("updateSemesterProfile with no updates in the "
                + "file.");
        String filename = TEST_FILES_LOCATION + "final1semester.json";
        FileController.loadSemesterDataFile(filename);
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("No updates were made. "
                + "Please, make sure that courseworks that you want to "
                + "update deadlines for exist. If error occurs again, "
                + "please, contact the system administrator.");
        FileController.updateSemesterProfile(filename);
    }
    
    /**
     * Test of updateSemesterProfile method, of class FileController, when 
     * selected JSON file is a an empty file.
     */
    @Test
    public void testUpdateSemesterProfileEmptyFile() throws Exception {
        System.out.println("updateSemesterProfile with an empty JSON file");
        String filename = TEST_FILES_LOCATION + "emptyJsonFile.json";
        thrown.expect(IllegalFileException.class);
        thrown.expectMessage("The program failed to read/write the file. "
                + "Please, check the path to the file and its format. If "
                + "error occurs again, please check your semester data file "
                + "or contact HUB/system administrator.");
        FileController.updateSemesterProfile(filename);
    }
    
    /**
     * Test of updateSemesterProfile method, of class FileController, when 
     * selected JSON file has wrong file extension.
     */
    @Test
    public void testUpdateSemesterProfileWrongFileFormat() throws Exception {
        System.out.println("updateSemesterProfile with an empty JSON file");
        String filename = TEST_FILES_LOCATION + "updated1semester.random";
        thrown.expect(IllegalFileException.class);
        thrown.expectMessage("The program failed to read/write the file. "
                + "Please, check the path to the file and its format. If "
                + "error occurs again, please check your semester data file "
                + "or contact HUB/system administrator.");
        FileController.updateSemesterProfile(filename);
    }
    
    /**
     * Test of updateSemesterProfile method, of class FileController, when 
     * selected JSON file has nulls as variables.
     */
    @Test
    public void testUpdateSemesterProfileFileWithNullValues() throws Exception{
        System.out.println("updateSemesterProfile with JSON file that has "
                + "null values (for assignments deadlines)");
        String filename = TEST_FILES_LOCATION + "jsonWithNullValues.json";
        thrown.expect(ItemNotFoundException.class);
        thrown.expectMessage("SEMESTER PROFILE  - specified item could be "
                + "found in the system. Please check your input and try "
                + "again.");
        FileController.updateSemesterProfile(filename);
    }     
    
    //-----------------------UPDATE_SEMESTER_PROFILE_DONE--------------------//
    
    //-----------------------REMOVE_LOG_FILE---------------------------------//
    
    /**
     * Test of removeLogFile method, of class FileController.
     */
    @Test
    public void testRemoveLogFile() throws Exception{
        System.out.println("removeLogFile");
        String filename = TEST_FILES_LOCATION + "updated1semester.json";
        FileController.loadSemesterDataFile(filename);
        SemesterProfile semP = SemesterProfileController.getSemProfiles()
                                                                    .get(0);
        boolean expResult = true;
        boolean result = FileController.removeLogFile(semP);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of removeLogFile method, of class FileController, when input 
     * parameter is null.
     */
    @Test
    public void testRemoveLogFileNull() throws Exception{
        System.out.println("removeLogFile with null as input parameter");
        SemesterProfile semP = null;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.removeLogFile(semP);
    }
    
    //------------------------REMOVE_LOG_FILE_DONE--------------------------//
    
    //------------------------SAVE_PROFILE----------------------------------//
    
    /**
     * Test of saveProfile method, of class FileController.
     */
    @Test
    public void testSaveProfile() throws Exception {
        System.out.println("saveProfile");
        String filename = TEST_FILES_LOCATION + "updated1semester.json";
        FileController.loadSemesterDataFile(filename);
        SemesterProfile semP = SemesterProfileController.getSemProfiles()
                                                                    .get(0);
        FileController.saveProfile(semP);
    }
    
    /**
     * Test of saveProfile method, of class FileController, when input semester
     * profile is null.
     */
    @Test
    public void testSaveProfileDuplicate() throws Exception {
        System.out.println("saveProfile with null input");
        SemesterProfile semP = null;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        FileController.saveProfile(semP);
    }   
    
    //-------------------------SAVE_PROFILE_DONE-----------------------------//
    
    //-------------------------READ_LOG_FILE---------------------------------//

    /**
     * Test of readLogFile method, of class FileController.
     */
    @Test
    public void testReadLogFile() throws Exception {
        System.out.println("readLogFile");
        FileController.readLogFile();
    }
    
    //-------------------------READ_LOG_FILE_DONE--------------------------//
    
    //-------------------------IMPORT_LOG_FILE-----------------------------//

    /**
     * Test of importLogFile method, of class FileController.
     */
    @Test
    public void testImportLogFile() throws Exception {
        System.out.println("importLogFile");
        String location = TEST_FILES_LOCATION + "ersasr";
        FileController.importLogFile(location);
    }
    
    /**
     * Test of importLogFile method, of class FileController, when bad log
     * files are used as input parameter.
     */
    @Test
    public void testImportLogFileWrongExtension() throws Exception {
        System.out.println("importLogFile");
        String location = TEST_FILES_LOCATION + "fakeLogFile.txt";
        thrown.expect(java.io.EOFException.class);
        FileController.importLogFile(location);
    }
    
    /**
     * Test of importLogFile method, of class FileController, when corrupted 
     * log files are used as input parameter.
     */
    @Test
    public void testImportLogFileCorrupted() throws Exception {
        System.out.println("importLogFile");
        String location = TEST_FILES_LOCATION + "corrupted.log";
        thrown.expect(java.io.UTFDataFormatException.class);
        FileController.importLogFile(location);        
    }
    
    /**
     * Test of importLogFile method, of class FileController, when non existing
     * log files are used as input parameter.
     */
    @Test
    public void testImportLogFileNotFound() throws Exception {
        System.out.println("importLogFile");
        String location = TEST_FILES_LOCATION + "random.log";
        thrown.expect(FileNotFoundException.class);
        thrown.expectMessage(TEST_FILES_LOCATION +
                "random.log (The system cannot find the file specified)");
        FileController.importLogFile(location);        
    }
    
    //----------------------IMPORT_LOG_FILE_DONE----------------------------//
    
    //----------------------EXPORT_LOG_FILE---------------------------------//
    
    /**
     * Test of exportLogFile method, of class FileController.
     */
    @Test
    public void testExportLogFile() throws Exception {
        System.out.println("exportLogFile");
        //Loading test semester
        String filename = TEST_FILES_LOCATION + "semesterToExport.json";
        FileController.loadSemesterDataFile(filename);
        
        String location = TEST_FILES_LOCATION + "semesterToExportOutput.json";
        //THIS WORKS, EVERY TEST RUN FORCES RECREATION OF TEST FILE
        FileController.exportLogFile(
                SemesterProfileController.getSemesterProfileHeaders().get(0),
                location);
    }
    
    /**
     * Test of exportLogFile method, of class FileController, with 
     * wrong file location.
     */
    @Test
    public void testExportLogFileWrongFileLocation() throws Exception {
        System.out.println("exportLogFile with wrong file location");
        //Loading test semester
        String filename = TEST_FILES_LOCATION + "semesterToExport.json";
        FileController.loadSemesterDataFile(filename);
        
        String location = "C:\\";
        
        thrown.expect(FileNotFoundException.class);
        thrown.expectMessage(location + " (The system cannot find the "
                + "path specified)");
        
        //!!!!!!!!!!!!!!!!!!!!!!!!!THIS_DOES_NOT_THROW_AN_EXCEPTION
        //WHEN_RANDOM_LETTERS_ARE_ENTERED!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //(Not in x:/ format....)
        FileController.exportLogFile(
                SemesterProfileController.getSemesterProfileHeaders().get(0),
                location);
    }
    
    //------------------------------TESTING_DONE-----------------------------//
}
