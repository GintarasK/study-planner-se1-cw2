package studyplanner.model;

import java.text.SimpleDateFormat;
import utilities.exceptions.*;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 * A class to test methods in model.Task class.
 */
public class TaskTest {
    
    public TaskTest() {
    }
    
    //JUnit Rule to define exceptions expected to be thrown
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    //--------------------VARIABLES_DECLARATION_FOR_TESTING-----------------//
    private static Date deadline;                       //Mock deadline
    
    private static String name;                         //Mock task name
    
    private static Task.Type type;                      //Mock task type
    
    private static int goal;                            //Mock task goal
    
    private static ArrayList<Activity> activityList;    //Mock activities list
    
    private static ArrayList<Milestone> milestoneList;  //Mock milestones list
    
    private static ArrayList<Note> noteList;            //Mock notes list
    
    private static Task baseInstance;                   //Mock Task instance
    
    private static Date startDate;                      //Mock start date
    
    private static boolean critical;                     //Mock critical flag
    
    private static Task dummyTask;                      //Mock Task for objects
    
    private static Task dummyTask2;                     //Mock Task for objects
    
    //-------------------------VARIABLE_DECLARATIONS_DONE-------------------//
    
    //------------------------EXCEPTIONS_TEXT_PLACEHOLDERS------------------//
    private static final String NULL_EXCEPTION = "Null parameter detected. "
            + "Please, check your"
            + " input (data file/form) and resubmit the form. If the"
            + " error persists, please contact your HUB/system"
            + " administrator.";

    private static final String EMPTY_INPUT_EXCEPTION = "Empty or incorrect"
            + " field was provided. Please, check your input (data file/form)"
            + " and try again. If the error persists, please"
            + " contact your HUB/system administrator.";
    
    private static final String DATES_EXCEPTION = "Invalid start and/or end "
            + "date format detected or dates are clashing(start date is after "
            + "end date/end date is before start date).";
    //----------------------END_OF_EXCEPTIONS_TEXT_PLACEHOLDERS-------------//
    
    /**
     * Testing variables initialisation.
     */
    @BeforeClass
    public static void setUpClass() throws Exception{
        deadline = new SimpleDateFormat("yyyy-MM-dd").parse("2017-09-22");
        startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2017-08-11");
        critical = false;
        name = "Mock task";
        type = Task.Type.PROGRAMMING;
        goal = 50;
        noteList = new ArrayList<Note>();
        noteList.add(new Note("Hello world!"));
        milestoneList = new ArrayList<Milestone>();
        milestoneList.add(new Milestone("Dummy milestone",
                new SimpleDateFormat("yyyy-MM-dd").parse("2017-06-23"), null));
        activityList = new ArrayList<Activity>();
        
        baseInstance = new Task(startDate, deadline, type, goal, name,
                critical); 
        
        dummyTask = new Task(new SimpleDateFormat("dd-MM-yyyy")
                .parse("01-06-2017") ,new SimpleDateFormat("dd-MM-yyyy")
                .parse("12-12-2017"),Task.Type.PROGRAMMING, 40, "Dummy task",
                false);
        
        dummyTask2 = new Task(new SimpleDateFormat("dd-MM-yyyy")
                .parse("01-06-2017") ,new SimpleDateFormat("dd-MM-yyyy")
                .parse("12-12-2017"),Task.Type.READING, 40, "Dummy task",
                false);
        
        activityList.add(new Activity("Test Activity", Task.Type.PROGRAMMING,
                dummyTask, noteList, 23, 20));
        System.out.println("\nMODEL.TASK TESTING-------------------------\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("\nEND_OF MODEL.TASK TESTING------------------\n");
    }
    
    /**
     * Re-initialising test data before each test.
     */
    @Before
    public void setUp() throws Exception{
        baseInstance = new Task(startDate, deadline, type, goal, name, critical); 
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addNote method, of class Task.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason addNote method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testAddNote() throws Exception {
        System.out.println("model.Task.addNote(Note note)");
        Note note = new Note("Test note");
        Task instance = baseInstance;
        instance.addNote(note);
    }
    
    /**
     * Test of addNote method, of class Task, when input is null.
     */
    @Test
    public void testAddNoteNull() throws Exception {
        System.out.println("model.Task.addNote(null)");
        Note note = null;
        Task instance = baseInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.addNote(note);
    }

    /**
     * Test of addNote method, of class Task, when input is duplicate note 
     * entry.
     */
    @Test
    public void testAddNoteDuplicate() throws Exception {
        System.out.println("model.Task.addNote(Note note) /Duplicate");
        Note note = new Note("Testing");
        Task instance = baseInstance;
        instance.addNote(note);
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("Duplicate note was detected. Please check your "
                + "input  and resubmit the form. If the error persists, "
                + "please contact system administrator.");
        instance.addNote(note);
    }
    
    /**
     * Test of setDeadline method, of class Task.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason setDeadline method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testSetDeadline() throws Exception {
        System.out.println("model.Task.setDeadline(Date deadline)");
        Date deadline = new SimpleDateFormat("yyyy-MM-dd").parse("2017-09-25");
        Task instance = baseInstance;
        instance.setDeadline(deadline);
    }
    
    /**
     * Test of setDeadline method, of class Task, when input parameter is null.
     */
    @Test
    public void testSetDeadlineNull() throws Exception {
        System.out.println("model.Task.setDeadline(null)");
        Date deadline = null;
        Task instance = baseInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.setDeadline(deadline);
    }
    
    /**
     * Test of setDeadline method, of class Task, when wrong deadline (too 
     * early) parameter is used.
     */
    @Test
    public void testSetDeadlineWrongDate() throws Exception {
        System.out.println("model.Task.setDeadline(Date tooEarlyDeadline)");
        Date deadline = new SimpleDateFormat("yyyy-MM-dd").parse("2017-04-22");
        Task instance = baseInstance;
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage(DATES_EXCEPTION);
        instance.setDeadline(deadline);
    }
    
    /**
     * Test of setDeadline method, of class Task, when wrong deadline (too 
     * late) parameter is used.
     */
    @Test
    public void testSetDeadlineWrongDate2() throws Exception {
        System.out.println("model.Tasl.setDeadline(Date tooLateDeadline)");
        Date deadline = new SimpleDateFormat("yyyy-MM-dd").parse("2019-04-22");
        Task instance = baseInstance;
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage(DATES_EXCEPTION);
        instance.setDeadline(deadline);
    }
   
    /**
     * Test of setName method, of class Task.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason setDeadline method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testSetName() throws Exception {
        System.out.println("model.Task.setName(String name)");
        String name = "Test name";
        Task instance = baseInstance;
        instance.setName(name);
    }
    
    /**
     * Test of setName method, of class Task, when input parameter is null.
     */
    @Test
    public void testSetNameNull() throws Exception {
        System.out.println("model.Task.setName(null)");
        String name = null;
        Task instance = baseInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.setName(name);
    }
    
    /**
     * Test of setName method, of class Task, when input parameter an empty
     * string.
     */
    @Test
    public void testSetNameEmpty() throws Exception {
        System.out.println("model.Task.setName(\"\")");
        String name = "";
        Task instance = baseInstance;
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage(EMPTY_INPUT_EXCEPTION);
        instance.setName(name);
    }

    /**
     * Test of setGoal method, of class Task.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason setGoal method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testSetGoal() throws Exception {
        System.out.println("model.Task.setGoal(int goal)");
        int goal = 9;
        Task instance = baseInstance;
        instance.setGoal(goal);
    }
    
    /**
     * Test of setGoal method, of class Task, when when input is out of range
     * (negative).
     */
    @Test
    public void testSetGoalNegative() throws Exception {
        System.out.println("model.Task.setGoal(-1)");
        int goal = -1;
        Task instance = baseInstance;
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was detected. Please check"
                + " your input (should be between " + 0 + " and " + 999 + ")"
                + " and resubmit the"
                + " form. If the error persists, please contact your"
                + " HUB/system administrator.");
        instance.setGoal(goal);
    }
    
    /**
     * Test of setGoal method, of class Task, when when input is out of range
     * (positive).
     */
    @Test
    public void testSetGoalPositive() throws Exception {
        System.out.println("model.Task.setGoal(1000)");
        int goal = 1000;
        Task instance = baseInstance;
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was detected. Please check"
                + " your input (should be between " + 0 + " and " + 999 + ")"
                + " and resubmit the"
                + " form. If the error persists, please contact your"
                + " HUB/system administrator.");
        instance.setGoal(goal);
    }

    /**
     * Test of addActivity method, of class Task.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason setDeadline method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testAddActivity() throws Exception {
        System.out.println("model.Task.addActivity(Activity activity)");
        Activity activity = activityList.get(0);
        Task instance = baseInstance;
        instance.addActivity(activity);
    }
    
    /**
     * Test of addActivity method, of class Task, where given Activity's type
     * differs from the Task's.
     */
    @Test
    public void testAddActivityWithWrongType() throws Exception {
        System.out.println("model.Task.addActivity(Activity activityWrongType)");
        Activity activity = new Activity("Test Activity2",
                Task.Type.READING, dummyTask2, noteList, 23, 20);
        Task instance = baseInstance;
        thrown.expect(IllegalTypeException.class);
        thrown.expectMessage("Task with incompatible type provided. Please "
                + "ensure, that the task given is compatable with this "
                + "activity. If error occurs again, please check entered "
                + "details or contact system administrator.");
        instance.addActivity(activity);
    }
    
    /**
     * Test of addActivity method, of class Task, when input parameter is null.
     */
    @Test
    public void testAddActivityNull() throws Exception {
        System.out.println("model.Task.addActivity(null)");
        Activity activity = null;
        Task instance = baseInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.addActivity(activity);
    }
    
    /**
     * Test of addActivity method, of class Task, when input parameter is a 
     * duplicate activity.
     */
    @Test
    public void testAddActivityDuplicate() throws Exception {
        System.out.println("model.Task.addActivity(Activity activity) /Duplicate");
        Activity activity = new Activity("Test Activity", Task.Type.PROGRAMMING,
                dummyTask, noteList, 9, 2);
        Task instance = baseInstance;
        instance.addActivity(activity);
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("Given activity already exists in the list.");
        instance.addActivity(activity);
    }

    /**
     * Test of assignMilestone method, of class Task.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason setDeadline method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testAssignMilestone() throws Exception {
        System.out.println("model.Task.assignMilestone(Milestones milestone)");
        Milestone milestone = new Milestone("Dummy milestone", 
                new SimpleDateFormat("yyyy-MM-dd").parse("2017-06-23"),
                null);
       Task instance = baseInstance;
       instance.assignMilestone(milestone);
    }
    
    /**
     * Test of assignMilestone method, of class Task, when input Milestone 
     * is already present in the list.
     */
    @Test
    public void testAssignMilestoneDuplicate() throws Exception {
        System.out.println("model.Task.assignMilestone(Milestones milestone)/"
                + " Duplicate");
        Milestone milestone = new Milestone("Dummy milestone",
                new SimpleDateFormat("yyyy-MM-dd").parse("2017-06-23"), null);
        Task instance = baseInstance;
        instance.assignMilestone(milestone);
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("The milestone that"
                        + " you are trying to add already exists."
                        + " Please, try again. If error occurs"
                        + " again, please, contact the system"
                        + " administrator.");
        instance.assignMilestone(milestone);
    }
    
    /**
     * Test of assignMilestone method, of class Task, when input Milestone 
     * is null.
     */
    @Test
    public void testAssignMilestoneNull() throws Exception {
        System.out.println("model.Task.assignMilestone(null)");
        Milestone milestone = null;
        Task instance = baseInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.assignMilestone(milestone);
    }

    /**
     * Test of setDependency method, of class Task.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason setDependency method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testSetDependency() throws Exception{
        System.out.println("model.Task.setDependency(Task task)");
        Task task = new Task(new SimpleDateFormat("dd-MM-yyyy")
                .parse("01-06-2017") ,new SimpleDateFormat("dd-MM-yyyy")
                .parse("12-12-2017"),Task.Type.PROGRAMMING, 40, "Dummy task",
                false);
        Task instance = baseInstance;
        instance.setDependency(task);
    }
    
    /**
     * Test of setDependency method, of class Task, where input is Null.
     */
    @Test
    public void testSetDependencyNull() throws Exception{
        System.out.println("model.Task.setDependency(null)");
        Task task = null;
        Task instance = baseInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.setDependency(task);
    }

    /**
     * Test of removeDependency method, of class Task.
     */
    @Test
    public void testRemoveDependency() throws Exception{
        System.out.println("model.Task.removeDependency()");
        Task instance = baseInstance;
        instance.setDependency(new Task(startDate, deadline, type, goal, name,
                critical));
        Task expResult = null;
        instance.removeDependency();
        Task result = instance.getDependency();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of removeDependency method, of class Task, when dependency is
     * not present.
     */
    @Test
    public void testRemoveDependencyTwice() {
        System.out.println("model.Task.removeDependency() x2");
        Task instance = baseInstance;
        Task expResult = null;
        instance.removeDependency();
        instance.removeDependency();
        Task result = instance.getDependency();
        assertEquals(expResult, result);
    }

    /**
     * Test of removeMilestone method, of class Task.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason removeMilestone method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testRemoveMilestone() throws Exception {
        System.out.println("model.Task.removeMilestone(Milestone milestone)");
        
        Milestone milestone = new Milestone("Dummy milestone", new Date(),
                null);
        Task instance = baseInstance;
        instance.assignMilestone(milestoneList.get(0));
        instance.removeMilestone(milestoneList.get(0));
    }
    
    /**
     * Test of removeMilestone method, of class Task.
     */
    @Test
    public void testRemoveMilestoneNull() throws Exception {
        System.out.println("model.Task.removeMilestone(null)");
        Milestone milestone = null;
        Task instance = baseInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.removeMilestone(milestone);
    }

    /**
     * Test of removeNote method, of class Task.
     */
    @Test
    public void testRemoveNote() throws Exception {
        System.out.println("model.Task.removeNote()");
        Note note = new Note("Hello world!");
        Task instance = baseInstance;
        instance.addNote(note);
        Task expResult = baseInstance;
        instance.removeNote(note);
        Task result = instance;
        assertEquals(expResult, result);
    }
    
    /**
     * Test of removeNotes method, of class Task.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason removeNotes method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testRemoveNotes() throws Exception {
        System.out.println("model.Task.removeNotes()");
        Task instance = baseInstance;
        instance.removeNotes();
        Task result = instance;
    }

    /**
     * Test of getNoteList method, of class Task.
     */
    @Test
    public void testGetNoteList() throws Exception{
        System.out.println("model.Task.getNoteList");
        Task instance = baseInstance;
        ArrayList<Note> expResult = new ArrayList<>();
        ArrayList<Note> result = instance.getNoteList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDependency method, of class Task.
     */
    @Test
    public void testGetDependency() {
        System.out.println("model.Task.getDependency");
        Task instance = baseInstance;
        Task expResult = null;
        Task result = instance.getDependency();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSetDate method, of class Task.
     */
    @Test
    public void testGetSetDate() throws Exception{
        System.out.println("model.Task.getSetDate()");
        Task instance = baseInstance;
        Date expResult =new SimpleDateFormat("dd-MM-yyyy").parse("11-08-2017");
        Date result = instance.getStartDate();
        assertEquals(expResult, result);
        //False positive as it differs by few milliseconds.
    }

    /**
     * Test of getDeadline method, of class Task.
     */
    @Test
    public void testGetDeadline() throws Exception{
        System.out.println("model.Task.getDeadline()");
        Task instance = baseInstance;
        Date expResult = new SimpleDateFormat("yyyy-MM-dd")
                .parse("2017-09-22");
        Date result = instance.getDeadline();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class Task.
     */
    @Test
    public void testGetName() {
        System.out.println("model.Task.getName()");
        Task instance = baseInstance;
        String expResult = name;
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getType method, of class Task.
     */
    @Test
    public void testGetType() {
        System.out.println("model.Task.getType()");
        Task instance = baseInstance;
        Task.Type expResult = type;
        Task.Type result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getGoal method, of class Task.
     */
    @Test
    public void testGetGoal() {
        System.out.println("model.Task.getGoal()");
        Task instance = baseInstance;
        int expResult = goal;
        int result = instance.getGoal();
        assertEquals(expResult, result);
    }

    /**
     * Test of getActivityList method, of class Task.
     */
    @Test
    public void testGetActivityList() {
        System.out.println("model.Task.getActivityList()");
        Task instance = baseInstance;
        ArrayList<Activity> expResult = new ArrayList<>();
        ArrayList<Activity> result = instance.getActivityList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMilestoneList method, of class Task.
     */
    @Test
    public void testGetMilestoneList() {
        System.out.println("model.Task.getMilestoneList()");
        Task instance = baseInstance;
        ArrayList<Milestone> expResult = new ArrayList<>();
        ArrayList<Milestone> result = instance.getMilestoneList();
        assertEquals(expResult, result);
    }

    /**
     * Test of isCompleted method, of class Task.
     */
    @Test
    public void testIsCompleted() {
        System.out.println("model.Task.isCompleted()");
        Task instance = baseInstance;
        boolean expResult = false;
        boolean result = instance.isCompleted();
        assertEquals(expResult, result);
    }
    
}
