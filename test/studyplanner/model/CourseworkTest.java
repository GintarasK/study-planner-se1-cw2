package studyplanner.model;

import utilities.exceptions.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 * A class to test all the methods of model.Coursework class.
 */
public class CourseworkTest {
    
    //JUnit Rule to define exceptions expected to be thrown
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    //--------------------VARIABLES_DECLARATION_FOR_TESTING-----------------//
    private static Coursework baseCourseworkInstance; //Mock Coursework object
    
    private static Module dummySetReference; //Mock Module object to be used in
                                             //setter methods
    
    private static Date dummyDeadlineDate; //Mock deadline Date object to be
                                           //used in Coursework constructor
    
    private static Date dummySetDeadlineDate; //Mock deadline Date object to 
                                              //be setter methods
    
    private static Date dummySetDate; //Mock set date Date object to be
                                      //used in Coursework constructor
    
    private static SimpleDateFormat dummySimpleDateFormat = //Mock Date format
                        new SimpleDateFormat("dd/MM/yyyy"); //object to parse
                                                            //Strings for Dates
    
    private static SimpleDateFormat dummyFullTimeFormat =  //Mock Date format
                 new SimpleDateFormat("dd/MM/yyyy HH:mm"); //to be used when
                                                           //parsing dates 
                                                           //with full time
    
    private static Date dummyTaskDeadline; //Mock Date object for Task deadline
    
    private static Task dummyTask; //Mock Task object for testing
    
    private static Task dummyAddRemoveTask; //Mock Task object for add and
    
    private static Task dummyTask2; //Mock 
     
    //remove methods
    //Mock type of Task
    private static Task.Type dummyTaskType = Task.Type.PROGRAMMING; 
    
    private static Milestone dummyMilestone; //Mock Milestone object
    
    private static Milestone dummyAddRemoveMilestone; //Mock Task object for 
                                                      //add and remove methods
    
    //-------------------------VARIABLE_DECLARATIONS_DONE-------------------//
    //------------------------EXCEPTIONS_TEXT_PLACEHOLDERS------------------//
    private static final String NULL_EXCEPTION = "Null parameter detected. "
            + "Please, check your"
            + " input (data file/form) and resubmit the form. If the"
            + " error persists, please contact your HUB/system"
            + " administrator.";

    private static final String EMPTY_INPUT_EXCEPTION = "Empty or incorrect"
            + " field was provided. Please, check your input (data file/form)"
            + " and try again. If the error persists, please"
            + " contact your HUB/system administrator.";
    
    //----------------------END_OF_EXCEPTIONS_TEXT_PLACEHOLDERS-------------//
    
    public CourseworkTest() {
    }
    
    /**
     * A method to set up the data for testing.
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Setting up test data..");
        
        //Initialising dummy deadline Date object
        dummyDeadlineDate = dummyFullTimeFormat.parse("13/11/2018 15:00");
        
        //Initialising dummy set Date object and dummy deadline Date object
        //for setter methods
        dummySetDate = dummySimpleDateFormat.parse("22/01/2018");
        
        dummySetDeadlineDate = dummyFullTimeFormat.parse("14/11/2018 15:00");
        
        baseCourseworkInstance = new Coursework("Digital assignment", 40,
        dummyDeadlineDate, dummySetDate);

        dummyTaskDeadline = dummyFullTimeFormat.parse("12/10/2018 18:13");
        
        dummyTask = new Task(new SimpleDateFormat("dd-MM-yyyy")
                .parse("01-06-2017") ,new SimpleDateFormat("dd-MM-yyyy")
                .parse("12-12-2017"),Task.Type.PROGRAMMING, 40, "Dummy task",
                false);
        
        dummyTask2 = new Task(new SimpleDateFormat("dd-MM-yyyy")
                .parse("01-06-2017") ,new SimpleDateFormat("dd-MM-yyyy")
                .parse("12-12-2017"),Task.Type.READING, 40, "D3ummy task",
                false);

        dummyMilestone = new Milestone("Dummy milestone", dummyTaskDeadline,
                new ArrayList<Task>());
        
        dummyAddRemoveMilestone = new Milestone("Dustone2", dummyTaskDeadline,
                new ArrayList<Task>());       
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        System.out.println("\n-----------Commencing new test..-------------");
            baseCourseworkInstance.taskList = new ArrayList<Task>();
            baseCourseworkInstance.taskList.add(dummyTask);
            baseCourseworkInstance.milestoneList = new ArrayList<Milestone>();
            baseCourseworkInstance.milestoneList.add(dummyMilestone);
            baseCourseworkInstance.result = 0;
            baseCourseworkInstance.weighting = 40;
            baseCourseworkInstance.deadline = dummyDeadlineDate;   
    }
    
    @After
    public void tearDown() {
       System.out.println("______________Test finished..___________________");
       baseCourseworkInstance.taskList.remove(dummyTask);
       baseCourseworkInstance.milestoneList.remove(dummyMilestone);
    }

    /**
     * Test of getSetDate method, of class Coursework.
     */
    @Test
    public void testGetSetDate() throws ParseException {
        System.out.println("Testing the getSetDate() method...");
        Coursework instance = baseCourseworkInstance;
        Date expResult = dummySetDate;
        System.out.println("\nExpected result: " + expResult.toString());
        Date result = instance.getSetDate();
        System.out.println("\nGot: " + result.toString());
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class Coursework.
     */
    @Test
    public void testGetName() {
        System.out.println("Testing the getName() method...");
        Coursework instance = baseCourseworkInstance;
        String expResult = "Digital assignment";
        System.out.println("\nExpected result: " + expResult);
        String result = instance.getName();
        System.out.println("\nGot: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getWeighting method, of class Coursework.
     */
    @Test
    public void testGetWeighting() {
        System.out.println("Testing the getWeighting() method...");
        Coursework instance = baseCourseworkInstance;
        int expResult = 40;
        System.out.println("\nExpected result: " + expResult);
        int result = instance.getWeighting();
        System.out.println("\nGot: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getResult method, of class Coursework.
     */
    @Test
    public void testGetResult() {
        System.out.println("Testing the getResult() method...");
        Coursework instance = baseCourseworkInstance;
        int expResult = 0;
        System.out.println("\nExpected result: " + expResult);
        int result = instance.getResult();
        System.out.println("\nGot: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDeadline method, of class Coursework.
     */
    @Test
    public void testGetDeadline() throws ParseException {
        System.out.println("Testing the getDeadline() method...");
        Coursework instance = baseCourseworkInstance;
            
        //Initialising dummy deadline Date object
        Date expResult = dummyDeadlineDate;
        
        System.out.println("\nExpected result: " + expResult.toString());
        Date result = instance.getDeadline();
        System.out.println("\nGot: " + result.toString());
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmptyTaskList method, of class Coursework, when no Tasks
     * are associated with this Coursework.
     */
    @Test
    public void testGetTaskList() {
        System.out.println("Testing the getTaskList() method, when the Task "
                         + "ArrayList is empty...");
        Coursework instance = baseCourseworkInstance;
        
        //Expecting an empty ArrayList of tasks, since none were initialised
        ArrayList<Task> expResult = new ArrayList<Task>();
        expResult.add(dummyTask);
        System.out.println("\nExpected result: " + expResult);
        ArrayList<Task> result = instance.getTaskList();
        System.out.println("\nGot: " + result);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of getMilestoneList method, of class Coursework, when no Milestones
     * are associated with this Coursework.
     */
    @Test
    public void testGetMilestoneList() {
        System.out.println("Testing the getMilestoneList() method, when the "
                         + "Milestone ArrayList is empty...");
        
        Coursework instance = baseCourseworkInstance;
        //Expecting an empty ArrayList of milestones, since none were 
        //initialised
        ArrayList<Milestone> expResult = new ArrayList<Milestone>();
        expResult.add(dummyMilestone);
        System.out.println("\nExpected result: " + expResult);
        ArrayList<Milestone> result = instance.getMilestoneList();
        System.out.println("\nGot: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDeadline method, of class Coursework.
     */
    @Test
    public void testSetDeadline() throws Exception {
        System.out.println("Testing setDeadline() method..");
        Date deadline = dummySetDeadlineDate;
        Coursework instance = baseCourseworkInstance;
       instance.setDeadline(deadline);
    }
    
    /**
     * Test of setDeadline method, of class Coursework, when NULL is passed
     * as a parameter.
     */
    @Test
    public void testSetNullDeadline() throws Exception {
        System.out.println("Testing setDeadline() method, with null as "
                + "parameter");
        Date deadline = null;
        Coursework instance = baseCourseworkInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.setDeadline(deadline);
    }

    /**
     * Test of addTask method, of class Coursework.
     */
    @Test
    public void testAddTask() throws Exception {
        System.out.println("Tesing the addTask() method..");
        Task task = dummyTask2;
        Coursework instance = baseCourseworkInstance;
       instance.addTask(task);       
    }
    
    /**
     * Test of addTask method, of class Coursework, when null is passed as
     * parameter.
     */
    @Test
    public void testAddNullTask() throws Exception {
        System.out.println("Tesing the addTask() method with null as "
                + "parameter");
        Task task = null;
        Coursework instance = baseCourseworkInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.addTask(task);       
    }
    
    /**
     * Test of addTask method, of class Coursework, when duplicate(already
     * existing) Task is to be added.
     */
    @Test
    public void testAddDuplicateTask() throws Exception {
        System.out.println("Tesing the addTask() method with duplicate "
                + "task");
        Task task = dummyTask;
        Coursework instance = baseCourseworkInstance;
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("Following task already"
                        + " exists. Please check your input "
                        + " and resubmit the form. If error occurs"
                        + " again, please contact system administrator.");
        instance.addTask(task);       
    }
    
    /**
     * Test of addMilestone method, of class Coursework.
     */
    @Test
    public void testAddMilestone() throws Exception {
        System.out.println("Testing the addMilestone() method...");
        Milestone milestone = dummyAddRemoveMilestone;
        Coursework instance = baseCourseworkInstance;
        instance.addMilestone(milestone);
    }
    
    /**
     * Test of addMilestone method, of class Coursework, when null is passed as
     * parameter.
     */
    @Test
    public void testAddNullMilestone() throws Exception {
        System.out.println("Tesing the addMilestone() method with null as"
                + " parameter");
        Milestone milestone = null;
        Coursework instance = baseCourseworkInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.addMilestone(milestone);       
    }
    
    /**
     * Test of addMilestone method, of class Coursework, when duplicate(already
     * existing) Milestone is to be added.
     */
    @Test
    public void testAddDuplicateMilestone() throws Exception {
        System.out.println("Tesing the addMilestone() method with duplicate "
                + "milestone");
        Milestone milestone = dummyMilestone;
        Coursework instance = baseCourseworkInstance;
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("The milestone that"
                        + " you are trying to add already exists."
                        + " Please, try again. If error occurs"
                        + " again, please, contact the system"
                        + " administrator.");
        instance.addMilestone(milestone);       
    }

    /**
     * Test of removeTask method, of class Coursework.
     */
    @Test
    public void testRemoveTask() throws Exception {
        System.out.println("Testing the removeTask() method...");
        Task task = dummyTask;
        Coursework instance = baseCourseworkInstance;
        instance.removeTask(task);
    }
    
    /**
     * Test of removeTask method, of class Coursework, when NULL is passed as
     * parameter.
     */
    @Test
    public void testRemoveNullTask() throws Exception {
        System.out.println("Tesing the removeTask() method with NULL as parameter");
        Task task = null;
        Coursework instance = baseCourseworkInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.removeTask(task);       
    }

    /**
     * Test of removeMilestone method, of class Coursework.
     */
    @Test
    public void testRemoveMilestone() throws Exception {
        System.out.println("Testing the removeMilestone() method");
        Milestone milestone = dummyMilestone;
        Coursework instance = baseCourseworkInstance;
        instance.removeMilestone(milestone);
    }
    
    /**
     * Test of removeMilestone method, of class Coursework, when null is 
     * passed as parameter.
     */
    @Test
    public void testRemoveNullMilestone() throws Exception {
        System.out.println("Tesing the removeMilestone() method with"
                + " NULL as parameter");
        Milestone milestone = null;
        Coursework instance = baseCourseworkInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.removeMilestone(milestone);       
    }

    /**
     * Test of setResult method, of class Coursework.
     */
    @Test
    public void testSetResult() throws Exception {
        System.out.println("Testing setResult() method...");
        int result_2 = 50;
        Coursework instance = baseCourseworkInstance;
        boolean expResult = true;
        instance.setResult(result_2);
    }
    
    /**
     * Test of setResult method, of class Coursework, with integer, that does
     * not fit the range of the error checking method.
     */
    @Test
    public void testSetWrongResult() throws Exception {
        System.out.println("Testing setResult() method with bad input data");
        int result_2 = -1;
        Coursework instance = baseCourseworkInstance;
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was detected. Please check"
                + " your input (should be between " + 0 + " and " + 100 + ")"
                + " and resubmit the"
                + " form. If the error persists, please contact your"
                + " HUB/system administrator.");
        instance.setResult(result_2);

    }

}
