package studyplanner.model;

import utilities.exceptions.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import studyplanner.controller.FileController;
import studyplanner.controller.SemesterProfileController;

/**
 * Class to test method in SemesterProfile class.
 */
public class SemesterProfileTest {

    public SemesterProfileTest() {
    }
    
    //JUnit Rule to define exceptions expected to be thrown
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    private static SemesterProfile baseInstance; //Base intance for testing  
    
    @BeforeClass
    public static void setUpClass() throws Exception{
        //For easier maintenance, data will be loaded from a data file
        FileController.loadSemesterDataFile(System.getProperty("user.dir")
            + "\\data_for_testing\\controller.FileController\\"
            + "final1semester.json");
        baseInstance =  SemesterProfileController.getSemProfiles().get(0);
        
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception{
        //SemesterProfileController.removeSemesterProfile(baseInstance);
    }
    
    @Before
    public void setUp() {
        baseInstance =  SemesterProfileController.getSemProfiles().get(0);
    }
    
    @After
    public void tearDown() {
    }
    

    @Test
    public void testDatesInput() throws IllegalDateException{
        
    }
    
    @Test
    public void testEmptyModulesListInput(){
        
    }

    @Test
    public void testSemesterTypes() throws IllegalTypeException{
        
    }
    /**
     * Test of getStartDate method, of class SemesterProfile.
     */
    @Test
    public void testGetStartDate() throws Exception {
        System.out.println("getStartDate");
        SemesterProfile instance = baseInstance;
        Date expResult = 
            new SimpleDateFormat("dd.MM.yyyy HH:mm").parse("25.09.2017 23:00");
        Date result = instance.getStartDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEndDate method, of class SemesterProfile.
     */
    @Test
    public void testGetEndDate() throws Exception{
        System.out.println("getEndDate");
        SemesterProfile instance = baseInstance;
        Date expResult = new SimpleDateFormat("dd.MM.yyyy").parse("16.12.2017");
        Date result = instance.getEndDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getType method, of class SemesterProfile.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        SemesterProfile instance = baseInstance;
        SemesterProfile.Type expResult = SemesterProfile.Type.AUTUMN;
        SemesterProfile.Type result = instance.getType();
        assertEquals(expResult, result);
    }

    //TO_BE_ADDED..
    /**
     * Test of getModuleList method, of class SemesterProfile.
     */
    @Test
    public void testGetModuleList() {
        System.out.println("getModuleList");
        SemesterProfile instance = baseInstance;
        ArrayList<Module> expResult = instance.getModuleList();
        ArrayList<Module> result = instance.getModuleList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getID method, of class SemesterProfile.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");
        SemesterProfile instance = baseInstance;
        
        int expResult = instance.getStartDate().hashCode() +
                instance.getEndDate().hashCode();
        
        int result = instance.getID();
        assertEquals(expResult, result);
    }

    /**
     * Test to test the setID method.
     */
    @Test
    public void testSetID() throws Exception {
        System.out.println("setID");
        int ID = 0;
        SemesterProfile instance = baseInstance;
        instance.setID(ID);
        int result = instance.getID();
        assertEquals(ID, result);
    }
    /**
     * Test to check if negative values can be plugged in the setID method.
     */
    @Test
    public void testSetIDNegative() throws Exception {
        System.out.println("setID");
        int ID = -3;
        SemesterProfile instance = baseInstance;
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was detected. Please check "
                + "your input (should be between 0 and 999) and resubmit the "
                + "form. If the error persists, please contact your HUB/system "
                + "administrator.");
        instance.setID(ID);
    }

}
