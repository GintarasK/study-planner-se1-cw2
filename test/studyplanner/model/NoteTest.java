package studyplanner.model;

import utilities.exceptions.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 * A class to test all the methods of model.Note class.
 */
public class NoteTest {
    
    //--------------------VARIABLES_DECLARATION_FOR_TESTING-----------------//
    
    private static Note baseInstance;
    private static String dummyContent;     //to be used in constructor
    
    //-----------------END_OF_VARIABLES_DECLARATION_FOR_TESTING-------------//
    
    //------------------------EXCEPTIONS_TEXT_PLACEHOLDERS------------------//
    
    private static final String NULL_EXCEPTION = "Null parameter detected. "
            + "Please, check your"
            + " input (data file/form) and resubmit the form. If the"
            + " error persists, please contact your HUB/system"
            + " administrator.";
    
    private static final String EMPTY_INPUT_EXCEPTION = "Empty or incorrect"
            + " field was provided. Please, check your input (data file/form)"
            + " and try again. If the error persists, please"
            + " contact your HUB/system administrator.";
    
    //----------------------END_OF_EXCEPTIONS_TEXT_PLACEHOLDERS-------------//
    
    public NoteTest() {
    }
    
    //JUnit Rule to define exceptions expected to be thrown
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    /**
     * A method to set up the data for testing.
     * 
     * A base case instance instance of Note is created, that holds
     * the content of "Hello world!". Since the mechanism of assigning IDs
     * for the Notes assumes, that the ID of the Note starts from 1 and is
     * incremented each time a new instance of Note is created, the ID for
     * base instance will be 1. The static variable in Note class "amount"
     * holds the total amount of Notes generated, it should be 1 as well.
     * 
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
        dummyContent = "Hello world!";
        baseInstance = new Note(dummyContent); 
        System.out.println("\nMODEL.NOTE TESTING-------------------------\n");
    }
    
    /**
     * A method to de-construct the test data from setUpClass() method.
     * Will not be used as Java has de-construction mechanism for the objects.
     */
    @AfterClass
    public static void tearDownClass() {
        System.out.println("\nEND_OF MODEL.NOTE TESTING------------------\n");
    }
    
    /**
     * A method to set up test data before each method test is applied.
     * 
     * Notes will be re-initialised with content of "Hello world!".
     * 
     */
    @Before
    public void setUp() throws Exception {
        baseInstance.setContent("Hello world!");
    }
    
    /**
     * A method to de-construct the test data from setUp() method.
     * As current implementation does not require de-composition for the
     * test data it is ignored for the moment.
     */
    @After
    public void tearDown() {
        
    }
    
    //----------------------CONSTRUCTOR_TESTING-----------------------------//
    
    /**
     * Test of class Note constructor, where content parameter is null.
     */
    @Test
    public void testConstructorNullContent() throws Exception{
        System.out.println("model.Note: new Note(null)");
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        new Note(null);
    }
    
    /**
     * Test of class Note constructor, where content parameter is an empty
     * string.
     */
    @Test
    public void testConstructorEmptyContent() throws Exception{
        System.out.println("model.Note: new Note(\"\")");
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage(EMPTY_INPUT_EXCEPTION);
        new Note("");
    }
    
    //-------------------------METHODS_TESTING------------------------------//
    /**
     * Test of getContent method, of class Note.
     */
    @Test
    public void testGetContent() {
        System.out.println("model.Note.getContent()");
        String expResult = "Hello world!";
        String result = baseInstance.getContent();
        assertEquals(expResult, result);
    }

    /**
     * Test of setContent method, of class Note.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason setContent method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testSetContent() throws Exception {
        System.out.println("model.Note.setContent(content)");
        String content = "Hello World! v2";
        baseInstance.setContent(content);
    }

    /**
     * Test of setContent method, of class Note, with NULL as parameter.
     */
    @Test
    public void testSetNullContent() throws Exception {
        System.out.println("model.Note.setContent(null)");
        String content = null;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        baseInstance.setContent(content);
    }
    
    /**
     * Test of setContent method, of class Note, with empty string as parameter.
     */
    @Test
    public void testSetEmptyContent() throws Exception {
        System.out.println("model.Note.setContent(\"\")");
        String content = "";
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage(EMPTY_INPUT_EXCEPTION);
        baseInstance.setContent(content);
    }
    
}
