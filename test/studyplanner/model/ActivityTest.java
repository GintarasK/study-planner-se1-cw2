package studyplanner.model;

import utilities.exceptions.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 * A class to test methods in model.Activity class.
 */
public class ActivityTest {

    public ActivityTest() {
    }

    //JUnit Rule to define exceptions expected to be thrown
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    //--------------------VARIABLES_DECLARATION_FOR_TESTING-----------------//
    private static Activity dummyActivityInstance; //Activity object to be
    //used for testing

    private static String dummyName = "Mock activity"; //Mock name String 
    //to construct a mock 
    //Activity object

    private static int dummyTimeTaken = 10; //Mock int variable for the mock
    //Activity object

    //Mock type enum to construct a mock Activity object
    private static Task.Type dummyType = Task.Type.PROGRAMMING;

    private static int dummyContribution = 40; //Dummy contribution variable
    //for the Activity constructor

    //Mock Task Reference list to be used to construct the mock Activity
    //object
    private static ArrayList<Task> dummyReferenceList = new ArrayList<Task>();

    //Mock Note objects list to be used to construct the mock Activity object
    private static ArrayList<Note> dummyNoteList = new ArrayList<Note>();
    
    private static Note testNote; //Dummy Note for mutator methods

    private static Milestone testMilestone; //Dummy Milestone for the mutator
    //methods

    private static Task testTask; //Dummy Task for the mutator methods

    private static Task testTask2; //Dummy Task for the mutator methods

    private static Task testTaskType; //Dummy Task with different type to 
    //check error checking on types

    private static SimpleDateFormat dateParser; //SimpleDateFormat to parse
    //Dates from String input

    //-------------------------VARIABLE_DECLARATIONS_DONE-------------------//
    //------------------------EXCEPTIONS_TEXT_PLACEHOLDERS------------------//
    private static final String NULL_EXCEPTION = "Null parameter detected. "
            + "Please, check your"
            + " input (data file/form) and resubmit the form. If the"
            + " error persists, please contact your HUB/system"
            + " administrator.";

    private static final String EMPTY_INPUT_EXCEPTION = "Empty or incorrect"
            + " field was provided. Please, check your input (data file/form)"
            + " and try again. If the error persists, please"
            + " contact your HUB/system administrator.";

    private static final String ILLEGAL_TASK_TYPE_EXCEPTION = "Task with"
            + " incompatible type provided. Please ensure, that the task"
            + " given is compatable with this activity."
            + " If error occurs again, please check entered"
            + " details or contact system administrator.";
    
    private static final String ITEM_NOT_FOUND_EXCEPTION = " - specified item "
            + "could be found in the system. Please check your input and try "
            + "again.";
    //----------------------END_OF_EXCEPTIONS_TEXT_PLACEHOLDERS-------------//

    /**
     * Testing variables initialisation.
     */
    @BeforeClass
    public static void setUpClass() throws Exception {

        dateParser = new SimpleDateFormat("dd-MM-yyyy");

        testTask = new Task(dateParser.parse("01-07-2017"),
                dateParser.parse("02-07-2017"), Task.Type.PROGRAMMING, 40,
                "Dummy task", false);

        testTask2 = new Task(dateParser.parse("01-07-2017"),
                dateParser.parse("03-07-2017"), Task.Type.PROGRAMMING, 40,
                "Dummy task2", false);

        testTaskType = new Task(dateParser.parse("01-07-2017"),
                dateParser.parse("02-07-2017"), Task.Type.READING, 40,
                "Dummy task6", false);

        //Adding a task to dummy reference list
        dummyReferenceList.add(testTask);
        
        //Dummy note for testing
        testNote = new Note("TTTT");

        //Adding a note to dummy note lsit
        dummyNoteList.add(new Note("Test note"));
        dummyNoteList.add(testNote);

        //Creating dummyActivityInstance
        dummyActivityInstance = new Activity(dummyName, dummyType,
                testTask, dummyNoteList, dummyTimeTaken,
                dummyContribution);
        System.out.println("\nMODEL.ACTIVITY TESTING---------------------\n");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("\nEND_OF MODEL.ACTIVITY TESTING--------------\n");
    }

    /**
     * Re-initialising test data before each test.
     */
    @Before
    public void setUp() throws Exception {
        dummyActivityInstance = new Activity(dummyName, dummyType,
                testTask, dummyNoteList, dummyTimeTaken,
                dummyContribution);
    }

    /**
     * A method to de-construct the test data from setUp() method. As current
     * implementation does not require de-composition for the test data it is
     * ignored for the moment.
     */
    @After
    public void tearDown(){
    }

    //-------------------------CONSTRUCTOR_TESTING------------------------//
    /**
     * Test to check if Activity class constructor handles error checking when
     * name variable is passed as a null object.
     */
    @Test
    public void testConstructorForNullNameInput() throws Exception {
        System.out.println("model.Activity: new Activity(null, Task.Type"
                + " type, Task reference, ArrayList<Note> noteList,"
                + " int timeTaken, int contribution)");
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        Activity instance = new Activity(null, dummyType,
                testTask, dummyNoteList, dummyTimeTaken,
                dummyContribution);
    }

    /**
     * Test to check if Activity constructor handles error checking for name
     * variable when empty String input ("") is used.
     */
    @Test
    public void testConstructorForEmptyNameInput() throws Exception {

        System.out.println("model.Activity: new Activity(\"\", Task.Type"
                + " type, Task reference, ArrayList<Note> noteList,"
                + " int timeTaken, int contribution)");
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage(EMPTY_INPUT_EXCEPTION);
        Activity instance = new Activity("", dummyType,
                testTask, dummyNoteList, dummyTimeTaken,
                dummyContribution);
    }

    /**
     * Test to check if Activity constructor handles error checking when null as
     * Activity type is used.
     */
    @Test
    public void testConstructorForNullType() throws Exception {

        System.out.println("model.Activity: new Activity(String name, null"
                + ", Task reference, ArrayList<Note> noteList,"
                + " int timeTaken, int contribution)");
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        Activity instance = new Activity(dummyName, null,
                testTask, dummyNoteList, dummyTimeTaken,
                dummyContribution);
    }

    /**
     * Test to check if Activity constructor handles error checking when null as
     * Activity type is used.
     */
    @Test
    public void testConstructorForWrongType() throws Exception {

        System.out.println("model.Activity: new Activity(String name,"
                + " Task.Type WRONG_TYPE"
                + ", Task reference, ArrayList<Note> noteList,"
                + " int timeTaken, int contribution)");
        thrown.expect(IllegalTypeException.class);
        thrown.expectMessage(ILLEGAL_TASK_TYPE_EXCEPTION);
        Activity instance = new Activity(dummyName, dummyType,
                testTaskType, dummyNoteList, dummyTimeTaken,
                dummyContribution);
    }

    /**
     * Test to check if the Activity constructor handles null as Task reference
     * parameter.
     */
    @Test
    public void testConstructorForNullReference() throws Exception {

        System.out.println("model.Activity: new Activity(String name,"
                + " Task.Type type"
                + ", null, ArrayList<Note> noteList,"
                + " int timeTaken, int contribution)");
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        Activity instance = new Activity(dummyName, dummyType,
                null, dummyNoteList, dummyTimeTaken,
                dummyContribution);
    }

    /**
     * Test to check if the Activity constructor handles Task reference
     * parameter, when it's type differs from type parameter.
     */
    @Test
    public void testConstructorForWrongTypeReference() throws Exception {

        System.out.println("model.Activity: new Activity(String name,"
                + " Task.Type type"
                + ", Task wrongTyped, ArrayList<Note> noteList,"
                + " int timeTaken, int contribution)");
        thrown.expect(IllegalTypeException.class);
        thrown.expectMessage(ILLEGAL_TASK_TYPE_EXCEPTION);
        Activity instance = new Activity(dummyName, dummyType,
                testTaskType, dummyNoteList, dummyTimeTaken,
                dummyContribution);
    }

    /**
     * Test to check if the Activity constructor creates an empty Note
     * ArrayList, when noteList parameter is null.
     */
    @Test
    public void testConstructorForNullNoteList() throws Exception {

        System.out.println("model.Activity: new Activity(String name,"
                + " Task.Type type"
                + ", Task reference, null,"
                + " int timeTaken, int contribution)");
        Activity instance = new Activity(dummyName, dummyType,
                testTask, null, dummyTimeTaken,
                dummyContribution);
        ArrayList<Note> result = instance.getNoteList();
        assertEquals(new ArrayList<>(), result);
    }

    /**
     * Test to check if the Activity constructor handles invalid time taken
     * parameter.
     */
    @Test
    public void testConstructorForWrongTimeTakenInput() throws Exception {
        System.out.println("model.Activity: new Activity(String name,"
                + " Task.Type type"
                + ", Task reference, ArrayList<Note> noteList,"
                + " -1, int contribution)");
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was"
                + " detected. Please check your input (should be between"
                + " " + 1 + " and " + 24 + ") and"
                + " resubmit the form. If the error persists, please"
                + " contact your HUB/system administrator.");
        Activity instance = new Activity(dummyName, dummyType,
                testTask, dummyNoteList, -1,
                dummyContribution);
    }

    /**
     * Test to check if the Activity constructor handles invalid time taken
     * parameter.
     */
    @Test
    public void testConstructorForWrongTimeTakenInput2() throws Exception {
        System.out.println("model.Activity: new Activity(String name,"
                + " Task.Type type"
                + ", Task reference, ArrayList<Note> noteList,"
                + " 25, int contribution)");
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was"
                + " detected. Please check your input (should be between"
                + " " + 1 + " and " + 24 + ") and"
                + " resubmit the form. If the error persists, please"
                + " contact your HUB/system administrator.");
        Activity instance = new Activity(dummyName, dummyType,
                testTask, dummyNoteList, 25,
                dummyContribution);
    }

    /**
     * Test to check if the Activity constructor handles invalid time taken
     * parameter.
     */
    @Test
    public void testConstructorForWrongContributionInput() throws Exception {
        System.out.println("model.Activity: new Activity(String name,"
                + " Task.Type type"
                + ", Task reference, ArrayList<Note> noteList,"
                + " int timeTaken, 0)");
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was"
                + " detected. Please check your input (should be between"
                + " " + 1 + " and " + 10000 + ") and"
                + " resubmit the form. If the error persists, please"
                + " contact your HUB/system administrator.");
        Activity instance = new Activity(dummyName, dummyType,
                testTask, dummyNoteList, dummyTimeTaken,
                0);
    }

    /**
     * Test to check if the Activity constructor handles invalid time taken
     * parameter.
     */
    @Test
    public void testConstructorForWrongContributionInput2() throws Exception {
        System.out.println("model.Activity: new Activity(String name,"
                + " Task.Type type"
                + ", Task reference, ArrayList<Note> noteList,"
                + " int timeTaken, 10001)");
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was"
                + " detected. Please check your input (should be between"
                + " " + 1 + " and " + 10000 + ") and"
                + " resubmit the form. If the error persists, please"
                + " contact your HUB/system administrator.");
        Activity instance2 = new Activity(dummyName, dummyType,
                testTask, dummyNoteList, dummyTimeTaken,
                10001);
    }

    //-----------------------CONSTRUCTOR_TESTING_DONE----------------------//
    /**
     * Test of removeNotes method, of class Activity.
     */
    @Test
    public void testRemoveNotes() {
        System.out.println("model.Activity.removeNotes()");
        Activity instance = dummyActivityInstance;
        ArrayList<Note> expResult = new ArrayList<>();
        instance.removeNotes();
        ArrayList<Note> result = instance.getNoteList();
        assertEquals(expResult, result);
    }

    /**
     * Test of addNote method, of class Activity.
     */
    @Test
    public void testAddNote() throws Exception {
        System.out.println("model.Activity.removeNotes(Note note)");
        Note note = new Note("TESTING");
        Activity instance = dummyActivityInstance;
        int expResult = dummyActivityInstance.getNoteList().size() + 1;
        instance.addNote(note);
        int result = dummyActivityInstance.getNoteList().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of addNote method, of class Activity, when null is used as Note
     * parameter.
     */
    @Test
    public void testAddNullNote() throws Exception {
        System.out.println("model.Activity.removeNotes(null)");
        Note note = null;
        Activity instance = dummyActivityInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.addNote(note);
    }

    /**
     * Test of addNote method, of class Activity, when user tries to add a
     * duplicate Note entry.
     */
    @Test
    public void testAddDuplicateNote() throws Exception {
        System.out.println("model.Activity.removeNotes(Note note) /Duplicate");
        Note note = new Note("TESTING");
        Activity instance = dummyActivityInstance;
        instance.addNote(note);
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("Duplicate note"
                + " was detected. Please check your input "
                + " and resubmit the form. If the error"
                + " persists, please contact system"
                + " administrator.");
        instance.addNote(note);
    }

    /**
     * Test of setContribution method, of class Activity.
     *
     * Note: there is no assertEquals by the end of the test, that is because of
     * the reason setContribution method returns void, thus if this test does
     * not throw an exception on run-time, test passes.
     */
    @Test
    public void testSetContribution() throws Exception {
        System.out.println("model.Activity.setContribution(int contribution)");
        int contribution = 1;
        Activity instance = dummyActivityInstance;
        instance.setContribution(contribution);
    }

    /**
     * Test of setContribution method, of class Activity, when given value is
     * out of method checking bounds(1-10000).
     */
    @Test
    public void testSetWrongContribution() throws Exception {
        System.out.println("model.Activity.setContribution(-1)");
        int contribution = -1;
        Activity instance = dummyActivityInstance;
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was"
                + " detected. Please check your input (should be between"
                + " " + 1 + " and " + 10000 + ") and"
                + " resubmit the form. If the error persists, please"
                + " contact your HUB/system administrator.");
        instance.setContribution(contribution);
    }

    /**
     * Test of setContribution method, of class Activity, when given value is
     * out of method checking bounds(1-10000).
     */
    @Test
    public void testSetWrongContribution2() throws Exception {
        System.out.println("model.Activity.setContribution(10001)");
        int contribution = 10001;
        Activity instance = dummyActivityInstance;
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was"
                + " detected. Please check your input (should be between"
                + " " + 1 + " and " + 10000 + ") and"
                + " resubmit the form. If the error persists, please"
                + " contact your HUB/system administrator.");
        instance.setContribution(contribution);
    }

    /**
     * Test of assignTask method, of class Activity.
     *
     * Note: there is no assertEquals by the end of the test, that is because of
     * the reason assignTask method returns void, thus if this test does not
     * throw an exception on run-time, test passes.
     */
    @Test
    public void testAssignTask() throws Exception {
        System.out.println("model.Activity.assignTask(Task task)");
        Task task = testTask2;
        Activity instance = dummyActivityInstance;
        instance.assignTask(task);
    }

    /**
     * Test of assignTask method, of class Activity, when NULL is passed as a
     * parameter.
     */
    @Test
    public void testAssignNullTask() throws Exception {
        System.out.println("model.Activity.assignTask(null)");
        Task task = null;
        Activity instance = dummyActivityInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.assignTask(task);
    }

    /**
     * Test of assignTask method, of class Activity, when Task with incompatible
     * type is passed as a parameter.
     */
    @Test
    public void testAssignWrongTask() throws Exception {
        System.out.println("model.Activity.assignTask(Task taskWithWrongType)");
        Task task = testTaskType;
        Activity instance = dummyActivityInstance;
        thrown.expect(IllegalTypeException.class);
        thrown.expectMessage(ILLEGAL_TASK_TYPE_EXCEPTION);
        instance.assignTask(task);
    }

    /**
     * Test of assignTask method, of class Activity, where given Task already
     * exists within Activities referenceList.
     */
    @Test
    public void testAssignTaskDuplicate() throws Exception {
        System.out.println("model.Activity.assignTask(Task taskDuplicate)");
        Task task = testTask;
        Activity instance = dummyActivityInstance;
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("Following task already exists. Please check "
                + "your input  and resubmit the form. If error occurs again, "
                + "please contact system administrator.");
        instance.assignTask(task);
    }

    /**
     * Test of setTimeTaken method, of class Activity.
     *
     * Note: there is no assertEquals by the end of the test, that is because of
     * the reason setTimeTaken method returns void, thus if this test does not
     * throw an exception on run-time, test passes.
     */
    @Test
    public void testSetTimeTaken() throws Exception {
        System.out.println("model.Activity.setTimeTaken(int timeTaken)");
        int timeTaken = 1;
        Activity instance = dummyActivityInstance;
        instance.setTimeTaken(timeTaken);
    }

    /**
     * Test of setTimeTaken method, of class Activity, when input parameter is
     * out of bounds of error checking (1-24).
     */
    @Test
    public void testSetWrongTimeTaken() throws Exception {
        System.out.println("model.Activity.setTimeTaken(0)");
        int timeTaken = -1;
        Activity instance = dummyActivityInstance;
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was"
                + " detected. Please check your input (should be between"
                + " " + 1 + " and " + 24 + ") and"
                + " resubmit the form. If the error persists, please"
                + " contact your HUB/system administrator.");
        instance.setTimeTaken(timeTaken);
    }

    /**
     * Test of setTimeTaken method, of class Activity, when input parameter is
     * out of bounds of error checking (1-24).
     */
    @Test
    public void testSetWrongTimeTaken2() throws Exception {
        System.out.println("model.Activity.setTimeTaken(25)");
        int timeTaken = 25;
        Activity instance = dummyActivityInstance;
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was"
                + " detected. Please check your input (should be between"
                + " " + 1 + " and " + 24 + ") and"
                + " resubmit the form. If the error persists, please"
                + " contact your HUB/system administrator.");
        instance.setTimeTaken(timeTaken);
    }

    /**
     * Test of setName method, of class Activity.
     *
     * Note: there is no assertEquals by the end of the test, that is because of
     * the reason setName method returns void, thus if this test does not throw
     * an exception on run-time, test passes.
     */
    @Test
    public void testSetName() throws Exception {
        System.out.println("model.Activity.setName(String name)");
        String name = "Test name";
        Activity instance = dummyActivityInstance;
        instance.setName(name);
    }

    /**
     * Test of setName method, of class Activity, when NULL is used as a
     * parameter.
     */
    @Test
    public void testSetNullName() throws Exception {
        System.out.println("model.Activity.setName(null)");
        String name = null;
        Activity instance = dummyActivityInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.setName(name);
    }

    /**
     * Test of setName method, of class Activity, when empty String is used as
     * parameter.
     */
    @Test
    public void testSetEmptyName() throws Exception {
        System.out.println("model.Activity.setName(\"\")");
        String name = "";
        Activity instance = dummyActivityInstance;
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage(EMPTY_INPUT_EXCEPTION);
        instance.setName(name);
    }

    /**
     * Test of getName method, of class Activity.
     */
    @Test
    public void testGetName() {
        System.out.println("model.Activity.getName()");
        Activity instance = dummyActivityInstance;
        String expResult = "Mock activity";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getType method, of class Activity.
     */
    @Test
    public void testGetType() {
        System.out.println("model.Activity.getType()");
        Activity instance = dummyActivityInstance;
        Task.Type expResult = Task.Type.PROGRAMMING;
        Task.Type result = instance.getType();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getContribution method, of class Activity.
     */
    @Test
    public void testGetContribution() {
        System.out.println("model.Activity.getContribution()");
        Activity instance = dummyActivityInstance;
        int expResult = dummyContribution;
        int result = instance.getContribution();
        assertEquals(expResult, result);
    }

    /**
     * Test of getReferenceList method, of class Activity.
     */
    @Test
    public void testGetReferenceList() {
        System.out.println("model.Activity.getReferenceList()");
        Activity instance = dummyActivityInstance;
        ArrayList<Task> expResult = dummyReferenceList;
        ArrayList<Task> result = instance.getReferenceList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTimeTaken method, of class Activity.
     */
    @Test
    public void testGetTimeTaken() throws Exception {
        System.out.println("model.Activity.getTimeTaken()");
        Activity instance = dummyActivityInstance;
        int expResult =  dummyTimeTaken;
        int result = instance.getTimeTaken();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNoteList method, of class Activity.
     */
    @Test
    public void testGetNoteList() {
        System.out.println("model.Activity.getNoteList()");
        Activity instance = dummyActivityInstance;
        ArrayList<Note> expResult = dummyNoteList;
        ArrayList<Note> result = instance.getNoteList();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSetDate method, of class Activity.
     */
    @Test
    public void testGetSetDate() {
        System.out.println("model.Activity.getSetDate()");
        Activity instance = dummyActivityInstance;
        Date expResult = new Date();
        Date result = instance.getSetDate();
        assertEquals(expResult.toGMTString(), result.toGMTString());
    }
    
    /**
     * Test of removeNote method, of class Activity.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason removeNote method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testRemoveNote() throws Exception {
        System.out.println("model.Activity.removeNote(Note note)");
        Note note = testNote;
        Activity instance = dummyActivityInstance;
        instance.addNote(note);
        instance.removeNote(note);
    }
    
//    /**
//     * Test of removeNote method, of class Activity, when given Note does not
//     * exist in the Activities noteList
//     */
//    @Test
//    public void testRemoveNoteNonExisting() throws Exception {
//        System.out.println("model.Activity.removeNote(Note nonExisting)");
//        Note note = new Note(" d");
//        Activity instance = dummyActivityInstance;
//        thrown.expect(ItemNotFoundException.class);
//        thrown.expectMessage("Error on given note" + ITEM_NOT_FOUND_EXCEPTION);
//        instance.removeNote(note);
//    }
    
    /**
     * Test of removeNote method, of class Activity, when NULL is used as
     * parameter.
     */
    @Test
    public void testRemoveNullNote() throws Exception {
        System.out.println("model.Activity.removeNote(null)");
        Note note = null;
        Activity instance = dummyActivityInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.removeNote(note);
    }
    
    /**
     * Test of removeTask method, of class Activity.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason removeNote method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     * 
     * Note: restriction to remove last reference was removed due to fact,
     * that user is restricted to do this action on controller and GUI level.
     */
    @Test
    public void testRemoveTask() throws Exception {
        System.out.println("model.Activity.removeNote(Task task)");
        Task task = testTask;
        Activity instance = dummyActivityInstance;
        instance.removeTask(task);
    }
    
//    /**
//     * Test of removeTask method, of class Activity, where given Task
//     * does not exist within this Activity.
//     */
//    @Test
//    public void testRemoveTaskNotExisting() throws Exception {
//        System.out.println("model.Activity.removeNote(Task notExisting)");
//        Task task = testTaskType;
//        Activity instance = dummyActivityInstance;
//        thrown.expect(ItemNotFoundException.class);
//        thrown.expectMessage("Error on given task" + ITEM_NOT_FOUND_EXCEPTION);
//        instance.removeTask(task);
//    }
    
    /**
     * Test of removeTask method, of class Activity, when NULL is passed as
     * parameter.
     */
    @Test
    public void testRemoveNullTask() throws Exception {
        System.out.println("model.Activity.removeTask(null)");
        Task task = null;
        Activity instance = dummyActivityInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.removeTask(task);
    }
    
}
