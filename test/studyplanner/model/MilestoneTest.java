package studyplanner.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import utilities.exceptions.*;

/**
 * A class to test methods in model.Milestone class.
 */
public class MilestoneTest {
    
    public MilestoneTest() {
    }
    
    //JUnit Rule to define exceptions expected to be thrown
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    //--------------------VARIABLES_DECLARATION_FOR_TESTING-----------------//
    
    private static String dummyName;                     //Mock milestone name
    private static Date dummyDeadline;                   //Mock deadline date
    private static ArrayList<Task> dummyTaskList;        //Mock task list
    private static Task testTask;                        //Mock Task1
    private static Task testTask2;                       //Mock Task2
    private static Date tooLateDeadline;                 //Mock Date(too late)
    private static Date tooEarlyDeadline;                //Mock Date(too early)
    
    private static SimpleDateFormat dateParser; //SimpleDateFormat to parse
    //Dates from String input
    
    private static Milestone dummyMilestoneInstance; //Dummy Milestone to test
    
    //-------------------------VARIABLE_DECLARATIONS_DONE-------------------//
    
    //------------------------EXCEPTIONS_TEXT_PLACEHOLDERS------------------//
    private static final String NULL_EXCEPTION = "Null parameter detected. "
            + "Please, check your"
            + " input (data file/form) and resubmit the form. If the"
            + " error persists, please contact your HUB/system"
            + " administrator.";

    private static final String EMPTY_INPUT_EXCEPTION = "Empty or incorrect"
            + " field was provided. Please, check your input (data file/form)"
            + " and try again. If the error persists, please"
            + " contact your HUB/system administrator.";
    
    private static final String ITEM_NOT_FOUND_EXCEPTION = " - specified item "
            + "could be found in the system. Please check your input and try "
            + "again.";
    
    private static final String DATES_EXCEPTION = "Invalid start and/or end "
            + "date format detected or dates are clashing(start date is after "
            + "end date/end date is before start date).";
    //----------------------END_OF_EXCEPTIONS_TEXT_PLACEHOLDERS-------------//
    
    /**
     * Testing variables initialisation.
     */
    @BeforeClass
    public static void setUpClass() throws Exception{
        dummyName = "Dummy Milestone";
        
        dateParser = new SimpleDateFormat("dd-MM-yyyy");
        
        dummyDeadline = new SimpleDateFormat("yyyy-MM-dd").parse("2017-07-29");
        
        tooLateDeadline = dateParser.parse("01-04-2019");
        
        tooEarlyDeadline = dateParser.parse("01-04-2015");
        
        dummyTaskList = new ArrayList<>();
        
        testTask = new Task(dateParser.parse("01-07-2017"),
                dateParser.parse("02-07-2017"), Task.Type.PROGRAMMING, 40,
                "Dummy task", false);
        
        testTask2 = new Task(dateParser.parse("01-07-2017"),
                dateParser.parse("03-07-2017"), Task.Type.PROGRAMMING, 40,
                "Dummy task2", false);
        
        dummyTaskList.add(testTask);
        
        dummyMilestoneInstance = new Milestone(dummyName, dummyDeadline,
                dummyTaskList);
        
        System.out.println("\nMODEL.MILESTONE TESTING--------------------\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("\nEND_OF MODEL.MILESTONE TESTING-------------\n");
    }
    
    /**
     * Re-initialising test data before each test.
     */
    @Before
    public void setUp() throws Exception{
        dummyMilestoneInstance = new Milestone(dummyName, dummyDeadline,
                dummyTaskList);
    }
    
    /**
     * A method to de-construct the test data from setUp() method. As current
     * implementation does not require de-composition for the test data it is
     * ignored for the moment.
     */
    @After
    public void tearDown() {
    }

    //-------------------------CONSTRUCTOR_TESTING------------------------//
    /**
     * Test to check if Milestone class constructor handles error checking when
     * name variable is passed as a null object.
     */
    @Test
    public void testConstructorForNullNameInput() throws Exception {
        System.out.println("model.Milestone: new Milestone(null,"
                + " Date deadline, ArrayList<Task> taskList)");
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        Milestone instance = new Milestone(null, dummyDeadline, dummyTaskList);
    }

    /**
     * Test to check if Milestone constructor handles error checking for name
     * variable when empty String input ("") is used.
     */
    @Test
    public void testConstructorForEmptyNameInput() throws Exception {

        System.out.println("model.Milestone: new Milestone(\"\","
                + " Date deadline, ArrayList<Task> taskList)");
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage(EMPTY_INPUT_EXCEPTION);
        Milestone instance = new Milestone("", dummyDeadline, dummyTaskList);
    }
    
    /**
     * Test to check if Milestone constructor handles error checking for
     * deadline parameter, when input is null.
     */
    @Test
    public void testConstructorForNullDate() throws Exception {

        System.out.println("model.Milestone: new Milestone(String name,"
                + " null, ArrayList<Task> taskList)");
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        Milestone instance = new Milestone(dummyName, null, dummyTaskList);
    }
    
    /**
     * Test to check if Milestone constructor handles error checking for
     * deadline parameter, when the passed Date is > (Local machine date + 1
     * year).
     */
    @Test
    public void testConstructorForTooLateDeadline() throws Exception {

        System.out.println("model.Milestone: new Milestone(String name,"
                + " Date tooLateDeadline, ArrayList<Task> taskList)");
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage(DATES_EXCEPTION);
        Milestone instance = new Milestone(dummyName, tooLateDeadline,
                dummyTaskList);
    }
    
    /**
     * Test to check if Milestone constructor handles error checking for
     * deadline parameter, when the passed Date is < (Local machine date).
     */
    @Test
    public void testConstructorForTooEarlyDeadline() throws Exception {

        System.out.println("model.Milestone: new Milestone(String name,"
                + " Date tooEarlyDeadline, ArrayList<Task> taskList)");
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage(DATES_EXCEPTION);
        Milestone instance = new Milestone(dummyName, tooEarlyDeadline,
                dummyTaskList);
    }
    
    /**
     * Test of setName method, of class Milestone.
     * 
     * Note: there is no assertEquals by the end of the test, that is because of
     * the reason setName method returns void, thus if this test does
     * not throw an exception on run-time, test passes.
     */
    @Test
    public void testSetName() throws Exception {
        System.out.println("model.Milestone.setName(String name)");
        String name = "TestName";
        Milestone instance = dummyMilestoneInstance;
        instance.setName(name);
    }

    /**
     * Test of setName method, of class Milestone, when NULL is passed as
     * parameter.
     */
    @Test
    public void testSetNullName() throws Exception {
        System.out.println("model.Milestone.setName(null)");
        String name = null;
        Milestone instance = dummyMilestoneInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.setName(name);
    }
    
    /**
     * Test of setName method, of class Milestone, when empty string is passed 
     * as parameter.
     */
    @Test
    public void testSetEmptyName() throws Exception {
        System.out.println("model.Milestone.setName(\"\")");
        String name = "";
        Milestone instance = dummyMilestoneInstance;
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage(EMPTY_INPUT_EXCEPTION);
        instance.setName(name);
    }
    
    /**
     * Test of setDeadline method, of class Milestone.
     * 
     * Note: there is no assertEquals by the end of the test, that is because of
     * the reason setDeadline method returns void, thus if this test does
     * not throw an exception on run-time, test passes.
     */
    @Test
    public void testSetDeadline() throws Exception {
        System.out.println("model.Milestone.setDeadline(Date deadline)");
        Date deadline = dateParser.parse("29-07-2017");
        Milestone instance = dummyMilestoneInstance;
        instance.setDeadline(deadline);
    }
    
    /**
     * Test of setDeadline method, of class Milestone, when null is passed
     * as parameter.
     */
    @Test
    public void testSetNullDeadline() throws Exception {
        System.out.println("model.Milestone.setDeadline(null)");
        Milestone instance = dummyMilestoneInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.setDeadline(null);
    }
    
   /**
     * Test of setDeadline method, of class Milestone, with wrong date as 
     * parameter (too late).
     */
    @Test
    public void testSetWrongDeadline() throws Exception {
        System.out.println("model.Milestone.setDeadline(Date tooLateDeadline)");
        Date deadline = tooLateDeadline;
        Milestone instance = dummyMilestoneInstance;
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage(DATES_EXCEPTION);
        instance.setDeadline(deadline);
    }
    
   /**
     * Test of setDeadline method, of class Milestone, with wrong date as 
     * parameter (too early).
     */
    @Test
    public void testSetWrongDeadline2() throws Exception {
        System.out.println("model.Milestone.setDeadline(Date tooEarlyDeadline)");
        Date deadline = tooEarlyDeadline;
        Milestone instance = dummyMilestoneInstance;
        thrown.expect(IllegalDateException.class);
        thrown.expectMessage(DATES_EXCEPTION);
        instance.setDeadline(deadline);
    }

    /**
     * Test of addTask method, of class Milestone.
     * 
     * Note: there is no assertEquals by the end of the test, that is because of
     * the reason addTask method returns void, thus if this test does
     * not throw an exception on run-time, test passes.
     */
    @Test
    public void testAddTask() throws Exception {
        System.out.println("model.Milestone.addTask(Task task)");
        Task task = testTask2;
        Milestone instance = dummyMilestoneInstance;
        instance.addTask(task);
    }

    /**
     * Test of addTask method, of class Milestone, when null is passed as
     * parameter.
     */
    @Test
    public void testAddNullTask() throws Exception {
        System.out.println("model.Milestone.addTask(null)");
        Task task = null;
        Milestone instance = dummyMilestoneInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.addTask(task);
    }
    
    /**
     * Test of assignTask method, of class Milestone, when duplicate Task
     * is passed as a parameter.
     */
    @Test
    public void testAssignDuplicateTask() throws Exception {
        System.out.println("model.Milestone.addTask(Task task) /Duplicate");
        Task task = testTask;
        Milestone instance = dummyMilestoneInstance;
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("Following task already"
                        + " exists. Please check your input "
                        + " and resubmit the form. If error occurs"
                        + " again, please contact system administrator.");
        instance.addTask(task);
    }
    
    /**
     * Test of removeTask method, of class Milestone.
     * 
     * Note: there is no assertEquals by the end of the test, that is because of
     * the reason removeTask method returns void, thus if this test does
     * not throw an exception on run-time, test passes.
     */
    @Test
    public void testRemoveTask() throws Exception {
        System.out.println("model.Milestone.removeTask(Task task)");
        Task task = testTask;
        Milestone instance = dummyMilestoneInstance;
        instance.removeTask(task);
    }

    /**
     * Test of removeTask method, of class Milestone, with NULL as parameter.
     */
    @Test
    public void testRemoveNullTask() throws Exception {
        System.out.println("model.Milestone.removeTask(null)");
        Task task = null;
        Milestone instance = dummyMilestoneInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        instance.removeTask(task);
    }
    
//    /**
//     * Test of removeTask method, of class Milestone, where parameter is Task
//     * that does not exist in the taskList.
//     */
//    @Test
//    public void testRemoveNonExistingTask() throws Exception {
//        System.out.println("model.Milestone.removeTask(null)");
//        Task task = new Task(dateParser.parse("01-07-2017"),
//                dateParser.parse("03-07-2017"), Task.Type.PROGRAMMING, 40,
//                "Dummy task3", false);
//        Milestone instance = dummyMilestoneInstance;
//        thrown.expect(ItemNotFoundException.class);
//        thrown.expectMessage("Error on given task" + ITEM_NOT_FOUND_EXCEPTION);
//        instance.removeTask(task);
//    }

    /**
     * Test of getDeadline method, of class Milestone.
     */
    @Test
    public void testGetDeadline() throws Exception{
        System.out.println("model.Milestone.getDeadline()");
        Milestone instance = dummyMilestoneInstance;
        Date expResult = new SimpleDateFormat("yyyy-MM-dd").parse("2017-07-29");
        Date result = instance.getDeadline();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class Milestone.
     */
    @Test
    public void testGetName() {
        System.out.println("model.Milestone.getName()");
        Milestone instance = dummyMilestoneInstance;
        String expResult = "Dummy Milestone";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTaskList method, of class Milestone.
     */
    @Test
    public void testGetTaskList() {
        System.out.println("model.Milestone.getTaskList()");
        Milestone instance = dummyMilestoneInstance;
        ArrayList<Task> expResult = dummyTaskList;
        ArrayList<Task> result = instance.getTaskList();
        assertEquals(expResult, result);
    }

    
}
