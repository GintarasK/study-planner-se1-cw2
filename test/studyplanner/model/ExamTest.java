package studyplanner.model;

import utilities.exceptions.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;


/**
 * A class to test all the methods of model.Exam class.
 */
public class ExamTest {
    //JUnit Rule to define exceptions expected to be thrown
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    /*
    * Section for mock objects that will be used within this test class.
    */
    private static Exam baseExamInstance; //Mock Exam object
    
    private static Date dummyDeadlineDate; //Mock deadline Date object to be
                                           //used in Exam constructor
    
    private static Date dummyStartDate;    //Mock deadline Date object to be
                                           //used in Exam constructor
    
    private static Date dummySetDeadlineDate; //Mock deadline Date object to be
                                              //setter methods
    
    private static Date dummySetDate; //Mock set date Date object to be
                                      //used in Exam constructor
    
    private static SimpleDateFormat dummyFullTimeFormat =  //Mock Date format
                 new SimpleDateFormat("dd/MM/yyyy HH:mm"); //to be used when
                                                           //parsing dates with
                                                           //full time
    
    private static Date dummyTaskDeadline; //Mock Date object for deadline
    
    private static Task dummyTask; //Mock Task object for testing
    
    private static Task dummyTask2; //Mock Task object for add and
                                            //remove methods
    
    private static Task.Type dummyTaskType = Task.Type.PROGRAMMING; //Mock type
                                                                    //of Task
    
    private static Milestone dummyMilestone; //Mock Milestone object 
    
    private static Milestone dummyAddRemoveMilestone; //Mock Task object for 
                                                      //add and remove methods
    
    private static String dummyLocation = "FTRoom";
    
    public ExamTest() {
    }
    
    /**
     * A method to set up the data for testing.
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Setting up test data..");
            
        //Initialising dummy deadline Date object

        dummyStartDate = dummyFullTimeFormat.parse("14/11/2018 11:00");
        
        dummyDeadlineDate = dummyFullTimeFormat.parse("14/11/2018 14:00");
        
        baseExamInstance = new Exam("Digital assignment", 40,
            dummyDeadlineDate, dummyLocation, dummyStartDate);
    
        dummyTaskDeadline = dummyFullTimeFormat.parse("12/10/2018 18:13");
        
        
        dummyTask = new Task(new SimpleDateFormat("dd-MM-yyyy")
                .parse("01-06-2017") ,new SimpleDateFormat("dd-MM-yyyy")
                .parse("12-12-2017"),Task.Type.PROGRAMMING, 40, "Dummy task",
                false);
        
        dummyTask2 = new Task(new SimpleDateFormat("dd-MM-yyyy")
                .parse("01-06-2017") ,new SimpleDateFormat("dd-MM-yyyy")
                .parse("12-12-2017"),Task.Type.READING, 40, "Dummy t4ask",
                false);
        
        dummyMilestone = new Milestone("Dummy milestone", dummyTaskDeadline,
                new ArrayList<Task>());
        
        dummyAddRemoveMilestone = new Milestone("Dustone2", dummyTaskDeadline,
                new ArrayList<Task>());

        }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws Exception {
        System.out.println("\n-----------Commencing new test..-------------");
            baseExamInstance.taskList = new ArrayList<Task>();
            baseExamInstance.taskList.add(dummyTask);
            baseExamInstance.milestoneList = new ArrayList<Milestone>();
            baseExamInstance.milestoneList.add(dummyMilestone);
            baseExamInstance.result = 0;
            baseExamInstance.weighting = 40;
            baseExamInstance.deadline = dummyDeadlineDate; 
            baseExamInstance.setLocation("FTRoom");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getLocation method, of class Exam.
     */
    @Test
    public void testGetLocation() {
        System.out.println("Testing getLocation method");
        Exam instance = baseExamInstance;
        String expResult = dummyLocation;
        String result = instance.getLocation();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLocation method, of class Exam.
     */
    @Test
    public void testSetLocation() throws Exception {
        System.out.println("Testing setLocation method");
        String location = "SCI 0.31";
        Exam instance = baseExamInstance;
        instance.setLocation(location);
    }
    
    /**
     * Test of setLocation method, of class Exam, when NULL is passed as 
     * parameter.
     */
    @Test
    public void testSetNullLocation() throws Exception {
        System.out.println("Testing setLocation method");
        String location = null;
        Exam instance = baseExamInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.setLocation(location);
    }
    
    /**
     * Test of setLocation method, of class Exam, when empty string is 
     * passed as parameter.
     */
    @Test
    public void testSetEmptyLocation() throws Exception {
        System.out.println("Testing setLocation method");
        String location = "";
        Exam instance = baseExamInstance;
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage("Empty or incorrect field was"
                    + " provided. Please, check your input (data file/form)"
                    + " and try again. If the error persists, please"
                    + " contact your HUB/system administrator.");
        instance.setLocation(location);
    }

    /**
     * Test of getName method, of class Exam.
     */
    @Test
    public void testGetName() {
        System.out.println("Testing the getName() method...");
        Exam instance = baseExamInstance;
        String expResult = "Digital assignment";
        System.out.println("\nExpected result: " + expResult);
        String result = instance.getName();
        System.out.println("\nGot: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getWeighting method, of class Exam.
     */
    @Test
    public void testGetWeighting() {
        System.out.println("Testing the getWeighting() method...");
        Exam instance = baseExamInstance;
        int expResult = 40;
        System.out.println("\nExpected result: " + expResult);
        int result = instance.getWeighting();
        System.out.println("\nGot: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getResult method, of class Exam.
     */
    @Test
    public void testGetResult() {
        System.out.println("Testing the getResult() method...");
        Exam instance = baseExamInstance;
        int expResult = 0;
        System.out.println("\nExpected result: " + expResult);
        int result = instance.getResult();
        System.out.println("\nGot: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDeadline method, of class Exam.
     */
    @Test
    public void testGetDeadline() throws ParseException {
        System.out.println("Testing the getDeadline() method...");
        Exam instance = baseExamInstance;
            
        //Initialising dummy deadline Date object
        Date expResult = dummyDeadlineDate;
        
        System.out.println("\nExpected result: " + expResult.toString());
        Date result = instance.getDeadline();
        System.out.println("\nGot: " + result.toString());
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmptyTaskList method, of class Exam, when no Tasks
     * are associated with this Exam.
     */
    @Test
    public void testGetTaskList() {
        System.out.println("Testing the getTaskList() method, when the Task "
                         + "ArrayList is empty...");
        Exam instance = baseExamInstance;
        
        //Expecting an empty ArrayList of tasks, since none were initialised
        ArrayList<Task> expResult = new ArrayList<Task>();
        expResult.add(dummyTask);
        System.out.println("\nExpected result: " + expResult);
        ArrayList<Task> result = instance.getTaskList();
        System.out.println("\nGot: " + result);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of getMilestoneList method, of class Exam, when no Milestones
     * are associated with this Coursework.
     */
    @Test
    public void testGetMilestoneList() {
        System.out.println("Testing the getMilestoneList() method, when the "
                         + "Milestone ArrayList is empty...");
        
        Exam instance = baseExamInstance;
        //Expecting an empty ArrayList of milestones, since none were 
        //initialised
        ArrayList<Milestone> expResult = new ArrayList<Milestone>();
        expResult.add(dummyMilestone);
        System.out.println("\nExpected result: " + expResult);
        ArrayList<Milestone> result = instance.getMilestoneList();
        System.out.println("\nGot: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDeadline method, of class Exam.
     */
    @Test
    public void testSetDeadline() throws Exception {
        System.out.println("Testing setDeadline() method..");
        Date deadline = dummyFullTimeFormat.parse("14/11/2018 14:00");
//        System.out.println("Input date: " + deadline.toString());
        Exam instance = baseExamInstance;
        instance.setDeadline(deadline);

    }
    
    /**
     * Test of setDeadline method, of class Exam, when NULL is passed
     * as a parameter.
     */
    @Test
    public void testSetNullDeadline() throws Exception {
        System.out.println("Testing setDeadline() method, with null as "
                + "parameter");
        Date deadline = null;
        Exam instance = baseExamInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.setDeadline(deadline);
    }
    
    /**
     * Test of addTask method, of class Exam.
     */
    @Test
    public void testAddTask() throws Exception {
        System.out.println("Tesing the addTask() method..");
        Task task = dummyTask2;
        Exam instance = baseExamInstance;
        instance.addTask(task);       
    }
    
    /**
     * Test of addTask method, of class Exam, when NULL is passed as
     * parameter.
     */
    @Test
    public void testAddNullTask() throws Exception {
        System.out.println("Tesing the addTask() method with NULL as "
                + "parameter");
        Task task = null;
        Exam instance = baseExamInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.addTask(task);       
    }
    
    /**
     * Test of addTask method, of class Exam, when duplicate(already
     * existing) Task is to be added.
     */
    @Test
    public void testAddDuplicateTask() throws Exception {
        System.out.println("Tesing the addTask() method with duplicate "
                + "task");
        Task task = dummyTask;
        Exam instance = baseExamInstance;
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("Following task already"
                        + " exists. Please check your input "
                        + " and resubmit the form. If error occurs"
                        + " again, please contact system administrator.");
        instance.addTask(task);       
    }
    
    /**
     * Test of addMilestone method, of class Exam.
     */
    @Test
    public void testAddMilestone() throws Exception{
        System.out.println("Testing the addMilestone() method...");
        Milestone milestone = dummyAddRemoveMilestone;
        Exam instance = baseExamInstance;
        instance.addMilestone(milestone);
    }
    
    /**
     * Test of addMilestone method, of class Exam, when NULL is passed as
     * parameter.
     */
    @Test
    public void testAddNullMilestone() throws Exception {
        System.out.println("Tesing the addMilestone() method with NULL"
                + " as parameter");
        Milestone milestone = null;
        Exam instance = baseExamInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.addMilestone(milestone);       
    }
    
    /**
     * Test of addMilestone method, of class Exam, when duplicate(already
     * existing) Milestone is to be added.
     */
    @Test
    public void testAddDuplicateMilestone() throws Exception {
        System.out.println("Tesing the addMilestone() method with duplicate "
                + "milestone");
        Milestone milestone = dummyMilestone;
        Exam instance = baseExamInstance;
        thrown.expect(DuplicateDataException.class);
        thrown.expectMessage("The milestone that"
                        + " you are trying to add already exists."
                        + " Please, try again. If error occurs"
                        + " again, please, contact the system"
                        + " administrator.");
        instance.addMilestone(milestone);       
    }

    /**
     * Test of removeTask method, of class Exam.
     */
    @Test
    public void testRemoveTask() throws Exception {
        System.out.println("Testing the removeTask() method...");
        Task task = dummyTask;
        Exam instance = baseExamInstance;
        instance.removeTask(task);
    }
    
    /**
     * Test of removeTask method, of class Exam, when NULL is passed as
     * parameter.
     */
    @Test
    public void testRemoveNullTask() throws Exception {
        System.out.println("Tesing the removeTask() method with NULL as "
                + "parameter");
        Task task = null;
        Exam instance = baseExamInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.removeTask(task);       
    }

    /**
     * Test of removeMilestone method, of class Exam.
     */
    @Test
    public void testRemoveMilestone() throws Exception {
        System.out.println("Testing the removeMilestone() method");
        Milestone milestone = dummyMilestone;
        Exam instance = baseExamInstance;
        instance.removeMilestone(milestone);

    }
    
    /**
     * Test of removeMilestone method, of class Exam, when NULL is passed as
     * parameter.
     */
    @Test
    public void testRemoveNullMilestone() throws Exception {
        System.out.println("Tesing the removeMilestone() method with NULL "
                + "as parameter");
        Milestone milestone = null;
        Exam instance = baseExamInstance;
        thrown.expect(NullInputException.class);
        thrown.expectMessage("Null parameter detected. Please, check your"
                + " input (data file/form) and resubmit the form. If the error"
                + " persists, please contact your HUB/system administrator.");
        instance.removeMilestone(milestone);       
    }

    /**
     * Test of setResult method, of class Exam.
     */
    @Test
    public void testSetResult() throws Exception {
        System.out.println("Testing setResult() method...");
        int result_2 = 50;
        Exam instance = baseExamInstance;        
        instance.setResult(result_2);
    }
    
    /**
     * Test of setResult method, of class Exam, with integer, that does
     * not fit the range of the error checking method.
     */
    @Test
    public void testSetWrongResult() throws Exception {
        System.out.println("Testing setResult() method with bad input data");
        int result_2 = -1;
        Exam instance = baseExamInstance;
        thrown.expect(InputOutOfRangeException.class);
        thrown.expectMessage("Incorrect ammount was"
                    + " detected. Please check your input (should be between"
                    + " " + 0 + " and " + 100 + ") and"
                    + " resubmit the form. If the error persists, please"
                    + " contact your HUB/system administrator.");
        instance.setResult(result_2);

    }

}
