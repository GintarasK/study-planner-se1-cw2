
package studyplanner.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import utilities.exceptions.*;

/**
 * A class to test methods in model.Module class.
 */
public class ModuleTest {
    
    public ModuleTest() {
    }
    
    //JUnit Rule to define exceptions that are expected to be thrown during
    //code execution
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    //--------------------VARIABLES_DECLARATION_FOR_TESTING-----------------//
    private static int dummyCreditValue;            //Mock Module credit value
    
    private static String dummyName;                //Mock Module name
    
    private static String dummyCode;                //Mock Module code
    
    private static String dummyModuleOrganiser;     //Mock Module organiser
    
    private static String dummyDescription;         //Mock Module description
    
    private static Module.Type dummyType;                //Mock module type
    
    //Mock Module Assignment list
    private static ArrayList<Assignment> dummyAssignmentList; 
    
    //Declaring mock Module for testing
    private static Module dummyModule;
    
    //Mock Module assignment list for setAssignemntList method
    private static ArrayList<Assignment> dummyAssignmentListForSetters;
    
    //Mock Module assignemnt to be added to dummyAssignmentListForSetters
    private static Coursework dummyCoursework;
    
    //-------------------------VARIABLE_DECLARATIONS_DONE-------------------//
    //------------------------EXCEPTIONS_TEXT_PLACEHOLDERS------------------//
    private static final String NULL_EXCEPTION = "Null parameter detected. "
            + "Please, check your"
            + " input (data file/form) and resubmit the form. If the"
            + " error persists, please contact your HUB/system"
            + " administrator.";

    private static final String EMPTY_INPUT_EXCEPTION = "Empty or incorrect"
            + " field was provided. Please, check your input (data file/form)"
            + " and try again. If the error persists, please"
            + " contact your HUB/system administrator.";
    
    //----------------------END_OF_EXCEPTIONS_TEXT_PLACEHOLDERS-------------//

    /**
     * Testing variables initialisation.
     */
    @BeforeClass
    public static void setUpClass() throws Exception{
        //Initialising all of the mock variables with mock test data
        dummyCreditValue = 50;
        dummyName = "Digital and Analogue Electronics";
        dummyCode = "CMP-5027A";
        dummyModuleOrganiser = "Mark Fisher";
        dummyDescription = "Dummy description";
        dummyType = Module.Type.A;
        dummyAssignmentList = new ArrayList<>();
        
        //Declaring and initialising dummy deadline Date for dummy Coursework
        Date courseworkSetDate =
                        new SimpleDateFormat("dd/MM/yyyy").parse("01/06/2017");
        
        //Declaring and initialising dummy set Date for dummy Coursework
        Date courseworkDeadlineDate =
            new SimpleDateFormat("dd/MM/yyyy HH:mm").parse("01/06/2017 15:00");
        
        //Initialising mock Coursework
        dummyCoursework = new Coursework("Digital assignment", 40,
                courseworkDeadlineDate, courseworkSetDate);
        
        //Adding an entry to the dummy assignment list
        dummyAssignmentList.add(dummyCoursework);
        
        //Initialising mock Module object for testing
        dummyModule = new Module(dummyName, dummyCode, dummyCreditValue,
                                 dummyModuleOrganiser, dummyDescription,
                                 dummyType, dummyAssignmentList);
        
        //Initialising mock assignments list for setter methods of Module class
        dummyAssignmentListForSetters = new ArrayList<>();
        System.out.println("\nMODEL.MODULE TESTING-----------------------\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("\nEND_OF MODEL.MODULE TESTING----------------\n");
    }
    
    /**
     * Re-initialising test data before each test.
     */
    @Before
    public void setUp() throws Exception{
        dummyModule = new Module(dummyName, dummyCode, dummyCreditValue,
                                 dummyModuleOrganiser, dummyDescription,
                                 dummyType, dummyAssignmentList);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Module.
     */
    @Test
    public void testGetName() {
        System.out.println("model.Module.getName()");
        Module instance = dummyModule;
        String expResult = dummyName;
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCode method, of class Module.
     */
    @Test
    public void testGetCode() {
        System.out.println("model.Module.getCode()");
        Module instance = dummyModule;
        String expResult = dummyCode;
        String result = instance.getCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOrganiser method, of class Module.
     */
    @Test
    public void testGetOrganiser() {
        System.out.println("model.Module.getOrganiser()");
        Module instance = dummyModule;
        String expResult = dummyModuleOrganiser;
        String result = instance.getOrganiser();
        assertEquals(expResult, result);
    }

    /**
     * Test of getType method, of class Module.
     */
    @Test
    public void testGetType() {
        System.out.println("model.Module.getType()");
        Module instance = dummyModule;
        Module.Type expResult = dummyType;
        Module.Type result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAssignmentList method, of class Module.
     * 
     * Note: there is no assertEquals by the end of the test, that is
     * because of the reason setAssignmentList method returns void, thus if 
     * this test does not throw an exception on run-time, test passes.
     */
    @Test
    public void testSetAssignmentList() throws Exception {
        System.out.println("model.Module.setAssignmentList"
                + "(ArrayList<Assignment> assignmentList)");
        ArrayList<Assignment> list = dummyAssignmentListForSetters;
        list.add(dummyCoursework);
        Module instance = dummyModule;
        instance.setAssignmentList(list);
    }
    
    /**
     * Test of setAssignmentList method, of class Module with NULL as parameter.
     */
    @Test
    public void testSetNullAssignmentList() throws Exception {
        System.out.println("model.Module.setAssignmentList"
                + "(null)");
        ArrayList<Assignment> list = null;
        thrown.expect(NullInputException.class);
        thrown.expectMessage(NULL_EXCEPTION);
        dummyModule.setAssignmentList(list);
    }

    /**
     * Test of setAssignmentList method, of class Module empty Assignment list
     * as parameter.
     */
    @Test
    public void testSetEmptyAssignmentList() throws Exception {
        System.out.println("model.Module.setAssignmentList"
                + "(ArrayList<Assignment> emptyList)");
        ArrayList<Assignment> list = new ArrayList<>();
        thrown.expect(EmptyInputException.class);
        thrown.expectMessage(EMPTY_INPUT_EXCEPTION);
        dummyModule.setAssignmentList(list);
    }
    
    /**
     * Test of getCredits method, of class Module.
     */
    @Test
    public void testGetCredits() {
        System.out.println("model.Module.getCredits()");
        Module instance = dummyModule;
        int expResult = dummyCreditValue;
        int result = instance.getCreditValue();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAssignmentList method, of class Module.
     */
    @Test
    public void testGetAssignmentList() {
        System.out.println("model.Module.getAssignmentList()");
        Module instance = dummyModule;
        ArrayList<Assignment> expResult = dummyAssignmentList;
        ArrayList<Assignment> result = instance.getAssignmentList();
        assertEquals(expResult, result);
    }   

    /**
     * Test of getDescription method, of class Module.
     */
    @Test
    public void testGetDescription() {
        System.out.println("model.Module.getDescription()");
        Module instance = dummyModule;
        String expResult = dummyDescription;
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }
    
}
