package test_runner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import studyplanner.controller.FileControllerTest;
import studyplanner.model.*;

@RunWith(Suite.class)

//Classes to run testing on (JUnit testing classes)
@Suite.SuiteClasses({
  ActivityTest.class, 
  CourseworkTest.class,
  ExamTest.class,
  MilestoneTest.class,
  ModuleTest.class,
  NoteTest.class,
  TaskTest.class,
  SemesterProfileTest.class,
  FileControllerTest.class}
)

public class TestSuite {
}