--JSON FILES--

To update a semester, load final2semester.json first.
Go to Networks module and see what the deadline is for "Monitoring Internet Performance".
Then, navigate back and select "Update Semester".
Select updated2semester.json
Go to Networks module and see what the deadline is for "Monitoring Internet Performance".

To populate a semester with milestones/tasks/activities we recommend using 1semesterDifferentYear.json
Since the system checks for assignment deadlines when users create items and does not allow date clashes
which means that you were not able to create anything for assignments that already passed


--LOG FILES--

We have prepared 2 populated log files that you can load.
They are called GanttChart and Dashboard Data


--REPORTS--

Documentation for Implementation phase can be found in Implementation phase documentation

Documentation for Design phase can be found in Design phase documentation